package ru.veb.eo.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import ru.veb.eo.model.crm.TypeDepositValuation;
import ru.veb.eo.model.crm.TypeSupply;

import java.io.IOException;

public class TypeDepositValuationSerializer extends JsonSerializer<TypeDepositValuation> {
    @Override
    public void serialize(TypeDepositValuation value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        if (value != null)
            gen.writeString(value.toString());
        else
            gen.writeString(value.toString());
    }
}
