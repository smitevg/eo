package ru.veb.eo.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import ru.veb.eo.model.crm.DateForm;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateDeserializer extends JsonDeserializer<DateForm> {
    @Override
    public DateForm deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            return new DateForm(formatter1.parse(p.getText()));
        } catch (ParseException e) {
            e.printStackTrace();
            try {
                return new DateForm(formatter2.parse(p.getText()));
            } catch (ParseException e1) {
                e1.printStackTrace();
                return new DateForm();
            }
        }
    }
}
