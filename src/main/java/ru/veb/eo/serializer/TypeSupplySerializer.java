package ru.veb.eo.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import ru.veb.eo.model.crm.TypeSupply;

import java.io.IOException;

public class TypeSupplySerializer extends JsonSerializer<TypeSupply> {
    @Override
    public void serialize(TypeSupply value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        if (value != null)
            gen.writeString(value.toString());
        else
            gen.writeNull();
    }
}
