package ru.veb.eo.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.common.base.Strings;
import org.thymeleaf.util.ArrayUtils;
import ru.veb.eo.model.crm.TypeSupply;

import java.io.IOException;

public class TypeSupplyDeserializer extends JsonDeserializer<TypeSupply> {
    @Override
    public TypeSupply deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        try {
            if (!Strings.isNullOrEmpty(p.getText())) {
                int i = Integer.valueOf(p.getText());
                return TypeSupply.values()[i];
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
