package ru.veb.eo.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.common.base.Strings;
import ru.veb.eo.model.crm.TypeDepositValuation;
import ru.veb.eo.model.crm.TypeSupply;

import java.io.IOException;

public class TypeDepositValuationDeserializer extends JsonDeserializer<TypeDepositValuation> {
    @Override
    public TypeDepositValuation deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        try {
            if (!Strings.isNullOrEmpty(p.getText())) {
                int i = Integer.valueOf(p.getText());
                return TypeDepositValuation.values()[i];
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
