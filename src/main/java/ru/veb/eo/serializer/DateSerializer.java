package ru.veb.eo.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import ru.veb.eo.model.crm.DateForm;

import java.io.IOException;

public class DateSerializer extends JsonSerializer<DateForm> {
    @Override
    public void serialize(DateForm value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        if (value != null && value.day != null && value.month != null && value.year != null)
            gen.writeString(value.toString());
        else
            gen.writeNull();
    }
}
