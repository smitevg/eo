package ru.veb.eo.serializer;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.apache.tomcat.util.buf.StringUtils;
import org.codehaus.groovy.util.StringUtil;

import java.io.IOException;
import java.util.Set;

public class StateSupportSerializer extends JsonSerializer<Set<Integer>> {

    @Override
    public void serialize(Set<Integer> value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        String result = "";
        if (value != null && value.size() > 0) {

            Integer[] array = value.toArray(new Integer[value.size()]);
            for (int i = 0; i < array.length; i++) {
                String t = "^" + String.valueOf(array[i]) + "^";
                if (i == array.length)
                    result += t;
                else
                    result += t + ",";
            }
        }

        gen.writeString(result);
    }
}
