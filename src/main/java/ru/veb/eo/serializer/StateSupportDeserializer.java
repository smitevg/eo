package ru.veb.eo.serializer;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Strings;
import org.thymeleaf.util.ArrayUtils;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StateSupportDeserializer extends JsonDeserializer<Set<Integer>> {

    @Override
    public Set<Integer> deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        Set<Integer> list = new HashSet<>();

        try {
            if (!Strings.isNullOrEmpty(p.getText()) && p.getText().contains("^")) {
                String[] arr = p.getText().replaceAll("\\Q^\\E", "").split(",");
//                System.out.println(Arrays.toString(arr));
                if (!ArrayUtils.isEmpty(arr)) {
                    for (String s : arr) {
                        list.add(Integer.valueOf(s));
                    }
                }
                return list;
            } else {
                return list;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return list;
        }
    }
}
