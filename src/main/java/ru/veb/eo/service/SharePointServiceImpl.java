package ru.veb.eo.service;


import com.google.common.base.Strings;
import com.microsoft.sharepoint.rest.SPFile;
import com.microsoft.sharepoint.rest.SPListFile;
import com.microsoft.sharepoint.ws.copy.*;
import com.microsoft.sharepoint.ws.dws.CreateFolder;
import com.microsoft.sharepoint.ws.dws.CreateFolderResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import org.thymeleaf.util.ListUtils;
import ru.veb.eo.NtlmAuthenticator;
import ru.veb.eo.Properties;
import ru.veb.eo.model.crm.DocumentAdd;

import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Primary
@Profile("!dev")
@Service
public class SharePointServiceImpl implements SharePointService {
    private static final Logger logger = LoggerFactory.getLogger(SharePointService.class);

    @Autowired
    Properties properties;

    @Autowired
    CrmService cs;

    @Autowired
    @Qualifier("restTemplateSharePoint")
    private RestTemplate restTemplate;

    private static final SoapActionCallback CREATE_FOLDER = new SoapActionCallback("http://schemas.microsoft.com/sharepoint/soap/dws/CreateFolder");
    private static final SoapActionCallback CREATE_DWS = new SoapActionCallback("http://schemas.microsoft.com/sharepoint/soap/dws/CreateDws");
    private static final SoapActionCallback GET_DWS_DATA = new SoapActionCallback("http://schemas.microsoft.com/sharepoint/soap/dws/GetDwsData");
    private static final SoapActionCallback GET_LIST_COLLECTION = new SoapActionCallback("http://schemas.microsoft.com/sharepoint/soap/GetListCollection");
    private static final SoapActionCallback GET_LIST = new SoapActionCallback("http://schemas.microsoft.com/sharepoint/soap/GetList");
    private static final SoapActionCallback COPY_LOCAL = new SoapActionCallback("http://schemas.microsoft.com/sharepoint/soap/CopyIntoItemsLocal");
    private static final SoapActionCallback COPY = new SoapActionCallback("http://schemas.microsoft.com/sharepoint/soap/CopyIntoItems");

    private WebServiceTemplate webServiceTemplate;

    @Autowired
    public SharePointServiceImpl(WebServiceTemplate webServiceTemplate) {
        this.webServiceTemplate = webServiceTemplate;
    }

    public void saveProjectFile(String projectId, MultipartFile file, String inn, String projectCode) {
//        createFolder("/Shared Documents");
        if (file != null) {
            createFolder("Shared Documents/" + inn);
            createFolder("Shared Documents/" + inn + "/" + projectCode);
            try {
                createDocument(projectId, "/Shared Documents/" + inn + "/" + projectCode, file.getOriginalFilename(), file.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveProjectBytesStream(String projectId, String filename, ByteArrayOutputStream out, String inn, String projectCode) {
        if (filename != null && out != null) {
            createFolder("Shared Documents/" + inn);
            createFolder("Shared Documents/" + inn + "/" + projectCode);
            createDocument(projectId,"/Shared Documents/" + inn + "/" + projectCode, filename, out.toByteArray());
        }
    }

    public void saveProjectBytes(String projectId, String filename, byte[] content, String inn, String projectCode) {
        if (filename != null && content.length > 0) {
            createFolder("Shared Documents/" + inn);
            createFolder("Shared Documents/" + inn + "/" + projectCode);
            createDocument(projectId,"/Shared Documents/" + inn + "/" + projectCode, filename, content);
        }
    }

    public void createFolder(String path) {
        CreateFolder request = new CreateFolder();
        request.setUrl(path);


        logger.debug("createFolder = {}", path);

        try {
            CreateFolderResponse response = (CreateFolderResponse) webServiceTemplate.marshalSendAndReceive(properties.getSharepointPathWS() + "/_vti_bin/dws.asmx", request, CREATE_FOLDER);

            logger.debug("CreateFolderResponse = {}", response.getCreateFolderResult());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createDocument(String projectId, String folder, String fileName, byte[] content) {

        String trPath = UtilsService.transliteration(fileName);
        fileName = trPath.substring(trPath.lastIndexOf("\\") + 1);

//
//        Path p = Paths.get(trPath);
//        fileName = p.getFileName().toString();

        try {
            String url = properties.getSharepointDestinationUrl() + folder + "/" + fileName;
            String sourceUrl = properties.getSharepointDestinationUrl() + folder + "/" + fileName;

            DestinationUrlCollection urls = new DestinationUrlCollection();
            urls.getStrings().add(url);

            FieldInformationCollection fields = new FieldInformationCollection();

            FieldInformation titleFieldInformation = new FieldInformation();
            titleFieldInformation.setDisplayName("Title");
            titleFieldInformation.setType(FieldType.TEXT);
            titleFieldInformation.setValue(fileName);
            titleFieldInformation.setInternalName(fileName);
            titleFieldInformation.setId("{fa564e0f-0c70-4ab9-b863-0177e6ddd247}");
            fields.getFieldInformations().add(titleFieldInformation);


            CopyIntoItems request = new CopyIntoItems();
            request.setDestinationUrls(urls);
            request.setFields(fields);
            request.setSourceUrl(sourceUrl);
            request.setStream(content);

            CopyIntoItemsResponse response = (CopyIntoItemsResponse) webServiceTemplate.marshalSendAndReceive(properties.getSharepointPathWS() + "/_vti_bin/copy.asmx", request, COPY);

            for (CopyResult copyResult : response.getResults().getCopyResults()) {
                if (response.getCopyIntoItemsResult() != 0 && copyResult.getErrorCode() != CopyErrorCode.SUCCESS) {
                    logger.error("Failed to upload, result: " +
                            response.getCopyIntoItemsResult() +
                            ", destinationUrl: " + copyResult.getDestinationUrl() +
                            ", message: " + copyResult.getErrorMessage() +
                            ", code: " + copyResult.getErrorCode());
                } else {
                    DocumentAdd documentAdd = new DocumentAdd(fileName, copyResult.getDestinationUrl());
                    cs.addDocument(projectId, documentAdd);
                    logger.info("Upload result: " +
                            response.getCopyIntoItemsResult() +
                            ", destinationUrl: " + copyResult.getDestinationUrl() +
                            ", message: " + copyResult.getErrorMessage() +
                            ", code: " + copyResult.getErrorCode());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void copyIntoItemsLocal(String sourceUrl, String destinationUrl) {
        CopyIntoItemsLocal request = new CopyIntoItemsLocal();
        request.setSourceUrl(sourceUrl);
        DestinationUrlCollection urlCollection = new DestinationUrlCollection();
        urlCollection.getStrings().add(destinationUrl);
        request.setDestinationUrls(urlCollection);

        try {
            CopyIntoItemsLocalResponse response = (CopyIntoItemsLocalResponse) webServiceTemplate.marshalSendAndReceive(properties.getSharepointPathWS() + "/_vti_bin/copy.asmx", request, COPY_LOCAL);

            logger.debug("CopyIntoItemsLocalResponse = {}", response.getCopyIntoItemsLocalResult());

            if (ListUtils.isEmpty(response.getResults().getCopyResults())) {
                logger.debug("CopyIntoItemsLocalResponse = {}", "null");
            } else {
                for (CopyResult o : response.getResults().getCopyResults()) {

                    logger.debug("CopyIntoItemsLocalResponse = {}", o.getDestinationUrl());
                }
            }
        } catch (SoapFaultClientException e) {
            logger.error(e.getFaultCode().toString());
            logger.error(e.getFaultStringOrReason());
            logger.error(e.getMessage());
            logger.error(e.getSoapFault().getFaultStringOrReason());

        }

    }

    public List<SPFile> getListFile(String folder) {

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<?> entity = new HttpEntity<>(headers);

        logger.debug("url={}", properties.getSharepointPathWS() + "/_api/web/GetFolderByServerRelativeUrl('" +  folder + "')/Files");
        HttpEntity<SPListFile> response = restTemplate.exchange(
                properties.getSharepointPathWS() + "/_api/web/GetFolderByServerRelativeUrl('{folder}')/Files",
                HttpMethod.GET,
                entity,
                SPListFile.class,
                folder
        );

        logger.debug(response.getBody().toString());

        if (response.getBody() != null)
            return response.getBody().getFiles();
        else
            return new ArrayList<>();
    }

    public byte[] getFile(String url) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        ResponseEntity<byte[]> response = restTemplate.exchange(properties.getSharepointPathWS() + "/_api/{url}/$value", HttpMethod.GET, entity, byte[].class, url);

        if (response.getStatusCode().equals(HttpStatus.OK)) {
            return response.getBody();
        } else {
            logger.error(response.getStatusCode().toString());
            return null;
        }
    }

}
