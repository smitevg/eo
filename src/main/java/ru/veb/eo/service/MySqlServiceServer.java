package ru.veb.eo.service;

import com.microsoft.sharepoint.rest.SPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import ru.veb.eo.VebEoApplication;
import ru.veb.eo.entity.*;
import ru.veb.eo.model.Role;
import ru.veb.eo.model.User;
import ru.veb.eo.model.crm.Manager;
import ru.veb.eo.model.crm.ProjectResponsePacket;

import java.util.*;

@Primary
@Service
@ConditionalOnProperty(name = "SQL_SERVICE", havingValue="suite")
public class MySqlServiceServer implements MySqlService {


    private static final Logger logger = LoggerFactory.getLogger(MySqlService.class);

    //@Autowired UserRoleAppendRepo userRoleAppendRepo;

    @Autowired
    UserProjectRepo userProjectRepo;

    @Autowired
    ProjectRepo projectRepo;

    @Autowired
    CrmService crmService;

    //@Override
   // public List<UserRoleAppend> findByUsername(String username) {
   //     return userRoleAppendRepo.findByUsername(username);
  //  }

    //@Override
    //public List<UserRoleAppend> findSindRole() {
//        return userRoleAppendRepo.findByRoleAndAddOrderByLastUpdateDate(Role.SIND, 1);
//    }

    //Все пользователи подходящие для проекта
    @Override
    public List<Manager> findUsers(Project project) {
        String userType = null;
        Role role = null;
        if (CrmService.PR_FABRIC.equals(project.getTypeProj())){
            userType = CrmService.FABRIC;
            role = Role.FAB;
        }
        if (CrmService.PR_SYNDICATE.equals(project.getTypeProj())){
            userType = CrmService.SYNDICATE;
            role = Role.SIND;
        }
        List<Manager> managers = crmService.getUserSind(userType);
        return managers;
    }



    //добавление роли из базы данных
  /*  @Override
    public void rolesUpdate(User user) {
        logger.trace("-- ups:" + user.getUsername());
        List<UserProject> ups = userProjectRepo.findByUserId(user.getCrmId());
        logger.trace("-- ups:" + ups.size());

        List<UserRoleAppend> list = userRoleAppendRepo.findByUsername(user.getCrmId());
        if (list.size() == 0) {
            return;
        }
        for (UserRoleAppend ura : list) {
            if (user.getAuthorities().contains(ura.getRole())){
                if (ura.getAdd() != 1) {
                    logger.trace("remove role : " + ura.toString());
                    user.getAuthorities().remove(ura.getRole());
                }
            } else {
                if (ura.getAdd() == 1)
                {
                    logger.trace("add role : " + ura.toString());
                    user.getAuthorities().add(ura.getRole());
                }
            }
        }
    }
*/


    @Override
    public List<UserProject> getUserProject(String userId) {
        return userProjectRepo.findByUserId(userId);
    }


    @Override
    public List<UserProject> getUsersProjects() {
        return userProjectRepo.findAll();
    }

    @Override
    public List<Project> getProjects() {
        return projectRepo.findAll();
    }

    @Override
    public void saveUserProject(UserProject userProject) {
        userProjectRepo.saveAndFlush(userProject);
    }

    @Override
    public void addUserProject(String userId, Project project) {
        UserProject userProject = new UserProject();
        userProject.setUserId(userId);
        userProject.setProject(project);
        userProject.setDateAppend(Calendar.getInstance().getTime());
        userProjectRepo.saveAndFlush(userProject);
    }

    @Override
    public void removeUserProject(String userId, Project project) {
        userProjectRepo.deleteUserProjectByUserIdAndProject(userId, project);
    }

    @Override
    public List<UserProject> listUserProjectByProject(Project project) {
        return userProjectRepo.findByProject(project);
    }

    @Override
    public void addProjectFromCrm(String type, List<ProjectResponsePacket> packets) {
        if (packets == null) return;
        if (type == null) {
            logger.debug("type is null");
        }


        for(ProjectResponsePacket projectResponsePacket : packets) {
            logger.debug(projectResponsePacket.projectId);
        }

        for(ProjectResponsePacket projectResponsePacket : packets) {

            if (projectResponsePacket.projectId != null){
                Project project = projectRepo.findOne(projectResponsePacket.projectId);
                if (project == null) {
                    project = new Project();
                    project.setProjectId(projectResponsePacket.projectId);
                    project.setCode(projectResponsePacket.projectCode);
                    project.setTitle(projectResponsePacket.getProjectNomination());
                    project.setTypeProj(type); //
                    project.setLink(projectResponsePacket.projectId);
                    projectRepo.saveAndFlush(project);
                }
            }
        }
    }

    @Override
    public Project getProject(String id) {
        return projectRepo.findOne(id);

    }

    @Override
    public void saveProject(Project project) {
        projectRepo.saveAndFlush(project);
    }


}
