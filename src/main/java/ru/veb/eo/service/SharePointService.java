package ru.veb.eo.service;


import com.microsoft.sharepoint.rest.SPFile;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;


import java.io.*;

import java.util.List;

public interface SharePointService {

    public void saveProjectFile(String projectId, MultipartFile file, String inn, String projectCode);

    public void saveProjectBytesStream(String projectId, String filename, ByteArrayOutputStream out, String inn, String projectCode);


    public void saveProjectBytes(String projectId, String filename, byte[] content, String inn, String projectCode);

    public void createFolder(String path);

    public void createDocument(String projectId, String folder, String fileName, byte[] content);

    public void copyIntoItemsLocal(String sourceUrl, String destinationUrl);

    public List<SPFile> getListFile(String folder);

    public byte[] getFile(String url) throws IOException;

}