package ru.veb.eo.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import fr.opensagres.xdocreport.core.utils.Base64Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.thymeleaf.util.ListUtils;
import ru.veb.eo.Properties;
import ru.veb.eo.model.User;
import ru.veb.eo.model.crm.*;
import ru.veb.eo.model.exchange.*;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.model.form.firstrequest.FirstRequest;
import ru.veb.eo.repository.RegionRepository;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Реализация интегрированная с SuiteCRM
 *
 * @author Ruslan
 */
public class CrmServiceSuite implements CrmService {

    private static final Logger logger = LoggerFactory.getLogger(CrmServiceSuite.class);

    public static final String FABRIC = "get_fabric";
    public static final String SYNDICATE = "get_syndicate";
    public static final String ADMIN = "get_admin";

    @Autowired
    Properties properties;

    @Autowired
    ReferenceService referenceService;

    @Autowired
    @Qualifier("restTemplateCRM")
    private RestTemplate restTemplate;

    @Autowired
    private UserService userService;

    @Autowired
    private RegionRepository regionRepository;

    public void addValuation(String projectId, String valuation, String comment, String managerId) {
        try {
            UtilsService.securityVueJS(comment);

            Map<String, String> uriParams = new HashMap<String, String>();
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(properties.getCrmHost())
                    .queryParam("entryPoint", "cm_valuation_create")
                    .queryParam("value", valuation)
                    .queryParam("project_id", projectId)
                    .queryParam("is_external", true)
                    .queryParam("description", comment);

            if (!Strings.isNullOrEmpty(managerId)) builder = builder.queryParam("employee_id", managerId);

            restTemplate.getForEntity(builder.buildAndExpand(uriParams).toUri(), String.class);
        } catch (Exception e) {
            logger.error("CRM add valuation. projectId={}, valuation={}, comment={}, managerId={}, ERROR={}", projectId, valuation, comment, managerId, e.getMessage());
        }
    }

    @Override
    public String addProject(ProjectRequest project) {

        try {
            UtilsService.securityVueJS(project);

            CrmAddRequest request = new CrmAddRequest(properties.getCrmSecret(), project);
            logger.info("CRM add. Request={}", request.toString());

            HttpEntity<Object> entity = this.getRequestWithAuthHeaders(request);

            ObjectMapper om = new ObjectMapper();
            logger.info("CRM add. Request JSON={}", om.writeValueAsString(request));
            CrmAddResponse response = restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&action=add", entity, CrmAddResponse.class);
            logger.info("CRM add. Response={}", response.toString());

            return response.getProjectId();

        } catch (Exception e) {

            logger.error("CRM add. ERROR={}", e.getMessage());
            return null;
        }
    }

    @Override
    public ProjectRequest getProjectRequest(String projectId, String applicantEmail, String projectCertifiedManagerId) {

        try {
            if (Strings.isNullOrEmpty(projectId) || !UtilsService.checkGUID(projectId))
                throw new Exception("Project id isn't correct.");

            CrmGetRequest request = new CrmGetRequest(projectId, properties.getCrmSecret(), applicantEmail, projectCertifiedManagerId);
            logger.debug("CRM get. Request={}", request.toString());

            HttpEntity<Object> entity = this.getRequestWithAuthHeaders(request);
            CrmGetResponse response = null;
            if (projectCertifiedManagerId == null || "".equals(projectCertifiedManagerId)) {
                response = restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&action=get&type=fabric", entity, CrmGetResponse.class);
            } else {
                response = restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&action=get", entity, CrmGetResponse.class);
            }

            logger.debug("CRM get. Response={}", response.toString());

            ProjectResponse projectResponse = response.getProjectResponse();
            ProjectRequest projectRequest = new ProjectRequest();

            if (projectResponse != null) {
                BeanUtils.copyProperties(projectResponse, projectRequest);
                if (projectResponse.initiator != null) {
                    BeanUtils.copyProperties(projectResponse.initiator, projectRequest);
                    if (projectResponse.initiator.collaborator != null)
                        BeanUtils.copyProperties(projectResponse.initiator.collaborator, projectRequest);
                    if (projectResponse.initiator.executive != null)
                        BeanUtils.copyProperties(projectResponse.initiator.executive, projectRequest);
                    if (projectResponse.declarer != null)
                        BeanUtils.copyProperties(projectResponse.declarer, projectRequest);
                    if (projectResponse.projectManager != null)
                        projectRequest.setProjectCertifiedManagerId(projectResponse.projectManager.getCrmId());
                    if (projectRequest.getInitiatorNameRus() == null)
                        projectRequest.setInitiatorNameRus(projectResponse.initiator.initiatorNameShort);
                }
                projectRequest.setParticipants(response.getParticipants());
                projectRequest.setTypeOfFinancings(response.getTypeOfFinancings());

                projectRequest.setSupplies(response.getSupplies());
                projectRequest.setParticipantsIndividuals(response.getParticipantsIndividuals());
            }

            logger.debug("CRM get detail. Convert to project={}", projectRequest.toString());
            UtilsService.securityVueJS(projectRequest);


            if (
                projectRequest != null &&
                    (
                        (projectRequest.getProjectCertifiedManagerId() != null && projectRequest.getProjectCertifiedManagerId().equals(projectCertifiedManagerId)) ||
                            (projectRequest.getApplicantEmailOld() != null && projectRequest.getApplicantEmailOld().toLowerCase().equals(applicantEmail)) ||
                            (projectRequest.getApplicantEmail() != null && projectRequest.getApplicantEmail().toLowerCase().equals(applicantEmail))
                    ) || projectCertifiedManagerId == null
                )
                return projectRequest;
            else {
                logger.error("CRM get detail. projectId={}, ACCESS DENIED={}", projectId, projectRequest);

                return null;

            }
        } catch (Exception e) {
            logger.error("CRM get detail. projectId={}, ERROR={}", projectId, e.getMessage());
            return null;
        }
    }

    public ProjectResponse getProject(String projectId, String applicantEmail, String projectCertifiedManagerId) {

        try {
            if (Strings.isNullOrEmpty(projectId) || !UtilsService.checkGUID(projectId))
                throw new Exception("Project id isn't correct.");

            CrmGetRequest request = new CrmGetRequest(projectId, properties.getCrmSecret(), applicantEmail, projectCertifiedManagerId);
            logger.debug("CRM get. Request={}", request.toString());

            HttpEntity<Object> entity = this.getRequestWithAuthHeaders(request);

            CrmGetResponse response = restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&action=get", entity, CrmGetResponse.class);
            logger.debug("CRM get. Response={}", response.toString());

            ProjectResponse projectResponse = response.getProjectResponse();
            UtilsService.securityVueJS(projectResponse);

            if (
                    projectResponse != null &&
                            (
                                    (projectResponse.getProjectManager() != null && projectResponse.getProjectManager().getCrmId() != null && projectResponse.getProjectManager().getCrmId().equals(projectCertifiedManagerId)) ||
                                            (projectResponse.getDeclarer() != null && projectResponse.getDeclarer().getApplicantEmails().contains(applicantEmail))
                            )
                    )
                return projectResponse;
            else {
                logger.error("CRM get. projectId={}, ACCESS DENIED={}", projectId, projectResponse);
                return null;
            }
        } catch (Exception e) {
            logger.error("CRM get. projectId={}, ERROR={}", projectId, e.getMessage());
            return null;
        }
    }

    public List<ProjectResponsePacket> getProjectPacket(Integer count, Integer offset, String applicantEmail, String projectCertifiedManagerId) {
        if (applicantEmail == null && projectCertifiedManagerId == null) {
            logger.error("ACCESS DENIED=getProjectPacket");
            return null;
        }
        CrmGetResponsePacket response = null;
        try {
            List<ProjectResponse> result = new ArrayList<>();

            CrmGetRequest request = new CrmGetRequest(null, 500, 0, properties.getCrmSecret(), applicantEmail, projectCertifiedManagerId);
            logger.debug("CRM get. Request={}", request.toString());

            HttpEntity<Object> entity = this.getRequestWithAuthHeaders(request);

            response = restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&action=get", entity, CrmGetResponsePacket.class);
            logger.debug("CRM get. Response={}", response.toString());
        } catch (Exception e) {
            logger.error("CRM get. ERROR={}", e.getMessage());
            return null;
        }

        if (response != null && response.getItems() != null && response.getItems().size() > 0) {
            if (projectCertifiedManagerId != null) {
                String managerName = "";
                try {
                    User manager = userService.getManagerByCrmId(projectCertifiedManagerId);
//                    Manager manager = getManagerById( projectCertifiedManagerId);
                    if (manager != null)
                        managerName = manager.getNameFirst() + " " + manager.getNameLast();

                } catch (Exception e) {
                    logger.error("CRM processing ProjectResponsePacket. ERROR={}", e.getMessage());
                    return null;
                }
                for (ProjectResponsePacket projectResponsePacket : response.getItems()) {
                    projectResponsePacket.setProjectCertifiedManagerName(managerName);

                    if(projectResponsePacket.projectRequestStatusDate != null) {
                        // разница в милисекундах между сегоднешнем днём и днем создания проекта
                        long diffMilleseconds = new Date().getTime() - projectResponsePacket.projectRequestStatusDate.getTime();

                        // запишем в поле разницу
                        projectResponsePacket.setDifferenceDaysBetweenTodayAndEnteredDate(TimeUnit.DAYS.convert(diffMilleseconds, TimeUnit.MILLISECONDS));
                    }

                    // добавим регион проекту
                    if(projectResponsePacket.getSaleRegionID() > 0){
                        projectResponsePacket.setSaleRegion(regionRepository.findOne(projectResponsePacket.getSaleRegionID()));
                    }

                    UtilsService.securityVueJS(projectResponsePacket);

                    if (projectResponsePacket.getProjectRequestStatus() != null)
                        projectResponsePacket.setProjectStatusName(referenceService.getProjectStatus(projectResponsePacket.getProjectRequestStatus()));
                }
            } else {
                for (ProjectResponsePacket projectResponsePacket : response.getItems()) {

                    try {
                        UtilsService.securityVueJS(projectResponsePacket);

                        User manager = userService.getManagerByCrmId(projectResponsePacket.getProjectCertifiedManagerId());
//                        Manager manager = getManagerById(projectResponsePacket.getProjectCertifiedManagerId());
                        if (manager != null)
                            projectResponsePacket.setProjectCertifiedManagerName(manager.getNameFirst() + " " + manager.getNameLast());

                        if (projectResponsePacket.getProjectRequestStatus() != null)
                            projectResponsePacket.setProjectStatusName(referenceService.getProjectStatus(projectResponsePacket.getProjectRequestStatus()));
                    } catch (Exception e) {
                        logger.error("CRM processing ProjectResponsePacket. ERROR={}", e.getMessage());
                        return null;
                    }
                }
            }
        }

        return response.getItems();

    }

    public List<Task> getTasks(String projectId) {
        try {
            if (Strings.isNullOrEmpty(projectId) || !UtilsService.checkGUID(projectId))
                throw new Exception("Project id isn't correct.");

            CrmGetTasksRequest request = new CrmGetTasksRequest(projectId, properties.getCrmSecret());
            logger.debug("CRM get tasks. Request={}", request.toString());

            HttpEntity<Object> entity = this.getRequestWithAuthHeaders(request);

            CrmGetTasksResponse response = restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&action=get_tasks", entity, CrmGetTasksResponse.class);
            logger.debug("CRM get tasks. Response={}", response.toString());

            return response.getItems();
        } catch (Exception e) {
            logger.error("CRM get tasks. projectId={}, ERROR={}", projectId, e.getMessage());
            return null;
        }
    }

    public CrmCloseTaskResponse closeTask(String taskId, Map<String, Object> variables) {

        try {
            CrmCloseTasksRequest request = new CrmCloseTasksRequest(taskId, properties.getCrmSecret(), variables);
            logger.debug("CRM close task. Request={}", request.toString());

            HttpEntity<Object> entity = this.getRequestWithAuthHeaders(request);

            CrmCloseTaskResponse response = restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&action=close_task", entity, CrmCloseTaskResponse.class);
            logger.debug("CRM close task. Response={}", response.toString());

            return response;
        } catch (Exception e) {
            logger.error("CRM close task. taskId={}, ERROR={}", taskId, e.getMessage());
            return null;
        }
    }

    @Override
    public Map<String, Integer> getRatings() {
        if (!properties.isRating()) return new HashMap<>();

        try {

            CrmGetRatingRequest request = new CrmGetRatingRequest(properties.getCrmSecret());
            logger.debug("CRM rating. Request={}", request.toString());

            HttpEntity<Object> entity = this.getRequestWithAuthHeaders(request);

            CrmGetRatingResponse response = restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&action=get_web_reports_cm", entity, CrmGetRatingResponse.class);
            logger.debug("CRM rating. Response={}", response.toString());

            Map<String, Integer> ratings = new HashMap<>();
            double max = 0;
            if (!ListUtils.isEmpty(response.getRatings())) {
                for (int i = 0; i < response.getRatings().size(); i++) {
                    if (response.getRatings().get(i).getRating() > max) max = response.getRatings().get(i).getRating();
                }
                for (int i = 0; i < response.getRatings().size(); i++) {
                    ratings.put(response.getRatings().get(i).getId(), (int) (100D * response.getRatings().get(i).getRating() / max));
                }
            }

            return ratings;
        } catch (Exception e) {
            logger.error("CRM rating. ERROR={}", e.getMessage());
            return new HashMap<>();
        }
    }

/*    /index.php?entryPoint=request&action=get_fabric
      /index.php?entryPoint=request&action=get_syndicate
      /index.php?entryPoint=request&action=get_admin
*/

    @Override
    public List<Manager> getUserSind(String type) {
        try {

            CrmGetManagersRequest request = new CrmGetManagersRequest(null, null, properties.getCrmSecret());
            logger.trace("CRM get managers. Request={}", request.toString());

            HttpEntity<Object> entity = this.getRequestWithAuthHeaders(request);

            CrmGetManagersResponse response =
                    restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&action="+type, entity,
                            CrmGetManagersResponse.class);
            logger.trace("CRM get managers. Response={}", response.toString());

            if (response.getManagers() != null) {
                for (Manager manager : response.getManagers()) UtilsService.securityVueJS(manager);
                return response.getManagers();
            } else
                return new ArrayList<Manager>();

        } catch (Exception e) {
            logger.info("CRM get UserSind. type={}, ERROR={}", type, e.getMessage());
            return new ArrayList<Manager>();
        }
    }

    //fabric|syndicate
    @Override
    public List<ProjectResponsePacket> getProjectPacketSind(Integer count,
                                                            Integer offset,
                                                            String type,
                                                            String applicantEmail,
                                                            String projectCertifiedManagerId) {
        if (logger.isDebugEnabled()) {
            logger.debug("count = " + count);
            logger.debug("offset = " + offset);
            logger.debug("type = " + type);
            logger.debug("applicantEmail = " + applicantEmail);
            logger.debug("projectCertifiedManagerId = " + projectCertifiedManagerId);
        }
        CrmGetResponsePacket response = null;
        try {
            List<ProjectResponse> result = new ArrayList<>();

            CrmGetRequest request = new CrmGetRequest(null, 500, 0, properties.getCrmSecret(), applicantEmail, projectCertifiedManagerId);
            logger.debug("CRM getProjectPacketSind. Request={}", request.toString());

            HttpEntity<Object> entity = this.getRequestWithAuthHeaders(request);
            logger.debug("CRM getProjectPacketSind. type={}", type);
            response = restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&action=get&type="+type, entity, CrmGetResponsePacket.class);

            logger.debug("CRM getProjectPacketSind. Response={}", response.toString());
        } catch (Exception e) {
            logger.error("CRM getProjectPacketSind. ERROR={}", e.getMessage());
            return null;
        }

        if (response != null && response.getItems() != null && response.getItems().size() > 0) {

            for (ProjectResponsePacket projectResponsePacket : response.getItems()) {
                try {
                    UtilsService.securityVueJS(projectResponsePacket);

                    /*User manager = userService.getManagerByCrmId(projectResponsePacket.getProjectCertifiedManagerId());

                    if (manager != null) {
                        projectResponsePacket.setProjectCertifiedManagerName(manager.getNameFirst() + " " + manager.getNameLast());
                    }*/

                    if (projectResponsePacket.getProjectRequestStatus() != null)
                        projectResponsePacket.setProjectStatusName(referenceService.getProjectStatus(projectResponsePacket.getProjectRequestStatus()));
                } catch (Exception e) {
                    logger.error("CRM processing ProjectResponsePacket. ERROR={}", e.getMessage());
                    return null;
                }
            }
        }
        return response.getItems();
    } // List<ProjectResponsePacket> getProjectPacketSind


    //index.php?entryPoint=request&action=get_project_documents_id
    @Override
    public List<DocumentSP> getSPFileProp(String projectId) {
        try {
            List<ProjectResponse> result = new ArrayList<>();
            CrmGetRequest request = new CrmGetRequest(projectId, 500, 0, properties.getCrmSecret(), null, null);
            logger.debug("CRM getSPFileProp. Request={}", request.toString());

            HttpEntity<Object> entity = this.getRequestWithAuthHeaders(request);
            CrmGetDocumentSPResponse response = null;
            response = restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&entryPoint=request&action=get_project_documents_id", entity, CrmGetDocumentSPResponse.class);

            logger.debug("CRM getSPFileProp. Response={}", response.toString());
            return response.documentSP;
        } catch (Exception e) {
            logger.error("CRM getProjectPacketSind. ERROR={}", e.getMessage());
            return null;
        }
    }

    @Override
    public void closeTasks(String projectId, List<String> taskNames, Map<String, Object> variables) {
        List<Task> tasks = getTasks(projectId);
        if (!ListUtils.isEmpty(tasks)) {
            for (Task task : tasks) {
                if (!Strings.isNullOrEmpty(task.getName()) && !ListUtils.isEmpty(taskNames) && taskNames.contains(task.getName())) {
                    closeTask(task.getId(), variables); //isDZO true false
                }
            }
        }
    }

    public void updateProject(Form form) {

        try {
            ProjectRequest project = new ProjectRequest();
            form.saveData(project);
            UtilsService.securityVueJS(project);

            CrmUpdateRequest request = new CrmUpdateRequest(project.getProjectId(), properties.getCrmSecret(), project);
            logger.debug("CRM update. Request={}", request.toString());

            HttpEntity<Object> entity = this.getRequestWithAuthHeaders(request);

            CrmUpdateResponse response = restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&action=update", entity, CrmUpdateResponse.class);
            logger.debug("CRM update. Response={}", response.toString());

        } catch (Exception e) {
            logger.error("CRM update. ERROR={}", e.getMessage());
        }
    }

    @Override
    public void updateProjectStage(int stage, ProjectRequest project) {
        project.setProjectRequestStatus(stage);

        // дата перехода в текущий статус
        project.setProjectRequestStatusDate(new Date());

        UtilsService.securityVueJS(project);
        updateProject(project);
    }

    public void updateProject(ProjectRequest project) {

        try {

            UtilsService.securityVueJS(project);

            CrmUpdateRequest request = new CrmUpdateRequest(project.getProjectId(), properties.getCrmSecret(), project);
            logger.debug("CRM update. Request={}", request.toString());

            HttpEntity<Object> entity = this.getRequestWithAuthHeaders(request);

            CrmUpdateResponse response = restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&action=update", entity, CrmUpdateResponse.class);
            logger.debug("CRM update. Response={}", response.toString());

        } catch (Exception e) {
            logger.error("CRM update. ERROR={}", e.getMessage());
        }
    }

    public List<Manager> getManagersByRegion(Integer regionId) {

        try {

            CrmGetManagersRequest request = new CrmGetManagersRequest(regionId, null, properties.getCrmSecret());
            logger.trace("CRM get managers. Request={}", request.toString());

            HttpEntity<Object> entity = this.getRequestWithAuthHeaders(request);

            CrmGetManagersResponse response = restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&action=get_managers", entity, CrmGetManagersResponse.class);
            logger.trace("CRM get managers. Response={}", response.toString());

            if (response.getManagers() != null) {
                for (Manager manager : response.getManagers()) UtilsService.securityVueJS(manager);
                return response.getManagers();
            } else
                return new ArrayList<Manager>();

        } catch (Exception e) {
            logger.error("CRM get managers. id={}, ERROR={}", regionId, e.getMessage());

            return null;
        }
    }

    /**
     * Подготовить запрос с авторизационными заголовкими
     * @param request   - объект запроса
     * @return          - сущность для запроса
     */
    public HttpEntity<Object> getRequestWithAuthHeaders(Object request){
        HttpHeaders headers = new HttpHeaders();
        this.setAuthData(headers, null, null);
        HttpEntity<Object> entity = new HttpEntity<Object>(request, headers);
        return entity;
    }

    /**
     * Добавить в заголовок авторизацию
     * @param headers   - заголовки
     * @param username  - имя пользователя (если null, то берется из свойств)
     * @param password  - пароль (если null, то берется из свойств)
     */
    void setAuthData(HttpHeaders headers, String username, String password){

        if(username == null){
            username = properties.getCrmAuthUsername();
        }

        if(password == null){
            password = properties.getCrmAuthPassword();
        }

        String userAndPass = username + ":" + password;

        headers.add("Authorization", "Basic " + Base64Utility.encode(userAndPass.getBytes()));
    }

    public Manager getManagerById(String managerId) {
        try {
            if (Strings.isNullOrEmpty(managerId) || !UtilsService.checkGUID(managerId))
                throw new Exception("Manager id isn't correct.");

            CrmGetManagersRequest request = new CrmGetManagersRequest(null, managerId, properties.getCrmSecret());
            logger.debug("CRM get managers. Request={}", request.toString());

            HttpEntity<Object> entity = this.getRequestWithAuthHeaders(request);

            CrmGetManagersResponse response = restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&action=get_managers", entity, CrmGetManagersResponse.class);
            logger.debug("CRM get managers. Response={}", response.toString());

            if (!ListUtils.isEmpty(response.getManagers())) {
                Manager manager = response.getManagers().get(0);
                UtilsService.securityVueJS(manager);
                return manager;
            } else {
                logger.error("CRM get managers. id={}, ACCESS DENIED={}", managerId, "getManagerById");
                return null;
            }

        } catch (Exception e) {
            logger.error("CRM get managers. id={}, ERROR={}", managerId, e.getMessage());
            return null;
        }
    }

    public List<Message> getMessages(String projectId) {

        try {
            if (Strings.isNullOrEmpty(projectId) || !UtilsService.checkGUID(projectId))
                throw new Exception("Project id isn't correct.");

            CrmGetMessagesRequest request = new CrmGetMessagesRequest(projectId, 500, 0, properties.getCrmSecret());
            logger.debug("CRM get messages. Request={}", request.toString());

            HttpEntity<Object> entity = this.getRequestWithAuthHeaders(request);

            CrmGetMessagesResponse response = restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&action=get_messages", entity, CrmGetMessagesResponse.class);
            logger.debug("CRM get messages. Response={}", response.toString());

            if (!ListUtils.isEmpty(response.getMessages())) {
                for (Message message : response.getMessages()) {
                    UtilsService.securityVueJS(message);
                }
                return response.getMessages();
            } else {
                logger.error("CRM get messages. id={}, ACCESS DENIED={}", projectId, "getMessages");
                return null;
            }

        } catch (Exception e) {
            logger.error("CRM get messages. id={}, ERROR={}", null, e.getMessage());
            return null;
        }
    }

    public String sendMessage(String projectId, Message message) {

        try {
            if (Strings.isNullOrEmpty(projectId) || !UtilsService.checkGUID(projectId))
                throw new Exception("Project id isn't correct.");

            UtilsService.securityVueJS(message);

            CrmSendMessageRequest request = new CrmSendMessageRequest(projectId, properties.getCrmSecret(), message);
            logger.debug("CRM send message. Request={}", request.toString());

            HttpEntity<Object> entity = this.getRequestWithAuthHeaders(request);

            CrmSendMessageResponse response = restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&action=send_message", entity, CrmSendMessageResponse.class);
            logger.debug("CRM send message. Response={}", response.toString());

            if (!"success".equals(response.getResult())) {
                logger.error("CRM send message. ERROR={}", response.getMessage());
                return null;
            } else {
                return response.getMessageId();
            }

        } catch (Exception e) {
            logger.error("CRM send message. id={}, ERROR={}", null, e.getMessage());
            return null;
        }
    }

    @Override
    public DocumentAdd addDocument(String projectId, DocumentAdd document) {
        try {
            CrmAddDocumentRequest request = new CrmAddDocumentRequest(projectId, properties.getCrmSecret(), document);
            logger.debug("CRM add document. Request={}", request.toString());

            HttpEntity<Object> entity = this.getRequestWithAuthHeaders(request);

            CrmAddDocumentResponse response = restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&action=add_document_to_project", entity, CrmAddDocumentResponse.class);
            logger.debug("CRM add document. Response={}", response.toString());

            if (null != response)
                return response.getDocument();
            else {
                logger.error("CRM add document. projectId={}, document={}, RESPONSE={}", projectId, document, response);
                return null;
            }
        } catch (Exception e) {
            logger.error("CRM add document. projectId={}, document={}, ERROR={}", projectId, document, e.getMessage());
            return null;
        }
    }
}
