package ru.veb.eo.service;

import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.veb.eo.model.Reference;
import ru.veb.eo.model.reference.*;
import ru.veb.eo.repository.*;

import java.util.List;

@Service
public class ReferenceService {
    Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private DaysRepository daysRepository;
    @Autowired
    private DocumentNameRepository documentNameRepository;
    @Autowired
    private FaceTypeRepository faceTypeRepository;
    @Autowired
    private FaceRoleRepository faceRoleRepository;
    @Autowired
    private FinancingCurrencyRepository financingCurrencyRepository;
    @Autowired
    private LimitationRepository limitationRepository;
    @Autowired
    private MonthsRepository monthsRepository;
    @Autowired
    private OpfRepository opfRepository;
    @Autowired
    private ProjectCostRepository projectCostRepository;
    @Autowired
    private ProjectPaybackTimeRepository projectPaybackTimeRepository;
    @Autowired
    private ProjectSectorsRepository projectSectorsRepository;
    @Autowired
    private ProjectTypeRepository projectTypeRepository;
    @Autowired
    private ProjectDirectionRepository projectType2Repository;
    @Autowired
    private RegionRepository regionRepository;
    @Autowired
    private YearsRepository yearsRepository;
    @Autowired
    private ProjectStatusRepository projectStatusRepository;
    @Autowired
    private ProjectDirectionRepository projectDirectionRepository;
    @Autowired
    private ParticipantRoleRepository participantRoleRepository;
    @Autowired
    private  DistrictRepository districtRepository;

    public List<District> getDistricts() {
        return districtRepository.findAll(new Sort("title"));
    }

    public String getRegion(int id) {
        return regionRepository.getOne(id).getTitle();
    }

    public List<Years> getYear() {
        return yearsRepository.findAll();
    }

    public List<Months> getMonth() {
        return monthsRepository.findAll();
    }

    public String getMonth(int id) {
        Months one = monthsRepository.findOne(id);
        return one != null ? one.getTitle() : null;
    }

    public List<Days> getDay() {
        return daysRepository.findAll();
    }

    public List<Region> getRegion() {
        return regionRepository.findAll(new Sort("title"));
    }

    public List<ProjectCost> getProjectCost() {
        return projectCostRepository.findAll();
    }

    public List<ProjectPaybackTime> getProjectPaybackTime() {
        return projectPaybackTimeRepository.findAll();
    }

    public List<ProjectSectors> getProjectSectors() {
        return projectSectorsRepository.findAll(new Sort("title"));
    }

    public List<ProjectSectors> getProjectSectorsByDirectionId(int directionId) {
        return projectSectorsRepository.findAllByDirectionId(directionId, new Sort("title"));
    }

    public String getProjectSectors(Integer id) {
        return projectSectorsRepository.findOne(id).getTitle();
    }

    public String getProjectSector(String id) {
        Integer integer = null;
        try {
            integer = Integer.valueOf(id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        ProjectSectors one = projectSectorsRepository.findOne(integer);
        return one != null ? one.getTitle() : null;
    }

    public ProjectSectors getProjectSectorByOrder(int sortOrder) {
        return projectSectorsRepository.findFirstBySortOrder(sortOrder);
    }

    public List<ProjectStatus> getProjectStatus() {
        return projectStatusRepository.findAll();
    }

    public List<ParticipantRole> getParticipantRole() {
        return participantRoleRepository.findAll(new Sort("title"));
    }

    public String getProjectStatus(Integer id) {
        ProjectStatus one = projectStatusRepository.findOne(id);
        if (one != null) {
            log.info("Found status by id {}: {}", id, one);
            return one.getTitle();
        } else {
            return null;
        }
    }

    public List<ProjectType> getProjectType() {
        return projectTypeRepository.findAll();
    }

    public String getProjectType(Integer id) {
        return projectTypeRepository.findOne(id).getTitle();
    }

    public List<ProjectDirection> getProjectDirections() {
        return projectDirectionRepository.findAll(new Sort("title"));
    }

    public String getProjectDirection(String id) {
        Integer integer = null;
        try {
            integer = Integer.valueOf(id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        ProjectDirection one = projectDirectionRepository.findOne(integer);
        return one != null ? one.getTitle() : null;
    }

    public String getOpf(String id) {
        Integer integer = null;
        try {
            integer = Integer.valueOf(id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        Opf one = opfRepository.findOne(integer);
        return one != null ? one.getTitle() : null;
    }

    public String getParticipantRole(String id) {
        Integer integer = null;
        try {
            integer = Integer.valueOf(id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        ParticipantRole one = participantRoleRepository.findOne(integer);
        return one != null ? one.getTitle() : null;
    }

    public String getProjectType(String id) {
        Integer integer = null;
        try {
            integer = Integer.valueOf(id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        ProjectType one = projectTypeRepository.findOne(integer);
        return one != null ? one.getTitle() : null;
    }

    public String getDocumentType(String id) {
        Integer integer = null;
        try {
            integer = Integer.valueOf(id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        DocumentName one = documentNameRepository.findOne(integer);
        return one != null ? one.getTitle() : null;
    }

    public String getProjectRegion(String id) {
        Integer integer = null;
        try {
            integer = Integer.valueOf(id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        Region one = regionRepository.findOne(integer);
        return one != null ? one.getTitle() : null;
    }

    public String getProjectPaybackTine(String id) {
        if (id == null) {
            return null;
        }

        Integer integer = null;
        try {
            integer = Integer.valueOf(id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        ProjectPaybackTime one = projectPaybackTimeRepository.findOne(integer);
        return one != null ? one.getTitle() : null;
    }

    public Reference getReference() {
        Reference ref = new Reference();
        ref.days = daysRepository.findAll();
        ref.documentName = documentNameRepository.findAll();
        ref.faceType = faceTypeRepository.findAll(new Sort("title"));
        ref.faceRole = faceRoleRepository.findAll(new Sort("title"));
        ref.financingCurrency = financingCurrencyRepository.findAll(new Sort("title"));
        ref.limitation = limitationRepository.findAll();
        ref.months = monthsRepository.findAll();
        ref.opf = opfRepository.findAll(new Sort("title"));
        ref.projectCost = projectCostRepository.findAll();
        ref.projectPaybackTime = projectPaybackTimeRepository.findAll();
        ref.projectSectors = projectSectorsRepository.findAll(new Sort("title"));;
        ref.projectType = projectTypeRepository.findAll();
        ref.projectDirection = projectType2Repository.findAll(new Sort("title"));
        ref.region = regionRepository.findAll(new Sort("title"));
        ref.years = yearsRepository.findAll();
        ref.projectStatus = projectStatusRepository.findAll();
        ref.projectDirection = projectDirectionRepository.findAll();
        ref.participantRole = participantRoleRepository.findAll(new Sort("title"));

        ref.documentNameResident = documentNameRepository.findAllByResident(true);
        ref.documentNameNotResident = documentNameRepository.findAllByResident(false);

        return ref;
    }

    public Object getFaceRole(String id) {
        if (id == null) {
            return null;
        }
        Integer integer = null;
        try {
            integer = Integer.valueOf(id);
        } catch (Exception e) {
            log.warn("FaceRole id invalid: {}, {}", id, e.toString(), e);
            return null;
        }
        FaceRole one = faceRoleRepository.findOne(integer);
        return one != null ? one.getTitle() : null;
    }

    public Object getFaceType(String id) {
        if (id == null) {
            return null;
        }

        Integer integer = null;
        try {
            integer = Integer.valueOf(id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        FaceType one = faceTypeRepository.findOne(integer);
        return one != null ? one.getTitle() : null;
    }
}




















