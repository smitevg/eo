package ru.veb.eo.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import ru.veb.eo.model.crm.*;
import ru.veb.eo.model.exchange.CrmCloseTaskResponse;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.model.form.firstrequest.FirstRequest;

@Service
public interface CrmService {

    public static final String FABRIC = "get_user_fabric";
    public static final String SYNDICATE = "get_user_syndicate";
    public static final String ADMIN = "get_user_admin";

    public static final String PR_FABRIC = "fabric";
    public static final String PR_SYNDICATE = "syndicate";



    public void addValuation(String projectId, String valuation, String comment, String managerId);

    public String addProject(ProjectRequest project);

    public ProjectRequest getProjectRequest(String projectId, String applicantEmail, String projectCertifiedManagerId);

    public ProjectResponse getProject(String projectId, String applicantEmail, String projectCertifiedManagerId);

    public List<ProjectResponsePacket> getProjectPacket(Integer count, Integer offset, String applicantEmail, String projectCertifiedManagerId);

    public List<Task> getTasks(String projectId);

    List<DocumentSP> getSPFileProp(String projectId);

    public void closeTasks(String projectId, List<String> taskNames, Map<String, Object> variables);

    public CrmCloseTaskResponse closeTask(String taskId, Map<String, Object> variables);

    public void updateProject(Form form);

    public void updateProject(ProjectRequest project);
    
    public List<Manager> getManagersByRegion(Integer regionId);

    public Manager getManagerById(String managerId);

    public List<Message> getMessages(String projectId);

    public String sendMessage(String projectId, Message message);

    public DocumentAdd addDocument(String projectId, DocumentAdd document);

    public void updateProjectStage(int stage, ProjectRequest project);

    public Map<String, Integer> getRatings();

    public List<Manager> getUserSind(String type);

    public List<ProjectResponsePacket> getProjectPacketSind(Integer count, Integer offset,
                                                            String type,
                                                            String applicantEmail,
                                                            String projectCertifiedManagerId);

}
