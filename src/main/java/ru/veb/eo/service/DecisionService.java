package ru.veb.eo.service;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.ListUtils;
import ru.veb.eo.Properties;
import ru.veb.eo.model.Decision;
import ru.veb.eo.model.User;
import ru.veb.eo.model.crm.Manager;
import ru.veb.eo.model.form.firstrequest.FirstRequest;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class DecisionService {

	private static final Logger logger = LoggerFactory.getLogger(DecisionService.class);

	@Autowired private CrmService cs;
	@Autowired private UserService us;
	@Autowired private ReferenceService rs;
	@Autowired private Properties properties;

	public Decision calculateDecision(FirstRequest firstRequest) {

		Decision decision = null;

		List<User> managers = us.getManagersByRegion(firstRequest.getProjectSalePlace());
        if (ListUtils.isEmpty(managers)) {
            User user = us.getUser(properties.getDefaultManager());
            if (user != null) {
                managers = Arrays.asList(user);
            }
        }
		if (
				String.valueOf(rs.getProjectSectors().size() - 1).equals(firstRequest.getProjectSectors1()) ||
				firstRequest.getProjectCost().equals("0") ||
				firstRequest.getProjectCost().equals("1") ||
				firstRequest.getProjectPaybackTime().equals("0") ||
				firstRequest.getProjectPaybackTime().equals("2")) {

			decision = new Decision("fail", managers);

		} else {
			decision = new Decision("success", managers);
		}

		logger.debug("\nThe decision has been calculated: {}", decision);
		return decision;
	}
}
