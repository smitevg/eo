package ru.veb.eo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class EmailService {
    private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

    @Qualifier("getJavaMailSender")
    @Autowired
    public JavaMailSender emailSender;

    public void sendSimpleMessage(String to, String subject, String text) {
        if (emailSender != null) {
            logger.debug("emailSender = {}", emailSender.toString());
            try {
                SimpleMailMessage message = new SimpleMailMessage();
                message.setTo(to);
                message.setSubject(subject);
                message.setText(text);
                logger.debug("Sending email to {} subject {} body {}", to, subject, text);
                emailSender.send(message);
            } catch (Exception e) {
                e.printStackTrace();
                logger.debug("emailSender error = {}", e.getMessage());
            }
        } else {
            logger.debug("emailSender is null, sending to {} subject {} text {}", to, subject, text);
        }
    }

    public void sendMessageWithAttachment(String to, String subject, String text, String fileName, InputStreamSource inputStreamSource) {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(text);
            helper.addAttachment(fileName, inputStreamSource);
            emailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
