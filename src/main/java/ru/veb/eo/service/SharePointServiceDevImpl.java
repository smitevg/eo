package ru.veb.eo.service;

import com.microsoft.sharepoint.rest.SPFile;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Profile("dev")
@Service
public class SharePointServiceDevImpl implements SharePointService {
    @Override
    public void saveProjectFile(String projectId, MultipartFile file, String inn, String projectCode) {

    }

    @Override
    public void saveProjectBytesStream(String projectId, String filename, ByteArrayOutputStream out, String inn, String projectCode) {

    }

    @Override
    public void saveProjectBytes(String projectId, String filename, byte[] content, String inn, String projectCode) {

    }

    @Override
    public void createFolder(String path) {

    }

    @Override
    public void createDocument(String projectId, String folder, String fileName, byte[] content) {

    }

    @Override
    public void copyIntoItemsLocal(String sourceUrl, String destinationUrl) {

    }

    @Override
    public List<SPFile> getListFile(String folder) {

        SPFile spFile = new SPFile();
        spFile.setLength("1000");
        spFile.setTitle("SampleFile");
        spFile.setEditLink("ya.pdf");

        SPFile spFile2 = new SPFile();
        spFile2.setLength("1002");
        spFile2.setTitle("SampleFile2");
        spFile2.setEditLink("ya2.pdf");

        ArrayList<SPFile> list = new ArrayList<>();
        list.add(spFile);
        list.add(spFile2);

        return list;
    }

    @Override
    public byte[] getFile(String url) throws IOException {
        return new byte[0];
    }
}
