package ru.veb.eo.service;

import com.microsoft.sharepoint.rest.SPFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import ru.veb.eo.entity.Project;
import ru.veb.eo.entity.UserProject;
import ru.veb.eo.entity.UserRoleAppend;
//import ru.veb.eo.entity.UserRoleAppendRepo;
import ru.veb.eo.model.User;
import ru.veb.eo.model.crm.Manager;
import ru.veb.eo.model.crm.ProjectResponsePacket;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class MySqlServiceStub implements MySqlService {

   // @Override
  //  public List<UserRoleAppend> findByUsername(String username) {
 //       return new ArrayList<UserRoleAppend>();
//    }

  //  @Override
  //  public List<UserRoleAppend> findSindRole() {
//        return new ArrayList<UserRoleAppend>();
 //   }

    @Override
    public List<Manager> findUsers(Project project) {
        return new ArrayList<Manager>();
    }

 //   @Override
  //  public void rolesUpdate(User u) {

 //   }

    @Override
    public List<UserProject> getUserProject(String username) {
        return null;
    }

    @Override
    public List<UserProject> getUsersProjects() {
        return new ArrayList<UserProject>();
    }

    @Override
    public List<Project> getProjects() {
        return null;
    }

    @Override
    public void saveUserProject(UserProject userProject) {

    }

    @Override
    public void addUserProject(String userId, Project project) {

    }

    @Override
    public void removeUserProject(String username, Project project) {

    }

    @Override
    public List<UserProject> listUserProjectByProject(Project project) {
        return null;
    }

    @Override
    public void addProjectFromCrm(String type, List<ProjectResponsePacket> packets) {

    }

    @Override
    public Project getProject(String id) {
        return null;
    }

    @Override
    public void saveProject(Project project) {

    }
}
