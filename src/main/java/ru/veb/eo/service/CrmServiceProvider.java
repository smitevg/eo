package ru.veb.eo.service;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import ru.veb.eo.model.crm.*;
import ru.veb.eo.model.exchange.CrmCloseTaskResponse;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.model.form.firstrequest.FirstRequest;

@Configuration
public class CrmServiceProvider {
    Logger log = LoggerFactory.getLogger(getClass().getName());
    
    @Autowired
    Environment env;
    
    @Bean
    @ConditionalOnProperty(name = "CRM_SERVICE", havingValue="suite")
    public CrmService getCrmService() {
        log.info("Using CrmServiceSuite, CRM_SERVICE = {}", env.getProperty("CRM_SERVICE"));
        return new CrmServiceSuite();
    }
    
    @Bean
    @ConditionalOnMissingBean(value = CrmService.class)
    public CrmService getCrmServiceEmulator() {
        log.info("Using CrmServiceEmulator, CRM_SERVICE = {}", env.getProperty("CRM_SERVICE"));
        return new CrmService() {
            
            @Override
            public void updateProject(ProjectRequest project) {
                log.info("Update project {}", project);
            }
            
            @Override
            public void updateProject(Form form) {
                log.info("Update project {}", form);
            }

            @Override
            public void updateProjectStage(int stage, ProjectRequest project) {
                log.info("Update project stage: project={}, stage={}", project, stage);
            }

            @Override
            public String sendMessage(String projectId, Message message) {
                log.info("Send message {} {}", projectId, message);
                return "deadbeef-" + UtilsService.generateRandom(5);
            }
            
            @Override
            public List<Task> getTasks(String projectId) {
                log.info("Get tasks {}", projectId);
                return new ArrayList<>();
            }

            @Override
            public List<DocumentSP> getSPFileProp(String projectId) {
                return null;
            }

            ProjectRequest oneRequest = ProjectRequest.builder().
                    projectDescription("dasdasds").
                    projectCertifiedManagerId("666").
                    applicantEmail("1@1.ru").
                    applicantNameFirst("appNameFirst").
                    applicantNameLast("appNameLast").
                    applicantPhoneNumber("89099422863").
                    build();
            
            @Override
            public ProjectRequest getProjectRequest(String projectId, String applicantEmail, String projectCertifiedManagerId) {
                log.info("Get project request {} {} {}", projectId, applicantEmail, projectCertifiedManagerId);
                Supply supply = new Supply();
                supply.setValue(1d);
                supply.setDepositWho("Who");
                oneRequest.supplies = new ArrayList<Supply>();
                oneRequest.projectRequestStatus = 1;
                oneRequest.projectCost = "1";
                oneRequest.projectPaybackTime = "1";
                oneRequest.projectSectors1 = "1";
                oneRequest.projectAmountRecipientsCash = 1d;
                oneRequest.supplies.add(supply);
                return oneRequest;
            }
            
            ProjectResponsePacket oneResponse = ProjectResponsePacket.builder().
                    projectCode("P666").
                    projectRegion("al").
                    projectDateEntered(new Date()).
                    projectNomination("dasdsadsa").
                    projectId("P666").
                    projectCertifiedManagerId("666").
                    build();
            
            @Override
            public List<ProjectResponsePacket> getProjectPacket(Integer count, Integer offset, String applicantEmail, String projectCertifiedManagerId) {
                log.info("Get project packet {} {} {} {}", count, offset, applicantEmail, projectCertifiedManagerId);
                return new ArrayList<>(Arrays.asList(new ProjectResponsePacket[] { oneResponse }));
            }
            
            @Override
            public ProjectResponse getProject(String projectId, String applicantEmail, String projectCertifiedManagerId) {
                log.info("Get project {} {} {}", projectId, applicantEmail, projectCertifiedManagerId);
                return null;
            }
            
            @Override
            public List<Message> getMessages(String projectId) {
                log.info("Get messages {}", projectId);
                return new ArrayList<>();
            }



            Manager theOne = Manager.builder().
                            crmId("666").
                            phone("+79191919191").
                            userName(env.getProperty("TEST_MANAGER", "test@ya.ru")).
                            regionId("1").
                            email(env.getProperty("TEST_MANAGER", "test@ya.ru")).
                            nameFirst("Test").
                            nameLast("Test").
                            nameMiddle("Test").build();
            
            @Override
            public List<Manager> getManagersByRegion(Integer regionId) {

                Manager u = new Manager();
                u.setCrmId("1");
                u.setEmail("1@1.ru");
                u.setNameFirst("NAME_FIRST_1");

                Manager u2 = new Manager();
                u2.setCrmId("2");
                u2.setEmail("2@2.ru");
                u2.setNameFirst("NAME_FIRST_2");

                Manager u3 = new Manager();
                u3.setCrmId("3");
                u3.setEmail("3@3.ru");
                u3.setNameFirst("NAME_FIRST_3");

                Manager u4 = new Manager();
                u4.setCrmId("4");
                u4.setEmail("adm@4.ru");
                u4.setNameFirst("ADMIN_4");
                u4.setRegionId("4");
                u4.setUserName("4admin4");


                if (regionId != null && regionId == 1) {
                    return new ArrayList<>(Arrays.asList(theOne, u, u2, u3, u4));
                } else {
                    return new ArrayList<>();
                }
            }
            
            @Override
            public Manager getManagerById(String managerId) {
                log.info("Get manager by id {}", managerId);
                if (managerId.equals("666")) {
                    return theOne;
                } else {
                    return null;
                }
            }
            
            @Override
            public CrmCloseTaskResponse closeTask(String taskId, Map<String, Object> variables) {
                log.info("Close task {}", taskId);
                return null;
            }
            
            @Override
            public void addValuation(String projectId, String valuation, String comment, String managerId) {
                log.info("Add valuation {} {} {} {}", projectId, valuation, comment, managerId);
            }
            
            @Override
            public String addProject(ProjectRequest project) {
                log.info("Add project {}", project);
                return "deadbeef-" + UtilsService.generateRandom(5);
            }

            @Override
            public DocumentAdd addDocument(String projectId, DocumentAdd document) {
                log.info("Add document {} to project id {}", document, projectId);
                return document;
            }

            @Override
            public void closeTasks(String projectId, List<String> taskNames, Map<String, Object> variables) {
                log.info("Close tasks {}", Arrays.toString(taskNames.toArray()));
            }

            @Override
            public Map<String, Integer> getRatings() {
                Map<String, Integer> rat = new HashMap<>();
                rat.put("1", 1);
                rat.put("2", 2);
                rat.put("3", 3);
                return rat;
            }

            @Override
            public List<Manager> getUserSind(String type) {
                Manager u = new Manager();
                u.setCrmId("5");
                u.setEmail("5@5.ru");
                u.setNameFirst("NAME_FIRST_5");

                return new ArrayList<>(Arrays.asList(theOne, u));

            }

            @Override
            public List<ProjectResponsePacket> getProjectPacketSind(Integer count, Integer offset, String type, String applicantEmail, String projectCertifiedManagerId) {
                return new ArrayList<>(Arrays.asList(new ProjectResponsePacket[] { oneResponse }));
            }
        };
    }
}
