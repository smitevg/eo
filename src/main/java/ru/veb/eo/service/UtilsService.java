package ru.veb.eo.service;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.ListUtils;
import org.thymeleaf.util.SetUtils;
import ru.veb.eo.model.User;
import ru.veb.eo.model.crm.*;
import sun.reflect.FieldInfo;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UtilsService {
    static Logger log = LoggerFactory.getLogger(UtilsService.class);

    @Autowired
    ReferenceService rs;

    public Map<String, Object> getContextObjects(ProjectRequest project) {
        Map<String, Object> map = new HashMap<>();

        map.put("year", Calendar.getInstance().get(Calendar.YEAR));
        map.put("month", getValue(rs.getMonth(Calendar.getInstance().get(Calendar.MONTH))));
        map.put("day", Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

        map.put("initiatorOrganizationalLegalForm", getValue(rs.getOpf(project.initiatorOrganizationalLegalForm)));
        map.put("initiatorNameRus", getValue(project.initiatorNameRus));
        map.put("initiatorNameShort", getValue(project.initiatorNameShort));
        map.put("initiatorFirmNameShort", getValue(project.initiatorNameShort));
        map.put("initiatorFirmNameForeign", getValue(project.initiatorFirmNameForeign));
        map.put("initiatorFirmNameForeignShort", getValue(project.initiatorFirmNameForeignShort));
        map.put("initiatorInn", getValue(project.initiatorInn));
        map.put("initiatorKio", getValue(project.initiatorKio));
        map.put("initiatorOgrn", getValue(project.initiatorOgrn));
        map.put("initiatorRegistrationNumber", getValue(project.initiatorRegistrationNumber));
        map.put("initiatorAddressRegistration", getValue(project.initiatorAddressRegistration));
        map.put("initiatorAddressFactual", getValue(project.initiatorAddressFactual));
        map.put("initiatorAddressFactualConfirm", getValue(project.initiatorAddressFactualConfirm));
        if (null != project.initiatorMainActivityAddress) {
            map.put("initiatorMainActivityAddressCountry", getValue(project.initiatorMainActivityAddress.country));
            map.put("initiatorMainActivityAddressRegion", getValue(project.initiatorMainActivityAddress.region));
            map.put("initiatorMainActivityAddressCity", getValue(project.initiatorMainActivityAddress.city));
        } else {
            map.put("initiatorMainActivityAddressCountry", " ");
            map.put("initiatorMainActivityAddressRegion", " ");
            map.put("initiatorMainActivityAddressCity", " ");
        }
        map.put("initiatorActivity", getValue(project.initiatorActivity));
        map.put("initiatorActivityTerm", getValue(project.initiatorActivityTerm));
        map.put("initiatorEmployeesNumber", getValue(project.initiatorEmployeesNumber));
        map.put("initiatorOwnersChain", getValue(project.initiatorOwnersChain));
        map.put("initiatorPhoneNumber", getValue(project.initiatorPhoneNumber));
        map.put("initiatorFaxNumber", getValue(project.initiatorFaxNumber));
        map.put("initiatorEmail", getValue(project.initiatorEmail));
        map.put("initiatorPostalAddress", getValue(project.initiatorPostalAddress));
        map.put("initiatorFaceRoleAtProject", getValue(rs.getFaceRole(project.initiatorFaceRoleAtProject)));
        map.put("initiatorFaceType", getValue(rs.getFaceType(project.initiatorFaceType)));
        map.put("initiatorStateRegistrationDate", getValue(project.initiatorStateRegistrationDate));
        map.put("initiatorRequisitesReceiver", getValue(project.initiatorRequisitesReceiver));
        map.put("initiatorRequisitesReceiversAccount", getValue(project.initiatorRequisitesReceiversAccount));
        map.put("initiatorRequisitesBankName", getValue(project.initiatorRequisitesBankName));
        map.put("initiatorRequisitesBankPlace", getValue(null));//project.initiatorRequisitesBankPlace));
        map.put("initiatorRequisitesBankCorrAccount", getValue(project.initiatorRequisitesBankCorrAccount));
        map.put("initiatorRequisitesBankBik", getValue(project.initiatorRequisitesBankBik));
        map.put("initiatorRequisitesForeignCurrency", getValue(project.initiatorRequisitesForeignCurrency));
        map.put("initiatorRequisitesForeignReceiver", getValue(project.initiatorRequisitesForeignReceiver));
        map.put("initiatorRequisitesForeignReceiverAccount", getValue(project.initiatorRequisitesForeignReceiverAccount));
        map.put("initiatorRequisitesForeignBankName", getValue(null));//project.initiatorRequisitesForeignBankName));
        map.put("initiatorRequisitesForeignBankPlace", getValue(null));//project.initiatorRequisitesForeignBankPlace));
        map.put("initiatorRequisitesForeignBankSwift", getValue(project.initiatorRequisitesForeignBankSwift));



        map.put("executiveNameLast", getValue(project.executiveNameLast));
        map.put("executiveNameFirst", getValue(project.executiveNameFirst));
        map.put("executiveNameMid", getValue(project.executiveNameMid));
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getValue(project.executiveNameLast));
        if (!Strings.isNullOrEmpty(project.executiveNameFirst)) {
            stringBuilder.append(" ");
            stringBuilder.append(project.executiveNameFirst.toUpperCase().charAt(0));
            stringBuilder.append(".");
        }
        if (!Strings.isNullOrEmpty(project.executiveNameMid)) {
            stringBuilder.append(" ");
            stringBuilder.append(project.executiveNameMid.toUpperCase().charAt(0));
            stringBuilder.append(".");
        }
        map.put("executiveNameLastAndInitials", getValue(stringBuilder));
        map.put("executiveInfoActivitiesParticipation", getValue(project.executiveInfoActivitiesParticipation));

        map.put("executiveBornDate", getValue(project.executiveBornDate));
        map.put("executiveBornPlaceAddress", getValue(project.executiveBornPlaceAddress));
        map.put("executiveCitizenship", getValue(project.executiveCitizenship));
        if (null != project.executiveAddressRegistration) {
            map.put("executiveAddressRegistrationRegion", getValue(project.executiveAddressRegistration.region));
            map.put("executiveAddressRegistrationCity", getValue(project.executiveAddressRegistration.city));
            map.put("executiveAddressRegistrationStreet", getValue(project.executiveAddressRegistration.street));
            map.put("executiveAddressRegistrationHouse", getValue(project.executiveAddressRegistration.house));
            map.put("executiveAddressRegistrationBuilding", getValue(project.executiveAddressRegistration.building));
            map.put("executiveAddressRegistrationApartment", getValue(project.executiveAddressRegistration.apartment));
        } else {
            map.put("executiveAddressRegistrationRegion", " ");
            map.put("executiveAddressRegistrationCity", " ");
            map.put("executiveAddressRegistrationStreet", " ");
            map.put("executiveAddressRegistrationHouse", " ");
            map.put("executiveAddressRegistrationBuilding", " ");
            map.put("executiveAddressRegistrationApartment", " ");
        }
        map.put("executiveIdName", getValue(rs.getDocumentType(project.executiveIdName)));

        map.put("executiveIdSeries", getValue(project.executiveIdSeries));
        map.put("executiveIdNumber", getValue(project.executiveIdNumber));
        map.put("executiveIdIssueDate", getValue(project.executiveIdIssueDate));
        map.put("executiveIdIssueAgencyName", getValue(project.executiveIdIssueAgencyName));
        map.put("executiveIdIssueAgencyCode", getValue(project.executiveIdIssueAgencyCode));
        map.put("executiveMigrationSeries", getValue(project.executiveMigrationSeries));
        map.put("executiveMigrationNumber", getValue(project.executiveMigrationNumber));
        map.put("executiveMigrationDateStart", getValue(project.executiveMigrationDateStart));
        map.put("executiveMigrationDateEnd", getValue(project.executiveMigrationDateEnd));

        map.put("employeeNameLast", getValue(project.employeeNameLast));
        map.put("employeeNameFirst", getValue(project.employeeNameFirst));
        map.put("employeeNameMid", getValue(project.employeeNameMid));
        map.put("employeePosition", getValue(project.employeePosition));
        map.put("employeePhoneNumber", getValue(project.employeePhoneNumber));
        map.put("employeeFaxNumber", getValue(project.employeeFaxNumber));
        map.put("employeeEmail", getValue(project.employeeEmail));

        map.put("projectNomination", getValue(project.projectNomination));
        map.put("projectTarget", getValue(project.projectTarget));
        map.put("projectExpectedResult", getValue(project.projectExpectedResult));
        if (Strings.isNullOrEmpty(project.projectType1Custom)) {
            map.put("projectType1", getValue(rs.getProjectType(project.projectType1)));
        } else {
            map.put("projectType1", "иное: " + getValue(project.projectType1Custom));
        }
        map.put("projectDescription", getValue(project.projectDescription));
        map.put("projectSalePlace", getValue(rs.getProjectRegion(project.projectSalePlace)));
        map.put("projectExperienceProjectManagers", getValue(project.projectExperienceProjectManagers));
        map.put("projectDegree", getValue(project.projectDegree));
        map.put("projectProjectAimReason", getValue(project.projectProjectAimReason));
        map.put("projectProjectAdvantagesInMarket", getValue(project.projectProjectAdvantagesInMarket));
        map.put("projectSectorsReason", getValue(project.projectSectorsReason));

        double sum = Double.valueOf(project.projectAmountRecipientsCash != null ? project.projectAmountRecipientsCash : 0D);
        StringBuilder participants = new StringBuilder();
        if (!ListUtils.isEmpty(project.participants)) {
            for (Participant participant : project.participants) {
                try {
                    participants
                            .append(getValue(rs.getOpf(participant.organizationType))).append(" ")
                            .append(getValue(participant.organizationTitle)).append("\n\r")
                            .append(" - ").append(participant.resident ? "резидент" : "не резидент").append("\n\r")
                            .append(" - ").append(participant.resident ? getValue(participant.inn) : getValue(participant.nalogNumber)).append("\n\r")
                            .append(" - ").append(participant.resident ? getValue(participant.ogrn) : getValue(participant.regNumber)).append("\n\r")
                            .append(" - ").append(getValue(participant.rules)).append("\n\r")
                            .append(" - ").append(getValue(rs.getParticipantRole(participant.status))).append("\n\r")
                            .append(" - ").append(getValue(participant.volume)).append(" " + project.projectCostCurrency).append("\n\r")
                            .append("\n\r");

                    sum += participant.volume;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (!ListUtils.isEmpty(project.participantsIndividuals)) {
            for (IndividualPerson participant : project.participantsIndividuals) {
                participants
                        .append("\n\r")
                        .append(getValue(participant.getNameLast())).append(" ")
                        .append(getValue(participant.getNameFirst())).append(" ")
                        .append(getValue(participant.getNameMid())).append("\n\r")
                        .append(" - ").append(participant.getResident() ? "резидент" : "не резидент").append("\n\r")
                        .append(" - ").append(getValue(participant.getCitizenship())).append("\n\r")
                        .append(" - ").append(getValue(participant.getBornDate())).append("\n\r")
                        .append(" - ").append(getValue(participant.getBornPlace())).append("\n\r")
                        .append(" - ").append(getValue(participant.getAddressRegistration())).append("\n\r")
                        .append(" - ").append(getValue(rs.getDocumentType(participant.getIdName()))).append("\n\r")
                        .append(" - ").append(getValue(participant.getIdSeries())).append("\n\r")
                        .append(" - ").append(getValue(participant.getIdNumber())).append("\n\r")
                        .append(" - ").append(getValue(participant.getIdIssueAgencyName())).append("\n\r")
                        .append(" - ").append(getValue(participant.getIdIssueAgencyCode())).append("\n\r")
                        .append(" - ").append(getValue(participant.getIdIssueDate())).append("\n\r")
                        .append(" - ").append(getValue(participant.rules)).append("\n\r")
                        .append(" - ").append(getValue(rs.getParticipantRole(participant.status))).append("\n\r")
                        .append(" - ").append(getValue(participant.volume)).append(" " + project.projectCostCurrency).append("\n\r")
                        .append("\n\r");

                sum += participant.volume != null ? participant.volume : 0;
            }
        }
        map.put("participants", getValue(participants));
        map.put("projectProjectIsAimedTo", getValue(rs.getProjectDirection(project.projectProjectIsAimedTo)));
        map.put("sector", getValue(rs.getProjectSector(project.getProjectSectors1())));
        map.put("projectWorkPlaceCount", getValue(project.projectWorkPlaceCount));
        map.put("projectEnvironmentalProtection", getValue(project.projectEnvironmentalProtection));
        map.put("projectTechnologiesImplementation", getValue(project.projectTechnologiesImplementation));
        map.put("projectTechnologicalOpportunities", getValue(project.projectTechnologicalOpportunities));


        StringBuilder projectStateSupport = new StringBuilder();
        if (!SetUtils.isEmpty(project.projectStateSupport)) {
            if (project.projectStateSupport.contains(0))
                projectStateSupport.append("Привлечение возвратной меры господдержки (по факту понесенных расходов): проект соответствует правилам получения субсидии.").append("\n\r").append("\n\r");
            if (project.projectStateSupport.contains(1))
                projectStateSupport.append("Привлечение авансовой меры господдержки: проект соответствует правилам получения субсидии или есть решение ГРБС о предоставлении возвратной меры господдержки по проекту.").append("\n\r").append("\n\r");
            if (project.projectStateSupport.contains(2))
                projectStateSupport.append("Есть решение ГРБС о получении авансовой меры господдержки под проект (до момента предоставления финансирования Внешэкономбанком.)").append("\n\r").append("\n\r");
        } else {
            projectStateSupport.append("нет").append("\n\r");
        }
        map.put("projectStateSupport", getValue(projectStateSupport));
        map.put("projectCostDouble", getValue(project.projectProjectCostDouble));

        if (project.projectProjectCostDouble != null) {
            try {
                map.put("projectCostDoubleM", getValue(project.projectProjectCostDouble / 1000000D));
            } catch (Exception e) {
                log.warn("Failed to parse projectCostDoubleM = {}, {}", project.projectProjectCostDouble, e.toString(), e);
            }
        }

        if (project.projectLastCost != null) {
            try {
                map.put("projectLastCostM", getValue(Double.valueOf(project.projectLastCost) / 1000000D));
            } catch (Exception e) {
                log.warn("Failed to parse projectCostDoubleM = {}, {}", project.projectLastCost, e.toString(), e);
            }
        }

        if (project.projectProjectUpcomingInvestments != null) {
            try {
                map.put("projectProjectUpcomingInvestmentsM", getValue(Double.valueOf(project.projectProjectUpcomingInvestments) / 1000000D));
            } catch (Exception e) {
                log.warn("Failed to parse projectCostDoubleM = {}, {}", project.projectProjectUpcomingInvestments, e.toString(), e);
            }
        }

        map.put("projectProjectPaybackTimeDouble", getValue(project.projectProjectPaybackTimeDouble));
        map.put("projectAmountFinancingRequest", getValue(project.projectAmountFinancingRequest));
        map.put("projectNetPresentValue", getValue(project.projectNetPresentValue));
        map.put("projectAdditionalInfo", getValue(project.projectAdditionalInfo));


        map.put("projectEffectEmployment", getValue(project.projectEffectEmployment ));
        map.put("projectCalculatingEffectComponents", getValue(project.projectCalculatingEffectComponents ));
        map.put("projectHaveFinancingAtRfBefore", getValue(project.projectHaveFinancingAtRfBefore ));
        map.put("projectProjectsAndConditionsForTheirFinancing", getValue(project.projectProjectsAndConditionsForTheirFinancing ));
        map.put("projectCanProjectFinancedByFinancialInstitutions", getValue(project.projectCanProjectFinancedByFinancialInstitutions ));
        map.put("projectReasonsForRefusalOfFinancing", getValue(project.projectReasonsForRefusalOfFinancing ));
        map.put("projectFinancialInstitutionsReadyToFinanceTheProject", getValue(project.projectFinancialInstitutionsReadyToFinanceTheProject ));
        map.put("projectWhichFinancialInstitutions", getValue(project.projectWhichFinancialInstitutions ));
        map.put("projectWhyBanksParticipationNotDamageFinancialOrganizations", getValue(project.projectWhyBanksParticipationNotDamageFinancialOrganizations ));
        map.put("projectEnvironmentalProtectionComment", getValue(project.projectEnvironmentalProtectionComment ));
        map.put("projectTypeOfAuthorizationDocument", getValue(project.projectTypeOfAuthorizationDocument ));
        map.put("projectAuthorizationDocumentIssuedBy", getValue(project.projectAuthorizationDocumentIssuedBy ));
        map.put("projectAuthorizationDocumentIssuedDate", getValue(project.projectAuthorizationDocumentIssuedDate ));
        map.put("projectEnvironmentalExpertiseIssuedBy", getValue(project.projectEnvironmentalExpertiseIssuedBy ));
        map.put("projectEnvironmentalExpertiseIssuedDate", getValue(project.projectEnvironmentalExpertiseIssuedDate ));
        map.put("projectEnvironmentalExpertiseIsNotRequired", getValue(project.projectEnvironmentalExpertiseIsNotRequired ));
        map.put("projectProjectTermConstructionPhase", getValue(project.projectProjectTermConstructionPhase ));
        map.put("projectTermOperationalPhase", getValue(project.projectTermOperationalPhase ));
        map.put("projectProjectIrr", getValue(project.projectProjectIrr ));

        StringBuilder participants2 = new StringBuilder();
        participants2
                .append(getValue(sum)).append(" ").append(project.projectCostCurrency).append("\n\r")
                .append("\n\r")
                .append("из них").append("\n\r")
                .append("\n\r")
                .append("Собственные инвестиции получателя средств/инициатора проекта")
                .append(" - ").append(getValue(project.projectAmountRecipientsCash)).append("\n\r")
                .append("\n\r")
                .append(participants != null ? participants.toString() : "");
        map.put("participants2", getValue(participants2));

        map.put("projectCostCurrency", getValue(project.projectCostCurrency));

        map.put("recipientsCash", getValue(sum));
        map.put("recipientsCashM", getValue(sum / 1000000D));
        map.put("recipientsPercent", getValue(sum / project.projectProjectCostDouble));

        map.put("projectAmountFinancingRequest", getValue(project.projectAmountFinancingRequest));
        map.put("projectAmountFinancingRequestM", getValue(project.projectAmountFinancingRequest / 1000000D));
        map.put("projectAmountFinancingRequestPercent", getValue(project.projectAmountFinancingRequest / project.projectProjectCostDouble));


        if (ListUtils.isEmpty(project.projectAmountBondArray)) {
            map.put("amountBondArrayFull", getValue(0));
            map.put("amountBondArraySum", getValue(0));
            map.put("amountBondArrayPercent", getValue(0));
        } else {
            double sum1 = 0;
            for (int i = 0; i < project.projectAmountBondArray.size(); i++) {
                sum1 += Double.valueOf(project.projectAmountBondArray.get(i).get(0));
            }
            map.put("amountBondArrayFull", getValue(sum1));
            map.put("amountBondArraySum", getValue(sum1 / 1000000D));
            map.put("amountBondArrayPercent", getValue(sum1 / project.projectProjectCostDouble));
        }

        if (ListUtils.isEmpty(project.projectAmountInvestorArray)) {
            map.put("amountInvestorCashSumFull", getValue(0));
            map.put("amountInvestorCashSum", getValue(0));
            map.put("amountInvestorCashPercent", getValue(0));
        } else {
            double sum2 = 0;
            for (int i = 0; i < project.projectAmountInvestorArray.size(); i++) {
                sum2 += Double.valueOf(project.projectAmountInvestorArray.get(i).get(0));
            }
            map.put("amountInvestorCashSumFull", getValue(sum2));
            map.put("amountInvestorCashSum", getValue(sum2 / 1000000D));
            map.put("amountInvestorCashPercent", getValue(sum2 / project.projectProjectCostDouble));
        }

        if (ListUtils.isEmpty(project.projectAmountFinanceArray)) {
            map.put("amountOthersCashSumFull", getValue(0));
            map.put("amountOthersCashSum", getValue(0));
            map.put("amountOthersCashPercent", getValue(0));
            map.put("amountOthersCashSources", getValue(" "));
        } else {
            double sum3 = 0;
            StringBuilder sources = new StringBuilder();
            for (int i = 0; i < project.projectAmountFinanceArray.size(); i++) {
                sum3 += Double.valueOf(project.projectAmountFinanceArray.get(i).get(1));
                sources
                        .append(" - ").append(getValue(project.projectAmountFinanceArray.get(i).get(0))).append("\n\r");
            }
            map.put("amountOthersCashSumFull", getValue(sum3));
            map.put("amountOthersCashSum", getValue(sum3 / 1000000D));
            map.put("amountOthersCashPercent", getValue(sum3 / project.projectProjectCostDouble));
            map.put("amountOthersCashSources", getValue(sources));
        }


        StringBuilder supplies = new StringBuilder();
        StringBuilder pledges = new StringBuilder();
        StringBuilder pledgeDeposite = new StringBuilder();

        if (!ListUtils.isEmpty(project.supplies)) {

            supplies.append(getValue("Банковская гарантия:")).append("\n\r");
            for (Supply supply : project.supplies) {
                if (supply.getType() != null && supply.getType().equals(TypeSupply.BANK)) {
                    supplies
                            .append(" - ")
                            .append(getValue(supply.getValue()))
                            .append(" ")
                            .append(getValue(supply.getCurrency()))
                            .append(" - ")
                            .append(getValue(supply.getGuarantor()))
                            .append("\n\r");
                }
            }
            supplies.append("\n\r");

            supplies.append(getValue("Гарантия:")).append("\n\r");
            for (Supply supply : project.supplies) {
                if (supply.getType() != null && supply.getType().equals(TypeSupply.GUARANTEE)) {
                    supplies
                            .append(" - ")
                            .append(getValue(supply.getValue()))
                            .append(" ")
                            .append(getValue(supply.getCurrency()))
                            .append(" - ")
                            .append(getValue(supply.getGuarantor()))
                            .append("\n\r");
                }
            }
            supplies.append("\n\r");

            supplies.append(getValue("Поручительство(а):")).append("\n\r");
            for (Supply supply : project.supplies) {
                if (supply.getType() != null && supply.getType().equals(TypeSupply.SURETY)) {
                    supplies
                            .append(" - ")
                            .append(getValue(supply.getValue()))
                            .append(" ")
                            .append(getValue(supply.getCurrency()))
                            .append(" - ")
                            .append(getValue(supply.getGuarantor()))
                            .append("\n\r");
                }
            }
            supplies.append("\n\r");

//    0 - банковская гарантия, 1 гарант, 2 поручитель, 3 залог, 4 другое
            supplies.append(getValue("Залог(и):")).append("\n\r");
            for (Supply supply : project.supplies) {
                if (supply.getType() != null && supply.getType().equals(TypeSupply.PLEDGE)) {
                    supplies
                            .append(" - ")
                            .append(getValue(supply.getValue()))
                            .append(" ")
                            .append(getValue(supply.getCurrency()))
                            .append(" - ")
                            .append(getValue(supply.getGuarantor()))
                            .append("\n\r");

                    pledges
                            .append(" - ")
                            .append(getValue(supply.getValue()))
                            .append(" ")
                            .append(getValue(supply.getCurrency()))
                            .append(" - ")
                            .append(getValue(supply.getGuarantor()))
                            .append("\n\r");

                    pledgeDeposite.append(" - ");
                    // 0 - самостоятельно, 1 - спец. компанией
                    String depositWho = supply.getDepositWho();
                    if (!Strings.isNullOrEmpty(depositWho)) {
                        if ("0".equals(depositWho)) {
                            pledgeDeposite.append("самостоятельно");
                        } else if ("1".equals(depositWho)) {
                            pledgeDeposite
                                    .append("cпециализированной компанией")
                                    .append(" ")
                                    .append(getValue(supply.getDepositCompany()))
                                    .append(" ")
                                    .append(getValue(supply.getDepositDate()));
                        } else {
                            pledgeDeposite.append("отсутствует");
                        }
                    }
                    pledgeDeposite.append("\n\r");
                }
            }
            supplies.append("\n\r");

            supplies.append(getValue("Другое обеспечение:")).append("\n\r");
            for (Supply supply : project.supplies) {
                if (supply.getType() != null && supply.getType().equals(TypeSupply.OTHER)) {
                    supplies
                            .append(" - ")
                            .append(getValue(supply.getValue()))
                            .append(" ")
                            .append(getValue(supply.getCurrency()))
                            .append(" - ")
                            .append(getValue(supply.getGuarantor()))
                            .append("\n\r");
                }
            }
            supplies.append("\n\r");

        } else {
            supplies.append("Обеспечение отсутствует");
            pledges.append("Залоги отсутствует");
            pledgeDeposite.append("Залоги отсутствует");
        }
        map.put("supplies", getValue(supplies));
        map.put("pledges", getValue(pledges));
        map.put("pledgeDeposite", getValue(pledgeDeposite));


        map.put("projectProductMainQualitativeCharacteristics", getValue(project.projectProductMainQualitativeCharacteristics));


        StringBuilder stage = new StringBuilder();
        stage
                .append(" - ").append(getValue(project.projectProjectDecisionDateAndLevel)).append("\n\r")
                .append(" - ").append(getValue(project.projectReadinessOfProjectDocumentation)).append("\n\r")
                .append(" - ").append(getValue(project.projectProjectImplementationActivities)).append("\n\r")
                .append(" - ").append(getValue(project.projectProjectTermConstructionPhase)).append("\n\r")
                .append(" - ").append(getValue(project.projectTermOperationalPhase)).append("\n\r");
        map.put("stage", getValue(stage));

        map.put("projectProductMainQualitativeCharacteristics", getValue(project.projectProductMainQualitativeCharacteristics));
        map.put("projectProjectPublicPrivatePartnership", getValue(project.projectProjectImplementationPublicPrivatePartnership));
        map.put("projectProjectAdvisabilityRealization", getValue(project.projectProjectAdvisabilityRealization));
        map.put("projectAgreementBasicParameters", getValue(project.projectAgreementBasicParameters));
        map.put("projectCorrespondsToPriorities", getValue(project.projectImplementationCorrespondsToPriorities));
        map.put("projectProjectCorrespondenceSectors", getValue(project.projectProjectCorrespondenceSectors));
        StringBuilder effectEmployment = new StringBuilder();
        effectEmployment
                .append(" - ").append(getValue(project.projectWorkPlaceCount)).append("\n\r")
                .append(" - ").append(getValue(project.projectEffectEmployment)).append("\n\r")
                .append(" - ").append(getValue(project.projectEvaluationMethod)).append("\n\r")
                .append(" - ").append(getValue(project.projectCalculationFormula)).append("\n\r")
                .append(" - ").append(getValue(project.projectQualitativeApproachesDescription)).append("\n\r")
                .append(" - ").append(getValue(project.projectCalculatingEffectComponents)).append("\n\r")
                .append(" - ").append(getValue(project.projectConfirmingBudgetaryEffect)).append("\n\r");
        map.put("effectEmployment", getValue(effectEmployment));
        map.put("projectTermsDeliveryPaymentExpected", getValue(project.projectTermsDeliveryPaymentExpected));
        map.put("projectOrderBook", getValue(project.projectOrderBook));
        map.put("projectProjectProvidesForACompetition", getValue(project.projectProjectProvidesForACompetition));
        map.put("projectProjectProvidesForACompetitionDescription", getValue(project.projectProjectProvidesForACompetitionDescription));
        map.put("projectRecipientAvailableResourcesEnum", getValue(project.projectRecipientAvailableResourcesEnum));
        map.put("projectDesignRisksAnalysis", getValue(project.projectDesignRisksAnalysis));
        map.put("projectExpenseInvestmentFund", getValue(project.projectProjectFinancingExpenseInvestmentFund));
        map.put("projectFinancingBasicTerms", getValue(project.projectFinancingBasicTerms));
        map.put("projectCalculationFormula", getValue(project.projectCalculationFormula));
        map.put("projectEvaluationMethod", getValue(project.projectEvaluationMethod));
        map.put("projectQualitativeApproachesDescription", getValue(project.projectQualitativeApproachesDescription));
        map.put("projectConfirmingBudgetaryEffect", getValue(project.projectConfirmingBudgetaryEffect));


        map.put("providesForACompetition", getValue(project.projectProjectProvidesForACompetition));
        map.put("competitionDescription", getValue(project.projectProjectProvidesForACompetitionDescription));
        map.put("projectFundsUsing", getValue(project.projectFundsUsingMonitoringMechanismsDescription));


        // типы финансирования
        List<TypeOfFinancing> typeOfFinancings = project.typeOfFinancings;
        StringBuilder listTypeOfFinancing = new StringBuilder();


        if (!ListUtils.isEmpty(typeOfFinancings)) {
            for (TypeOfFinancing typeOfFinancing : typeOfFinancings) {
                if ("0".equals(typeOfFinancing.type)) {
                    listTypeOfFinancing.append("Долгосрочное финансирование, срок ").append(typeOfFinancing.financeLongTerm).append("\n\r");
                }
                if ("1".equals(typeOfFinancing.type)) {
                    listTypeOfFinancing.append("Мезонинное финансирование").append("\n\r");
                }
                if ("2".equals(typeOfFinancing.type)) {
                    listTypeOfFinancing.append("Синдицированное кредитование").append("\n\r");
                }
                if ("3".equals(typeOfFinancing.type)) {
                    listTypeOfFinancing.append("Финансирование через участие в уставном капитале/покупку портфеля акций").append("\n\r");
                }
                if ("4".equals(typeOfFinancing.type)) {
                    listTypeOfFinancing.append("Финансирование через приобретение облигаций/векселей").append("\n\r");
                }
                if ("5".equals(typeOfFinancing.type)) {
                    listTypeOfFinancing.append("Предоставление гарантии").append("\n\r");
                }
                if ("6".equals(typeOfFinancing.type)) {
                    listTypeOfFinancing.append("Предоставление поручительства").append("\n\r");
                }
                if ("7".equals(typeOfFinancing.type)) {
                    listTypeOfFinancing.append("Аккредитивные формы расчетов").append("\n\r");
                }
                if ("8".equals(typeOfFinancing.type)) {
                    listTypeOfFinancing.append("Иное").append("\n\r");
                }
            }
        }

        map.put("listTypeOfFinancing", getValue(listTypeOfFinancing));

        return map;
    }


    public static String getValue(Object value) {
        if (value == null) {
            return " ";
        } else if (value instanceof DateForm) {
            DateForm date = (DateForm) value;
            return date.toFormatString();
        } else if (value instanceof Address) {
            Address address = (Address) value;
            return getValue(address.name);
        } else if (value instanceof String) {
            String temp = (String) value;
            if (Strings.isNullOrEmpty(temp)) return " ";
            else if ("true".equals(temp)) return "да";
            else if ("false".equals(temp)) return "нет";
            else return temp;
        } else if (value instanceof Double) {
            DecimalFormat df = new DecimalFormat("#.#");
            return df.format((Double) value);
        } else if (value instanceof StringBuilder) {
            return ((StringBuilder) value).length() > 0 ? ((StringBuilder) value).toString() : " ";
        } else if (value instanceof Boolean) {
            return ((Boolean) value) ? "да" : "нет";
        } else {
            return value.toString();
        }
    }


    private static final String characters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";

    public static String generateRandom(int length) {
        if (length <= 0) {
            throw new IllegalArgumentException("String length must be a positive integer");
        }

        Random random = new SecureRandom();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) sb.append(characters.charAt(random.nextInt(characters.length())));

        return sb.toString();
    }

    public static boolean checkGUID(String guid) {
        Pattern p = Pattern.compile("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$");
        Matcher m = p.matcher(guid);
        return m.matches();
    }

    public static boolean checkAccess(Object obj, User user) {
        if (obj instanceof ProjectResponse) {
            ProjectResponse projectResponse = (ProjectResponse) obj;
            if (
                    projectResponse != null &&
                            (
                                    (projectResponse.getProjectManager() != null && projectResponse.getProjectManager().getCrmId() != null && projectResponse.getProjectManager().getCrmId().equals(user.getCrmId())) ||
                                            (projectResponse.getDeclarer() != null && projectResponse.getDeclarer().getApplicantEmails().contains(user.getUsername()))
                            )
                    )
                return true;
            else
                return false;
        } else if (obj instanceof ProjectRequest) {
            ProjectRequest projectRequest = (ProjectRequest) obj;
            if (
                    projectRequest != null &&
                            (
                                    (projectRequest.getProjectCertifiedManagerId() != null && projectRequest.getProjectCertifiedManagerId().equals(user.getCrmId())) ||
                                            (projectRequest.getApplicantEmail() != null && projectRequest.getApplicantEmail().equals(user.getUsername()))
                            )
                    )
                return true;
            else
                return false;
        } else if (obj instanceof List) {
            List<ProjectResponsePacket> projectResponsePackets = (List<ProjectResponsePacket>) obj;
            if (!ListUtils.isEmpty(projectResponsePackets)) {
                boolean result = true;
                for (ProjectResponsePacket projectResponsePacket : projectResponsePackets) {
                    if (!((projectResponsePacket.applicantEmail != null && projectResponsePacket.applicantEmail.equals(user.getUsername())) ||
                            (projectResponsePacket.projectCertifiedManagerId != null && projectResponsePacket.projectCertifiedManagerId.equals(user.getCrmId()))))
                        result = false;
                }
                return result;
            } else {
                return true;
            }
        } else if (obj == null) {
            return true;
        } else {
            return false;
        }
    }

    public static void securityVueJS(Object obj) {
        Class c = obj.getClass();
        Field[] publicFields = c.getFields();
        for (Field field : publicFields) {
            Class fieldType = field.getType();
            if ("java.lang.String".equals(fieldType.getName())) {
                try {
                    String nameValue = (String) field.get(obj);
//                    nameValue = nameValue.replaceAll("\\{\\{(.*)\\}\\}", "");
                    field.set(obj, nameValue == null ? null : nameValue.replaceAll("\\{\\{[^\\}]+\\}\\}", ""));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

        List<Field> privateFields = new ArrayList<>();
        Field[] allFields = c.getDeclaredFields();
        for (Field field : allFields) {
            if (Modifier.isPrivate(field.getModifiers())) {
                privateFields.add(field);
            }
        }

        if (!ListUtils.isEmpty(privateFields)) {
            for (Field field : privateFields) {
                Class fieldType = field.getType();
                if ("java.lang.String".equals(fieldType.getName())) {
                    try {
                        field.setAccessible(true);
                        String nameValue = (String) field.get(obj);
                        field.set(obj, nameValue == null ? null : nameValue.replaceAll("\\{\\{[^\\}]+\\}\\}", ""));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }


    public static String transliteration(String str) {
//        str = str.replace(" ", "+");
        str = str.replace("А", "A");
        str = str.replace("а", "a");
        str = str.replace("Б", "B");
        str = str.replace("б", "b");
        str = str.replace("В", "V");
        str = str.replace("в", "v");
        str = str.replace("Г", "G");
        str = str.replace("г", "g");
        str = str.replace("Д", "D");
        str = str.replace("д", "d");
        str = str.replace("Е", "E");
        str = str.replace("е", "e");
        str = str.replace("Ё", "E");
        str = str.replace("ё", "e");
        str = str.replace("Ж", "ZH");
        str = str.replace("ж", "zh");
        str = str.replace("З", "Z");
        str = str.replace("з", "z");
        str = str.replace("И", "I");
        str = str.replace("и", "i");
        str = str.replace("Й", "Y");
        str = str.replace("й", "y");
        str = str.replace("К", "K");
        str = str.replace("к", "k");
        str = str.replace("Л", "L");
        str = str.replace("л", "l");
        str = str.replace("М", "M");
        str = str.replace("м", "m");
        str = str.replace("Н", "N");
        str = str.replace("н", "n");
        str = str.replace("О", "O");
        str = str.replace("о", "o");
        str = str.replace("П", "P");
        str = str.replace("п", "p");
        str = str.replace("Р", "R");
        str = str.replace("р", "r");
        str = str.replace("С", "S");
        str = str.replace("с", "s");
        str = str.replace("Т", "T");
        str = str.replace("т", "t");
        str = str.replace("У", "U");
        str = str.replace("у", "u");
        str = str.replace("Ф", "F");
        str = str.replace("ф", "f");
        str = str.replace("Х", "KH");
        str = str.replace("х", "kh");
        str = str.replace("Ц", "C");
        str = str.replace("ц", "c");
        str = str.replace("Ч", "CH");
        str = str.replace("ч", "ch");
        str = str.replace("Ш", "SH");
        str = str.replace("ш", "sh");
        str = str.replace("Щ", "SHCH");
        str = str.replace("щ", "shch");
        str = str.replace("Ъ", "");
        str = str.replace("ъ", "");
        str = str.replace("Ы", "Y");
        str = str.replace("ы", "y");
        str = str.replace("Ь", "");
        str = str.replace("ь", "");
        str = str.replace("Э", "E");
        str = str.replace("э", "e");
        str = str.replace("Ю", "YU");
        str = str.replace("ю", "yu");
        str = str.replace("Я", "YA");
        str = str.replace("я", "ya");
        return str;
    }

    public static String strUriEncoding(String str) {
        str = str.replace("%", "%25");
        str = str.replaceAll("\r\n", "%0A");
        str = str.replaceAll("\"", "'");
        str = str.replaceAll("[^a-zA-Zа-яА-Я0-9/-/-/_/./~/!/#/%/&/*/,/:/;/</=/>/?/[/]/^/`/{/|/}/'/(/)]", " ");
        str = str.replace("!", "%21");
//        str = str.replace("\"", "%22");
        str = str.replace("#", "%23");
        str = str.replace("&", "%26");
        str = str.replace("'", "%27");
        str = str.replace("*", "%2a");
        str = str.replace(",", "%2c");
        str = str.replace(":", "%3a");
        str = str.replace(";", "%3b");
        str = str.replace("<", "%3c");
        str = str.replace("=", "%3d");
        str = str.replace(">", "%3e");
        str = str.replace("?", "%3f");
        str = str.replace("[", "%5b");
        str = str.replace("]", "%5d");
        str = str.replace("^", "%5e");
        str = str.replace("`", "%60");
        str = str.replace("{", "%7b");
        str = str.replace("|", "%7c");
        str = str.replace("}", "%7d");
        str = str.replace(" ", "%20");

        str = str.replace("А", "%D0%90");
        str = str.replace("а", "%D0%B0");
        str = str.replace("Б", "%D0%91");
        str = str.replace("б", "%D0%B1");
        str = str.replace("В", "%D0%92");
        str = str.replace("в", "%D0%B2");
        str = str.replace("Г", "%D0%93");
        str = str.replace("г", "%D0%B3");
        str = str.replace("Д", "%D0%94");
        str = str.replace("д", "%D0%B4");
        str = str.replace("Е", "%D0%95");
        str = str.replace("е", "%D0%B5");
        str = str.replace("Ё", "%D0%81");
        str = str.replace("ё", "%D1%91");
        str = str.replace("Ж", "%D0%96");
        str = str.replace("ж", "%D0%B6");
        str = str.replace("З", "%D0%97");
        str = str.replace("з", "%D0%B7");
        str = str.replace("И", "%D0%98");
        str = str.replace("и", "%D0%B8");
        str = str.replace("Й", "%D0%99");
        str = str.replace("й", "%D0%B9");
        str = str.replace("К", "%D0%9A");
        str = str.replace("к", "%D0%BA");
        str = str.replace("Л", "%D0%9B");
        str = str.replace("л", "%D0%BB");
        str = str.replace("М", "%D0%9C");
        str = str.replace("м", "%D0%BC");
        str = str.replace("Н", "%D0%9D");
        str = str.replace("н", "%D0%BD");
        str = str.replace("О", "%D0%9E");
        str = str.replace("о", "%D0%BE");
        str = str.replace("П", "%D0%9F");
        str = str.replace("п", "%D0%BF");
        str = str.replace("Р", "%D0%A0");
        str = str.replace("р", "%D1%80");
        str = str.replace("С", "%D0%A1");
        str = str.replace("с", "%D1%81");
        str = str.replace("Т", "%D0%A2");
        str = str.replace("т", "%D1%82");
        str = str.replace("У", "%D0%A3");
        str = str.replace("у", "%D1%83");
        str = str.replace("Ф", "%D0%A4");
        str = str.replace("ф", "%D1%84");
        str = str.replace("Х", "%D0%A5");
        str = str.replace("х", "%D1%85");
        str = str.replace("Ц", "%D0%A6");
        str = str.replace("ц", "%D1%86");
        str = str.replace("Ч", "%D0%A7");
        str = str.replace("ч", "%D1%87");
        str = str.replace("Ш", "%D0%A8");
        str = str.replace("ш", "%D1%88");
        str = str.replace("Щ", "%D0%A9");
        str = str.replace("щ", "%D1%89");
        str = str.replace("Ъ", "%D0%AA");
        str = str.replace("ъ", "%D1%8A");
        str = str.replace("Ы", "%D0%AB");
        str = str.replace("ы", "%D1%8B");
        str = str.replace("Ь", "%D0%AC");
        str = str.replace("ь", "%D1%8C");
        str = str.replace("Э", "%D0%AD");
        str = str.replace("э", "%D1%8D");
        str = str.replace("Ю", "%D0%AE");
        str = str.replace("ю", "%D1%8E");
        str = str.replace("Я", "%D0%AF");
        str = str.replace("я", "%D1%8F");
        return str;
    }
}
