package ru.veb.eo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.ListUtils;
import ru.veb.eo.Properties;
import ru.veb.eo.model.Role;
import ru.veb.eo.model.User;
import ru.veb.eo.model.crm.Manager;
import ru.veb.eo.repository.UserRepository;

import javax.validation.constraints.NotNull;
import java.util.*;


@Service
public class UserService implements UserDetailsService {



    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    private UserRepository ur;
    private Properties properties;
    //private MySqlService mySqlService;
    //private CrmService crmService;

    @Autowired
    public UserService(/*MySqlService mySqlService, */UserRepository ur, Properties properties /*, CrmService crmService*/) {
        this.ur = ur;
        this.properties = properties;
        //this.mySqlService = mySqlService;
        //this.crmService = crmService;
    }

    public void saveUser(@NotNull User user) {
        ur.saveAndFlush(user);
    }

    public void deleteUser(@NotNull User user) {
        ur.delete(user.getUsername());
        ur.flush();
    }

    public User getUser(String userName) {
        return ur.findFirstByUsername(userName.toLowerCase());
    }

    public List<User> getManagers() {
        logger.trace("getManagers");
        //return ur.findByAuthoritiesContains(Role.MANAGER);
        return ur.findAll();
    }

    public List<User> getManagersByRegion(String regionId) {
        List<User> users = ur.findByAuthoritiesContainsAndRegionId(Role.MANAGER, regionId);
        if (ListUtils.isEmpty(users)) {
            ur.findByUsername(properties.getDefaultManager().toLowerCase());
        }
        return users;
    }

    public User getManagerByCrmId(String crmId) {
        logger.trace("getManagerByCrmId: " + crmId);
        return ur.findFirstByAuthoritiesContainsAndCrmId(Role.MANAGER, crmId);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        String lowerUserName = username.toLowerCase();
        List<User> users = ur.findByUsernameOrLogin(lowerUserName, lowerUserName);
        if (users == null || users.size() == 0) {
            logger.trace("no found");

            throw new UsernameNotFoundException("Пользователь " + username + " не найден");
        } else {
            logger.trace("loadUserByUsername: " + users.get(0).getUsername());
            return users.get(0);
            //mySqlService.rolesUpdate(user);
            //return user;
        }
    }
}
