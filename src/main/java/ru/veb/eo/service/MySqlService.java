package ru.veb.eo.service;

import com.microsoft.sharepoint.rest.SPFile;
import ru.veb.eo.entity.Project;
import ru.veb.eo.entity.UserProject;
import ru.veb.eo.entity.UserRoleAppend;
import ru.veb.eo.model.User;
import ru.veb.eo.model.crm.Manager;
import ru.veb.eo.model.crm.ProjectResponsePacket;

import java.util.List;


public interface MySqlService {

    //Список добавленных или отнятых ролей по имени пользователя
//    List<UserRoleAppend> findByUsername(String username);

    //List<UserRoleAppend> findSindRole();

    public List<Manager> findUsers(Project project);

    // Обновление списка ролей
    //void rolesUpdate(User u);

    List<UserProject> getUserProject(String username);
    List<UserProject> getUsersProjects();


    List<Project> getProjects();

    void saveUserProject(UserProject userProject);

    void addUserProject(String userId, Project project);
    void removeUserProject(String userId, Project project);

    //загрузка голосований по проекту
    List<UserProject> listUserProjectByProject(Project project);

    void addProjectFromCrm(String type, List<ProjectResponsePacket> packets);

    Project getProject(String id);

    void saveProject(Project project);
}
