package ru.veb.eo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import ru.veb.eo.Properties;

@Service
public class PasswordService {
    Logger log = LoggerFactory.getLogger(getClass());
    
    @Autowired
    UtilsService utils;
    
    @Autowired
    Properties properties;

    public String generatePassword(String subject) {
        if (!properties.isEnablePassword()) {
//            log.info("Using fixed password for {}", subject);
            return "";
        } else {
            log.info("Generating password for {}", subject);
            String psw = utils.generateRandom(10);
            log.info("pwd {}", psw);
            return psw;
        }
    }
}
