package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.veb.eo.model.reference.ProjectPaybackTime;

public interface ProjectPaybackTimeRepository extends JpaRepository<ProjectPaybackTime, Integer> {

}
