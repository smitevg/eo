package ru.veb.eo.repository;

import ru.veb.eo.model.reference.Opf;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OpfRepository extends JpaRepository<Opf, Integer> {

}
