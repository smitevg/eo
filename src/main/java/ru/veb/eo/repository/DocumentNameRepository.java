package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.veb.eo.model.reference.DocumentName;

import java.util.List;

public interface DocumentNameRepository extends JpaRepository<DocumentName, Integer> {
    List<DocumentName> findAllByResident(boolean resident);
}
