package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.veb.eo.model.reference.ProjectEntity;

import java.util.List;

public interface ProjectRepository extends JpaRepository<ProjectEntity, String> {
    List<ProjectEntity> findAllByProjectSalePlace(String projectSalePlace);
    int countAllByProjectSalePlaceAndProjectProjectIsAimedTo(String projectSalePlace, String projectProjectIsAimedTo);
    int countAllByProjectSalePlaceAndProjectRequestStatusIn(String projectSalePlace, List<Integer> projectRequestStatus);
}
