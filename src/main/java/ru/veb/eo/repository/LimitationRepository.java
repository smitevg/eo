package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.veb.eo.model.reference.Limitation;

public interface LimitationRepository extends JpaRepository<Limitation, Integer> {

}
