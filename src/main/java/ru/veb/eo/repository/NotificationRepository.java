package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.veb.eo.model.reference.Notification;

import java.util.List;

public interface NotificationRepository extends JpaRepository<Notification, Integer> {

    List<Notification> findByApplicantEmail(String applicantEmail);
    List<Notification> findByProjectCertifiedManagerId(String projectCertifiedManagerId);

    List<Notification> findByApplicantEmailAndIsViewed(String applicantEmail, boolean isViewed);
    List<Notification> findByProjectCertifiedManagerIdAndIsViewed(String projectCertifiedManagerId, boolean isViewed);
    List<Notification> findByProjectId(String projectId);
}
