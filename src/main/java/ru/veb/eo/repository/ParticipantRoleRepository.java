package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.veb.eo.model.reference.ParticipantRole;

public interface ParticipantRoleRepository extends JpaRepository<ParticipantRole, Integer> {

}
