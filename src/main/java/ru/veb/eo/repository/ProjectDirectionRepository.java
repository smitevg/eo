package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.veb.eo.model.reference.ProjectDirection;

public interface ProjectDirectionRepository extends JpaRepository<ProjectDirection, Integer> {

}
