package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.veb.eo.model.reference.Passwords;

public interface PasswordsRepository extends JpaRepository<Passwords, Integer> {
    Passwords findFirstByUserName(String userName);
}
