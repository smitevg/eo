package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.veb.eo.model.reference.Field;

public interface FieldsRepository extends JpaRepository<Field, Integer> {
    Field findFirstByField(String field);

}
