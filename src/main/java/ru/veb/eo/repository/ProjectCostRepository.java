package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.veb.eo.model.reference.ProjectCost;

public interface ProjectCostRepository extends JpaRepository<ProjectCost, Integer> {

}
