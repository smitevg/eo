package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.veb.eo.model.reference.FaceRole;

public interface FaceRoleRepository extends JpaRepository<FaceRole, Integer> {

}
