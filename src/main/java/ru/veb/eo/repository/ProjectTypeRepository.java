package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.veb.eo.model.reference.ProjectType;

public interface ProjectTypeRepository extends JpaRepository<ProjectType, Integer> {

}
