package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.veb.eo.model.Role;
import ru.veb.eo.model.User;

import java.util.List;
import java.util.UUID;

@Transactional(transactionManager="transactionManager", readOnly=true )
public interface UserRepository extends JpaRepository<User, String> {
    List<User> findByUsername(String username);
    List<User> findByUsernameOrLogin(String username, String login);
    User findFirstByUsername(String username);
    List<User> findByAuthoritiesContains(Role role);
    List<User> findByAuthoritiesContaining(List<Role> roles);
    List<User> findByAuthoritiesContainsAndRegionId(Role role, String regionId);

    User findFirstByAuthoritiesContainsAndCrmId(Role role, String crmId);

    void deleteUserByUsername(String username);
}
