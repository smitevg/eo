package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.veb.eo.model.reference.Months;

public interface MonthsRepository extends JpaRepository<Months, Integer> {

}
