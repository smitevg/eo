package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.veb.eo.model.reference.FaceType;

public interface FaceTypeRepository extends JpaRepository<FaceType, Integer>{

}
