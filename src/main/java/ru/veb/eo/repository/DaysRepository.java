package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.veb.eo.model.reference.Days;

public interface DaysRepository extends JpaRepository<Days, Integer> {

}
