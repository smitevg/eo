package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.veb.eo.model.reference.Years;

public interface YearsRepository extends JpaRepository<Years, Integer> {

}
