package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.veb.eo.Role;
import ru.veb.eo.model.reference.MessageEntity;

import java.util.List;

public interface MessageRepository extends JpaRepository<MessageEntity, String> {

    List<MessageEntity> findAllByProjectId(String projectId);

    List<MessageEntity> findAllByProjectIdAndAuthorRole(String projectId, Role authorRole);

    List<MessageEntity> findAllByProjectIdAndRecipientsContains(String projectId, Role recipients);

    List<MessageEntity> findAllByProjectIdAndRecipientsID(String projectId, String recipientsID);

}
