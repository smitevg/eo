package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.veb.eo.model.reference.District;
import ru.veb.eo.model.reference.Region;

public interface DistrictRepository extends JpaRepository<District, Integer> {
    District findFirstByRegionsContains(Region region);
}
