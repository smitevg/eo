package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.veb.eo.model.reference.FinancingCurrency;

public interface FinancingCurrencyRepository extends JpaRepository<FinancingCurrency, Integer> {

}
