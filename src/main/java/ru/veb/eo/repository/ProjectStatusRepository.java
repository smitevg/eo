package ru.veb.eo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.veb.eo.model.reference.ProjectCost;
import ru.veb.eo.model.reference.ProjectStatus;

public interface ProjectStatusRepository extends JpaRepository<ProjectStatus, Integer> {

}
