package ru.veb.eo.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.veb.eo.model.reference.ProjectSectors;

import java.util.List;

public interface ProjectSectorsRepository extends JpaRepository<ProjectSectors, Integer> {
    List<ProjectSectors> findAllByDirectionId(int directionId, Sort sort);

    ProjectSectors findFirstBySortOrder(int sortOrder);
}
