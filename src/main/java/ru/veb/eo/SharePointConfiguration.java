package ru.veb.eo;

import com.google.common.base.Strings;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.NTCredentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.transport.WebServiceMessageSender;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.Proxy;

@Configuration
public class SharePointConfiguration {

    @Autowired
    Properties properties;

    @Bean
    public HttpComponentsMessageSender httpComponentsMessageSender() {
        HttpComponentsMessageSender httpComponentsMessageSender = new HttpComponentsMessageSender();

        String domain = properties.getSharepointDomain();
        String username = properties.getSharepointUsername();
        String password = properties.getSharepointPassword();

        if (!Strings.isNullOrEmpty(username) && !Strings.isNullOrEmpty(password)) {
            if ("NTLM".equals(properties.getSharepointAuth()) || "ntlm".equals(properties.getSharepointAuth())) {
                Credentials credentials = new NTCredentials(username, password, null, domain.toUpperCase());
                httpComponentsMessageSender.setCredentials(credentials);
            } else if ("BASIC".equals(properties.getSharepointAuth()) || "basic".equals(properties.getSharepointAuth())) {
                Credentials credentials = new UsernamePasswordCredentials(username, password);
                httpComponentsMessageSender.setCredentials(credentials);
            }
        }

        return httpComponentsMessageSender;
    }

    @Bean(name = "restTemplateSharePoint")
    @Autowired
    public RestTemplate configRestTemplateSharePoint(RestTemplateBuilder restTemplateBuilder) {

        String domain = properties.getSharepointDomain();
        String username = properties.getSharepointUsername();
        String password = properties.getSharepointPassword();

        if (!Strings.isNullOrEmpty(username) && !Strings.isNullOrEmpty(password)) {
            String login = Strings.isNullOrEmpty(domain) ? username : domain.toUpperCase() + "\\" + username;
            NtlmAuthenticator authenticator = new NtlmAuthenticator(login, password);
            Authenticator.setDefault(authenticator);
        }

        RestTemplate restTemplate = null;
        if (properties.getProxyHost() != null && !properties.getProxyHost().isEmpty() && properties.getProxyPort() > 0) {

            SimpleClientHttpRequestFactory clientHttpReq = new SimpleClientHttpRequestFactory();
            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(properties.getProxyHost(), properties.getProxyPort()));
            clientHttpReq.setProxy(proxy);

            restTemplate = restTemplateBuilder.requestFactory(clientHttpReq).build();
        } else {
            restTemplate = restTemplateBuilder.requestFactory(new SimpleClientHttpRequestFactory()).build();
        }

        restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());

        return restTemplate;
    }

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setPackagesToScan("com.microsoft.sharepoint.ws");
        return marshaller;
    }

    @Bean
    public WebServiceTemplate webServiceTemplate(Jaxb2Marshaller marshaller, WebServiceMessageSender messageSender) {
        WebServiceTemplate webServiceTemplate = new WebServiceTemplate(marshaller, marshaller);
        webServiceTemplate.setMessageSender(messageSender);
        return webServiceTemplate;
    }
}
