package ru.veb.eo.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder(alphabetic = true)
@Entity
@Table(name = "project")
public class Project implements Serializable {
    @Id
    @Column(name = "id", nullable = false)
    private String projectId;

    @Column(name = "code")
    private String code;

    @Column(name = "title")
    private String title;

    @Column(name = "region")
    private String projectSalePlace;

    @Column(name = "direction")
    private String projectProjectIsAimedTo;

    /*
    1 - Архивный
    * */
    @Column(name = "status")
    private Integer projectRequestStatus;

    //@DateTimeFormat(pattern="dd-MMM-YYYY")
    @Column(name = "date_begin")
    private Date dateBegin;

    //@DateTimeFormat(pattern="dd-MMM-YYYY")
    @Column(name = "date_end")
    private Date dateEnd;

    @Column(name = "admin_proj")
    private String adminProj;

    @Column(name = "link")
    private String link;

    @Column(name = "type_proj")
    private String typeProj;

    @Transient
    private String statusTr;
    @Transient
    private String countTr;
}
