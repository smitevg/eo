package ru.veb.eo.entity;

import org.hibernate.sql.Update;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(transactionManager="transactionManagerMySql", readOnly=false )
public interface ProjectRepo extends JpaRepository<Project, String> {

    public static final int STATUS_ARCH = 1000;

    Project findOne(String s);



}

