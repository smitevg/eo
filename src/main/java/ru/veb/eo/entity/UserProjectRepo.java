package ru.veb.eo.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(transactionManager="transactionManagerMySql",  readOnly=false )
public interface UserProjectRepo extends JpaRepository<UserProject, Integer> {
    List<UserProject> findByUserId(String userId);

    List<UserProject> findByProject(Project project);

    UserProject saveAndFlush(UserProject userProject);

    void deleteUserProjectByUserIdAndProject(String userId, Project userProject);
}

