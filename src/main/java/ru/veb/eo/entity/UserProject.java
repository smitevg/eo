package ru.veb.eo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.veb.eo.model.Role;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

import javax.persistence.ManyToOne;

import static javax.persistence.CascadeType.ALL;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder(alphabetic = true)
@Entity
@Table(name = "user_project")
public class UserProject implements Serializable {
    @Id
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Transient
    private String username;

    @Column(name = "user_id", nullable = false)
    private String userId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id")
    private Project project;

    @Column(name = "type_voice")
    private String typeVoice;

    @Column(name = "sum")
    private String sum;

    @Column(name = "sum_cur")
    private String sumCur;

    @Column(name = "date_voice")
    private Date dateVoice;

    //Дата добавления в голосующие
    @Column(name = "date_append")
    private Date dateAppend;

    // было ли голосование?
    @JsonProperty("isVoice")
    public boolean isVoice() {
        if (typeVoice == null) return false;
        if ("".equals(typeVoice.trim())) return false;
        return true;
    }
}
