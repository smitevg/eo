package ru.veb.eo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import ru.veb.eo.service.UserService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthFilter authFilter;

    @Autowired
    private AuthFilterAfterAuth authFilterAfterAuth;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);

        http
                .addFilterBefore(authFilter, UsernamePasswordAuthenticationFilter.class)
                .addFilterAfter(authFilterAfterAuth, UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                    .antMatchers("/files/rm/**").hasRole("MANAGER")
                    .anyRequest().authenticated()
                    .and()
                .formLogin()
                    .loginPage("/login")
                    .usernameParameter("login")
                    .permitAll()
                    .and()
                        .httpBasic().disable()
                .exceptionHandling().accessDeniedPage("/403").and()
                .logout()
                    .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                    .clearAuthentication(true)
                    .invalidateHttpSession(true)
                    .logoutSuccessUrl("/login");
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/", "/firstRequest",
                "/firstRequest/**", "/reference/**",
                "/js/**", "/css/**",
                "/h2-console/**",
                "/img/**", "/svg/**", "/vendor/**",
                "/favicon.ico", "/fonts/**",
                "/manager-list.html",
                "/industries-finance.html",
                "/project-history.html",
                "/map.html",
                "/403.html",
                "/recovery");
    }

    @Bean
    PasswordEncoder bcryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void auth(AuthenticationManagerBuilder auth, UserService userService) throws Exception {
        auth
                .userDetailsService(userService)
                .passwordEncoder(bcryptPasswordEncoder());
    }
}
