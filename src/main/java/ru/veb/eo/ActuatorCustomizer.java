package ru.veb.eo;

import java.util.LinkedHashSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.actuate.endpoint.mvc.ActuatorMediaTypes;
import org.springframework.boot.actuate.endpoint.mvc.EndpointHandlerMapping;
import org.springframework.boot.actuate.endpoint.mvc.EndpointHandlerMappingCustomizer;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * Делает чтобы API Spring Boot Actuator выадвало по умолчнанию содержимое 
 * с Content-Type <code>application/json</code>.
 */
@Component
public class ActuatorCustomizer implements EndpointHandlerMappingCustomizer {

	static class Fix extends HandlerInterceptorAdapter {
		@Override
		public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
			String accept = request.getHeader("Accept");
			if (accept != null && accept.indexOf(ActuatorMediaTypes.APPLICATION_ACTUATOR_V1_JSON_VALUE) >= 0) {
			    return true;
			}
			
		    Object attribute = request.getAttribute(HandlerMapping.PRODUCIBLE_MEDIA_TYPES_ATTRIBUTE);
			if (attribute instanceof LinkedHashSet) {
				@SuppressWarnings("unchecked")
				LinkedHashSet<MediaType> lhs = (LinkedHashSet<MediaType>) attribute;
				MediaType amt = ActuatorMediaTypes.APPLICATION_ACTUATOR_V1_JSON;
                if (lhs.remove(amt)) {
					lhs.add(amt);
				}
			}
			return true;
		}
	}

	@Override
	public void customize(EndpointHandlerMapping mapping) {
		mapping.setInterceptors(new Object[] { new Fix() });
	}
}