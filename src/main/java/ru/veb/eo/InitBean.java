package ru.veb.eo;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.thymeleaf.util.ListUtils;
import org.thymeleaf.util.StringUtils;
import ru.veb.eo.model.Role;
import ru.veb.eo.model.User;
import ru.veb.eo.model.crm.Manager;
import ru.veb.eo.model.crm.ProjectRequest;
import ru.veb.eo.model.crm.ProjectResponsePacket;
import ru.veb.eo.model.reference.Notification;
import ru.veb.eo.model.reference.Passwords;
import ru.veb.eo.model.reference.ProjectEntity;
import ru.veb.eo.model.reference.Region;
import ru.veb.eo.repository.NotificationRepository;
import ru.veb.eo.repository.PasswordsRepository;
import ru.veb.eo.repository.ProjectRepository;
import ru.veb.eo.service.*;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class InitBean {
    private UserService us;
    private CrmService crmService;
    private ReferenceService referenceService;
    private PasswordsRepository passwordsRepository;
    private Properties properties;
    private NotificationRepository nr;
    private EmailService es;
    private ProjectRepository pr;


    @Autowired
    PasswordService pwd;

    @Autowired
    PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer;

    @Value("${git.branch}")
    private String branch;

    @Value("${git.commit.id}")
    private String commitId;

    @Value("${git.commit.time}")
    private String commitTime;

    private static final Logger logger = LoggerFactory.getLogger(InitBean.class);

    @Autowired
    public InitBean(UserService userService, CrmService crmService, ReferenceService referenceService, PasswordsRepository passwordsRepository, Properties properties, NotificationRepository nr, ProjectRepository pr, EmailService es) {
        this.us = userService;
        this.crmService = crmService;
        this.referenceService = referenceService;
        this.passwordsRepository = passwordsRepository;
        this.properties = properties;
        this.nr = nr;
        this.es = es;
        this.pr = pr;
    }

    @PostConstruct
    private void initManagers() {
        logger.debug("DZO_ON={}", properties.isDzo());
        logger.debug("RATING_ON={}", properties.isRating());
        logger.debug("DEBUG_MODE={}", properties.getDebugMode());
        if (!properties.getDebugMode()) {
            logger.debug("git.branch={}", branch);
            logger.debug("git.commit.id={}", commitId);
            logger.debug("git.commit.time={}", commitTime);
            syncManagers();
            syncProjects();
        }
    }

    @Scheduled(cron = "${sync.manager.cron}")
    private void schedulerManagers() {
        syncManagers();
    }

    @Scheduled(cron = "0 0 4 * * *")
    private void schedulerProjects() {
        syncProjects();
    }

    private void syncProjects() {
        logger.debug("start scheduler - sync projects");

        Notification notification1 = new Notification();
        notification1.setMessage("Sunc");
        nr.saveAndFlush(notification1);

        // добавим мапы для email'ов манагеров, чтобы повторно потом не запрашивать почты если мы их уже запрашивали
        Map<String, Manager> managersMap = new HashMap<String, Manager>();
        List<ProjectResponsePacket> projects = null;
        try {
            logger.info("Getting project packet 1000 all");
            projects = crmService.getProjectPacket(1000, 0, null, "all");
        } catch (Exception e) {
            logger.error("Error getting Project Packet {}", e.toString(), e);
        }
        List<ProjectEntity> projectsSync = pr.findAll();

        if (ListUtils.isEmpty(projects)) {
            logger.info("Empty project packet, deleting all");
            pr.deleteAll();
        } else if (!ListUtils.isEmpty(projectsSync)) {
            for (ProjectEntity projectEntity : projectsSync) {
                ProjectResponsePacket projectResponse = projects.stream()
                        .filter(e -> e.projectCode != null && e.projectCode.equals(projectEntity.getProjectCode()))
                        .findFirst()
                        .orElse(null);

                if (null == projectEntity) continue;

                if (projectResponse == null) {
                    pr.delete(projectEntity);
                } else {
                    try {
                        // если проект на экспресс оценке и задержка проекта уже более 5 дней
                        if (Preconditions.checkNotNull(projectResponse.projectRequestStatus, "projectRequestStatus is NULL") == 1 && Preconditions.checkNotNull(projectResponse.differenceDaysBetweenTodayAndEnteredDate, "differenceDaysBetweenTodayAndEnteredDate is NULL") > 5) {

                            if (managersMap.get(projectResponse.projectCertifiedManagerId) == null) {
                                managersMap.put(projectResponse.projectCertifiedManagerId, crmService.getManagerById(projectResponse.projectCertifiedManagerId));
                            }
                            Manager projectManager = managersMap.get(projectResponse.projectCertifiedManagerId);

                            // сообщение в колокольчик
                            Notification notification = new Notification();
                            notification.setApplicantEmail(projectResponse.getApplicantEmail());
                            notification.setProjectCertifiedManagerId(projectResponse.getProjectCertifiedManagerId());
                            notification.setProjectId(projectResponse.getProjectId());
                            notification.setMessage(String.format(" прошло %d дня(ей) с момента как надо было провести экспресс-оценку проекта \"%s\"", projectResponse.differenceDaysBetweenTodayAndEnteredDate, projectResponse.getProjectNomination()));
                            notification.setProjectNomination(projectResponse.getProjectNomination());
                            nr.saveAndFlush(notification);

                            // если у манагера есть email
                            if (!StringUtils.isEmpty(projectManager.getEmail())) {

                                StringBuilder sb = new StringBuilder();

                                sb.append(String.format("Вам надо провести экспресс оценку проекта \"%s\".", projectResponse.getProjectNomination()));
                                sb.append(String.format(" ID проекта: %s", projectResponse.getProjectId()));

                                // отправим письма
                                es.sendSimpleMessage(projectManager.getEmail(),
                                        String.format("Прошло %d дня(ей) как надо провести экспресс-оценку проекта \"%s\"", projectResponse.differenceDaysBetweenTodayAndEnteredDate, projectResponse.projectNomination),
                                        sb.toString()
                                );
                            }
                        }
                    } catch (Exception e) {
                        logger.error("Error send e-mail and message about days to express assesment: projectEntity={}, projectResponse={}", projectEntity, projectResponse);
                        e.printStackTrace();
                    }
                    // если проект перешел в какой-то другой стутус
                    try {
                        if (!Objects.equals(projectEntity.getProjectRequestStatus(), projectResponse.projectRequestStatus)) {

                            if (managersMap.get(projectResponse.projectCertifiedManagerId) == null) {
                                managersMap.put(projectResponse.projectCertifiedManagerId, crmService.getManagerById(projectResponse.projectCertifiedManagerId));
                            }
                            Manager projectManager = managersMap.get(projectResponse.projectCertifiedManagerId);

                            // в колокольчик
                            nr.saveAndFlush(new Notification(projectResponse.getApplicantEmail(), projectResponse.getProjectCertifiedManagerId(), projectResponse.getProjectId(), projectResponse.getProjectNomination(), " успешно перешёл на стадию \"Подготовка к предварителной экспертизе\".", true));

                            // если у манагера есть email
                            if (!StringUtils.isEmpty(projectManager.getEmail())) {

                                // письмо манагеру
                                es.sendSimpleMessage(projectManager.getEmail(),
                                        String.format("Проект \"%s\" перешел на стадию \"%s\"", projectResponse.projectNomination, projectResponse.projectStatusName),
                                        String.format("Проект \"%s\" перешёл на стадию \"%s\".", projectResponse.projectNomination, projectResponse.projectStatusName)
                                );

                            }

                            // письмо клиенту
                            es.sendSimpleMessage(projectResponse.applicantEmail,
                                    String.format("Проект \"%s\" перешел на стадию \"%s\"", projectResponse.projectNomination, projectResponse.projectStatusName),
                                    String.format("Ваш проект \"%s\" перешёл на стадию \"%s\".", projectResponse.projectNomination, projectResponse.projectStatusName)
                            );

                        }
                    } catch (Exception e) {
                        logger.error("Error send e-mail and message about change stage project: projectEntity={}, projectResponse={}", projectEntity, projectResponse);
                        e.printStackTrace();
                    }
                }
            }
        }

        if (!ListUtils.isEmpty(projects)) {
            for (ProjectResponsePacket project : projects) {
                try {
                    ProjectRequest projectRequest = null;
                    if (!Strings.isNullOrEmpty(project.getApplicantEmail())) {
                        projectRequest = crmService.getProjectRequest(project.projectId, project.getApplicantEmail(), null);
                    } else {
                        projectRequest = crmService.getProjectRequest(project.projectId, null, project.getProjectCertifiedManagerId());
                    }

                    if (projectRequest == null) {
                        throw new IllegalStateException("projectRequest is null: " + project.projectId);
                    }

                    ProjectEntity projectEntity = new ProjectEntity();
                    BeanUtils.copyProperties(projectRequest, projectEntity);
                    logger.info("Saving project {} {}", projectEntity.getProjectId(), projectEntity);
                    pr.saveAndFlush(projectEntity);
                } catch (Exception e) {
                    logger.error("Failed to save project {}: {}", project.projectId, e.toString(), e);
                }
            }
        }
    }

    //
    private List<Role> getAuthorities(Manager manager, List<Manager> listAdm, List<Manager> listFab, List<Manager> listSynd) {
        ArrayList<Role> roles = new ArrayList<>();
        for (Manager ma : listAdm){
            if (ma.getCrmId().equals(manager.getCrmId())) {
                roles.add(Role.SIND_ADM);
            }
        }
        for (Manager ma : listFab){
            if (ma.getCrmId().equals(manager.getCrmId())) {
                roles.add(Role.FAB);
            }
        }
        for (Manager ma : listSynd){
            if (ma.getCrmId().equals(manager.getCrmId())) {
                roles.add(Role.SIND);
            }
        }
        //
        if (roles.size() == 0){
            roles.add(Role.MANAGER);
        }
        return roles;
    }

    private void syncManagers() {
        logger.debug("start scheduler - sync managers");


        List<Region> regions = referenceService.getRegion();

        if (!ListUtils.isEmpty(regions)) {
            // загрузка СМ из CRM
            List<Manager> managersCrm = new ArrayList<>();
            
            /*for (Region region : regions) {
                List<Manager> managersByRegion = crmService.getManagersByRegion(region.getId());
                if (!ListUtils.isEmpty(managersByRegion)) managersCrm.addAll(managersByRegion);
            }*/

            List<Manager> listAdm = crmService.getUserSind(CrmService.ADMIN);
            List<Manager> listFab = crmService.getUserSind(CrmService.FABRIC);
            List<Manager> listSynd = crmService.getUserSind(CrmService.SYNDICATE);

            HashSet hashSet = new HashSet();
            hashSet.addAll(listAdm);
            logger.info("listAdm: " + listAdm.size());
            logger.info("hashSet: " + hashSet.size());
            hashSet.addAll(listFab);
            logger.info("listAdm: " + listFab.size());
            logger.info("hashSet: " + hashSet.size());
            hashSet.addAll(listSynd);
            logger.info("listAdm: " + listSynd.size());
            logger.info("hashSet: " + hashSet.size());


            /*if (!ListUtils.isEmpty(listAdm)) managersCrm.addAll(listAdm);
            if (!ListUtils.isEmpty(listFab)) managersCrm.addAll(listFab);
            if (!ListUtils.isEmpty(listSynd)) managersCrm.addAll(listSynd);*/
            managersCrm.addAll(hashSet);

            managersCrm = managersCrm.stream()
                    .filter(e -> e.getEmail() != null && !e.getEmail().toLowerCase().equals("sheyka@invest-tula.com"))
                    .collect(Collectors.toList());
            ;
//


            // очистка от удалённх СМ
            List<User> managersEo = us.getManagers();
            for (User u : managersEo) {
                logger.info(u.getCrmId());
                logger.info(u.getUsername());
            }
            logger.info("managersEo.size: " + managersEo.size());
            if (!ListUtils.isEmpty(managersEo)) {
                for (User user : managersEo) {
                    logger.info("for: " + user.getUsername());
                    try {
                        if (managersCrm.size() > 0) { // не удаляем если вообще нет пользователе. тк это возможно недоступность сервиса
                            Manager manager = managersCrm.stream()
                                    .filter(e -> e.getEmail() != null && e.getEmail().toLowerCase().equals(user.getUsername().toLowerCase()))
                                    .findFirst()
                                    .orElse(null);
                            if (manager == null) {
                                us.deleteUser(user);
                                logger.info("Менеджер {} удалён.", user.getUsername());
                            }
                        }
                    } catch (Exception e) {
                        logger.error(e.getMessage());
                        e.printStackTrace();
                    }
                }
            }

            // слитие СМ
            if (!ListUtils.isEmpty(managersCrm)) {
                for (Manager manager : managersCrm) {
                    try {
                        logger.info("managerCrm for: " + manager.getEmail());
                        // Проверяем, существует ли уже пользователь
                        User user = managersEo.stream()
                                .filter(e -> e.getUsername() != null && manager.getEmail() != null && e.getUsername().equals(manager.getEmail().toLowerCase()))
                                .findFirst()
                                .orElse(null);

                        if (null == user) {
                            logger.info("!null! for: " + manager.getEmail());
                            user = User.builder()
                                    .crmId(manager.getCrmId())
                                    .username(manager.getEmail().toLowerCase())
                                    .password((new BCryptPasswordEncoder()).encode(pwd.generatePassword(manager.getEmail())))
                                    .nameFirst(manager.getNameFirst())
                                    .nameLast(manager.getNameLast())
                                    .nameMiddle(manager.getNameMiddle())
                                    .phone(manager.getPhone())
                                    .regionId(manager.getRegionId())
                                    .authorities(getAuthorities(manager, listAdm, listFab, listSynd))
                                    .image(manager.getImage())
                                    .accountNonExpired(true)
                                    .accountNonLocked(true)
                                    .credentialsNonExpired(true)
                                    .enabled(true)
                                    .build();
                        } else {
                            logger.info("!not null! for: " + manager.getEmail());
                            user.setCrmId(manager.getCrmId());
                            user.setUsername(manager.getEmail().toLowerCase());
                            if (properties.isEnablePassword())
                                user.setPassword(user.getPassword());
                            else
                                user.setPassword((new BCryptPasswordEncoder()).encode(pwd.generatePassword(manager.getEmail()))); // если пароли отключены, то сбрасываем пароль
                            user.setNameFirst(manager.getNameFirst());
                            user.setNameLast(manager.getNameLast());
                            user.setNameMiddle(manager.getNameMiddle());
                            user.setPhone(manager.getPhone());
                            user.setRegionId(manager.getRegionId());
                            user.setAuthorities(getAuthorities(manager, listAdm, listFab, listSynd));
                            user.setImage(manager.getImage());
                            user.setAccountNonExpired(true);
                            user.setAccountNonLocked(true);
                            user.setCredentialsNonExpired(true);
                            user.setEnabled(true);
                        }

//                        user.handleAvatars(this.properties);
                        us.saveUser(user);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        logger.debug("end scheduler - sync managers");
    }

    @PostConstruct
    private void initOneManagerAndUser() {
        if (properties.getDebugMode()) {


//            us.addUser(User.builder()
//                    .crmId("dee285e7-657d-a042-558e-593aa4927d52")
//                    .username("TikhonovaAV@veb.ru")
//                    .password((new BCryptPasswordEncoder()).encode("123"))
//                    .nameFirst("Анна")
//                    .nameLast("Тихонова")
//                    .nameMiddle("Отчество")
//                    .phone("телефон")
//                    .authorities(Arrays.asList(Role.MANAGER))
//                    .image(null)
//                    .accountNonExpired(true)
//                    .accountNonLocked(true)
//                    .credentialsNonExpired(true)
//                    .enabled(true)
//                    .build()
//            );
//
//            Notification notification = new Notification();
//            notification.setMessage("test");
//            notification.setApplicantEmail("asd@asd.asd");
//            notification.setProjectCertifiedManagerId("dee285e7-657d-a042-558e-593aa4927d52");
//            notification.setIsGood(true);
//            notification.setProjectId("3b82535a-2df0-c1c7-5167-59ae445780b2");
//            notification.setViewed(false);
//            notification.setProjectNomination("3b82535a");
//            nr.saveAndFlush(notification);
//
//
//            Notification notification2 = new Notification();
//            notification2.setMessage("test");
//            notification2.setApplicantEmail("asd@asd.asd");
//            notification2.setProjectCertifiedManagerId("dee285e7-657d-a042-558e-593aa4927d52");
//            notification2.setIsGood(false);
//            notification2.setProjectId("d9aac0c9-0dee-414b-d0e6-59b008c47fb6");
//            notification2.setViewed(false);
//            notification2.setProjectNomination("d9aac0c9");
//            nr.saveAndFlush(notification2);
//
//            Notification notification3 = new Notification();
//            notification3.setMessage("test");
//            notification3.setApplicantEmail("asd@asd.asd");
//            notification3.setProjectCertifiedManagerId("dee285e7-657d-a042-558e-593aa4927d52");
//            notification3.setIsGood(true);
//            notification3.setProjectId("d9aac0c9-0dee-414b-d0e6-59b008c47fb6");
//            notification3.setViewed(false);
//            notification3.setProjectNomination("d9aac0c9");
//            nr.saveAndFlush(notification3);

//            us.addUser(User.builder()
//                    .id(UUID.randomUUID().toString())
//                    .username("asd@asd.asd")
//                    .password((new BCryptPasswordEncoder()).encode("123"))
//                    .nameFirst("Имя")
//                    .nameLast("Фамилия")
//                    .nameMiddle(null)
//                    .phone(null)
//                    .authorities(Arrays.asList(Role.USER))
//                    .image(null)
//                    .accountNonExpired(true)
//                    .accountNonLocked(true)
//                    .credentialsNonExpired(true)
//                    .enabled(true)
//                    .build()
//            );
        }
    }
}

