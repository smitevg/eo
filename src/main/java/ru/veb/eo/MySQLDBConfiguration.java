package ru.veb.eo;


import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@ConditionalOnProperty(name = "SQL_SERVICE", havingValue="suite")
@EnableJpaRepositories(basePackages = {"ru.veb.eo.entity"},
                       entityManagerFactoryRef = "entityManagerMySql",
                       transactionManagerRef = "transactionManagerMySql")        // 2
public class MySQLDBConfiguration {

    @Autowired
    private Environment env;


    @Autowired
    @Qualifier("dataSourceMySQL")
    DataSource ds;

    private HibernateJpaVendorAdapter vendorAdaptor() {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        //vendorAdapter.setShowSql(true);
        //vendorAdapter.setPrepareConnection(true);
        return vendorAdapter;
    }

    @Bean(name = "entityManagerMySql")
    public LocalContainerEntityManagerFactoryBean entityManagerMySql() {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setJpaVendorAdapter(vendorAdaptor());
        entityManagerFactoryBean.setDataSource(ds);
        entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        entityManagerFactoryBean.setPackagesToScan("ru.veb.eo.entity");
        entityManagerFactoryBean.setJpaProperties(jpaProperties());
        return entityManagerFactoryBean;
    }

    @Bean(name = "transactionManagerMySql")
    public JpaTransactionManager transactionManagerMySql() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerMySql().getObject());
        return transactionManager;
    }

    private Properties jpaProperties() {
        Properties props = new Properties();
   //     props.put("hibernate.ddl-auto", env.getProperty("spring.jpa.hibernate.ddl-auto"));
   //     props.put("hibernate.ejb.naming_strategy", env.getProperty("spring.jpa.hibernate.ejb.naming_strategy", "org.springframework.boot.orm.jpa.hibernate.SpringNamingStrategy"));
   //     props.put("generate-ddl", env.getProperty("spring.jpa.generate-ddl"));
        return props;
    }
}
