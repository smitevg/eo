package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import ru.veb.eo.serializer.DateDeserializer;
import ru.veb.eo.serializer.DateSerializer;

import java.util.Date;
import java.util.List;

@Data
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Declarer {

    // ПОЛЯ ПОМЕЧЕННЫЙ с fl_*** идут на фронт

    @JsonProperty("id")
    private String crmId;

    // ПОЛЯ ДЛЯ CRM
    @JsonProperty("first_name")
    public String applicantNameFirst;
    @JsonProperty("last_name")
    public String applicantNameLast;
    @JsonProperty("mid_name")
    public String applicantNameMid;
    @JsonProperty("emails")
    public List<String> applicantEmails;
    @JsonProperty("phone_numbers")
    public String applicantPhoneNumbers;

    // РЕЗИДЕНТ
    @JsonProperty("resident")
    private Boolean resident;

    // ДАТА РОЖДЕНИЯ
    @JsonProperty("born_date")
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using= DateDeserializer.class)
    public DateForm bornDate;

    // МЕСТО РОЖДЕНИЯ
    @JsonProperty("born_place")
    public Address bornPlace;

    @JsonProperty("citizenship")
    private String fl_citizen;

    @JsonProperty("primary_address_state")
    private String fl_region;

    @JsonProperty("primary_address_city")
    private String fl_town;

    // МЕСТО ЖИТЕЛЬСТВА
    @JsonProperty("primary_address_street")
    private String primaryAddressStreet; // улица, домб корпус, квартира

    @JsonIgnore
    private String fl_street;
    @JsonIgnore
    private String fl_house;
    @JsonIgnore
    private String fl_section;
    @JsonIgnore
    private String fl_apartment;

    // ТИП ДОКУМНТА
    @JsonProperty("document_name")
    private String fl_document_type;

    // СЕРИЯ ДОКУМЕНТА
    @JsonProperty("document_series")
    private String fl_document_series;

    // НОМЕР ДОКУМЕНТА
    @JsonProperty("document_number")
    private String fl_document_number;

    // ДАТА ВЫДАЧИ
    @JsonProperty("document_issue_date")
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using= DateDeserializer.class)
    public DateForm idIssueDate;


    // КЕМ ВЫДАН
    @JsonProperty("document_issue_who")
    private String fl_document_issued;

    // код подразделения
    @JsonProperty("document_issue_who_code")
    private String fl_document_code_department;

}

