package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import ru.veb.eo.serializer.*;

import java.util.List;
import java.util.Set;

@Data
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectResponse {
    @JsonProperty("code")
    public String projectCode;
    @JsonProperty("request_status")
    public Integer projectRequestStatus;
    @JsonProperty("id")
    public String projectId;

    @JsonProperty("initiator")
    public Initiator initiator;
    @JsonProperty("certified_manager")
    public Manager projectManager;
    @JsonProperty("declarer")
    public Declarer declarer;


    @JsonProperty("bond_custom")
    public List<List<String>> projectAmountBondArray;
    @JsonProperty("coinvestor_custom")
    public List<List<String>> projectAmountInvestorArray;
    @JsonProperty("finance_custom")
    public List<List<String>> projectAmountFinanceArray;


    @JsonProperty("confirming_budgetary_effect")
    public String projectConfirmingBudgetaryEffect;

    @JsonProperty("estimated_amount_of_budget_revenues")
    public String projectBudgetaryEffectEstimatedAmount;

    @JsonProperty("estimated_value_of_budget_expenditures")
    public String projectBudgetaryEffectEstimatedValue;

    @JsonProperty("new_work_places_count")
    public String projectWorkPlaceCount;


    @JsonProperty("additional_info")
    public String projectAdditionalInfo;
    @JsonProperty("agreement_basic_parameters")
    public String projectAgreementBasicParameters;
    @JsonProperty("assurance_amount")
    @JsonDeserialize(using = DoubleDeserializer.class)
    public Double projectAssuranceAmount;
    @JsonProperty("assurance_currency")
    public String projectAssuranceCurrency;
    @JsonProperty("authorization_document_issued_by")
    public String projectAuthorizationDocumentIssuedBy;
    @JsonProperty("authorization_document_issued_date")
    @JsonDeserialize(using = DateDeserializer.class)
    public DateForm projectAuthorizationDocumentIssuedDate;
    @JsonProperty("availability_of_state_support")
    @JsonDeserialize(using = StateSupportDeserializer.class)
    public Set<Integer> projectStateSupport;


    @JsonProperty("availability_of_state_support_comment0")
    public String projectImpactInfoAvailabilityOfStateSupportComment0;
    @JsonProperty("availability_of_state_support_comment1")
    public String projectImpactInfoAvailabilityOfStateSupportComment1;
    @JsonProperty("availability_of_state_support_comment2")
    public String projectImpactInfoAvailabilityOfStateSupportComment2;



    @JsonProperty("banks_of_the_syndicate")
    public String projectBanksOfTheSyndicate;
    @JsonProperty("banks_withdrawal_from_project")
    public String projectBanksWithdrawalFromProject;
    @JsonProperty("calculating_effect_components")
    public String projectCalculatingEffectComponents;
    @JsonProperty("calculation_formula")
    public String projectCalculationFormula;
    @JsonProperty("can_project_financed_by_financial_institutions")
    public String projectCanProjectFinancedByFinancialInstitutions;
    @JsonProperty("credit_resources_cost_percent")
    public String projectCreditResourcesCostPercent;
    @JsonProperty("date_creating")
    @JsonDeserialize(using = DateDeserializer.class)
    public DateForm projectDateEntered;
    @JsonProperty("debt_repayment_delay")
    public String projectDebtRepaymentDelay;
    @JsonProperty("debt_repayment_period")
    public String projectDebtRepaymentPeriod;
    @JsonProperty("debt_repayment_schedule")
    public String projectDebtRepaymentSchedule;
    @JsonProperty("design_risks_analysis")
    public String projectDesignRisksAnalysis;
    @JsonProperty("employment_effect_information")
    public String projectEffectEmployment;
    @JsonProperty("environmental_expertise_conclusion_not_required")
    public String projectEnvironmentalExpertiseConclusionNotRequired;
    @JsonProperty("environmental_expertise_is_not_required")
    public String projectEnvironmentalExpertiseIsNotRequired;
    @JsonProperty("environmental_expertise_issued_by")
    public String projectEnvironmentalExpertiseIssuedBy;
    @JsonProperty("environmental_expertise_issued_date")
    @JsonDeserialize(using = DateDeserializer.class)
    public DateForm projectEnvironmentalExpertiseIssuedDate;
    @JsonProperty("evaluation_method")
    public String projectEvaluationMethod;
    @JsonProperty("expected_result")
    public String projectExpectedResult;
    @JsonProperty("experience_of_project_managers")
    public String projectExperienceProjectManagers;
    @JsonProperty("express_valuating1")
    public Integer expressValuating1;
    @JsonProperty("express_valuating1_description")
    public String expressValuating1Description;
    @JsonProperty("express_valuating2")
    public Integer expressValuating2;
    @JsonProperty("express_valuating2_description")
    public String expressValuating2Description;
    @JsonProperty("express_valuating3")
    public Integer expressValuating3;
    @JsonProperty("express_valuating3_description")
    public String expressValuating3Description;
    @JsonProperty("express_valuating4")
    public Integer expressValuating4;
    @JsonProperty("express_valuating4_description")
    public String expressValuating4Description;
    @JsonProperty("express_valuating5")
    public Integer expressValuating5;
    @JsonProperty("express_valuating5_description")
    public String expressValuating5Description;
    @JsonProperty("express_valuating6")
    public Integer expressValuating6;
    @JsonProperty("express_valuating6_description")
    public String expressValuating6Description;
    @JsonProperty("express_valuating7")
    public Integer expressValuating7;
    @JsonProperty("express_valuating7_description")
    public String expressValuating7Description;
    @JsonProperty("financial_institutions_participation_terms_in_project")
    public String projectFinancialInstitutionsParticipationTermsInProject;
    @JsonProperty("financial_institutions_ready_to_finance_the_project")
    public String projectFinancialInstitutionsReadyToFinanceTheProject;
    @JsonProperty("financial_model_comment")
    public String projectFinancialModelComment;
    @JsonProperty("financial_model_document")
    public String projectFinancialModelDocument;
    @JsonProperty("financial_plan_document")
    public String projectFinancialPlanDocument;
    @JsonProperty("financing_basic_terms")
    public String projectFinancingBasicTerms;
    @JsonProperty("project_cost_currency")
    public String projectCostCurrency;
    @JsonProperty("financing_plan_document")
    public String projectFinancingPlanDocument;
    @JsonProperty("financing_request_type")
    public String projectFinancingRequestType;
    @JsonProperty("financing_request_type_other")
    public String projectFinancingRequestTypeOther;
    @JsonProperty("financing_request_type_time")
    public String projectFinancingRequestTypeTime;
    @JsonProperty("funds_using_monitoring_mechanisms")
    public String projectFundsUsingMonitoringMechanisms;

    @JsonProperty("funds_using_monitoring_mechanisms_description")
    public String projectFundsUsingMonitoringMechanismsDescription;

    @JsonProperty("guarantor_name")
    public String projectAssuranceGuarantor;
    @JsonProperty("have_financing_at_rf_before")
    public String projectHaveFinancingAtRfBefore;
    @JsonProperty("last_cost")
    public String projectLastCost;
    @JsonProperty("market_value_of_collateral")
    public String projectAssuranceValueCollateral;
    @JsonProperty("nomination")
    public String projectNomination;
    @JsonProperty("order_book")
    public String projectOrderBook;
    @JsonProperty("other_financing_sources")
    public String projectFinancingSourcesOther;
    @JsonProperty("product_description_document")
    public String projectProductDescriptionDocument;
    @JsonProperty("product_main_qualitative_characteristics")
    public String projectProductMainQualitativeCharacteristics;
    @JsonProperty("project_advantages_in_market")
    public String projectProjectAdvantagesInMarket;
    @JsonProperty("project_advisability_realization")
    public String projectProjectAdvisabilityRealization;
    @JsonProperty("project_aim_reason")
    public String projectProjectAimReason;
    @JsonProperty("project_assurance")
    public Integer projectAssuranceType;
    @JsonProperty("project_correspondence_sectors")
    public String projectProjectCorrespondenceSectors;
    @JsonProperty("project_cost")
    public String projectCost;

    @JsonProperty("project_cost_double")
    @JsonDeserialize(using = DoubleDeserializer.class)
    public Double projectProjectCostDouble;

    @JsonProperty("banks_cash_amount")
    @JsonDeserialize(using = DoubleDeserializer.class)
    public Double projectAmountBanksCash;

    @JsonProperty("banks_cash_part")
    @JsonDeserialize(using = DoubleDeserializer.class)
    public Double projectAmountBanksPart;

    @JsonProperty("bond_issue")
    @JsonDeserialize(using = DoubleDeserializer.class)
    public Double projectAmountBondCash;

    @JsonProperty("bond_issue_percent")
    @JsonDeserialize(using = DoubleDeserializer.class)
    public Double projectAmountBondPart;

    @JsonProperty("co_investor_financing_funds")
    @JsonDeserialize(using = DoubleDeserializer.class)
    public Double projectAmountInvestorCash;

    @JsonProperty("co_investor_financing_funds_percent")
    @JsonDeserialize(using = DoubleDeserializer.class)
    public Double projectAmountInvestorPart;

    @JsonProperty("financing_request_amount")
    @JsonDeserialize(using = DoubleDeserializer.class)
    public Double projectAmountFinancingRequest;

    @JsonProperty("others_cash_amount")
    @JsonDeserialize(using = DoubleDeserializer.class)
    public Double projectAmountOthersCash;

    @JsonProperty("others_cash_part")
    @JsonDeserialize(using = DoubleDeserializer.class)
    public Double projectAmountOthersPart;

    @JsonProperty("recipients_cash_amount")
    @JsonDeserialize(using = DoubleDeserializer.class)
    public Double projectAmountRecipientsCash;

    @JsonProperty("recipients_cash_part")
    @JsonDeserialize(using = DoubleDeserializer.class)
    public Double projectAmountRecipientsPart;

    @JsonProperty("syndicated_debt_amount")
    @JsonDeserialize(using = DoubleDeserializer.class)
    public Double projectSyndicatedDebtAmount;

    @JsonProperty("project_decision_date_and_level")
    public String projectProjectDecisionDateAndLevel;
    @JsonProperty("project_description")
    public String projectDescription;
    @JsonProperty("project_environmental_protection_require")
    public String projectEnvironmentalProtection;
    @JsonProperty("project_environmental_protection_require_comment")
    public String projectEnvironmentalProtectionComment;




    @JsonProperty("project_financing_expense_investment_fund")
    @JsonDeserialize(using = BooleanDeserializer.class)
    public Boolean projectProjectFinancingExpenseInvestmentFund;
    @JsonProperty("project_implementation_activities")
    public String projectProjectImplementationActivities;
    @JsonProperty("project_implementation_corresponds_to_priorities")
    @JsonDeserialize(using = BooleanDeserializer.class)
    public Boolean projectImplementationCorrespondsToPriorities;
    @JsonProperty("project_implementation_public_private_partnership")
    @JsonDeserialize(using = BooleanDeserializer.class)
    public Boolean projectProjectImplementationPublicPrivatePartnership;
    @JsonProperty("project_implementation_technological_opportunities")
    public String projectTechnologicalOpportunities;
    @JsonProperty("project_implementation_technologies")
    public String projectTechnologiesImplementation;
    @JsonProperty("project_irr")
    @JsonDeserialize(using = DoubleDeserializer.class)
    public Double projectProjectIrr;
    @JsonProperty("project_is_aimed_to")
    public String projectProjectIsAimedTo;
    @JsonProperty("project_is_aimed_to_custom")
    public String projectProjectIsAimedToCustom;
    @JsonProperty("project_net_present_value")
    public String projectNetPresentValue;
    @JsonProperty("project_payback_time")
    public String projectPaybackTime;

    @JsonProperty("project_payback_time_double")
    public Integer projectProjectPaybackTimeDouble;

    @JsonProperty("project_provides_for_a_competition")
    public String projectProjectProvidesForACompetition;
    @JsonProperty("project_provides_for_a_competition_description")
    public String projectProjectProvidesForACompetitionDescription;
    @JsonProperty("project_sectors")
    public String projectSectors;
    @JsonProperty("project_sectors_custom")
    public String projectSectorsCustom;
    @JsonProperty("project_sectors_reason")
    public String projectSectorsReason;
    @JsonProperty("project_sectors1")
    public String projectSectors1;
    @JsonProperty("project_target")
    public String projectTarget;
    @JsonProperty("project_term_construction_phase")
    public String projectProjectTermConstructionPhase;
    @JsonProperty("project_term_operational_phase")
    public String projectTermOperationalPhase;
    @JsonProperty("project_type")
    public String projectType;
    @JsonProperty("project_type_custom")
    public String projectTypeCustom;
    @JsonProperty("project_type1")
    public String projectType1;
    @JsonProperty("project_type1_custom")
    public String projectType1Custom;
    @JsonProperty("project_upcoming_investments")
    public String projectProjectUpcomingInvestments;
    @JsonProperty("projects_and_conditions_for_their_financing")
    public String projectProjectsAndConditionsForTheirFinancing;
    @JsonProperty("qualitative_approaches_description")
    public String projectQualitativeApproachesDescription;
    @JsonProperty("readiness_of_project_documentation")
    public String projectReadinessOfProjectDocumentation;
    @JsonProperty("reasons_for_refusal_of_financing")
    public String projectReasonsForRefusalOfFinancing;
    @JsonProperty("recipient_available_resources_enum")
    public String projectRecipientAvailableResourcesEnum;
    @JsonProperty("return_on_bank_equity")
    public String projectReturnOnBankEquity;
    @JsonProperty("sale_place")
    public String projectSalePlace;
    @JsonProperty("sale_place_custom")
    public List<String> projectSalePlaceCustom;
    @JsonProperty("syndicate_conditions")
    public String projectSyndicateConditions;
    @JsonProperty("terms_delivery_payment_expected")
    public String projectTermsDeliveryPaymentExpected;
    @JsonProperty("type_of_authorization_document")
    public String projectTypeOfAuthorizationDocument;
    @JsonProperty("way_of_banks_withdrawal_from_project")
    public String projectWayOfBanksWithdrawalFromProject;
    @JsonProperty("which_financial_institutions")
    public String projectWhichFinancialInstitutions;
    @JsonProperty("why_banks_participation_not_damage_financial_organizations")
    public String projectWhyBanksParticipationNotDamageFinancialOrganizations;

    public String initiatorActualPlaceState;
    public String initiatorActualPlaceTerritory;
    public String initiatorActualPlaceCity;

    @JsonProperty("study_degree")
    public String projectDegree;

    @JsonDeserialize(using = BooleanDeserializer.class)
    public Boolean projectEnvironmentalExpertiseConclusionNotRequiredIs;
    @JsonDeserialize(using = BooleanDeserializer.class)
    public Boolean projectEnvironmentalExpertiseIsNotRequiredIs;


    public String getStatus() {
        if (initiator == null || initiator.getInitiatorOgrn() == null || initiator.getInitiatorOgrn().isEmpty())
            return "resume";
        if (expressValuating1 == null) return "express-assessment";
        return "finance-request";
//        "first-request"/"resume"/"express-assessment"/"finance-request"/"express-expertise"/"preliminary-expertise"/"complex-expertise"
    }
}
