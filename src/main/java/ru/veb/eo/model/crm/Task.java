package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class Task {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("date_entered")
    private Date date_entered;

    @JsonProperty("status")
    private String status;

}
