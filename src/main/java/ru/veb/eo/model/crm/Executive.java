package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import ru.veb.eo.serializer.BooleanDeserializer;
import ru.veb.eo.serializer.DateDeserializer;

@Data
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Executive {
    @JsonProperty("born_place_address")
    public Address executiveBornPlaceAddress;
    @JsonProperty("id")
    private String crmId;

    @JsonProperty("born_date")
    @JsonDeserialize(using = DateDeserializer.class)
    public DateForm executiveBornDate;
    @JsonProperty("born_place")
    public Address executiveBornPlace;
    @JsonProperty("citizenship")
    public String executiveCitizenship;
    @JsonProperty("control_company_delegate")
    @JsonDeserialize(using = BooleanDeserializer.class)
    public Boolean executiveControlCompanyDelegate;
    @JsonProperty("control_company_delegate_name")
    public String executiveControlCompanyDelegateName;
    @JsonProperty("control_company_delegate_type")
    public String executiveControlCompanyDelegateType;
    @JsonProperty("document_issue_date")
    @JsonDeserialize(using = DateDeserializer.class)
    public DateForm executiveIdIssueDate;
    @JsonProperty("document_issue_who")
    public String executiveIdIssueAgencyName;
    @JsonProperty("document_issue_who_code")
    public String executiveIdIssueAgencyCode;
    @JsonProperty("document_name")
    public String executiveIdName;
    @JsonProperty("document_number")
    public String executiveIdNumber;
    @JsonProperty("document_series")
    public String executiveIdSeries;
    @JsonProperty("first_name")
    public String executiveNameFirst;
    @JsonProperty("last_name")
    public String executiveNameLast;
    @JsonProperty("mid_name")
    public String executiveNameMid;
    @JsonProperty("migration_number")
    public String executiveMigrationNumber;
    @JsonProperty("migration_series")
    public String executiveMigrationSeries;
    @JsonProperty("migration_term_end_date")
    @JsonDeserialize(using = DateDeserializer.class)
    public DateForm executiveMigrationDateEnd;
    @JsonProperty("migration_term_start_date")
    @JsonDeserialize(using = DateDeserializer.class)
    public DateForm executiveMigrationDateStart;
    @JsonProperty("other_persons_activities_participation_description")
    public String executiveInfoActivitiesParticipation;
    @JsonProperty("position")
    public String executivePosition;
    @JsonProperty("registration_address")
    public Address executiveAddressRegistration;
    @JsonProperty("resident")
    @JsonDeserialize(using = BooleanDeserializer.class)
    public Boolean executiveResident;
}
