package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Document {
    @JsonProperty("id") public String id;
//    @JsonProperty("date_entered") public String dateEntered;
    @JsonProperty("description") public String description;
    @JsonProperty("document_name") public String name;
    @JsonProperty("doc_id") public String docId;
    @JsonProperty("doc_type") public String docType;
    @JsonProperty("doc_url") public String docUrl;
//    @JsonProperty("active_date") public String activeDate;
//    @JsonProperty("exp_date") public String expDate;
    @JsonProperty("category_id") public String categoryId;
    @JsonProperty("subcategory_id") public String subcategoryId;
    @JsonProperty("status_id") public String statusId;
    @JsonProperty("document_revision_id") public String documentRevisionId;
    @JsonProperty("related_doc_id") public String relatedDocId;
    @JsonProperty("related_doc_rev_id") public String relatedDocRevId;
    @JsonProperty("is_template") public Boolean isTemplate;
    @JsonProperty("template_type") public String templateType;
    @JsonProperty("project_id") public String projectId;
    @JsonProperty("code") public String code;
    @JsonProperty("document_number") public String documentNumber;
    @JsonProperty("document_extension") public String documentExtension;
    @JsonProperty("document_linc") public String documentLinc;
    @JsonProperty("sharepoint_id") public String sharepointId;
    @JsonProperty("sharepoint_error") public String sharepointError;
    @JsonProperty("sharepoint_name") public String sharepointName;
}
