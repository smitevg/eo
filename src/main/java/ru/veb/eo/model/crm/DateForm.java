package ru.veb.eo.model.crm;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateForm {

    public Integer day = null;
    public Integer month = null;
    public Integer year = null;

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public DateForm(Date date) {
        if (date != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            day = cal.get(Calendar.DAY_OF_MONTH);
            month = cal.get(Calendar.MONTH) + 1;
            year = cal.get(Calendar.YEAR);
        }
    }

    public Date getDate() {
        if (day != null && month != null && year != null) {
            Calendar cal = GregorianCalendar.getInstance();
            cal.set(year, month - 1, day);
//            cal.set(1900 + year, month, day);
            return cal.getTime();
        } else {
            return null;
        }
    }

    public void setDate(Date date) {
        if (date != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            day = cal.get(Calendar.DAY_OF_MONTH);
            month = cal.get(Calendar.MONTH);
//            year = cal.get(Calendar.YEAR) - 1900;
            year = cal.get(Calendar.YEAR);
        }
    }

    public DateForm() {
    }

    public DateForm(Integer day, Integer month, Integer year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    @Override
    public String toString() {
        if (day != null && month != null && year != null)
            return String.format("%04d", year) + "-" + String.format("%02d", month) + "-" + String.format("%02d", day);
        else
            return null;
    }


    public String toFormatString() {
        if (day != null && month != null && year != null)
            return String.format("%02d", day) + "." + String.format("%02d", month) + "." + String.format("%04d", year);
        else
            return null;
    }
}
