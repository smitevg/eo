package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
public class DocumentAdd {
    @JsonProperty("id")
    public String id;

    @JsonProperty("name")
    public String name;

    @JsonProperty("sharepoint_url")
    public String sharepointUrl;

    public DocumentAdd(String name, String sharepointUrl) {
        this.name = name;
        this.sharepointUrl = sharepointUrl;
    }
}
