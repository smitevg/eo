package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Address {
    //    @JsonProperty("date_entered")
//    public Date dateEntered;
    @JsonProperty("id")
    private String id;
    public String name;
    public String description;
    public String country;
    public String region;
    public String city;
    public String street;
    public String house;
    @JsonProperty("house_corps")
    public String building;
    public String apartment;

    @JsonProperty("postal_index")
    public String index;


    public void setId(String id) {
        this.id = id.replaceAll("\\{\\{[^\\}]+\\}\\}", "");
    }

    public void setName(String name) {
        this.name = name.replaceAll("\\{\\{[^\\}]+\\}\\}", "");
    }

    public void setDescription(String description) {
        this.description = description.replaceAll("\\{\\{[^\\}]+\\}\\}", "");
    }

    public void setCountry(String country) {
        this.country = country.replaceAll("\\{\\{[^\\}]+\\}\\}", "");
    }

    public void setRegion(String region) {
        this.region = region.replaceAll("\\{\\{[^\\}]+\\}\\}", "");
    }

    public void setCity(String city) {
        this.city = city.replaceAll("\\{\\{[^\\}]+\\}\\}", "");
    }

    public void setStreet(String street) {
        this.street = street.replaceAll("\\{\\{[^\\}]+\\}\\}", "");
    }

    public void setHouse(String house) {
        this.house = house.replaceAll("\\{\\{[^\\}]+\\}\\}", "");
    }

    public void setBuilding(String building) {
        this.building = building.replaceAll("\\{\\{[^\\}]+\\}\\}", "");
    }

    public void setApartment(String apartment) {
        this.apartment = apartment.replaceAll("\\{\\{[^\\}]+\\}\\}", "");
    }
}
