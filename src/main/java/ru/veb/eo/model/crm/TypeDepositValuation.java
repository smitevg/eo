package ru.veb.eo.model.crm;

public enum TypeDepositValuation {
    YOURSELF(0), COMPANY(1);

    private final int code;

    private TypeDepositValuation(int code) {
        this.code = code;
    }

    public int toInt() {
        return code;
    }

    @Override
    public String toString() {
        return String.valueOf(code);
    }
}

