package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.util.List;

@Data
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Collaborator {
    @JsonProperty("id")
    private String crmId;
    @JsonProperty("emails")
    public List<String> employeeEmails;
    @JsonProperty("first_name")
    public String employeeNameFirst;
    @JsonProperty("last_name")
    public String employeeNameLast;
    @JsonProperty("mid_name")
    public String employeeNameMid;
    @JsonProperty("position")
    public String employeePosition;

    @JsonProperty("photo")
    public String employeeImage;


    @JsonProperty("phone_numbers")
    private String employeePhoneNumbers;
}
