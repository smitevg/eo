package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DocumentSP {
    @JsonProperty("id")
    public String id;
    @JsonProperty("sharepoint_title")
    public String title;
    @JsonProperty("name")
    public String name;
    @JsonProperty("security_type")
    public String securityType;
    @JsonProperty("syndicate_layer_one")
    public String syndicateLayerOne;
    @JsonProperty("syndicate_layer_two")
    public String syndicateLayerTwo;
    @JsonProperty("fabric_layer_one")
    public String fabricLayerOne;
    @JsonProperty("fabric_layer_two")
    public String fabricLayerTwo;

}
