package ru.veb.eo.model.crm;

import java.util.ArrayList;
import java.util.List;

public class Assessment {
    public int goodAssessment;
    public int badAssessment;
    public List<String> badFields = new ArrayList<>();

    public int result() {
        if (goodAssessment + badAssessment > 0)
            return 100 * goodAssessment / (goodAssessment + badAssessment);
        else
            return 0;
    }

    public int getResult() {
        if (goodAssessment + badAssessment > 0)
            return 100 * goodAssessment / (goodAssessment + badAssessment);
        else
            return 0;
    }
}
