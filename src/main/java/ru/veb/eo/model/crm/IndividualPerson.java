package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import ru.veb.eo.serializer.BooleanDeserializer;
import ru.veb.eo.serializer.BooleanSerializer;
import ru.veb.eo.serializer.DateDeserializer;
import ru.veb.eo.serializer.DateSerializer;

import java.util.List;

@Data
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class IndividualPerson {
    @JsonProperty("id")
    private String id;
    @JsonProperty("emails")
    private List<String> emails;
    @JsonProperty("phone_numbers")
    private String phoneNumbers;
    @JsonProperty("born_date")
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using= DateDeserializer.class)
    private DateForm bornDate;
    @JsonProperty("born_place_address")
    private Address bornPlace;
    @JsonProperty("citizenship")
    private String citizenship;
    @JsonProperty("control_company_delegate")
    @JsonSerialize(using = BooleanSerializer.class)
    private Boolean controlCompanyDelegate;
    @JsonProperty("control_company_delegate_name")
    private String controlCompanyDelegateName;
    @JsonProperty("document_issue_date")
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using= DateDeserializer.class)
    private DateForm idIssueDate;
    @JsonProperty("document_issue_who")
    private String idIssueAgencyName;
    @JsonProperty("document_issue_who_code")
    private String idIssueAgencyCode;
    @JsonProperty("document_name")
    private String idName;
    @JsonProperty("document_number")
    private String idNumber;
    @JsonProperty("document_series")
    private String idSeries;
    @JsonProperty("first_name")
    private String nameFirst;
    @JsonProperty("last_name")
    private String nameLast;
    @JsonProperty("mid_name")
    private String nameMid;
    @JsonProperty("migration_number")
    private String migrationNumber;
    @JsonProperty("migration_series")
    private String migrationSeries;
    @JsonProperty("migration_term_end_date")
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using= DateDeserializer.class)
    private DateForm migrationDateEnd;
    @JsonProperty("migration_term_start_date")
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using= DateDeserializer.class)
    private DateForm migrationDateStart;
    @JsonProperty("other_persons_activities_participation_description")
    private String infoActivitiesParticipation;
    @JsonProperty("position")
    private String position;
    @JsonProperty("registration_address")
    private Address addressRegistration;
    @JsonProperty("resident")
    @JsonSerialize(using = BooleanSerializer.class)
    @JsonDeserialize(using = BooleanDeserializer.class)
    private Boolean resident;
    @JsonProperty("enclosed_funds")
    public Double volume;
    @JsonProperty("member_status")
    public String status;
    @JsonProperty("investment_terms")
    public String rules;
}
