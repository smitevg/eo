package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import ru.veb.eo.serializer.BooleanDeserializer;
import ru.veb.eo.serializer.BooleanSerializer;

@Data
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LegalEntityDataTransfer extends ParticipantDataTransfer{
    public String id;
    public String organizationType;
    public String organizationTitle;
    public Boolean resident;
    public String ogrn;
    public String inn;
    public String kpp;
    public String nalogNumber;
    public String regNumber;

    public LegalEntityDataTransfer(Participant legalEntity) {
        BeanUtils.copyProperties(legalEntity, this);
    }
}
