package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Strings;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.thymeleaf.util.ArrayUtils;
import org.thymeleaf.util.ListUtils;
import ru.veb.eo.serializer.BooleanSerializer;
import ru.veb.eo.serializer.DateSerializer;
import ru.veb.eo.serializer.StateSupportSerializer;

import java.util.*;
import java.util.stream.Collectors;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectRequest {
    @JsonIgnore
    public String projectCode;

    @JsonIgnore
    public String projectId;

    public Assessment checkEmpty(Object object, Assessment assessment, String fieldName) {

        int badAssessment = assessment.badAssessment;

        if (object instanceof String) {
            if (Strings.isNullOrEmpty((String) object)) assessment.badAssessment++;
            else assessment.goodAssessment++;
        } else if (object instanceof List) {
            if (ListUtils.isEmpty((List) object)) assessment.badAssessment++;
            else assessment.goodAssessment++;
        } else if (object instanceof String[]) {
            if (ArrayUtils.isEmpty((String[]) object)) assessment.badAssessment++;
            else assessment.goodAssessment++;
        } else if (object instanceof Address) {
            Address address = (Address) object;
            if (address == null) assessment.badAssessment++;
            else {
                if (
                        checkEmptyAndTrue(address.name) ||
                                checkEmptyAndTrue(address.country) ||
                                checkEmptyAndTrue(address.region) ||
                                checkEmptyAndTrue(address.street) ||
                                checkEmptyAndTrue(address.house) ||
                                checkEmptyAndTrue(address.building) ||
                                checkEmptyAndTrue(address.apartment))
                    assessment.goodAssessment++;
                else
                    assessment.badAssessment++;
            }
        } else if (object instanceof Participant) {
            Participant participant = (Participant) object;
            if (participant == null) assessment.badAssessment++;
            else {
                if ((checkEmptyAndTrue(participant.inn) || checkEmptyAndTrue(participant.nalogNumber)) &&
                        (checkEmptyAndTrue(participant.ogrn) || checkEmptyAndTrue(participant.regNumber)) &&
                        checkEmptyAndTrue(participant.status) &&
                        checkEmptyAndTrue(participant.rules) &&
                        checkEmptyAndTrue(participant.organizationTitle) &&
                        checkEmptyAndTrue(participant.organizationType) &&
                        checkEmptyAndTrue(participant.volume))
                    assessment.goodAssessment++;
                else
                    assessment.badAssessment++;
            }
        } else if (object instanceof DateForm) {
            DateForm date = (DateForm) object;
            if (date == null) assessment.badAssessment++;
            else {
                if (
                        checkEmptyAndTrue(date.day) ||
                                checkEmptyAndTrue(date.month) ||
                                checkEmptyAndTrue(date.year))
                    assessment.goodAssessment++;
                else
                    assessment.badAssessment++;
            }
        } else {
            if (object == null) assessment.badAssessment++;
            else assessment.goodAssessment++;
        }

        if (badAssessment != assessment.badAssessment) {
            assessment.badFields.add(fieldName);
        }

        return assessment;
    }

    // if object instanceof Boolean then return object
    public boolean checkEmptyAndTrue(Object object) {
        if (object instanceof String) {
            if (Strings.isNullOrEmpty((String) object)) return false;
            else return true;
        } else if (object instanceof List) {
            if (ListUtils.isEmpty((List) object)) return false;
            else return true;
        } else if (object instanceof String[]) {
            if (ArrayUtils.isEmpty((String[]) object)) return false;
            else return true;
        } else if (object instanceof Boolean) {
            if (object == null) return false;
            else return (boolean) object;
        } else {
            if (object == null) return false;
            else return true;
        }
    }

    public Assessment calcResume1() {
        Assessment assessment = new Assessment();

        // resume 1
        assessment = checkEmpty(initiatorOrganizationalLegalForm, assessment, "initiatorOrganizationalLegalForm");
        assessment = checkEmpty(initiatorNameRus, assessment, "initiatorNameRus");
        assessment = checkEmpty(initiatorOgrn, assessment, "initiatorOgrn");
        assessment = checkEmpty(initiatorInn, assessment, "initiatorInn");
        assessment = checkEmpty(initiatorAddressRegistration, assessment, "initiatorAddressRegistration");
        assessment = checkEmpty(initiatorMainActivityAddress, assessment, "initiatorMainActivityAddress");
        assessment = checkEmpty(initiatorActivity, assessment, "initiatorActivity");
        assessment = checkEmpty(initiatorActivityTerm, assessment, "initiatorActivityTerm");
        assessment = checkEmpty(initiatorEmployeesNumber, assessment, "initiatorEmployeesNumber");
        assessment = checkEmpty(initiatorOwnersChain, assessment, "initiatorOwnersChain");

        // resume 2
        assessment = checkEmpty(executiveControlCompanyDelegate, assessment, "executiveControlCompanyDelegate");
        if (checkEmptyAndTrue(executiveControlCompanyDelegate)) {
            assessment = checkEmpty(executiveControlCompanyDelegateName, assessment, "executiveControlCompanyDelegateName");
            assessment = checkEmpty(executiveControlCompanyDelegateType, assessment, "executiveControlCompanyDelegateType");
        }

        assessment = checkEmpty(executiveNameLast, assessment, "executiveNameLast");
        assessment = checkEmpty(executiveNameFirst, assessment, "executiveNameFirst");
        assessment = checkEmpty(executiveNameMid, assessment, "executiveNameMid");
        assessment = checkEmpty(executiveBornDate, assessment, "executiveBornDate");
        assessment = checkEmpty(executiveBornPlaceAddress, assessment, "executiveBornPlaceAddress");
        assessment = checkEmpty(executiveCitizenship, assessment, "executiveCitizenship");
        assessment = checkEmpty(executiveAddressRegistration, assessment, "executiveAddressRegistration");
        assessment = checkEmpty(executiveIdName, assessment, "executiveIdName");
        assessment = checkEmpty(executiveIdSeries, assessment, "executiveIdSeries");
        assessment = checkEmpty(executiveIdNumber, assessment, "executiveIdNumber");
        assessment = checkEmpty(executiveIdIssueDate, assessment, "executiveIdIssueDate");
        assessment = checkEmpty(executiveIdIssueAgencyName, assessment, "executiveIdIssueAgencyName");
        assessment = checkEmpty(executiveIdIssueAgencyCode, assessment, "executiveIdIssueAgencyCode");
        assessment = checkEmpty(executiveResident, assessment, "executiveResident");
        if (!checkEmptyAndTrue(executiveResident)) {
            assessment = checkEmpty(executiveMigrationSeries, assessment, "executiveMigrationSeries");
            assessment = checkEmpty(executiveMigrationNumber, assessment, "executiveMigrationNumber");
            assessment = checkEmpty(executiveMigrationDateStart, assessment, "executiveMigrationDateStart");
            assessment = checkEmpty(executiveMigrationDateEnd, assessment, "executiveMigrationDateEnd");
        }

        // resume 3
        assessment = checkEmpty(applicantNameLast, assessment, "applicantNameLast");
        assessment = checkEmpty(applicantNameFirst, assessment, "applicantNameFirst");
        assessment = checkEmpty(applicantNameMid, assessment, "applicantNameMid");
        assessment = checkEmpty(employeePosition, assessment, "employeePosition");
        assessment = checkEmpty(employeePhoneNumber, assessment, "employeePhoneNumber");
        assessment = checkEmpty(employeeFaxNumber, assessment, "employeeFaxNumber");
        assessment = checkEmpty(applicantEmail, assessment, "applicantEmail");

        // resume 4
        assessment = checkEmpty(initiatorPhoneNumber, assessment, "initiatorPhoneNumber");
        assessment = checkEmpty(initiatorFaxNumber, assessment, "initiatorFaxNumber");
        assessment = checkEmpty(initiatorEmail, assessment, "initiatorEmail");
        assessment = checkEmpty(initiatorPostalAddress, assessment, "initiatorPostalAddress");

        return assessment;
    }

    public Assessment calcResume2() {
        Assessment assessment = new Assessment();
        // resume 5
        assessment = checkEmpty(projectNomination, assessment, "projectNomination");
        assessment = checkEmpty(projectTarget, assessment, "projectTarget");
        assessment = checkEmpty(projectExpectedResult, assessment, "projectExpectedResult");
        assessment = checkEmpty(projectType1, assessment, "projectType1");
//        assessment = checkEmpty(projectType1Custom, assessment, "projectType1Custom");
        assessment = checkEmpty(projectDescription, assessment, "projectDescription");
        assessment = checkEmpty(projectSalePlace, assessment, "projectSalePlace");
//        assessment = checkEmpty(projectSalePlaceCustom, assessment, "projectSalePlaceCustom");
        assessment = checkEmpty(projectExperienceProjectManagers, assessment, "projectExperienceProjectManagers");
        assessment = checkEmpty(projectDegree, assessment, "projectDegree");

        // resume 6
        assessment = checkEmpty(projectProjectIsAimedTo, assessment, "projectProjectIsAimedTo");
        assessment = checkEmpty(projectSectors1, assessment, "projectSectors1");
        assessment = checkEmpty(projectEffectEmployment, assessment, "projectEffectEmployment");
        assessment = checkEmpty(projectEnvironmentalProtection, assessment, "projectEnvironmentalProtection");
        assessment = checkEmpty(projectTechnologiesImplementation, assessment, "projectTechnologiesImplementation");
        assessment = checkEmpty(projectTechnologicalOpportunities, assessment, "projectTechnologicalOpportunities");
        if (projectStateSupport != null) {
            if (projectStateSupport.contains(0))
                assessment = checkEmpty(projectImpactInfoAvailabilityOfStateSupportComment0, assessment, "projectImpactInfoAvailabilityOfStateSupportComment0");

            if (projectStateSupport.contains(1))
                assessment = checkEmpty(projectImpactInfoAvailabilityOfStateSupportComment1, assessment, "projectImpactInfoAvailabilityOfStateSupportComment1");

            if (projectStateSupport.contains(2))
                assessment = checkEmpty(projectImpactInfoAvailabilityOfStateSupportComment2, assessment, "projectImpactInfoAvailabilityOfStateSupportComment2");

        }

        // resume 7
        assessment = checkEmpty(projectProjectCostDouble, assessment, "projectProjectCostDouble");
        assessment = checkEmpty(projectCostCurrency, assessment, "projectCostCurrency");
        assessment = checkEmpty(projectProjectPaybackTimeDouble, assessment, "projectProjectPaybackTimeDouble");
//        assessment = checkEmpty(projectAmountFinancingRequest, assessment, "projectAmountFinancingRequest"); // дубль projectAmountBanksCash
        assessment = checkEmpty(participants, assessment, "participants");
        assessment = checkEmpty(projectAmountRecipientsCash, assessment, "projectAmountRecipientsCash");
        assessment = checkEmpty(projectAmountRecipientsPart, assessment, "projectAmountRecipientsPart");
        assessment = checkEmpty(projectAmountBanksCash, assessment, "projectAmountBanksCash");
        assessment = checkEmpty(projectAmountBanksPart, assessment, "projectAmountBanksPart");
//        assessment = checkEmpty(projectAmountInvestorCash, assessment, "projectAmountInvestorCash");
//        assessment = checkEmpty(projectAmountInvestorPart, assessment, "projectAmountInvestorPart");
//        assessment = checkEmpty(projectAmountBondCash, assessment, "projectAmountBondCash");
//        assessment = checkEmpty(projectAmountBondPart, assessment, "projectAmountBondPart");
//        assessment = checkEmpty(projectAmountOthersCash, assessment, "projectAmountOthersCash");
//        assessment = checkEmpty(projectAmountOthersPart, assessment, "projectAmountOthersPart");
//        if (checkEmptyAndTrue(projectAmountOthersCash))
//            assessment = checkEmpty(projectFinancingSourcesOther, assessment, "projectFinancingSourcesOther");
        //    assessment = checkEmpty(projectAssuranceType, assessment, "projectAssuranceType");
        //    assessment = checkEmpty(projectAssuranceGuarantor, assessment, "projectAssuranceGuarantor");
        //    assessment = checkEmpty(projectAssuranceValueCollateral, assessment, "projectAssuranceValueCollateral");
        assessment = checkEmpty(projectNetPresentValue, assessment, "projectNetPresentValue");
        assessment = checkEmpty(projectAdditionalInfo, assessment, "projectAdditionalInfo");

        return assessment;
    }

    public Assessment calcInitiator() {
        Assessment assessment = new Assessment();

        assessment = checkEmpty(initiatorFaceRoleAtProject, assessment, "initiatorFaceRoleAtProject");
        assessment = checkEmpty(initiatorResident, assessment, "initiatorResident");
        assessment = checkEmpty(initiatorFaceType, assessment, "initiatorFaceType");

//        0: 'Юридическое лицо',
//        1: 'Юридическое лицо - нерезидент РФ',
//        2: 'Иностранная структура без образования юридического лица',
//        3: 'Физическое лицо',
//        4: 'Индивидуальный предприниматель',
//        5: 'Физическое лицо, занимающееся частной практикой в установленном законодательством Российской Федерации порядке'

        // юр. лицо
        if ("0".equals(initiatorFaceType) || "1".equals(initiatorFaceType)) {
            assessment = checkEmpty(initiatorOrganizationalLegalForm, assessment, "initiatorOrganizationalLegalForm");
            assessment = checkEmpty(initiatorNameRus, assessment, "initiatorNameRus");
            assessment = checkEmpty(initiatorFirmNameRusShort, assessment, "initiatorFirmNameRusShort");
            assessment = checkEmpty(initiatorFirmNameForeign, assessment, "initiatorFirmNameForeign");
            assessment = checkEmpty(initiatorFirmNameForeignShort, assessment, "initiatorFirmNameForeignShort");
            if (checkEmptyAndTrue(initiatorResident)) {
                assessment = checkEmpty(initiatorInn, assessment, "initiatorInn");
                assessment = checkEmpty(initiatorOgrn, assessment, "initiatorOgrn");
            } else {
                assessment = checkEmpty(initiatorKio, assessment, "initiatorKio");
                assessment = checkEmpty(initiatorRegistrationNumber, assessment, "initiatorRegistrationNumber");
            }
            assessment = checkEmpty(initiatorAddressRegistration, assessment, "initiatorAddressRegistration");
            assessment = checkEmpty(initiatorAddressFactual, assessment, "initiatorAddressFactual");
            assessment = checkEmpty(initiatorAddressFactualConfirm, assessment, "initiatorAddressFactualConfirm");
            assessment = checkEmpty(initiatorMainActivityAddress, assessment, "initiatorMainActivityAddress");
            assessment = checkEmpty(initiatorActivity, assessment, "initiatorActivity");
            assessment = checkEmpty(initiatorActivityTerm, assessment, "initiatorActivityTerm");
            assessment = checkEmpty(initiatorEmployeesNumber, assessment, "initiatorEmployeesNumber");
        }
        if ("2".equals(initiatorFaceType) || "3".equals(initiatorFaceType) || "4".equals(initiatorFaceType) || "5".equals(initiatorFaceType)) {
//            assessment = checkEmpty(initiatorLastName, assessment, "initiatorLastName");
//            assessment = checkEmpty(initiatorFirstName, assessment, "initiatorFirstName");
//            assessment = checkEmpty(initiatorMidName, assessment, "initiatorMidName");
//            assessment = checkEmpty(initiatorBornDate, assessment, "initiatorBornDate");
//            assessment = checkEmpty(initiatorBornPlace, assessment, "initiatorBornPlace");
//            assessment = checkEmpty(initiatorCitizenship, assessment, "initiatorCitizenship");
//            assessment = checkEmpty(initiatorAddressRegistration, assessment, "initiatorAddressRegistration");
//            assessment = checkEmpty(initiatorDocumentName, assessment, "initiatorDocumentName");
//            assessment = checkEmpty(initiatorDocumentSeries, assessment, "initiatorDocumentSeries");
//            assessment = checkEmpty(initiatorDocumentNumber, assessment, "initiatorDocumentNumber");
//            assessment = checkEmpty(initiatorDocumentIssueDate, assessment, "initiatorDocumentIssueDate");
//            assessment = checkEmpty(initiatorDocumentIssueWho, assessment, "initiatorDocumentIssueWho");
//            assessment = checkEmpty(initiatorDocumentIssueWhoCode, assessment, "initiatorDocumentIssueWhoCode");
//            assessment = checkEmpty(initiatorStateRegistrationDate, assessment, "initiatorStateRegistrationDate");
        }

        // ИП, частная практика
        if ("4".equals(initiatorFaceType) || "5".equals(initiatorFaceType)) {
            assessment = checkEmpty(initiatorOgrn, assessment, "initiatorOgrn");
            assessment = checkEmpty(initiatorStateRegistrationDate, assessment, "initiatorStateRegistrationDate");
            assessment = checkEmpty(initiatorAddressRegistration, assessment, "initiatorAddressRegistration");
        }
        // иностранное лицо
        if ("1".equals(initiatorFaceType) || "2".equals(initiatorFaceType)) {
//            assessment = checkEmpty(initiatorMigrationSeries, assessment, "initiatorMigrationSeries");
//            assessment = checkEmpty(initiatorMigrationNumber, assessment, "initiatorMigrationNumber");
//            assessment = checkEmpty(initiatorMigrationTermStartDate, assessment, "initiatorMigrationTermStartDate");
//            assessment = checkEmpty(initiatorMigrationTermEndDate, assessment, "initiatorMigrationTermEndDate");
        }

//        assessment = checkEmpty(initiatorOtherPersonsActivitiesParticipationDescription, assessment, "initiatorOtherPersonsActivitiesParticipationDescription");


        // page 2
        assessment = checkEmpty(executiveControlCompanyDelegate, assessment, "executiveControlCompanyDelegate");
        if (checkEmptyAndTrue(executiveControlCompanyDelegate)) {
//            assessment = checkEmpty(executiveControlCompanyDelegateType, assessment, "executiveControlCompanyDelegateType");
            assessment = checkEmpty(executiveControlCompanyDelegateName, assessment, "executiveControlCompanyDelegateName");
        }
        assessment = checkEmpty(executiveNameLast, assessment, "executiveNameLast");
        assessment = checkEmpty(executiveNameFirst, assessment, "executiveNameFirst");
        assessment = checkEmpty(executiveNameMid, assessment, "executiveNameMid");
        assessment = checkEmpty(executivePosition, assessment, "executivePosition");
        assessment = checkEmpty(executiveBornDate, assessment, "executiveBornDate");
//        assessment = checkEmpty(executiveBornPlace, assessment, "executiveBornPlace");
        assessment = checkEmpty(executiveCitizenship, assessment, "executiveCitizenship");
        assessment = checkEmpty(executiveAddressRegistration, assessment, "executiveAddressRegistration");
        assessment = checkEmpty(executiveIdName, assessment, "executiveIdName");
        assessment = checkEmpty(executiveIdSeries, assessment, "executiveIdSeries");
        assessment = checkEmpty(executiveIdNumber, assessment, "executiveIdNumber");
        assessment = checkEmpty(executiveIdIssueDate, assessment, "executiveIdIssueDate");
        assessment = checkEmpty(executiveIdIssueAgencyName, assessment, "executiveIdIssueAgencyName");
        assessment = checkEmpty(executiveIdIssueAgencyCode, assessment, "executiveIdIssueAgencyCode");
        assessment = checkEmpty(executiveInfoActivitiesParticipation, assessment, "executiveInfoActivitiesParticipation");

        assessment = checkEmpty(employeeNameLast, assessment, "employeeNameLast");
        assessment = checkEmpty(employeeNameFirst, assessment, "employeeNameFirst");
        assessment = checkEmpty(employeeNameMid, assessment, "employeeNameMid");
//        assessment = checkEmpty(employeePosition, assessment, "employeePosition");
        assessment = checkEmpty(employeePhoneNumber, assessment, "employeePhoneNumber");
        assessment = checkEmpty(employeeFaxNumber, assessment, "employeeFaxNumber");
        assessment = checkEmpty(employeeEmail, assessment, "employeeEmail");

        assessment = checkEmpty(initiatorPhoneNumber, assessment, "initiatorPhoneNumber");
        assessment = checkEmpty(initiatorFaxNumber, assessment, "initiatorFaxNumber");
        assessment = checkEmpty(initiatorEmail, assessment, "initiatorEmail");
        assessment = checkEmpty(initiatorPostalAddress, assessment, "initiatorPostalAddress");
        assessment = checkEmpty(initiatorRequisitesReceiver, assessment, "initiatorRequisitesReceiver");
        assessment = checkEmpty(initiatorRequisitesReceiversAccount, assessment, "initiatorRequisitesReceiversAccount");
        assessment = checkEmpty(initiatorRequisitesBankName, assessment, "initiatorRequisitesBankName");
//        assessment = checkEmpty(initiatorRequisitesBankPlace, assessment, "initiatorRequisitesBankPlace");
        assessment = checkEmpty(initiatorRequisitesBankCorrAccount, assessment, "initiatorRequisitesBankCorrAccount");
        assessment = checkEmpty(initiatorRequisitesBankBik, assessment, "initiatorRequisitesBankBik");
        assessment = checkEmpty(initiatorDissatisfiedClaims, assessment, "initiatorDissatisfiedClaims");
        assessment = checkEmpty(initiatorTaxArrearsInfo, assessment, "initiatorTaxArrearsInfo");

//        public String initiatorRequisitesForeignCurrency;
//        public String initiatorRequisitesForeignReceiver;
//        public String initiatorRequisitesForeignReceiverAccount;
//        public String initiatorRequisitesForeignBankName;
//        public String initiatorRequisitesForeignBankPlace;
//        public String initiatorRequisitesForeignBankSwift;
//        public String initiatorRequisitesForeignBankCode;


        return assessment;
    }

    public Assessment calcProjectMain() {
        Assessment assessment = new Assessment();

        assessment = checkEmpty(projectNomination, assessment, "projectNomination");
        assessment = checkEmpty(projectTarget, assessment, "projectTarget");
        assessment = checkEmpty(projectExpectedResult, assessment, "projectExpectedResult");
        assessment = checkEmpty(projectDescription, assessment, "projectDescription");
        assessment = checkEmpty(projectSalePlace, assessment, "projectSalePlace");
//        assessment = checkEmpty(projectSalePlaceCustom, assessment, "projectSalePlaceCustom");
        assessment = checkEmpty(projectExperienceProjectManagers, assessment, "projectExperienceProjectManagers");
        assessment = checkEmpty(projectProjectDecisionDateAndLevel, assessment, "projectProjectDecisionDateAndLevel");
        assessment = checkEmpty(projectReadinessOfProjectDocumentation, assessment, "projectReadinessOfProjectDocumentation");
        assessment = checkEmpty(projectProjectImplementationActivities, assessment, "projectProjectImplementationActivities");

        return assessment;
    }

    public Assessment calcDirectionIndustry() {
        Assessment assessment = new Assessment();

        assessment = checkEmpty(projectProjectIsAimedTo, assessment, "projectProjectIsAimedTo");
        if (checkEmptyAndTrue(projectProjectIsAimedTo) && "5".equals(projectProjectIsAimedTo)) {
            assessment = checkEmpty(projectProjectIsAimedToCustom, assessment, "projectProjectIsAimedToCustom");
        }
        assessment = checkEmpty(projectProjectAimReason, assessment, "projectProjectAimReason");
        assessment = checkEmpty(projectProjectAdvantagesInMarket, assessment, "projectProjectAdvantagesInMarket");
        assessment = checkEmpty(projectSectors1, assessment, "projectSectors1");
        if (checkEmptyAndTrue(projectSectors1) && "53".equals(projectSectors1)) {
            assessment = checkEmpty(projectSectorsCustom, assessment, "projectSectorsCustom");
        }

        assessment = checkEmpty(projectSectorsReason, assessment, "projectSectorsReason");

        return assessment;
    }

    public Assessment calcInfluence() {
        Assessment assessment = new Assessment();

        assessment = checkEmpty(projectImplementationCorrespondsToPriorities, assessment, "projectImplementationCorrespondsToPriorities");
        if (checkEmptyAndTrue(projectImplementationCorrespondsToPriorities))
            assessment = checkEmpty(projectProjectCorrespondenceSectors, assessment, "projectProjectCorrespondenceSectors");

        assessment = checkEmpty(projectProjectImplementationPublicPrivatePartnership, assessment, "projectProjectImplementationPublicPrivatePartnership");
        if (checkEmptyAndTrue(projectProjectImplementationPublicPrivatePartnership))
            assessment = checkEmpty(projectAgreementBasicParameters, assessment, "projectAgreementBasicParameters");

        assessment = checkEmpty(projectProjectFinancingExpenseInvestmentFund, assessment, "projectProjectFinancingExpenseInvestmentFund");
        if (checkEmptyAndTrue(projectProjectFinancingExpenseInvestmentFund)) {
            assessment = checkEmpty(projectFinancingBasicTerms, assessment, "projectFinancingBasicTerms");
//            assessment = checkEmpty(projectConfirmingBudgetaryEffect, assessment, "projectConfirmingBudgetaryEffect");
        }

        assessment = checkEmpty(projectEffectEmployment, assessment, "projectEffectEmployment");
        assessment = checkEmpty(projectEvaluationMethod, assessment, "projectEvaluationMethod");
        assessment = checkEmpty(projectCalculationFormula, assessment, "projectCalculationFormula");
        assessment = checkEmpty(projectQualitativeApproachesDescription, assessment, "projectQualitativeApproachesDescription");
        assessment = checkEmpty(projectCalculatingEffectComponents, assessment, "projectCalculatingEffectComponents");


        assessment = checkEmpty(projectHaveFinancingAtRfBefore, assessment, "projectHaveFinancingAtRfBefore");
        if ("true".equals(projectHaveFinancingAtRfBefore))
            assessment = checkEmpty(projectProjectsAndConditionsForTheirFinancing, assessment, "projectProjectsAndConditionsForTheirFinancing");

        assessment = checkEmpty(projectCanProjectFinancedByFinancialInstitutions, assessment, "projectCanProjectFinancedByFinancialInstitutions");
        if ("false".equals(projectCanProjectFinancedByFinancialInstitutions))
            assessment = checkEmpty(projectReasonsForRefusalOfFinancing, assessment, "projectReasonsForRefusalOfFinancing");

        assessment = checkEmpty(projectFinancialInstitutionsReadyToFinanceTheProject, assessment, "projectFinancialInstitutionsReadyToFinanceTheProject");
        if ("true".equals(projectFinancialInstitutionsReadyToFinanceTheProject)) {
            assessment = checkEmpty(projectWhichFinancialInstitutions, assessment, "projectWhichFinancialInstitutions");
            assessment = checkEmpty(projectFinancialInstitutionsParticipationTermsInProject, assessment, "projectFinancialInstitutionsParticipationTermsInProject");
        }
        assessment = checkEmpty(projectWhyBanksParticipationNotDamageFinancialOrganizations, assessment, "projectWhyBanksParticipationNotDamageFinancialOrganizations");
//        assessment = checkEmpty(projectEnvironmentalProtection, assessment, "projectEnvironmentalProtection");

        assessment = checkEmpty(projectTypeOfAuthorizationDocument, assessment, "projectTypeOfAuthorizationDocument");
//        assessment = checkEmpty(projectAuthorizationDocumentIssuedBy, assessment, "projectAuthorizationDocumentIssuedBy");
//        assessment = checkEmpty(projectAuthorizationDocumentIssuedDate, assessment, "projectAuthorizationDocumentIssuedDate");

//        assessment = checkEmpty(projectEnvironmentalExpertiseIssuedBy, assessment, "projectEnvironmentalExpertiseIssuedBy");
//        assessment = checkEmpty(projectEnvironmentalExpertiseIssuedDate, assessment, "projectEnvironmentalExpertiseIssuedDate");


//        assessment = checkEmpty(projectEnvironmentalExpertiseConclusionNotRequired, assessment, "projectEnvironmentalExpertiseConclusionNotRequired");
//        if (checkEmptyAndTrue(projectEnvironmentalExpertiseConclusionNotRequired)) {
//            assessment = checkEmpty(projectEnvironmentalExpertiseIsNotRequired, assessment, "projectEnvironmentalExpertiseIsNotRequired");
//        }
//        assessment = checkEmpty(projectEnvironmentalExpertiseConclusionNotRequiredIs, assessment, "projectEnvironmentalExpertiseConclusionNotRequiredIs");
//        if (checkEmptyAndTrue(projectEnvironmentalExpertiseConclusionNotRequiredIs)) {
//            assessment = checkEmpty(projectEnvironmentalExpertiseIsNotRequiredIs, assessment, "projectEnvironmentalExpertiseIsNotRequiredIs");
//        }

        assessment = checkEmpty(projectTechnologiesImplementation, assessment, "projectTechnologiesImplementation");
        assessment = checkEmpty(projectTechnologicalOpportunities, assessment, "projectTechnologicalOpportunities");

        if (projectStateSupport != null) {
            if (projectStateSupport.contains(0))
                assessment = checkEmpty(projectImpactInfoAvailabilityOfStateSupportComment0, assessment, "projectImpactInfoAvailabilityOfStateSupportComment0");

            if (projectStateSupport.contains(1))
                assessment = checkEmpty(projectImpactInfoAvailabilityOfStateSupportComment1, assessment, "projectImpactInfoAvailabilityOfStateSupportComment1");

            if (projectStateSupport.contains(2))
                assessment = checkEmpty(projectImpactInfoAvailabilityOfStateSupportComment2, assessment, "projectImpactInfoAvailabilityOfStateSupportComment2");

        }

        return assessment;
    }

    public Assessment calcProjectCharacteristic() {
        Assessment assessment = new Assessment();

        assessment = checkEmpty(projectCost, assessment, "projectCost");
//        assessment = checkEmpty(projectLastCost, assessment, "projectLastCost");
//        assessment = checkEmpty(projectProjectUpcomingInvestments, assessment, "projectProjectUpcomingInvestments");
        assessment = checkEmpty(projectPaybackTime, assessment, "projectPaybackTime");
        assessment = checkEmpty(projectProjectTermConstructionPhase, assessment, "projectProjectTermConstructionPhase");
        assessment = checkEmpty(projectTermOperationalPhase, assessment, "projectTermOperationalPhase");
//
//
//        assessment = checkEmpty(projectProjectProvidesForACompetition, assessment, "projectProjectProvidesForACompetition");
        assessment = checkEmpty(projectProjectProvidesForACompetitionDescription, assessment, "projectProjectProvidesForACompetitionDescription");
        assessment = checkEmpty(projectFundsUsingMonitoringMechanismsDescription, assessment, "projectFundsUsingMonitoringMechanismsDescription");
        assessment = checkEmpty(projectNetPresentValue, assessment, "projectNetPresentValue");
//        assessment = checkEmpty(projectProjectIrr, assessment, "projectProjectIrr");

        return assessment;
    }

    public Assessment calcBusinessModel() {
        Assessment assessment = new Assessment();

        assessment = checkEmpty(projectProjectAdvisabilityRealization, assessment, "projectProjectAdvisabilityRealization");
        assessment = checkEmpty(projectProductMainQualitativeCharacteristics, assessment, "projectProductMainQualitativeCharacteristics");
        assessment = checkEmpty(projectTermsDeliveryPaymentExpected, assessment, "projectTermsDeliveryPaymentExpected");
        assessment = checkEmpty(projectOrderBook, assessment, "projectOrderBook");
        assessment = checkEmpty(projectRecipientAvailableResourcesEnum, assessment, "projectRecipientAvailableResourcesEnum");
        assessment = checkEmpty(projectDesignRisksAnalysis, assessment, "projectDesignRisksAnalysis");
        assessment = checkEmpty(projectProjectAdvisabilityRealization, assessment, "projectProjectAdvisabilityRealization");

        return assessment;
    }

    public int calcTotal() {
        int resultGood = 0;
        int resultBad = 0;

        if (calcInitiator().result() > 99.5) resultGood++;
        else resultBad++;

        if (calcProjectMain().result() > 99.5) resultGood++;
        else resultBad++;

        if (calcDirectionIndustry().result() > 99.5) resultGood++;
        else resultBad++;

        if (calcInfluence().result() > 99.5) resultGood++;
        else resultBad++;

        if (calcProjectCharacteristic().result() > 99.5) resultGood++;
        else resultBad++;

        if (calcBusinessModel().result() > 99.5) resultGood++;
        else resultBad++;

        if (resultGood + resultBad > 0)
            return 100 * resultGood / (resultGood + resultBad);
        else return 0;
    }


    @JsonProperty("project_request_status_date")
    public Date projectRequestStatusDate;

    @JsonProperty("project_request_status")
    public Integer projectRequestStatus;

    @JsonProperty("project_confirming_budgetary_effect")
    public String projectConfirmingBudgetaryEffect;

    @JsonProperty("project_estimated_amount_of_budget_revenues")
    public String projectBudgetaryEffectEstimatedAmount;

    @JsonProperty("project_estimated_value_of_budget_expenditures")
    public String projectBudgetaryEffectEstimatedValue;

    @JsonProperty("project_new_work_places_count")
    public String projectWorkPlaceCount;

    @JsonProperty("project_environmental_expertise_conclusion_not_required_is")
    @JsonSerialize(using = BooleanSerializer.class)
    public Boolean projectEnvironmentalExpertiseConclusionNotRequiredIs;
    @JsonProperty("project_environmental_expertise_is_not_required_is")
    @JsonSerialize(using = BooleanSerializer.class)
    public Boolean projectEnvironmentalExpertiseIsNotRequiredIs;
    @JsonProperty("initiator_face_role_at_project")
    public String initiatorFaceRoleAtProject;

    @JsonProperty("participants")
    public List<Participant> participants;

    @JsonProperty("additional_persons")
    public List<IndividualPerson> participantsIndividuals;

    @JsonProperty("request_finance")
    public List<TypeOfFinancing> typeOfFinancings;

    @JsonProperty("supplies")
    public List<Supply> supplies;


    @JsonProperty("project_additional_info")
    public String projectAdditionalInfo;
    @JsonProperty("project_agreement_basic_parameters")
    public String projectAgreementBasicParameters;
    @JsonProperty("project_assurance_amount")
    public Double projectAssuranceAmount;
    @JsonProperty("project_assurance_currency")
    public String projectAssuranceCurrency;
    @JsonProperty("project_authorization_document_issued_by")
    public String projectAuthorizationDocumentIssuedBy;
    @JsonProperty("project_authorization_document_issued_date")
    @JsonSerialize(using = DateSerializer.class)
    public DateForm projectAuthorizationDocumentIssuedDate;
    @JsonProperty("project_availability_of_state_support")
    @JsonSerialize(using = StateSupportSerializer.class)
    public Set<Integer> projectStateSupport;


    @JsonProperty("project_availability_of_state_support_comment0")
    public String projectImpactInfoAvailabilityOfStateSupportComment0;
    @JsonProperty("project_availability_of_state_support_comment1")
    public String projectImpactInfoAvailabilityOfStateSupportComment1;
    @JsonProperty("project_availability_of_state_support_comment2")
    public String projectImpactInfoAvailabilityOfStateSupportComment2;

    @JsonProperty("project_banks_cash_amount")
    public Double projectAmountBanksCash;
    @JsonProperty("project_banks_cash_part")
    public Double projectAmountBanksPart;
    @JsonProperty("project_banks_of_the_syndicate")
    public String projectBanksOfTheSyndicate;
    @JsonProperty("project_banks_withdrawal_from_project")
    public String projectBanksWithdrawalFromProject;

    @JsonProperty("project_bond_custom")
    public List<List<String>> projectAmountBondArray;
    @JsonProperty("project_coinvestor_custom")
    public List<List<String>> projectAmountInvestorArray;
    @JsonProperty("project_finance_custom")
    public List<List<String>> projectAmountFinanceArray;


    @JsonProperty("project_bond_issue")
    public Double projectAmountBondCash;
    @JsonProperty("project_bond_issue_percent")
    public Double projectAmountBondPart;
    @JsonProperty("project_calculating_effect_components")
    public String projectCalculatingEffectComponents;
    @JsonProperty("project_calculation_formula")
    public String projectCalculationFormula;
    @JsonProperty("project_can_project_financed_by_financial_institutions")
    public String projectCanProjectFinancedByFinancialInstitutions;
    @JsonProperty("project_certified_manager_id")
    public String projectCertifiedManagerId;
    @JsonProperty("project_co_investor_financing_funds")
    public Double projectAmountInvestorCash;
    @JsonProperty("project_co_investor_financing_funds_percent")
    public Double projectAmountInvestorPart;
    @JsonProperty("project_credit_resources_cost_percent")
    public String projectCreditResourcesCostPercent;
    @JsonProperty("project_date_creating")
    @JsonSerialize(using = DateSerializer.class)
    public DateForm projectDateEntered;
    @JsonProperty("project_debt_repayment_delay")
    public String projectDebtRepaymentDelay;
    @JsonProperty("project_debt_repayment_period")
    public String projectDebtRepaymentPeriod;
    @JsonProperty("project_debt_repayment_schedule")
    public String projectDebtRepaymentSchedule;
    @JsonProperty("project_design_risks_analysis")
    public String projectDesignRisksAnalysis;
    @JsonProperty("project_employment_effect_information")
    public String projectEffectEmployment;
    @JsonProperty("project_environmental_expertise_conclusion_not_required")
    public String projectEnvironmentalExpertiseConclusionNotRequired;
    @JsonProperty("project_environmental_expertise_is_not_required")
    public String projectEnvironmentalExpertiseIsNotRequired;
    @JsonProperty("project_environmental_expertise_issued_by")
    public String projectEnvironmentalExpertiseIssuedBy;
    @JsonProperty("project_environmental_expertise_issued_date")
    @JsonSerialize(using = DateSerializer.class)
    public DateForm projectEnvironmentalExpertiseIssuedDate;
    @JsonProperty("project_evaluation_method")
    public String projectEvaluationMethod;
    @JsonProperty("project_expected_result")
    public String projectExpectedResult;
    @JsonProperty("project_experience_of_project_managers")
    public String projectExperienceProjectManagers;
    @JsonProperty("project_express_valuating1")
    public Integer expressValuating1;
    @JsonProperty("project_express_valuating1_description")
    public String expressValuating1Description;
    @JsonProperty("project_express_valuating2")
    public Integer expressValuating2;
    @JsonProperty("project_express_valuating2_description")
    public String expressValuating2Description;
    @JsonProperty("project_express_valuating3")
    public Integer expressValuating3;
    @JsonProperty("project_express_valuating3_description")
    public String expressValuating3Description;
    @JsonProperty("project_express_valuating4")
    public Integer expressValuating4;
    @JsonProperty("project_express_valuating4_description")
    public String expressValuating4Description;
    @JsonProperty("project_express_valuating5")
    public Integer expressValuating5;
    @JsonProperty("project_express_valuating5_description")
    public String expressValuating5Description;
    @JsonProperty("project_express_valuating6")
    public Integer expressValuating6;
    @JsonProperty("project_express_valuating6_description")
    public String expressValuating6Description;
    @JsonProperty("project_express_valuating7")
    public Integer expressValuating7;
    @JsonProperty("project_express_valuating7_description")
    public String expressValuating7Description;
    @JsonProperty("project_financial_institutions_participation_terms_in_project")
    public String projectFinancialInstitutionsParticipationTermsInProject;
    @JsonProperty("project_financial_institutions_ready_to_finance_the_project")
    public String projectFinancialInstitutionsReadyToFinanceTheProject;
    @JsonProperty("project_financial_model_comment")
    public String projectFinancialModelComment;
    @JsonProperty("project_financial_model_document")
    public String projectFinancialModelDocument;
    @JsonProperty("project_financial_plan_document")
    public String projectFinancialPlanDocument;
    @JsonProperty("project_financing_basic_terms")
    public String projectFinancingBasicTerms;
    @JsonProperty("project_project_cost_currency")
    public String projectCostCurrency;
    @JsonProperty("project_financing_plan_document")
    public String projectFinancingPlanDocument;
    @JsonProperty("project_financing_request_amount")
    public Double projectAmountFinancingRequest;
    @JsonProperty("project_financing_request_type")
    public String projectFinancingRequestType;
    @JsonProperty("project_financing_request_type_other")
    public String projectFinancingRequestTypeOther;
    @JsonProperty("project_financing_request_type_time")
    public String projectFinancingRequestTypeTime;
    @JsonProperty("project_funds_using_monitoring_mechanisms")
    public String projectFundsUsingMonitoringMechanisms;
    @JsonProperty("project_funds_using_monitoring_mechanisms_description")
    public String projectFundsUsingMonitoringMechanismsDescription;
    @JsonProperty("project_guarantor_name")
    public String projectAssuranceGuarantor;
    @JsonProperty("project_have_financing_at_rf_before")
    public String projectHaveFinancingAtRfBefore;
    @JsonProperty("initiator_activities_term")
    public String initiatorActivityTerm;
    @JsonProperty("initiator_born_date")
    @JsonSerialize(using = DateSerializer.class)
    public DateForm initiatorBornDate;
    @JsonProperty("initiator_born_place")
    public String initiatorBornPlace;
    @JsonProperty("initiator_citizenship")
    public String initiatorCitizenship;

    @JsonProperty("external_first_name")
    public String employeeNameFirst;
    @JsonProperty("external_last_name")
    public String employeeNameLast;
    @JsonProperty("external_mid_name")
    public String employeeNameMid;
    @JsonProperty("external_position")
    public String employeePosition;

    @JsonProperty("external_fax_number")
    public String employeeFaxNumber;
    @JsonProperty("external_phone_number")
    public String employeePhoneNumber;
    @JsonIgnore
    public String employeePhoneNumbers;

    public void setEmployeePhoneNumbers(String employeePhoneNumbers) {
        this.employeePhoneNumbers = employeePhoneNumbers;
        if (!Strings.isNullOrEmpty(this.employeePhoneNumbers)) {
            final String[] phones = this.employeePhoneNumbers.split("\\Q^|^\\E");
            if (!ArrayUtils.isEmpty(phones)) {
                for (String phone : phones) {
                    final String[] values = phone.split("\\Q^,^\\E");
                    if (!ArrayUtils.isEmpty(values)) {
                        List<String> list = Arrays.asList(values);
                        if (list.contains("mobile")) {
                            list = list.stream()
                                    .filter(el -> !Strings.isNullOrEmpty(el) && !"mobile".equals(el) && el.length() > 7)
                                    .collect(Collectors.toList());
                            if (!ListUtils.isEmpty(list)) this.employeePhoneNumber = list.get(0);
                        } else if (list.contains("fax")) {
                            list = list.stream()
                                    .filter(el -> !Strings.isNullOrEmpty(el) && !"fax".equals(el) && el.length() > 7)
                                    .collect(Collectors.toList());
                            if (!ListUtils.isEmpty(list)) this.employeeFaxNumber = list.get(0);
                        }
                    }
                }
            }
        }
    }

    @JsonProperty("external_emails")
    public List<String> employeeEmails;

    public void setEmployeeEmails(List<String> employeeEmails) {
        this.employeeEmails = employeeEmails;
        if (!ListUtils.isEmpty(employeeEmails)) {
            this.employeeEmail = employeeEmails.get(0);
        }
    }

    @JsonIgnore
    public String employeeEmail;

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
        if (null != employeeEmail) this.employeeEmails = Arrays.asList(employeeEmail);
        else this.employeeEmails = new ArrayList<>();
    }


    @JsonProperty("initiator_company_name_short")
    public String initiatorNameShort;
    @JsonProperty("initiator_dissatisfied_claims")
    public String initiatorDissatisfiedClaims;
    @JsonProperty("initiator_document_issue_date")
    public Date initiatorDocumentIssueDate;
    @JsonProperty("initiator_document_issue_who")
    public String initiatorDocumentIssueWho;
    @JsonProperty("initiator_document_issue_who_code")
    public String initiatorDocumentIssueWhoCode;
    @JsonProperty("initiator_document_name")
    public String initiatorDocumentName;
    @JsonProperty("initiator_document_number")
    public String initiatorDocumentNumber;
    @JsonProperty("initiator_document_series")
    public String initiatorDocumentSeries;
    @JsonProperty("initiator_face_type")
    public String initiatorFaceType;
    @JsonProperty("initiator_factual_address")
    public Address initiatorAddressFactual;
    @JsonProperty("initiator_factual_address_confirm")
    public String initiatorAddressFactualConfirm;
    @JsonProperty("initiator_fax_numbers")
    public String initiatorFaxNumbers;
    @JsonProperty("initiator_firm_name_foreign")
    public String initiatorFirmNameForeign;
    @JsonProperty("initiator_firm_name_foreign_short")
    public String initiatorFirmNameForeignShort;
    @JsonProperty("initiator_firm_name_rus")
    public String initiatorNameRus;
    @JsonProperty("initiator_firm_name_rus_short")
    public String initiatorFirmNameRusShort;
    @JsonProperty("initiator_first_name")
    public String initiatorFirstName;
    @JsonProperty("initiator_inn")
    public String initiatorInn;
    @JsonProperty("initiator_kpp")
    public String initiatorKpp;
    @JsonProperty("initiator_kio")
    public String initiatorKio;
    @JsonProperty("initiator_last_name")
    public String initiatorLastName;
    @JsonProperty("initiator_main_activities")
    public String initiatorActivity;
    @JsonProperty("initiator_mid_name")
    public String initiatorMidName;
    @JsonProperty("initiator_migration_number")
    public String initiatorMigrationNumber;
    @JsonProperty("initiator_migration_series")
    public String initiatorMigrationSeries;
    @JsonProperty("initiator_migration_term_end_date")
    public Date initiatorMigrationTermEndDate;
    @JsonProperty("initiator_migration_term_start_date")
    public Date initiatorMigrationTermStartDate;
    @JsonProperty("initiator_ogrn")
    public String initiatorOgrn;
    @JsonProperty("initiator_organizational_legal_form")
    public String initiatorOrganizationalLegalForm;
    @JsonProperty("initiator_other_persons_activities_participation_description")
    public String initiatorOtherPersonsActivitiesParticipationDescription;
    @JsonProperty("initiator_owners_chain_info")
    public String initiatorOwnersChain;


    @JsonProperty("initiator_phone_number")
    public String initiatorPhoneNumber;
    @JsonIgnore
    public String initiatorPhoneNumberAdditional;
    @JsonProperty("initiator_fax_number")
    public String initiatorFaxNumber;
    @JsonIgnore
    public String initiatorPhoneNumbers;

    public void setInitiatorPhoneNumbers(String initiatorPhoneNumbers) {
        this.initiatorPhoneNumbers = initiatorPhoneNumbers;
        if (!Strings.isNullOrEmpty(this.initiatorPhoneNumbers)) {
            final String[] phones = this.initiatorPhoneNumbers.split("\\Q^|^\\E");
            if (!ArrayUtils.isEmpty(phones)) {
                for (String phone : phones) {
                    final String[] values = phone.split("\\Q^,^\\E");
                    if (!ArrayUtils.isEmpty(values)) {
                        List<String> list = Arrays.asList(values);
                        if (list.contains("mobile")) {
                            list = list.stream()
                                    .filter(el -> !Strings.isNullOrEmpty(el) && !"mobile".equals(el) && el.length() > 7)
                                    .collect(Collectors.toList());
                            if (!ListUtils.isEmpty(list)) this.initiatorPhoneNumber = list.get(0);
                        } else if (list.contains("fax")) {
                            list = list.stream()
                                    .filter(el -> !Strings.isNullOrEmpty(el) && !"fax".equals(el) && el.length() > 7)
                                    .collect(Collectors.toList());
                            if (!ListUtils.isEmpty(list)) this.initiatorFaxNumber = list.get(0);
                        }
                    }
                }
            }
        }
    }

    @JsonProperty("initiator_emails")
    public List<String> initiatorEmails;

    public void setInitiatorEmails(List<String> initiatorEmails) {
        this.initiatorEmails = initiatorEmails;
        if (!ListUtils.isEmpty(initiatorEmails)) {
            this.initiatorEmail = initiatorEmails.get(0);
        }
    }

    @JsonIgnore
    public String initiatorEmail;

    public void setInitiatorEmail(String initiatorEmail) {
        this.initiatorEmail = initiatorEmail;
        if (null != initiatorEmail) this.initiatorEmails = Arrays.asList(initiatorEmail);
        else this.initiatorEmails = new ArrayList<String>();
    }


    @JsonProperty("initiator_postal_address")
    public Address initiatorPostalAddress;
    @JsonProperty("initiator_registration_address")
    public Address initiatorAddressRegistration;
    @JsonProperty("initiator_registration_number")
    public String initiatorRegistrationNumber;
    @JsonProperty("initiator_requisites_bank_bik")
    public String initiatorRequisitesBankBik;
    @JsonProperty("initiator_requisites_bank_corr_account")
    public String initiatorRequisitesBankCorrAccount;
    @JsonProperty("initiator_requisites_bank_name")
    public String initiatorRequisitesBankName;
    @JsonProperty("initiator_requisites_foreign_bank_swift")
    public String initiatorRequisitesForeignBankSwift;
    @JsonProperty("initiator_requisites_foreign_currency")
    public String initiatorRequisitesForeignCurrency;
    @JsonProperty("initiator_requisites_foreign_receiver")
    public String initiatorRequisitesForeignReceiver;
    @JsonProperty("initiator_rtequisites_foreign_receiver_accoun")
    public String initiatorRequisitesForeignReceiverAccount;
    @JsonProperty("initiator_requisites_receiver")
    public String initiatorRequisitesReceiver;
    @JsonProperty("initiator_requisites_receivers_account")
    public String initiatorRequisitesReceiversAccount;

    @JsonProperty("initiator_resident")
    @JsonSerialize(using = BooleanSerializer.class)
    public Boolean initiatorResident;

    @JsonProperty("executive_born_date")
    @JsonSerialize(using = DateSerializer.class)
    public DateForm executiveBornDate;
    @JsonProperty("executive_born_place")
    public Address executiveBornPlace;
    @JsonProperty("executive_citizenship")
    public String executiveCitizenship;
    @JsonProperty("executive_control_company_delegate")
    @JsonSerialize(using = BooleanSerializer.class)
    public Boolean executiveControlCompanyDelegate;
    @JsonProperty("executive_control_company_delegate_name")
    public String executiveControlCompanyDelegateName;
    @JsonProperty("executive_document_issue_date")
    @JsonSerialize(using = DateSerializer.class)
    public DateForm executiveIdIssueDate;
    @JsonProperty("executive_document_issue_who")
    public String executiveIdIssueAgencyName;
    @JsonProperty("executive_document_issue_who_code")
    public String executiveIdIssueAgencyCode;
    @JsonProperty("executive_document_name")
    public String executiveIdName;
    @JsonProperty("executive_document_number")
    public String executiveIdNumber;
    @JsonProperty("executive_document_series")
    public String executiveIdSeries;
    @JsonProperty("executive_first_name")
    public String executiveNameFirst;
    @JsonProperty("executive_last_name")
    public String executiveNameLast;
    @JsonProperty("executive_mid_name")
    public String executiveNameMid;
    @JsonProperty("executive_migration_number")
    public String executiveMigrationNumber;
    @JsonProperty("executive_migration_series")
    public String executiveMigrationSeries;
    @JsonProperty("executive_migration_term_end_date")
    @JsonSerialize(using = DateSerializer.class)
    public DateForm executiveMigrationDateEnd;
    @JsonProperty("executive_migration_term_start_date")
    @JsonSerialize(using = DateSerializer.class)
    public DateForm executiveMigrationDateStart;
    @JsonProperty("executive_other_persons_activities_participation_description")
    public String executiveInfoActivitiesParticipation;
    @JsonProperty("executive_position")
    public String executivePosition;
    @JsonProperty("executive_registration_address")
    public Address executiveAddressRegistration;
    @JsonProperty("executive_resident")
    @JsonSerialize(using = BooleanSerializer.class)
    public Boolean executiveResident;

    @JsonProperty("initiator_state_registration_date")
    public Date initiatorStateRegistrationDate;
    @JsonProperty("initiator_tax_arrears_info")
    public String initiatorTaxArrearsInfo;
    @JsonProperty("initiator_workers_count_average")
    public String initiatorEmployeesNumber;
    @JsonProperty("project_last_cost")
    public String projectLastCost;


    @JsonProperty("project_first_name")
    public String applicantNameFirstOld;
    @JsonProperty("project_last_name")
    public String applicantNameLastOld;
    @JsonProperty("project_mid_name")
    public String applicantNameMidOld;
    @JsonProperty("project_lead_phone_number")
    public String applicantPhoneNumberOld;
    @JsonProperty("project_lead_email")
    public String applicantEmailOld;

    @JsonProperty("declarer_first_name")
    public String applicantNameFirst;
    @JsonProperty("declarer_last_name")
    public String applicantNameLast;
    @JsonProperty("declarer_mid_name")
    public String applicantNameMid;
    @JsonProperty("declarer_phone_numbers")
    public String applicantPhoneNumber;
    @JsonIgnore
    public String applicantPhoneNumbers;

    public void setApplicantPhoneNumbers(String applicantPhoneNumbers) {
        this.applicantPhoneNumbers = applicantPhoneNumbers;
        this.applicantPhoneNumber = applicantPhoneNumbers;

//        if (!Strings.isNullOrEmpty(this.applicantPhoneNumbers)) {
//            final String[] phones = this.applicantPhoneNumbers.split("\\Q^|^\\E");
//            if (!ArrayUtils.isEmpty(phones)) {
//                for (String phone : phones) {
//                    final String[] values = phone.split("\\Q^,^\\E");
//                    if (!ArrayUtils.isEmpty(values)) {
//                        List<String> list = Arrays.asList(values);
//                        if (list.contains("mobile")) {
//                            list = list.stream()
//                                    .filter(el -> !Strings.isNullOrEmpty(el) && !"mobile".equals(el) && el.length() > 7)
//                                    .collect(Collectors.toList());
//                            if (!ListUtils.isEmpty(list)) this.employeePhoneNumber = list.get(0);
//                        } else if (list.contains("fax")) {
//                            list = list.stream()
//                                    .filter(el -> !Strings.isNullOrEmpty(el) && !"fax".equals(el) && el.length() > 7)
//                                    .collect(Collectors.toList());
//                            if (!ListUtils.isEmpty(list)) this.employeeFaxNumber = list.get(0);
//                        }
//                    }
//                }
//            }
//        }
    }

    public void setApplicantNameFirst(String applicantNameFirst) {
        this.applicantNameFirst = applicantNameFirst;
        this.applicantNameFirstOld = applicantNameFirst;
    }

    public void setApplicantNameLast(String applicantNameLast) {
        this.applicantNameLast = applicantNameLast;
        this.applicantNameLastOld = applicantNameLast;
    }

    public void setApplicantNameMid(String applicantNameMid) {
        this.applicantNameMid = applicantNameMid;
        this.applicantNameMidOld = applicantNameMid;
    }

    public void setApplicantPhoneNumber(String applicantPhoneNumber) {
        this.applicantPhoneNumber = applicantPhoneNumber;
        this.applicantPhoneNumberOld = applicantPhoneNumber;
    }

    //    @JsonProperty("project_lead_email")
    @JsonIgnore
    public String applicantEmail;

    public void setApplicantEmail(String applicantEmail) {
        this.applicantEmail = applicantEmail;
        this.applicantEmailOld = applicantEmail;
        if (!Strings.isNullOrEmpty(applicantEmail)) {
            this.applicantEmails = new ArrayList<>();
            this.applicantEmails.add(applicantEmail);
        }
    }

    @JsonProperty("declarer_emails")
    public List<String> applicantEmails;

    public void setApplicantEmails(List<String> applicantEmails) {
        this.applicantEmails = applicantEmails;
        if (!ListUtils.isEmpty(applicantEmails)) {
            this.applicantEmail = applicantEmails.get(0);
            this.applicantEmailOld = applicantEmails.get(0);
        }
    }


    @JsonProperty("project_market_value_of_collateral")
    public String projectAssuranceValueCollateral;
    @JsonProperty("project_nomination")
    public String projectNomination;
    @JsonProperty("project_order_book")
    public String projectOrderBook;
    @JsonProperty("project_other_financing_sources")
    public String projectFinancingSourcesOther;
    @JsonProperty("project_others_cash_amount")
    public Double projectAmountOthersCash;
    @JsonProperty("project_others_cash_part")
    public Double projectAmountOthersPart;
    @JsonProperty("project_product_description_document")
    public String projectProductDescriptionDocument;
    @JsonProperty("project_product_main_qualitative_characteristics")
    public String projectProductMainQualitativeCharacteristics;
    @JsonProperty("project_project_advantages_in_market")
    public String projectProjectAdvantagesInMarket;
    @JsonProperty("project_project_advisability_realization")
    public String projectProjectAdvisabilityRealization;
    @JsonProperty("project_project_aim_reason")
    public String projectProjectAimReason;
    @JsonProperty("project_project_assurance")
    public Integer projectAssuranceType;
    @JsonProperty("project_project_correspondence_sectors")
    public String projectProjectCorrespondenceSectors;
    @JsonProperty("project_project_cost")
    public String projectCost;
    @JsonProperty("project_project_cost_double")
    public Double projectProjectCostDouble;
    @JsonProperty("project_project_decision_date_and_level")
    public String projectProjectDecisionDateAndLevel;
    @JsonProperty("project_project_description")
    public String projectDescription;
    @JsonProperty("project_project_environmental_protection_require")
    public String projectEnvironmentalProtection;

    @JsonProperty("project_project_environmental_protection_require_comment")
    public String projectEnvironmentalProtectionComment;


    @JsonProperty("project_project_financing_expense_investment_fund")
    @JsonSerialize(using = BooleanSerializer.class)
    public Boolean projectProjectFinancingExpenseInvestmentFund;
    @JsonProperty("project_project_implementation_activities")
    public String projectProjectImplementationActivities;
    @JsonProperty("project_project_implementation_corresponds_to_priorities")
    @JsonSerialize(using = BooleanSerializer.class)
    public Boolean projectImplementationCorrespondsToPriorities;
    @JsonProperty("project_project_implementation_public_private_partnership")
    @JsonSerialize(using = BooleanSerializer.class)
    public Boolean projectProjectImplementationPublicPrivatePartnership;
    @JsonProperty("project_project_implementation_technological_opportunities")
    public String projectTechnologicalOpportunities;
    @JsonProperty("project_project_implementation_technologies")
    public String projectTechnologiesImplementation;
    @JsonProperty("project_project_irr")
    public Double projectProjectIrr;
    @JsonProperty("project_project_is_aimed_to")
    public String projectProjectIsAimedTo;
    @JsonProperty("project_project_is_aimed_to_custom")
    public String projectProjectIsAimedToCustom;
    @JsonProperty("project_project_net_present_value")
    public String projectNetPresentValue;
    @JsonProperty("project_project_payback_time")
    public String projectPaybackTime;
    @JsonProperty("project_project_payback_time_double")
    public Integer projectProjectPaybackTimeDouble;
    @JsonProperty("project_project_provides_for_a_competition")
    public String projectProjectProvidesForACompetition;
    @JsonProperty("project_project_provides_for_a_competition_description")
    public String projectProjectProvidesForACompetitionDescription;
    @JsonProperty("project_project_sectors")
    public String projectSectors;
    @JsonProperty("project_project_sectors_custom")
    public String projectSectorsCustom;
    @JsonProperty("project_project_sectors_reason")
    public String projectSectorsReason;
    @JsonProperty("project_project_sectors1")
    public String projectSectors1;
    @JsonProperty("project_project_target")
    public String projectTarget;
    @JsonProperty("project_project_type")
    public String projectType;
    @JsonProperty("project_project_type_custom")
    public String projectTypeCustom;
    @JsonProperty("project_project_type1")
    public String projectType1;
    @JsonProperty("project_project_type1_custom")
    public String projectType1Custom;
    @JsonProperty("project_project_upcoming_investments")
    public String projectProjectUpcomingInvestments;
    @JsonProperty("project_projects_and_conditions_for_their_financing")
    public String projectProjectsAndConditionsForTheirFinancing;
    @JsonProperty("project_qualitative_approaches_description")
    public String projectQualitativeApproachesDescription;
    @JsonProperty("project_readiness_of_project_documentation")
    public String projectReadinessOfProjectDocumentation;
    @JsonProperty("project_reasons_for_refusal_of_financing")
    public String projectReasonsForRefusalOfFinancing;
    @JsonProperty("project_recipient_available_resources_enum")
    public String projectRecipientAvailableResourcesEnum;
    @JsonProperty("project_recipients_cash_amount")
    public Double projectAmountRecipientsCash;
    @JsonProperty("project_recipients_cash_part")
    public Double projectAmountRecipientsPart;
    @JsonProperty("project_return_on_bank_equity")
    public String projectReturnOnBankEquity;
    @JsonProperty("project_sale_place")
    public String projectSalePlace;
    @JsonProperty("project_sale_place_custom")
    public List<String> projectSalePlaceCustom;
    @JsonProperty("project_syndicate_conditions")
    public String projectSyndicateConditions;
    @JsonProperty("project_syndicated_debt_amount")
    public Double projectSyndicatedDebtAmount;
    @JsonProperty("project_terms_delivery_payment_expected")
    public String projectTermsDeliveryPaymentExpected;
    @JsonProperty("project_type_of_authorization_document")
    public String projectTypeOfAuthorizationDocument;
    @JsonProperty("project_way_of_banks_withdrawal_from_project")
    public String projectWayOfBanksWithdrawalFromProject;
    @JsonProperty("project_which_financial_institutions")
    public String projectWhichFinancialInstitutions;
    @JsonProperty("project_why_banks_participation_not_damage_financial_organizations")
    public String projectWhyBanksParticipationNotDamageFinancialOrganizations;
    @JsonProperty("project_project_term_construction_phase")
    public String projectProjectTermConstructionPhase;
    @JsonProperty("project_project_term_operational_phase")
    public String projectTermOperationalPhase;


    @JsonProperty("initiator_actual_place_state")
    public String initiatorActualPlaceState;
    @JsonProperty("initiator_actual_place_territory")
    public String initiatorActualPlaceTerritory;
    @JsonProperty("initiator_actual_place_city")
    public String initiatorActualPlaceCity;
    @JsonProperty("project_study_degree")
    public String projectDegree;
    @JsonProperty("executive_control_company_delegate_type")
    public String executiveControlCompanyDelegateType;
    @JsonProperty("initiator_main_activity_address")
    public Address initiatorMainActivityAddress;
    @JsonProperty("executive_born_place_address")
    public Address executiveBornPlaceAddress;


    public void setProjectType(String projectType) {
        this.projectType = projectType;
        this.projectType1 = projectType;
    }

    public void setProjectTypeCustom(String projectTypeCustom) {
        this.projectTypeCustom = projectTypeCustom;
        this.projectType1Custom = projectTypeCustom;
    }

    public void setProjectType1(String projectType1) {
        this.projectType1 = projectType1;
        this.projectType = projectType1;
    }

    public void setProjectType1Custom(String projectType1Custom) {
        this.projectType1Custom = projectType1Custom;
        this.projectTypeCustom = projectType1Custom;
    }

    public void setProjectSectors(String projectSectors) {
        this.projectSectors = projectSectors;

        if (!Strings.isNullOrEmpty(this.projectSectors) && Strings.isNullOrEmpty(this.projectSectors1)) {
            String[] arr = this.projectSectors.replaceAll("^", "").split(",");
            this.projectSectors1 = arr[0];
        }

        if (null != projectSectors) {
            String[] arr = projectSectors.replaceAll("^", "").split(",");

            if (!ArrayUtils.isEmpty(arr)) this.projectSectors1 = arr[0];
        } else {
            this.projectSectors1 = null;
        }
    }

    public void setProjectSectors1(String projectSectors1) {
        this.projectSectors1 = projectSectors1;
        if (Strings.isNullOrEmpty(this.projectSectors) && !Strings.isNullOrEmpty(this.projectSectors1)) {
            this.projectSectors = "^" + projectSectors1 + "^";
        }
    }
}
