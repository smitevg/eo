package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import ru.veb.eo.serializer.BooleanDeserializer;
import ru.veb.eo.serializer.BooleanSerializer;

@Data
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Participant {

    @JsonProperty("id")
    public String id;

    @JsonProperty("organizational_legal_form")
    public String organizationType;

    @JsonProperty("company_name")
    public String organizationTitle;

    @JsonProperty("resident")
    @JsonSerialize(using = BooleanSerializer.class)
    @JsonDeserialize(using = BooleanDeserializer.class)
    public Boolean resident;

    @JsonProperty("ogrn")
    public String ogrn;

    @JsonProperty("inn")
    public String inn;

    @JsonProperty("kpp")
    public String kpp;

    @JsonProperty("kio")
    public String nalogNumber;

    @JsonProperty("registration_number")
    public String regNumber;

    @JsonProperty("enclosed_funds")
//    @JsonDeserialize(using = DoubleDeserializer.class)
//    @JsonSerialize(using = DoubleSerializer.class)
    public Double volume;

    @JsonProperty("member_status")
    public String status;

    @JsonProperty("investment_terms")
    public String rules;

    @JsonProperty("type")
    public String type;

}
