package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class TypeOfFinancing {
    @JsonProperty("id")
    public String id;
    @JsonProperty("name")
    public String name;
    @JsonProperty("date_entered")
    public Date date_entered;

    @JsonProperty("description")
    public String description;
    @JsonProperty("financing_request_type")
    public String type;
    @JsonProperty("financing_request_type_other")
    public String other;


    @JsonProperty("financing_request_type_time")
    public String financeLongTerm;
    @JsonProperty("credit_resources_cost_percent")
    public String financeLongPercent;
    @JsonProperty("debt_repayment_delay")
    public String financeLongDelay;
    @JsonProperty("debt_repayment_period")
    public String financeLongTermReturn;
    @JsonProperty("debt_repayment_schedule")
    public String financeLongSсhedule;

    @JsonProperty("return_on_bank_equity")
    public String financeMezonYield;
    @JsonProperty("banks_withdrawal_from_project")
    public String financeMezonExitTerm;
    @JsonProperty("way_of_banks_withdrawal_from_project")
    public String financeMezonExitType;

    // гарантии
    @JsonProperty("warranty_size")
    public String financeGarantSize;
    @JsonProperty("granting_term")
    public String financeGarantTerm;
    @JsonProperty("warranty_provision_cost")
    public String financeGarantCost;

    @JsonProperty("syndicated_debt_amount")
    public String financeSidicirYield;
    @JsonProperty("banks_of_the_syndicate")
    public String financeSidicirParticipants;
    @JsonProperty("syndicate_term")
    public String financeSidicirTerm;
    @JsonProperty("syndicate_conditions")
    public String financeSidicirCondition;

    @JsonProperty("other_something")
    public String financeOtherOther;
}
