package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import ru.veb.eo.serializer.BooleanDeserializer;
import ru.veb.eo.serializer.DateDeserializer;

import java.util.List;

@Data
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Initiator {
    @JsonProperty("sole_executive_body")   public Executive executive;
    @JsonProperty("collaborator")   public Collaborator collaborator;
    @JsonProperty("id")
    private String crmId;
    @JsonProperty("emails")   public List<String> initiatorEmails;
    @JsonProperty("phone_numbers")   public String initiatorPhoneNumbers;

    @JsonProperty("face_role_at_project")
    public String initiatorFaceRoleAtProject;
    @JsonProperty("kpp")
    public String initiatorKpp;
    @JsonProperty("activities_term")   public String initiatorActivityTerm;
    @JsonProperty("born_date")   @JsonDeserialize(using = DateDeserializer.class) public DateForm initiatorBornDate=new DateForm();
    @JsonProperty("born_place")   public String initiatorBornPlace;
    @JsonProperty("citizenship")   public String initiatorCitizenship;
    @JsonProperty("company_name_short")   public String initiatorNameShort;
    @JsonProperty("dissatisfied_claims")   public String initiatorDissatisfiedClaims;
    @JsonProperty("document_issue_date")  @JsonDeserialize(using = DateDeserializer.class) public DateForm initiatorDocumentIssueDate=new DateForm();
    @JsonProperty("document_issue_who")   public String initiatorDocumentIssueWho;
    @JsonProperty("document_issue_who_code")   public String initiatorDocumentIssueWhoCode;
    @JsonProperty("document_name")   public String initiatorDocumentName;
    @JsonProperty("document_number")   public String initiatorDocumentNumber;
    @JsonProperty("document_series")   public String initiatorDocumentSeries;
    @JsonProperty("eligibility_name")   public String initiatorEligibilityName;
    @JsonProperty("eligibility_number")   public String initiatorEligibilityNumber;
    @JsonProperty("eligibility_series")   public String initiatorEligibilitySeries;
    @JsonProperty("eligibility_term_end_date")   @JsonDeserialize(using = DateDeserializer.class) public DateForm initiatorEligibilityTermEndDate=new DateForm();
    @JsonProperty("eligibility_term_start_date")  @JsonDeserialize(using = DateDeserializer.class) public DateForm initiatorEligibilityTermStartDate=new DateForm();
    @JsonProperty("face_type")   public String initiatorFaceType;
    @JsonProperty("factual_address")   public Address initiatorAddressFactual;
    @JsonProperty("factual_address_confirm")   public String initiatorAddressFactualConfirm;
    @JsonProperty("fax_numbers")   public String initiatorFaxNumbers;
    @JsonProperty("firm_name_foreign")   public String initiatorFirmNameForeign;
    @JsonProperty("firm_name_foreign_short")   public String initiatorFirmNameForeignShort;
    @JsonProperty("firm_name_rus")   public String initiatorNameRus;
    @JsonProperty("firm_name_rus_short")   public String initiatorFirmNameRusShort;
    @JsonProperty("first_name")   public String initiatorFirstName;
    @JsonProperty("inn")   public String initiatorInn;
    @JsonProperty("kio")   public String initiatorKio;
    @JsonProperty("last_name")   public String initiatorLastName;
    @JsonProperty("main_activities")   public String initiatorActivity;
    @JsonProperty("mid_name")   public String initiatorMidName;
    @JsonProperty("migration_number")   public String initiatorMigrationNumber;
    @JsonProperty("migration_series")   public String initiatorMigrationSeries;
    @JsonProperty("migration_term_end_date") @JsonDeserialize(using = DateDeserializer.class)  public DateForm initiatorMigrationTermEndDate;
    @JsonProperty("migration_term_start_date")  @JsonDeserialize(using = DateDeserializer.class) public DateForm initiatorMigrationTermStartDate;
    @JsonProperty("ogrn")   public String initiatorOgrn;
    @JsonProperty("organizational_legal_form")   public String initiatorOrganizationalLegalForm;
    @JsonProperty("other_persons_activities_participation_description")   public String initiatorOtherPersonsActivitiesParticipationDescription;
    @JsonProperty("owners_chain_info")   public String initiatorOwnersChain;
    @JsonProperty("postal_address")   public Address initiatorPostalAddress;
    @JsonProperty("registration_address")   public Address initiatorAddressRegistration;
    @JsonProperty("registration_number")   public String initiatorRegistrationNumber;
    @JsonProperty("requisites_bank_bik")   public String initiatorRequisitesBankBik;
    @JsonProperty("requisites_bank_corr_account")   public String initiatorRequisitesBankCorrAccount;
    @JsonProperty("requisites_bank_name")   public String initiatorRequisitesBankName;
    @JsonProperty("requisites_foreign_bank_swift")   public String initiatorRequisitesForeignBankSwift;
    @JsonProperty("requisites_foreign_currency")   public String initiatorRequisitesForeignCurrency;
    @JsonProperty("requisites_foreign_receiver")   public String initiatorRequisitesForeignReceiver;
    @JsonProperty("rtequisites_foreign_receiver_accoun")   public String initiatorRequisitesForeignReceiverAccount;
    @JsonProperty("requisites_receiver")   public String initiatorRequisitesReceiver;
    @JsonProperty("requisites_receivers_account")   public String initiatorRequisitesReceiversAccount;

    @JsonProperty("resident")
    @JsonDeserialize(using=BooleanDeserializer.class)
    public Boolean initiatorResident;

    @JsonProperty("state_registration_date") @JsonDeserialize(using = DateDeserializer.class)  public DateForm initiatorStateRegistrationDate;
    @JsonProperty("tax_arrears_info")   public String initiatorTaxArrearsInfo;
    @JsonProperty("workers_count_average")   public String initiatorEmployeesNumber;
    @JsonProperty("main_activity_address")   public Address initiatorMainActivityAddress;



}
