package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public class Manager {

    @JsonProperty("id")
    private String crmId;

    @JsonProperty("photo_url")
    private String image;

    @JsonProperty("first_name")
    private String nameFirst;

    @JsonProperty("last_name")
    private String nameLast;

    @JsonProperty("mid_name")
    private String nameMiddle;

    @JsonProperty("phone_mobile")
    private String phone;

    @JsonProperty("email_address")
    private String email;

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("region_id")
    private String regionId;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        this.userName = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
        this.email = userName;
    }
}
