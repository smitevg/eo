package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.veb.eo.model.reference.Region;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@JsonPropertyOrder(alphabetic = true)
public class ProjectResponsePacket {
    @JsonProperty("id")   public String projectId;
    @JsonProperty("code")  public String projectCode;
    @JsonProperty("nomination")   public String projectNomination;
    @JsonProperty("date_entered")   public Date projectDateEntered;
    @JsonProperty("express_valuating1")   public Integer expressValuating1;
    @JsonProperty("request_status")   public Integer projectRequestStatus;
    @JsonProperty("request_status_date")   public Date projectRequestStatusDate;
    @JsonProperty("lead_email")   public String applicantEmail;
    @JsonProperty("first_name")   public String applicantNameFirst;
    @JsonProperty("last_name")   public String applicantNameLast;
    @JsonProperty("mid_name")   public String applicantNameMid;
    @JsonProperty("certified_manager_id")   public String projectCertifiedManagerId;

    @JsonProperty("realization_region")
    public int realizationRegionID;

    @JsonIgnore
    public Region realizationRegion;

    @JsonProperty("sale_place")
    public int saleRegionID;

    @JsonIgnore
    public Region saleRegion;

    @JsonIgnore
    public String projectCertifiedManagerName;
    @JsonIgnore
    public String projectRegion;
    @JsonIgnore
    public String projectStatusName;
    @JsonIgnore
    public Long differenceDaysBetweenTodayAndEnteredDate;
    @JsonIgnore
    public Date needDateExpressAssessment;
}
