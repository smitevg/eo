package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.veb.eo.serializer.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Supply {
    @JsonProperty("id")
    private String id;

    // 0 - банковская гарантия, 1 гарант, 2 поручитель, 3 залог, 4 другое
    @JsonProperty("name")
    @JsonSerialize(using = TypeSupplySerializer.class)
    @JsonDeserialize(using = TypeSupplyDeserializer.class)
    private TypeSupply type;

    @JsonProperty("date_entered")
    private String dateEntered;
    @JsonProperty("guarantor_name")
    private String guarantor;
    @JsonProperty("supply_amount")
    @JsonSerialize(using = DoubleSerializer.class)
    @JsonDeserialize(using = DoubleDeserializer.class)
    private Double value;
    @JsonProperty("supply_currency")
    private String currency;
    @JsonProperty("description")
    private String description;

    // 3 залог
    // 0 - самостоятельно, 1 - спец. компанией
    @JsonProperty("valuation_of_collateral_was_made")
    private String depositWho;
    @JsonProperty("company_name")
    private String depositCompany;
    @JsonProperty("date_of_evaluation")
    @JsonSerialize(using = DateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
    private DateForm depositDate;

    public void setDepositDate(DateForm depositDate) {
        this.depositDate = depositDate;
        this.dateDay = this.depositDate != null ? depositDate.day : null;
        this.dateMonth = this.depositDate != null ? depositDate.month : null;
        this.dateYear = this.depositDate != null ? depositDate.year : null;
    }

    // Этих атрибутов нет на форме
    // рыночная стоимость
    @JsonProperty("market_value_of_collateral")
    @JsonSerialize(using = DoubleSerializer.class)
    @JsonDeserialize(using = DoubleDeserializer.class)
    private Double depositMarketValue;

    // обременение
    @JsonProperty("encumbrance_on_collateral")
    private String depositEncumbrance;


    Integer getDateDay() {
        return depositDate != null ? depositDate.day : null;
    }

    Integer getDateMonth() {
        return depositDate != null ? depositDate.month : null;
    }

    Integer getDateYear() {
        return depositDate != null ? depositDate.year : null;
    }

    @JsonProperty("dateDay")
    private Integer dateDay;
    @JsonProperty("dateMonth")
    private Integer dateMonth;
    @JsonProperty("dateYear")
    private Integer dateYear;
}
