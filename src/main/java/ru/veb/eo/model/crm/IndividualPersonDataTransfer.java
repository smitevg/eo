package ru.veb.eo.model.crm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import ru.veb.eo.serializer.BooleanSerializer;
import ru.veb.eo.serializer.DateDeserializer;
import ru.veb.eo.serializer.DateSerializer;

import java.util.List;

@Data
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class IndividualPersonDataTransfer extends ParticipantDataTransfer{
    private String id;
    private List<String> emails;
    private String phoneNumbers;
    private DateForm bornDate;
    private Address bornPlace;
    private String citizenship;
    private Boolean controlCompanyDelegate;
    private String controlCompanyDelegateName;
    private DateForm idIssueDate;
    private String idIssueAgencyName;
    private String idIssueAgencyCode;
    private String idName;
    private String idNumber;
    private String idSeries;
    private String nameFirst;
    private String nameLast;
    private String nameMid;
    private String migrationNumber;
    private String migrationSeries;
    private DateForm migrationDateEnd;
    private DateForm migrationDateStart;
    private String infoActivitiesParticipation;
    private String position;
    private Address addressRegistration;
    private Boolean resident;

    public IndividualPersonDataTransfer(IndividualPerson individualPerson) {
        BeanUtils.copyProperties(individualPerson, this);
    }
}
