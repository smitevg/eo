package ru.veb.eo.model.crm;

public enum TypeSupply {
//    0 - банковская гарантия, 1 гарант, 2 поручитель, 3 залог, 4 другое
    BANK(0), GUARANTEE(1), SURETY(2), PLEDGE(3), OTHER(4);
    private final int code;

    private TypeSupply(int code) {
        this.code = code;
    }

    public int toInt() {
        return code;
    }


    @Override
    public String toString() {
        return String.valueOf(code);
    }
}

