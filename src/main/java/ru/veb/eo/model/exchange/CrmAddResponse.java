package ru.veb.eo.model.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CrmAddResponse {

	private String result;

	private String message;

	private String code;

	@JsonProperty("project_id") private String projectId;
}
