package ru.veb.eo.model.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.veb.eo.model.crm.DocumentAdd;

@Data
@NoArgsConstructor
@JsonPropertyOrder(alphabetic = true)
public class CrmAddDocumentResponse {

    @JsonProperty("result")
    private String result;

    @JsonProperty("message")
    private String message;

    @JsonProperty("document")
    private DocumentAdd document;
}
