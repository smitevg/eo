package ru.veb.eo.model.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import ru.veb.eo.model.crm.ProjectRequest;

@Data
@JsonPropertyOrder(alphabetic = true)
public class CrmUpdateRequest {

	@JsonProperty("id") private String projectId;

	private String key;

	@JsonProperty("params") private ProjectRequest project;

	public CrmUpdateRequest(String projectId, String key, ProjectRequest project) {

		this.projectId = projectId;
		this.key = key;
		this.project = project;
	}
}
