package ru.veb.eo.model.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import ru.veb.eo.model.crm.ProjectRequest;

@Data
@JsonPropertyOrder(alphabetic = true)
public class CrmAddRequest {

    private String key;

    @JsonProperty("params")
    private ProjectRequest project;

    public CrmAddRequest(String key, ProjectRequest project) {

        super();
        this.key = key;
        this.project = project;
    }
}
