package ru.veb.eo.model.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import ru.veb.eo.model.crm.*;

import java.util.List;

@Data
public class CrmGetRatingResponse {

	private String result;
	private String message;

	@JsonProperty("report")
	private List<Rating> ratings;
}
