package ru.veb.eo.model.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.veb.eo.model.crm.DocumentAdd;

import java.util.Map;

@Data
@NoArgsConstructor
@JsonPropertyOrder(alphabetic = true)
public class CrmAddDocumentRequest {

    @JsonProperty("id")
    private String projectId;

    @JsonProperty("key")
    private String key;

    @JsonProperty("document")
    private DocumentAdd document;

    public CrmAddDocumentRequest(String projectId, String key, DocumentAdd document) {
        this.projectId = projectId;
        this.key = key;
        this.document = document;
    }
}
