package ru.veb.eo.model.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonPropertyOrder(alphabetic = true)
public class CrmGetRatingRequest {

    @JsonProperty("key")
    private String key;

    public CrmGetRatingRequest(String key) {
        this.key = key;
    }
}
