package ru.veb.eo.model.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.veb.eo.model.crm.Message;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder(alphabetic = true)
public class CrmSendMessageRequest {
    @JsonProperty("project_id")
    private String projectId;

    @JsonProperty("key")
    private String key;

    @JsonProperty("message")
    private Message message;

}
