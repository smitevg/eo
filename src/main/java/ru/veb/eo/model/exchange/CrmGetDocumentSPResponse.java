package ru.veb.eo.model.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import ru.veb.eo.model.crm.DocumentSP;

import java.util.List;

@Data
public class CrmGetDocumentSPResponse {

	private String result;

	private String message;

	@JsonProperty("documents")
	public List<DocumentSP> documentSP;
}
