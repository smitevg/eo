package ru.veb.eo.model.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@JsonPropertyOrder(alphabetic = true)
public class CrmCloseTasksRequest {

    @JsonProperty("id")
    private String taskId;

    @JsonProperty("key")
    private String key;

    @JsonProperty("variables")
    private Map<String, Object> variables;

    public CrmCloseTasksRequest(String taskId, String key, Map<String, Object> variables) {
        this.taskId = taskId;
        this.key = key;
        this.variables = variables;
    }
}
