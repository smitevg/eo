package ru.veb.eo.model.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.veb.eo.model.crm.Task;

import java.util.List;

@Data
@NoArgsConstructor
@JsonPropertyOrder(alphabetic = true)
public class CrmCloseTaskResponse {

    @JsonProperty("result")
    private String result;

    @JsonProperty("message")
    private String message;

}
