package ru.veb.eo.model.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import ru.veb.eo.model.crm.*;

import java.util.List;

@Data
public class CrmGetResponse {

	private String result;

	private String message;

	private String code;

	@JsonProperty("project")
	private ProjectResponse projectResponse;

	@JsonProperty("participants")
	private List<Participant> participants;

	@JsonProperty("request_finance")
	public List<TypeOfFinancing> typeOfFinancings;

	@JsonProperty("supplies")
	private List<Supply> supplies;

	@JsonProperty("additional_persons")
	public List<IndividualPerson> participantsIndividuals;
}
