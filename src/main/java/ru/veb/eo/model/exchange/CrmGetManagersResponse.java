package ru.veb.eo.model.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import ru.veb.eo.model.crm.Manager;

import java.util.List;

@Data
public class CrmGetManagersResponse {

    private String result;
    private String message;
    private String code;
    private String total;
    @JsonProperty("items")
    private List<Manager> managers;
}
