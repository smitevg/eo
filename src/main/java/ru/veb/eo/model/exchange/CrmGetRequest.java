package ru.veb.eo.model.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder(alphabetic = true)
public class CrmGetRequest {

    @JsonProperty("id")
    private String projectId;

    @JsonProperty("count")
    private Integer count;

    @JsonProperty("offset")
    private Integer offset;

    @JsonProperty("key")
    private String key;

    @JsonProperty("lead_email")
    private String applicantEmail;

    @JsonProperty("certified_manager_id")
    private String projectCertifiedManagerId;


    public CrmGetRequest(String projectId, String key, String applicantEmail, String projectCertifiedManagerId) {
        this.projectId = projectId;
        this.key = key;
        this.applicantEmail = applicantEmail;
        this.projectCertifiedManagerId = projectCertifiedManagerId;
    }
}
