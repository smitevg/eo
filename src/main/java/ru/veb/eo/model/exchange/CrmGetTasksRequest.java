package ru.veb.eo.model.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonPropertyOrder(alphabetic = true)
public class CrmGetTasksRequest {

    @JsonProperty("project_id")
    private String projectId;

    @JsonProperty("key")
    private String key;


    public CrmGetTasksRequest(String projectId, String key) {
        this.projectId = projectId;
        this.key = key;
    }
}
