package ru.veb.eo.model.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import ru.veb.eo.model.crm.ProjectResponsePacket;

import java.util.List;

@Data
public class CrmGetResponsePacket {
    @JsonProperty("result")
    private String result;
    @JsonProperty("message")
    private String message;
    @JsonProperty("code")
    private String code;
    @JsonProperty("offset")
    private Integer offset;
    @JsonProperty("count")
    private Integer count;
    @JsonProperty("items")
    private List<ProjectResponsePacket> items;
}
