package ru.veb.eo.model.exchange;

import lombok.Data;

@Data
public class CrmUpdateResponse {

	private String result;

	private String message;

	private String code;
}
