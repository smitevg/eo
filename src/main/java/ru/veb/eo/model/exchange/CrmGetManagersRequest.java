package ru.veb.eo.model.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder(alphabetic = true)
public class CrmGetManagersRequest {

	@JsonProperty("region_id") private Integer regionId;
	@JsonProperty("certified_manager_id") private String projectCertifiedManagerId;

	private String key;

	public CrmGetManagersRequest(Integer regionId, String projectCertifiedManagerId, String key) {
		this.regionId = regionId;
		this.projectCertifiedManagerId = projectCertifiedManagerId;
		this.key = key;
	}
}
