package ru.veb.eo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.io.Files;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import ru.veb.eo.Properties;
import ru.veb.eo.model.reference.Notification;

import javax.imageio.ImageIO;
import javax.persistence.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder(alphabetic = true)
@Entity
@Table(name = "USER")
public class User implements UserDetails {
    static Logger log = LoggerFactory.getLogger(User.class);

    @Column(name = "ID")
    private String id;

    @Column(name = "CRM_ID")
    private String crmId;

    @JsonIgnore
    @Column(name = "REGION_ID")
    private String regionId;

    @JsonIgnore
    @Transient
    private String page;

    @JsonIgnore
    @Transient
    private List<Notification> notifications;

    @JsonIgnore
    String token;

    @JsonIgnore
    @Transient
    protected boolean auth2;

    @Column(name = "NAME_FIRST")
    private String nameFirst;

    @Column(name = "NAME_LAST")
    private String nameLast;

    @Column(name = "NAME_MIDDLE")
    private String nameMiddle;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "IMAGE_URL")
    private String image;

    @Column(name = "LOGIN")
    private String login;

    // for UserDetails
    @JsonIgnore
    @Column(name = "AUTHORITIES")
    @Enumerated
    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    private List<Role> authorities;

    @JsonIgnore
    private String password;

    @Id
    @Column(unique = true, nullable = false)
    private String username; // email
    @JsonIgnore
    private boolean accountNonLocked;
    @JsonIgnore

    private boolean accountNonExpired;
    @JsonIgnore
    private boolean credentialsNonExpired;
    @JsonIgnore
    private boolean enabled;

    @JsonIgnore
    public List<String> getRoles() {
        List<String> roles = new ArrayList<>();
        if (authorities != null && authorities.size() > 0) {
            for (Role authority : authorities) {
                roles.add(authority.getAuthority());
            }
        }
        return roles;
    }

    /**
     * Обработать аватарку пользователя. Если есть изображение из CRM, то на фронте создастся папка с ID юзера и файлом ava.jpg (или какое там используется расширение). Если нет фотки, но была, удалится папка с аватаркой и если нет фотки, то ничего не сделается
     * @param properties
     */
    public void handleAvatars(Properties properties){
        // отчищенный crmid от точек и слешей (чтобы не поломали)
        String cleanUserCrmId = this.getCrmId().replace(".", "").replace("/", "");
        File userFolderWithPics = new File(properties.getPathToAvatarsOfManager() + cleanUserCrmId);

        // если нет аватарки
        if (this.getImage() == null) {
            // но была аватарка
            if (userFolderWithPics.exists()) {
                userFolderWithPics.delete();
            }
            return;
        }

        // если нет папки, то создать
        if (!userFolderWithPics.exists()) {
            userFolderWithPics.mkdirs();
        }

        String avatarExtension = Files.getFileExtension(this.getImage());
        String avatarsName = String.format("ava.%s", avatarExtension);
        File avatar = new File(userFolderWithPics.getAbsolutePath() + "/" + avatarsName);
        if (!avatar.exists()) {
            try {
                // загрузим аватарку с сервера
                log.info("Saving user {} avatar to {}",  id, avatar);
                BufferedImage img = ImageIO.read(new URL(this.getImage()));
                avatar.createNewFile();
                ImageIO.write(img, avatarExtension, avatar);
            } catch (Exception e) {
                log.warn("Failed to save avatar image to {}: {}", avatar, e.toString(), e);
            }
        }
        this.setImage(String.format("/img/managers/%s/%s", cleanUserCrmId, avatarsName));
    }
}
