package ru.veb.eo.model.reference;

import lombok.Data;
import ru.veb.eo.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name = "MESSAGES_ENTITY")
public class MessageEntity {
    @Id
    @Column(name = "crm_id")
    private String crmId;

    @Column(name = "subject")
    private String subject;

    @Column(name = "date_entered")
    private Date dateEntered;

    @Column(name = "text")
    private String text;

    @Column(name = "project_id")
    public String projectId;

    @Column(name = "author_id")
    private String authorId;

    @Column(name = "author_title")
    private String authorTitle;

    @Column(name = "author_role")
    private Role authorRole;

    @Column(name = "recipients_id")
    @ElementCollection
    private Set<String> recipientsID;

    @Column(name = "recipients")
    @Enumerated
    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    private List<Role> recipients;
}
