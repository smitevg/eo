package ru.veb.eo.model.reference;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="PROJECT_SECTORS")
@Data
public class ProjectSectors  extends GeneralEntity{
    @Column(name = "direction_id")
    private int directionId;

    @Column(name = "sort_order")
    private int sortOrder;
}
