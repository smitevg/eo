package ru.veb.eo.model.reference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="NOTIFICATION")
@Data
public class Notification extends GeneralEntity{
    // в каких ЛК отображать
    private String applicantEmail;
    private String projectCertifiedManagerId;

    // для получение полной инфы об проекте
    private String projectId;

    // формирование иконок
    private String projectNomination;
    private String message;
    private Boolean isGood;
    private boolean isViewed;

    public Notification() {
    }

    public Notification(String applicantEmail, String projectCertifiedManagerId, String projectId, String projectNomination, String message, Boolean isGood) {
        this.applicantEmail = applicantEmail;
        this.projectCertifiedManagerId = projectCertifiedManagerId;
        this.projectId = projectId;
        this.projectNomination = projectNomination;
        this.message = message;
        this.isGood = isGood;
    }
}
