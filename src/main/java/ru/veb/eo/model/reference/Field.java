package ru.veb.eo.model.reference;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="FIELDS")
@Data
public class Field extends GeneralEntity {
    @Column(name = "FIELD", length = 2000)
    private String field;
}
