package ru.veb.eo.model.reference;

import lombok.Data;

import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "PROJECT_DIRECTION")
@Data
public class ProjectDirection extends GeneralEntity {
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "direction_id")
	private Set<ProjectSectors> projectSectors;
}
