package ru.veb.eo.model.reference;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "DISTRICTS")
@Data
public class District extends GeneralEntity {
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "district_id")
    private List<Region> regions;

    @Column(name = "DISTRICT_FRONT")
    private String districtFront;
}
