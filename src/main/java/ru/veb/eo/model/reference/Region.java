package ru.veb.eo.model.reference;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="REGION")
@Data
public class Region extends GeneralEntity {

    @Column(name = "region_front_id", length = 5)
    private String frontId;
}
