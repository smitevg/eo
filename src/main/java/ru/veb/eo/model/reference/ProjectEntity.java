package ru.veb.eo.model.reference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="PROJECTS")
@Data
public class ProjectEntity{
    @Id
    @Column(name = "id")
    private String projectId;

    @Column(name = "code")
    private String projectCode;

    @Column(name = "title")
    private String projectNomination;

    @Column(name = "region")
    private String projectSalePlace;

    @Column(name = "direction")
    private String projectProjectIsAimedTo;

    @Column(name = "status")
    private Integer projectRequestStatus;
}
