package ru.veb.eo.model.reference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "PASSWORDS")
public class Passwords extends GeneralEntity {
    @Column(name = "USERNAME")
    private String userName;
}
