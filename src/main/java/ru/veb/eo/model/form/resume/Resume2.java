package ru.veb.eo.model.form.resume;

import com.google.common.base.Strings;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import ru.veb.eo.model.crm.Address;
import ru.veb.eo.model.crm.DateForm;
import ru.veb.eo.model.crm.ProjectRequest;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.service.UtilsService;

@Data
public class Resume2 implements Form {
    public String back = null;
    public String save = null;
    public String next = null;
    public int numberPage;
    private String projectId;

    private Boolean executiveControlCompanyDelegate;
    private String executiveControlCompanyDelegateName;
    private String executiveControlCompanyDelegateType;
    public String executiveNameLast;
    public String executiveNameFirst;
    public String executiveNameMid;
    public DateForm executiveBornDate = new DateForm();
    public Address executiveBornPlaceAddress = new Address();
    public String executiveCitizenship;
    public Address executiveAddressRegistration = new Address();
    public String executiveIdName;
    public String executiveIdSeries;
    public String executiveIdNumber;
    public DateForm executiveIdIssueDate = new DateForm();
    public String executiveIdIssueAgencyName;
    public String executiveIdIssueAgencyCode;
    public Boolean executiveResident;
    public String executiveMigrationSeries;
    public String executiveMigrationNumber;
    public DateForm executiveMigrationDateStart = new DateForm();
    public DateForm executiveMigrationDateEnd = new DateForm();

    @Override
    public void getData(ProjectRequest from) {
        BeanUtils.copyProperties(from, this);
        if (executiveBornPlaceAddress == null) executiveBornPlaceAddress = new Address();
        if (executiveAddressRegistration == null) executiveAddressRegistration = new Address();
        if (executiveBornDate == null) executiveBornDate = new DateForm();
        if (executiveIdIssueDate == null) executiveIdIssueDate = new DateForm();
        if (executiveMigrationDateStart == null) executiveMigrationDateStart = new DateForm();
        if (executiveMigrationDateEnd == null) executiveMigrationDateEnd = new DateForm();
        if (executiveResident == null) executiveResident = true;
        UtilsService.securityVueJS(this);
    }

    @Override
    public void saveData(ProjectRequest to) {
        UtilsService.securityVueJS(this);
        BeanUtils.copyProperties(this, to);
    }
}
