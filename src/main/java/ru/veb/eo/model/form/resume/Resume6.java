package ru.veb.eo.model.form.resume;

import com.google.common.base.Strings;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.thymeleaf.util.ArrayUtils;
import ru.veb.eo.model.crm.*;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.service.UtilsService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
public class Resume6 implements Form {
    public String back = null;
    public String save = null;
    public String next = null;
    public int numberPage;
    private String projectId;

    public String projectProjectIsAimedTo;
    public String projectSectors1;
    public String projectEffectEmployment;
    public String projectWorkPlaceCount;

    public void setProjectWorkPlaceCount(String projectWorkPlaceCount) {
        if (!Strings.isNullOrEmpty(projectWorkPlaceCount)) {
            this.projectWorkPlaceCount = projectWorkPlaceCount;
            this.projectEffectEmployment = projectWorkPlaceCount;
        }
    }

    public void setProjectEffectEmployment(String projectEffectEmployment) {
        if (!Strings.isNullOrEmpty(projectEffectEmployment)) {
            this.projectEffectEmployment = projectEffectEmployment;
            this.projectWorkPlaceCount = projectEffectEmployment;
        }
    }

    public String projectEnvironmentalProtection;
    public String projectTechnologiesImplementation;
    public String projectTechnologicalOpportunities;

    public Set<Integer> projectStateSupport = new HashSet<>();

    public String projectImpactInfoAvailabilityOfStateSupportComment0;
    public String projectImpactInfoAvailabilityOfStateSupportComment1;
    public String projectImpactInfoAvailabilityOfStateSupportComment2;

    private Boolean val9;
    private Boolean govSupportNot;

    private Boolean val10;
    private Boolean govSupport1;

    private Boolean val12;
    private Boolean govSupport2;

    private Boolean val14;
    private Boolean govSupport3;

    public void setVal9(Boolean val) {
        this.val9 = val;
        if (val) projectStateSupport = new HashSet<>();
        this.govSupportNot = val;
    }

    public void setVal10(Boolean val) {
        this.val10 = val;
        projectStateSupport.add(0);
        this.govSupport1 = val;
        if (val != null && val) {
            val9 = false;
            govSupportNot = false;
        }
    }

    public void setVal12(Boolean val) {
        this.val12 = val;
        projectStateSupport.add(1);
        this.govSupport2 = val;
        if (val != null && val) {
            val9 = false;
            govSupportNot = false;
        }
    }

    public void setVal14(Boolean val) {
        this.val14 = val;
        projectStateSupport.add(2);
        this.govSupport3 = val;
        if (val != null && val) {
            val9 = false;
            govSupportNot = false;
        }
    }

    public void setProjectStateSupport(Set<Integer> projectStateSupport) {
        this.projectStateSupport = projectStateSupport;
        this.val9 = false;
        this.val10 = false;
        this.val12 = false;
        this.val14 = false;
        this.govSupportNot = false;
        this.govSupport1 = false;
        this.govSupport2 = false;
        this.govSupport3 = false;

        if (projectStateSupport == null) {
            this.val9 = true;
            this.govSupportNot = true;
        } else {
            if (projectStateSupport.contains(0)) {
                this.val10 = true;
                this.govSupport1 = true;
            }
            if (projectStateSupport.contains(1)) {
                this.val12 = true;
                this.govSupport2 = true;
            }
            if (projectStateSupport.contains(2)) {
                this.val14 = true;
                this.govSupport3 = true;
            }
        }
    }

    @Override
    public void getData(ProjectRequest from) {
        BeanUtils.copyProperties(from, this);
        if (this.projectStateSupport == null) this.projectStateSupport = new HashSet<>();
        UtilsService.securityVueJS(this);
    }

    @Override
    public void saveData(ProjectRequest to) {
        UtilsService.securityVueJS(this);
        BeanUtils.copyProperties(this, to);
    }
}
