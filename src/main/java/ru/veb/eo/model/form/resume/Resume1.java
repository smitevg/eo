package ru.veb.eo.model.form.resume;

import com.google.common.base.Strings;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import ru.veb.eo.model.crm.Address;
import ru.veb.eo.model.crm.DateForm;
import ru.veb.eo.model.crm.ProjectRequest;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.service.UtilsService;

@Data
public class Resume1 implements Form {
    public String back = null;
    public String save = null;
    public String next = null;
    public int numberPage;
    private String projectId;

    public String initiatorOrganizationalLegalForm;
    public String initiatorNameRus;
    public String initiatorOgrn;
    public String initiatorInn;
    public String initiatorKpp;
    public Address initiatorAddressRegistration = new Address();
    public Address initiatorMainActivityAddress = new Address();
    public String initiatorActivity;
    public String initiatorActivityTerm;
    public String initiatorEmployeesNumber;
    public String initiatorOwnersChain;

    @Override
    public void getData(ProjectRequest from) {
        BeanUtils.copyProperties(from, this);
        if (initiatorAddressRegistration == null) initiatorAddressRegistration = new Address();
        if (initiatorMainActivityAddress == null) initiatorMainActivityAddress = new Address();
        UtilsService.securityVueJS(this);
    }

    @Override
    public void saveData(ProjectRequest to) {
        UtilsService.securityVueJS(this);
        BeanUtils.copyProperties(this, to);
    }
}
