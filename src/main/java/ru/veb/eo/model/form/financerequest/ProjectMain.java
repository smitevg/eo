package ru.veb.eo.model.form.financerequest;

import com.google.common.base.Strings;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import ru.veb.eo.model.crm.Participant;
import ru.veb.eo.model.crm.ProjectRequest;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.service.UtilsService;

import java.util.ArrayList;
import java.util.List;

@Data
public class ProjectMain implements Form {
    public String back = null;
    public String save = null;
    public String next = null;
    public int numberPage;
    private String projectId;

    public String projectNomination;
    public String projectTarget;
    public String projectExpectedResult;
    public String projectDescription;
    public String projectSalePlace;
    public String projectType1;
    public String projectType1Custom;

    public List<String> projectSalePlaceCustom;

    public void setProjectSalePlace(String projectSalePlace) {
        if (!Strings.isNullOrEmpty(projectSalePlace)) {
            String[] arr = projectSalePlace.split(",");
            this.projectSalePlaceCustom = new ArrayList<String>();
            for (int i = 0; i < arr.length; i++) {
                if (i == 0) {
                    this.projectSalePlace = arr[i];
                } else {
                    this.projectSalePlaceCustom.add(arr[i]);
                }
            }
        } else {
            this.projectSalePlace = null;
        }
    }

    public String projectExperienceProjectManagers;
    public String projectProjectDecisionDateAndLevel;
    public String projectReadinessOfProjectDocumentation;
    public String projectProjectImplementationActivities;


    @Override
    public void getData(ProjectRequest from) {
        BeanUtils.copyProperties(from, this);
        UtilsService.securityVueJS(this);
    }

    @Override
    public void saveData(ProjectRequest to) {

        UtilsService.securityVueJS(this);
        BeanUtils.copyProperties(this, to);
    }
}
