package ru.veb.eo.model.form.resume;

import com.google.common.base.Strings;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import ru.veb.eo.model.crm.Address;
import ru.veb.eo.model.crm.DateForm;
import ru.veb.eo.model.crm.ProjectRequest;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.service.UtilsService;

@Data
public class Resume3 implements Form {
    public String back = null;
    public String save = null;
    public String next = null;
    public int numberPage;
    private String projectId;

    public String applicantNameLast;
    public String applicantNameFirst;
    public String applicantNameMid;
    public String employeePosition;
    public String employeePhoneNumber;
    public String employeeFaxNumber;
    public String applicantEmail;

    @Override
    public void getData(ProjectRequest from) {
        BeanUtils.copyProperties(from, this);
//        if (Strings.isNullOrEmpty(employeeNameLast)) employeeNameLast = from.getApplicantNameLast();
//        if (Strings.isNullOrEmpty(employeeNameFirst)) employeeNameFirst = from.getApplicantNameFirst();
//        if (Strings.isNullOrEmpty(employeeNameMid)) employeeNameMid = from.getApplicantNameMid();
        if (Strings.isNullOrEmpty(employeePhoneNumber)) employeePhoneNumber = from.getApplicantPhoneNumber();
        if (Strings.isNullOrEmpty(employeeFaxNumber)) employeeFaxNumber = from.getApplicantPhoneNumber();
//        if (Strings.isNullOrEmpty(employeeEmail)) employeeEmail = from.getApplicantEmail();
        UtilsService.securityVueJS(this);
    }

    @Override
    public void saveData(ProjectRequest to) {
        UtilsService.securityVueJS(this);
        BeanUtils.copyProperties(this, to);
    }
}
