package ru.veb.eo.model.form.resume;

import com.google.common.base.Strings;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.thymeleaf.util.ArrayUtils;
import ru.veb.eo.model.crm.*;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.service.UtilsService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
public class Resume8 implements Form {
    public String back = null;
    public String save = null;
    public String next = null;
    public int numberPage;
    private String projectId;

    private String where_send;
    private String dzo_send[];

    @Override
    public void getData(ProjectRequest from) {
        BeanUtils.copyProperties(from, this);
        UtilsService.securityVueJS(this);
    }

    @Override
    public void saveData(ProjectRequest to) {
        UtilsService.securityVueJS(this);
        BeanUtils.copyProperties(this, to);
    }
}
