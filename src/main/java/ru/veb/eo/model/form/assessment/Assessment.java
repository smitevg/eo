package ru.veb.eo.model.form.assessment;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import ru.veb.eo.model.crm.ProjectRequest;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.service.UtilsService;

@Data
@JsonPropertyOrder(alphabetic = true)
public class Assessment implements Form {
    public String back = null;
    public String save = null;
    public String next = null;
    public int numberPage;
    private String projectId;

    public Boolean fn_reputationCheck;
    public String fn_reputationDesc;

    public Boolean fn_costCheck;
    public String fn_costDesc;

    public Boolean fn_paybackTimeCheck;
    public String fn_paybackTimeDesc;

    public Boolean fn_industryCheck;
    public String fn_industryDesc;

    public Boolean fn_strategyCheck;
    public String fn_strategyDesc;

    public Integer fn_participationValue;
    public String fn_participationValueString;

    public Boolean fn_breakEvenCheck;
    public String fn_breakEvenDesc;


    public String nomination;
    public String last_name;
    public String phone_number;
    public String first_name;
    public String mid_name;
    public String email1;
    public String face_type;
    public Boolean resident;
    public String firm_name_rus;
    public String registration_address;
    public String factual_address;
    public String project_target;
    public String expected_result;
    public String project_type;
    public String project_description;
    public String sale_place;
    public String project_sectors;
    public Double project_cost;
    public String project_payback_time;
    public Double last_cost;
    public Double financing_request_amount;


    @Override
    public void getData(ProjectRequest from) {

        this.fn_reputationCheck = from.getExpressValuating1() != null ? from.getExpressValuating1() == 1 : null;
        this.fn_costCheck = from.getExpressValuating2() != null ? from.getExpressValuating2() == 1 : null;
        this.fn_paybackTimeCheck = from.getExpressValuating3() != null ? from.getExpressValuating3() == 1 : null;
        this.fn_industryCheck = from.getExpressValuating4() != null ? from.getExpressValuating4() == 1 : null;
        this.fn_strategyCheck = from.getExpressValuating5() != null ? from.getExpressValuating5() == 1 : null;
        this.fn_breakEvenCheck = from.getExpressValuating7() != null ? from.getExpressValuating7() == 1 : null;

        this.fn_reputationDesc = from.getExpressValuating1Description();
        this.fn_costDesc = from.getExpressValuating2Description();
        this.fn_paybackTimeDesc = from.getExpressValuating3Description();
        this.fn_industryDesc = from.getExpressValuating4Description();
        this.fn_strategyDesc = from.getExpressValuating5Description();
//        this.fn_investedDesc = from.getExpressValuating8Description();
        //System.out.println("this.fn_participationValueString" + this.fn_participationValueString);
        //System.out.println("from.getExpressValuating6Description()" + from.getExpressValuating6Description());
        if (from.getExpressValuating6Description() != null) {
            this.fn_participationValue = Integer.valueOf(from.getExpressValuating6Description().replaceAll("%", "").replaceAll(" ", ""));
        }
        this.fn_breakEvenDesc = from.getExpressValuating7Description();

        this.nomination = from.getProjectNomination();
        this.last_name = from.getApplicantNameLast();
        this.phone_number = from.getApplicantPhoneNumber();
        this.first_name = from.getApplicantNameFirst();
        this.mid_name = from.getApplicantNameMid();
        this.email1 = from.getApplicantEmail();
        this.face_type = from.getInitiatorOrganizationalLegalForm();
        this.resident = from.getInitiatorResident();
        this.firm_name_rus = from.getInitiatorNameRus();
        this.registration_address = from.getInitiatorAddressRegistration() != null ? from.getInitiatorAddressRegistration().name : null;
        this.factual_address = from.getInitiatorAddressFactual()!= null ? from.getInitiatorAddressFactual().name : null;
        this.project_target = from.getProjectTarget();
        this.expected_result = from.getProjectExpectedResult();
        this.project_type = from.getProjectType();
        this.project_description = from.getProjectDescription();
        this.sale_place = from.getProjectSalePlace();
        this.project_sectors = from.getProjectSectors();
        this.project_cost = from.getProjectProjectCostDouble();
        this.project_payback_time = from.getProjectPaybackTime();
        this.last_cost = from.getProjectAmountRecipientsCash();//Наличие вложенных средств
        this.financing_request_amount = from.getProjectAmountFinancingRequest();

        UtilsService.securityVueJS(this);
    }

    @Override
    public void saveData(ProjectRequest to) {
        UtilsService.securityVueJS(this);

        to.setExpressValuating1(this.fn_reputationCheck ? 1 : 0);
        to.setExpressValuating1Description(this.fn_reputationDesc);

        to.setExpressValuating2(this.fn_costCheck ? 1 : 0);
        to.setExpressValuating2Description(this.fn_costDesc);

        to.setExpressValuating3(this.fn_paybackTimeCheck ? 1 : 0);
        to.setExpressValuating3Description(this.fn_paybackTimeDesc);

        to.setExpressValuating4(this.fn_industryCheck ? 1 : 0);
        to.setExpressValuating4Description(this.fn_industryDesc);

        to.setExpressValuating5(this.fn_strategyCheck ? 1 : 0);
        to.setExpressValuating5Description(this.fn_strategyDesc);

        //System.out.println("fn_participationValueString" + fn_participationValueString);
        //System.out.println("Double.valueOf(fn_participationValueString.replaceAll(\",\", \".\").replaceAll(\"[^0-9,\\\\.]+\", \"\")).intValue()" + Double.valueOf(fn_participationValueString.replaceAll(",", ".").replaceAll("[^0-9,\\.]+", "")).intValue());
        //System.out.println("fn_participationValueString.replaceAll(\",\", \".\")" + fn_participationValueString.replaceAll(",", "."));
        //System.out.println("fn_participationValueString.replaceAll(\",\", \".\").replaceAll(\"[^0-9,\\\\.]+\", \"\")" + fn_participationValueString.replaceAll(",", ".").replaceAll("[^0-9,\\.]+", ""));
        //System.out.println("(fn_participationValueString.replaceAll(\",\", \".\").replaceAll(\"[^0-9,\\\\.]+\", \"\")).intValue()" + (fn_participationValueString.replaceAll(",", ".").replaceAll("[^0-9,\\.]+", "")).intValue());
        fn_participationValue = Double.valueOf(fn_participationValueString.replaceAll(",", ".").replaceAll("[^0-9,\\.]+", "")).intValue();
        //System.out.println("fn_participationValue" + fn_participationValue);
        fn_participationValueString = String.valueOf(fn_participationValue);
        if (this.fn_participationValue != null && this.fn_participationValue >= 15)
            to.setExpressValuating6(1);
        else
            to.setExpressValuating6(0);

        to.setExpressValuating6Description(this.fn_participationValue.toString());

        to.setExpressValuating7(this.fn_breakEvenCheck ? 1 : 0);
        to.setExpressValuating7Description(this.fn_breakEvenDesc);
    }

}
