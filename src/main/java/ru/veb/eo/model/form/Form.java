package ru.veb.eo.model.form;

import ru.veb.eo.model.crm.ProjectRequest;

public interface Form {
	void getData(ProjectRequest from);
	void saveData(ProjectRequest to);
	String getNext();
	String getBack();
	String getSave();
	void setNext(String value);
	void setBack(String value);
	void setSave(String value);
	int getNumberPage();
	void setNumberPage(int page);
    String getProjectId();
    void setProjectId(String projectId);
}
