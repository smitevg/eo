package ru.veb.eo.model.form.resume;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Strings;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.thymeleaf.util.ArrayUtils;
import ru.veb.eo.model.crm.*;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.service.UtilsService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
public class Resume7 implements Form {
    public String back = null;
    public String save = null;
    public String next = null;
    public int numberPage;
    private String projectId;

    public Double projectProjectCostDouble;
    public String projectCostCurrency;
    public Integer projectProjectPaybackTimeDouble;
    public Double projectAmountFinancingRequest; // дубль projectAmountBanksCash
    public Double projectAmountRecipientsCash;
    public Double projectAmountRecipientsPart;
    public Double projectAmountBanksCash;
    public Double projectAmountBanksPart;
    public Double projectAmountInvestorCash;

    public Long amountInvestor[];
    public Long amountBondArray[];
    public Long amountFinanceArray[];
    public String amountFinanceSourceArray[];
    public List<List<String>> projectAmountInvestorArray;
    public List<List<String>> projectAmountBondArray;
    public List<List<String>> projectAmountFinanceArray;

    public Double projectAmountInvestorPart;
    public Double projectAmountBondCash;
    public Double projectAmountBondPart;
    public Double projectAmountOthersCash;
    public Double projectAmountOthersPart;
    public String projectFinancingSourcesOther;
    public String projectNetPresentValue;

    public void setProjectNetPresentValue(String projectNetPresentValue) {
        if (!Strings.isNullOrEmpty(projectNetPresentValue))
            this.projectNetPresentValue = projectNetPresentValue.replaceAll(" ", "");
    }

    public String projectAdditionalInfo;

    public List<Participant> participants;
    public List<IndividualPerson> participantsIndividuals;
    private String id[]; // id
    private String type[]; // физ лицо, юр лицо
    private String status[]; // роль участника
    private String resident[]; // резидент
    // физическое лицо
    private String nameLast[];
    private String nameFirst[];
    private String nameMid[];
    private String bornDate_day[];
    private String bornDate_month[];
    private String bornDate_year[];
    private String bornPlace_name[];
    private String citizenship[];
    // место жительства, регистрации
    private String addressRegistration_region[]; // регион
    private String addressRegistration_city[]; // населенный пункт
    private String addressRegistration_street[]; // улица
    private String addressRegistration_house[]; // дом
    private String addressRegistration_building[]; // корпус
    private String addressRegistration_apartment[]; // квартира
    private String addressRegistration_index[]; // индекс
    // паспортные данные
    private String idName[]; // тип документа
    private String idSeries[]; // серия
    private String idNumber[]; // номер
    private String idIssueDate_day[]; // день выдачи
    private String idIssueDate_month[]; // месяц выдачи
    private String idIssueDate_year[]; // год выдачи
    private String idIssueAgencyName[]; // кем выдан
    private String idIssueAgencyCode[]; // код подразделения
    // юридическое лицо
    private String organizationType[]; // ОПФ
    private String organizationTitle[]; // наименовани
    private String ogrn[]; // огрн
    private String inn[]; // инн
    private String kpp[]; // КПП
    private String kio[]; // КПП
    private String nalogNumber[]; // налоговый номер
    private String regNumber[]; // регистрационный номер

    private Double volume[]; // объём вложеннх средств
    private String rules[]; // условия


    private String itemsId[];
    private String itemsType[];
    private String itemsGuarantor[];
    private Double itemsValue[];
    private String itemsCurrency[];
    private String itemsDepositType[];
    private String itemsDepositWho[];
    private String itemsDepositCompany[];
    private String itemsDepositCompanyDay[];
    private String itemsDepositCompanyMonth[];
    private String itemsDepositCompanyYear[];

    public List<Supply> supplies;

    public void setProjectAmountFinancingRequest(Double projectAmountFinancingRequest) {
        if (projectAmountFinancingRequest != null) {
            this.projectAmountFinancingRequest = projectAmountFinancingRequest;
            this.projectAmountBanksCash = projectAmountFinancingRequest;
        }
    }

    public void setProjectAmountBanksCash(Double projectAmountBanksCash) {
        if (projectAmountBanksCash != null) {
            this.projectAmountFinancingRequest = projectAmountBanksCash;
            this.projectAmountBanksCash = projectAmountBanksCash;
        }
    }

    // decision
    private Integer projectRequestStatus;

    @Override
    public void getData(ProjectRequest from) {
        BeanUtils.copyProperties(from, this);
        if (this.supplies == null) this.supplies = new ArrayList<>();
        if (this.projectAmountInvestorArray == null) this.projectAmountInvestorArray = new ArrayList<>();
        if (this.projectAmountBondArray == null) this.projectAmountBondArray = new ArrayList<>();
        if (this.projectAmountFinanceArray == null) this.projectAmountFinanceArray = new ArrayList<>();
        if (this.participants == null) this.participants = new ArrayList<>();
        if (this.participantsIndividuals == null) this.participantsIndividuals = new ArrayList<>();

        UtilsService.securityVueJS(this);
    }

    @Override
    public void saveData(ProjectRequest to) {

        UtilsService.securityVueJS(this);

        if (!ArrayUtils.isEmpty(amountInvestor)) {
            projectAmountInvestorArray = new ArrayList<>();
            for (Long aDouble : amountInvestor) {
                if (aDouble > 0) {
                    List<String> el = new ArrayList<>();
                    el.add(String.format("%d", aDouble));
                    el.add(Double.toString(100d * aDouble / projectProjectCostDouble));
                    projectAmountInvestorArray.add(el);
                }
            }
        }

        if (!ArrayUtils.isEmpty(amountBondArray)) {
            projectAmountBondArray = new ArrayList<>();
            for (Long aDouble : amountBondArray) {
                if (aDouble > 0) {
                    List<String> el = new ArrayList<>();
                    el.add(String.format("%d", aDouble));
                    el.add(Double.toString(100d * aDouble / projectProjectCostDouble));
                    projectAmountBondArray.add(el);
                }
            }
        }

        if (!ArrayUtils.isEmpty(amountFinanceArray)) {
            projectAmountFinanceArray = new ArrayList<>();
            for (int i = 0; i < amountFinanceArray.length; i++) {
                if (amountFinanceArray[i] > 0) {
                    List<String> el = new ArrayList<>();
                    el.add(amountFinanceSourceArray[i]);
                    el.add(String.format("%d", amountFinanceArray[i]));
                    el.add(Double.toString(100d * amountFinanceArray[i] / projectProjectCostDouble));
                    projectAmountFinanceArray.add(el);
                }
            }
        }

        try {
            supplies = new ArrayList<>();
            if (!ArrayUtils.isEmpty(itemsValue)) {
                for (int i = 0; i < itemsValue.length; i++) {

                    Supply supply = new Supply();
                    if ("0".equals(itemsType[i])) {
                        supply.setType(TypeSupply.BANK);
                        supply.setValue(itemsValue[i]);
                    } else if ("1".equals(itemsType[i])) {
                        supply.setType(TypeSupply.GUARANTEE);
                        supply.setValue(itemsValue[i]);
                    } else if ("2".equals(itemsType[i])) {
                        supply.setType(TypeSupply.SURETY);
                        supply.setValue(itemsValue[i]);
                    } else if ("3".equals(itemsType[i])) {
                        supply.setType(TypeSupply.PLEDGE);
                        supply.setValue(itemsValue[i]);
                        supply.setDepositWho(itemsDepositWho[i]);
                        supply.setDepositCompany(itemsDepositCompany[i]);
                        try {
                            if (!Strings.isNullOrEmpty(itemsDepositCompanyDay[i]) && !Strings.isNullOrEmpty(itemsDepositCompanyMonth[i]) && !Strings.isNullOrEmpty(itemsDepositCompanyYear[i]))
                                supply.setDepositDate(new DateForm(Integer.valueOf(itemsDepositCompanyDay[i]), Integer.valueOf(itemsDepositCompanyMonth[i]), Integer.valueOf(itemsDepositCompanyYear[i])));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if ("4".equals(itemsType[i])) {
                        supply.setType(TypeSupply.OTHER);
                        supply.setValue(itemsValue[i]);
                    }
                    supply.setId(Strings.isNullOrEmpty(itemsId[i]) ? null : itemsId[i]);
                    supply.setGuarantor(itemsGuarantor[i]);
                    supply.setCurrency(itemsCurrency[i]);
                    if (!Strings.isNullOrEmpty(supply.getCurrency()) && itemsValue[i] > 0) supplies.add(supply);


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (null != projectProjectCostDouble && 0 != projectProjectCostDouble) {
            projectAmountRecipientsPart = 100d * projectAmountRecipientsCash / projectProjectCostDouble;
            projectAmountBanksPart = 100d * projectAmountBanksCash / projectProjectCostDouble;
        }

        if (volume != null) {
            this.participants = new ArrayList<>();
            this.participantsIndividuals = new ArrayList<>();
            for (int i = 0; i < volume.length; i++) {

                try {
                    switch (this.type[i]){
                        case "ul":
                            Participant participant = new Participant();
                            participant.setId(Strings.isNullOrEmpty(id[i]) ? null : id[i]);
                            participant.setInn(Strings.isNullOrEmpty(inn[i]) ? null : inn[i]);
                            participant.setNalogNumber(Strings.isNullOrEmpty(nalogNumber[i]) ? null : nalogNumber[i]);
                            participant.setOgrn(Strings.isNullOrEmpty(ogrn[i]) ? null : ogrn[i]);
                            participant.setOrganizationTitle(Strings.isNullOrEmpty(organizationTitle[i]) ? null : organizationTitle[i]);
                            participant.setOrganizationType(Strings.isNullOrEmpty(organizationType[i]) ? null : organizationType[i]);
                            participant.setRegNumber(Strings.isNullOrEmpty(regNumber[i]) ? null : regNumber[i]);
                            participant.setResident(Strings.isNullOrEmpty(resident[i]) ? null : "true".equals(resident[i]));
                            participant.setRules(Strings.isNullOrEmpty(rules[i]) ? null : rules[i]);
                            participant.setKpp(Strings.isNullOrEmpty(kpp[i]) ? null : kpp[i]);
                            participant.setStatus(Strings.isNullOrEmpty(status[i]) ? null : status[i]);
                            participant.setVolume(volume[i] == null ? null : volume[i]);
                            this.participants.add(participant);
                            break;
                        case "fl":
                            IndividualPerson individualPerson = new IndividualPerson();
                            individualPerson.setNameFirst(Strings.isNullOrEmpty(this.nameFirst[i]) ? null : this.nameFirst[i]);
                            individualPerson.setNameLast(Strings.isNullOrEmpty(this.nameLast[i]) ? null : this.nameLast[i]);
                            individualPerson.setNameMid(Strings.isNullOrEmpty(this.nameMid[i]) ? null : this.nameMid[i]);
                            if(!Strings.isNullOrEmpty(this.bornDate_day[i]) && !Strings.isNullOrEmpty(this.bornDate_month[i]) && !Strings.isNullOrEmpty(this.bornDate_year[i])){
                                individualPerson.setBornDate(new DateForm(
                                        Strings.isNullOrEmpty(this.bornDate_day[i]) ? null : Integer.valueOf(this.bornDate_day[i]),
                                        Strings.isNullOrEmpty(this.bornDate_month[i]) ? null : Integer.valueOf(this.bornDate_month[i]),
                                        Strings.isNullOrEmpty(this.bornDate_year[i]) ? null : Integer.valueOf(this.bornDate_year[i]))
                                );
                            }

                            Address addressBorn = new Address();
                            addressBorn.setName(Strings.isNullOrEmpty(this.bornPlace_name[i]) ? null : this.bornPlace_name[i]);
                            individualPerson.setBornPlace(addressBorn);

                            individualPerson.setCitizenship(Strings.isNullOrEmpty(this.citizenship[i]) ? null : this.citizenship[i]);
                            individualPerson.setResident(Boolean.valueOf(this.resident[i]));

                            Address addressRegistration = new Address();
                            addressRegistration.setRegion(Strings.isNullOrEmpty(this.addressRegistration_region[i]) ? null : this.addressRegistration_region[i]);
                            addressRegistration.setCity(Strings.isNullOrEmpty(this.addressRegistration_city[i]) ? null : this.addressRegistration_city[i]);
                            addressRegistration.setStreet(Strings.isNullOrEmpty(this.addressRegistration_street[i]) ? null : this.addressRegistration_street[i]);
                            addressRegistration.setHouse(Strings.isNullOrEmpty(this.addressRegistration_house[i]) ? null : this.addressRegistration_house[i]);
                            addressRegistration.setBuilding(Strings.isNullOrEmpty(this.addressRegistration_building[i]) ? null : this.addressRegistration_building[i]);
                            addressRegistration.setApartment(Strings.isNullOrEmpty(this.addressRegistration_apartment[i]) ? null : this.addressRegistration_apartment[i]);
                            addressRegistration.setIndex(Strings.isNullOrEmpty(this.addressRegistration_index[i]) ? null : this.addressRegistration_index[i]);
                            individualPerson.setAddressRegistration(addressRegistration);

                            individualPerson.setIdName(Strings.isNullOrEmpty(this.idName[i]) ? null : this.idName[i]);
                            individualPerson.setIdSeries(Strings.isNullOrEmpty(this.idSeries[i]) ? null : this.idSeries[i]);
                            individualPerson.setIdNumber(Strings.isNullOrEmpty(this.idNumber[i]) ? null : this.idNumber[i]);
                            if(!Strings.isNullOrEmpty(this.idIssueDate_day[i]) && !Strings.isNullOrEmpty(this.idIssueDate_month[i]) && !Strings.isNullOrEmpty(this.idIssueDate_year[i])){
                                individualPerson.setIdIssueDate(new DateForm(
                                        Strings.isNullOrEmpty(this.idIssueDate_day[i]) ? null : Integer.valueOf(this.idIssueDate_day[i]),
                                        Strings.isNullOrEmpty(this.idIssueDate_month[i]) ? null : Integer.valueOf(this.idIssueDate_month[i]),
                                        Strings.isNullOrEmpty(this.idIssueDate_year[i]) ? null : Integer.valueOf(this.idIssueDate_year[i]))
                                );
                            }
                            individualPerson.setIdIssueAgencyName(Strings.isNullOrEmpty(this.idIssueAgencyName[i]) ? null : this.idIssueAgencyName[i]);
                            individualPerson.setIdIssueAgencyCode(Strings.isNullOrEmpty(this.idIssueAgencyCode[i]) ? null : this.idIssueAgencyCode[i]);

                            individualPerson.setRules(Strings.isNullOrEmpty(rules[i]) ? null : rules[i]);
                            individualPerson.setStatus(Strings.isNullOrEmpty(status[i]) ? null : status[i]);
                            individualPerson.setVolume(volume[i] == null ? null : volume[i]);

                            this.participantsIndividuals.add(individualPerson);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        BeanUtils.copyProperties(this, to);
    }
}
