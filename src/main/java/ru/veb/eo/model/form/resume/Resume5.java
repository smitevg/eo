package ru.veb.eo.model.form.resume;

import com.google.common.base.Strings;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.thymeleaf.util.ArrayUtils;
import ru.veb.eo.model.crm.*;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.service.UtilsService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
public class Resume5 implements Form {
    public String back = null;
    public String save = null;
    public String next = null;
    public int numberPage;
    private String projectId;

    public String projectNomination;
    public String projectTarget;
    public String projectExpectedResult;
    public String projectType1;
    public String projectType1Custom;
    public String projectDescription;
    public String projectSalePlace;
    private List<String> projectSalePlaceCustom;
    public String projectExperienceProjectManagers;
    public String projectDegree;

    public void setProjectSalePlace(String projectSalePlace) {
        if (!Strings.isNullOrEmpty(projectSalePlace)) {
            String[] arr = projectSalePlace.split(",");
            this.projectSalePlaceCustom = new ArrayList<String>();
            for (int i = 0; i < arr.length; i++) {
                if (i == 0) {
                    this.projectSalePlace = arr[i];
                } else {
                    this.projectSalePlaceCustom.add(arr[i]);
                }
            }
        } else {
            this.projectSalePlace = null;
        }
    }

    @Override
    public void getData(ProjectRequest from) {
        BeanUtils.copyProperties(from, this);
        UtilsService.securityVueJS(this);
    }

    @Override
    public void saveData(ProjectRequest to) {
        UtilsService.securityVueJS(this);
        BeanUtils.copyProperties(this, to);
    }
}
