package ru.veb.eo.model.form.sindrequest;

import lombok.Data;

@Data
public class SindAdminEdit {
    private String id;
    private String dateBegin;
    private String dateEnd;
    private String adminProj;
    private String toArch;
}
