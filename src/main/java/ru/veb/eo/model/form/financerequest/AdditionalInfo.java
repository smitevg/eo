package ru.veb.eo.model.form.financerequest;

import lombok.Data;
import org.springframework.beans.BeanUtils;
import ru.veb.eo.model.crm.ProjectRequest;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.service.UtilsService;

@Data
public class AdditionalInfo implements Form {
    public String back = null;
    public String save = null;
    public String next = null;
    public int numberPage;
    private String projectId;

    public String projectAdditionalInfo;

    @Override
    public void getData(ProjectRequest from) {
        BeanUtils.copyProperties(from, this);
        UtilsService.securityVueJS(this);
    }

    @Override
    public void saveData(ProjectRequest to) {
        UtilsService.securityVueJS(this);
        BeanUtils.copyProperties(this, to);
    }

}
