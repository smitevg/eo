package ru.veb.eo.model.form;

import com.google.common.base.Strings;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import ru.veb.eo.model.crm.ProjectRequest;

import java.util.ArrayList;
import java.util.List;

@Data
public class ChangeManager implements Form {
    public String back = null;
    public String save = null;
    public String next = null;
    public int numberPage;
    private String projectId;

    public String projectCertifiedManagerId;


    @Override
    public void getData(ProjectRequest from) {
        BeanUtils.copyProperties(from, this);
    }

    @Override
    public void saveData(ProjectRequest to) {
        BeanUtils.copyProperties(this, to);
    }
}
