package ru.veb.eo.model.form.sindrequest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.pdf.BaseFont;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.itext.extension.font.IFontProvider;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import lombok.Data;
import lombok.Getter;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import ru.veb.eo.VebEoApplication;
import ru.veb.eo.entity.Project;
import ru.veb.eo.entity.UserProject;
import ru.veb.eo.entity.UserRoleAppend;
import ru.veb.eo.model.User;
import ru.veb.eo.model.crm.Manager;
import ru.veb.eo.model.crm.ProjectRequest;
import ru.veb.eo.repository.UserRepository;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
public class SindAdmRequest {
    //все с ролью SIND или FAB
    @Getter
    public List<Manager> listUserSind;

    //пользователи проекта
    @Getter
    public List<UserProject> listUserProject;

    @Getter
    public List<Project> listProject;

    @Getter
    @JsonProperty("selectProject")
    public Project selectProject;

    // Установка выбранного проекта
    public void processSelectProject(String id){
        for(Project project : listProject){
            if (project.getProjectId().equals(id)){
                selectProject = project;
            }
        }
    }


    public List<Manager> listNotUseUserSind;

    // инициализация списка из базы
    public void initListUserSind(List<Manager> listUserSind) {
        this.listUserSind = listUserSind;
    }

    // голосование одного проекта
    // инициализация списка из базы
    public void initListUserProject(List<UserProject> listUserProject) {
        this.listUserProject = listUserProject;

        ////
        listNotUseUserSind = new ArrayList<Manager>();
        for(Manager r : listUserSind) {
            if (!listNotUseUserSind.contains(r)) {

                listNotUseUserSind.add(r);
            }
        }

        for (UserProject up : this.listUserProject) {
            Optional<Manager> m = listNotUseUserSind.stream().filter(e -> e.getCrmId().equals(up.getUserId())).findFirst();
            if (m.isPresent()) {
                Manager mm = m.get();
                up.setUsername(mm.getNameFirst()+"("+mm.getNameLast()+")");
            }
        }

        for (UserProject userProject : listUserProject) {
            listNotUseUserSind.removeIf(e -> e.getCrmId().equals(userProject.getUserId()));
        }
    }
    // инициализация списка из базы
    public void initListProject(List<Project> listProject) {
        this.listProject = listProject;
    }


}
