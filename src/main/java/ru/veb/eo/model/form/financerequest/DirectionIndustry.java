package ru.veb.eo.model.form.financerequest;

import lombok.Data;
import org.springframework.beans.BeanUtils;
import ru.veb.eo.model.crm.ProjectRequest;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.service.UtilsService;

@Data
public class DirectionIndustry implements Form {
    public String back = null;
    public String save = null;
    public String next = null;
    public int numberPage;
    private String projectId;

    public String projectProjectIsAimedTo;
    public String projectProjectIsAimedToCustom;
    public String projectProjectAimReason;
    public String projectProjectAdvantagesInMarket;
    public String projectSectors1;
    public String projectSectorsCustom;
    public String projectSectorsReason;

    @Override
    public void getData(ProjectRequest from) {
        BeanUtils.copyProperties(from, this);
        UtilsService.securityVueJS(this);
    }

    @Override
    public void saveData(ProjectRequest to) {
        UtilsService.securityVueJS(this);
        BeanUtils.copyProperties(this, to);
    }
}
