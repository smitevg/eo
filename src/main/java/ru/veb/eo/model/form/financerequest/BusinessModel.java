package ru.veb.eo.model.form.financerequest;

import lombok.Data;
import org.springframework.beans.BeanUtils;
import ru.veb.eo.model.crm.ProjectRequest;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.service.UtilsService;

@Data
public class BusinessModel implements Form {
    public String back = null;
    public String save = null;
    public String next = null;
    public int numberPage;
    private String projectId;

    public String projectProjectAdvisabilityRealization;
    public String projectProductMainQualitativeCharacteristics;
    public String projectTermsDeliveryPaymentExpected;
    public String projectOrderBook;
    public String projectRecipientAvailableResourcesEnum;
    public String projectDesignRisksAnalysis;

    @Override
    public void getData(ProjectRequest from) {
        BeanUtils.copyProperties(from, this);
        UtilsService.securityVueJS(this);
    }

    @Override
    public void saveData(ProjectRequest to) {
        UtilsService.securityVueJS(this);
        BeanUtils.copyProperties(this, to);
    }
}
