package ru.veb.eo.model.form.financerequest;

import com.google.common.base.Strings;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import ru.veb.eo.model.crm.DateForm;
import ru.veb.eo.model.crm.ProjectRequest;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.service.UtilsService;

import java.util.HashSet;
import java.util.Set;

@Data
public class Influence implements Form {
    public String back = null;
    public String save = null;
    public String next = null;
    public int numberPage;
    private String projectId;

    public Boolean projectImplementationCorrespondsToPriorities;
    public String projectProjectCorrespondenceSectors;
    public Boolean projectProjectImplementationPublicPrivatePartnership;
    public String projectAgreementBasicParameters;
    public Boolean projectProjectFinancingExpenseInvestmentFund;
    public String projectFinancingBasicTerms;

    public String projectEffectEmployment;
    public String projectEvaluationMethod;
    public String projectCalculationFormula;
    public String projectQualitativeApproachesDescription;
    public String projectCalculatingEffectComponents;
    public String projectConfirmingBudgetaryEffect;

    public String projectHaveFinancingAtRfBefore;
    public String projectProjectsAndConditionsForTheirFinancing;
    public String projectCanProjectFinancedByFinancialInstitutions;
    public String projectReasonsForRefusalOfFinancing;
    public String projectFinancialInstitutionsReadyToFinanceTheProject;
    public String projectWhichFinancialInstitutions;
    public String projectFinancialInstitutionsParticipationTermsInProject;
    public String projectWhyBanksParticipationNotDamageFinancialOrganizations;
    public String projectEnvironmentalProtection;
    public String projectEnvironmentalProtectionComment;

    public void setProjectEnvironmentalProtection(String projectEnvironmentalProtection) {
        this.projectEnvironmentalProtection = projectEnvironmentalProtection;
        if (!Strings.isNullOrEmpty(projectEnvironmentalProtection) && Strings.isNullOrEmpty(projectEnvironmentalProtectionComment)) {
            if ("true".equals(projectEnvironmentalProtection)) projectEnvironmentalProtectionComment = "да";
            else if ("false".equals(projectEnvironmentalProtection)) projectEnvironmentalProtectionComment = "нет";
        }
    }

    public String projectTypeOfAuthorizationDocument;
    public String projectAuthorizationDocumentIssuedBy;
    public DateForm projectAuthorizationDocumentIssuedDate;


    public String projectEnvironmentalExpertiseIssuedBy;
    public DateForm projectEnvironmentalExpertiseIssuedDate;
    public String projectEnvironmentalExpertiseConclusionNotRequired;
    public String projectEnvironmentalExpertiseIsNotRequired;
    public Boolean projectEnvironmentalExpertiseConclusionNotRequiredIs;
    public Boolean projectEnvironmentalExpertiseIsNotRequiredIs;

    public String projectTechnologiesImplementation;
    public String projectTechnologicalOpportunities;
    public Set<Integer> projectStateSupport = new HashSet<>();

    public String projectImpactInfoAvailabilityOfStateSupportComment0;
    public String projectImpactInfoAvailabilityOfStateSupportComment1;
    public String projectImpactInfoAvailabilityOfStateSupportComment2;

    public Boolean noSupport;
    public Boolean refundableSupport; // краткое обоснование
    public Boolean advanceSupport; // Привлечение авансовой меры господдержки
    public Boolean grbsDecision; // Есть решение ГРБС о получении

    public void setNoSupport(Boolean val) {
        this.noSupport = val;
        projectStateSupport = null;
    }

    public void setRefundableSupport(Boolean val) {
        this.refundableSupport = val;
        projectStateSupport.add(0);
        if (val != null && val) noSupport = false;
    }

    public void setAdvanceSupport(Boolean val) {
        this.advanceSupport = val;
        projectStateSupport.add(1);
        if (val != null && val) noSupport = false;
    }

    public void setGrbsDecision(Boolean val) {
        this.grbsDecision = val;
        projectStateSupport.add(2);
        if (val != null && val) noSupport = false;
    }

    public void setProjectStateSupport(Set<Integer> projectStateSupport) {
        this.projectStateSupport = projectStateSupport;
        this.noSupport = false;
        this.refundableSupport = false;
        this.advanceSupport = false;
        this.grbsDecision = false;

        if (projectStateSupport == null) {
            this.noSupport = true;
        } else {
            if (projectStateSupport.contains(0)) {
                this.refundableSupport = true;
            }
            if (projectStateSupport.contains(1)) {
                this.advanceSupport = true;
            }
            if (projectStateSupport.contains(2)) {
                this.grbsDecision = true;
            }
        }
    }

    @Override
    public void getData(ProjectRequest from) {
        BeanUtils.copyProperties(from, this);
        if (projectAuthorizationDocumentIssuedDate == null) projectAuthorizationDocumentIssuedDate = new DateForm();
        if (projectEnvironmentalExpertiseIssuedDate == null) projectEnvironmentalExpertiseIssuedDate = new DateForm();
        if (projectStateSupport == null) projectStateSupport = new HashSet<>();
        UtilsService.securityVueJS(this);
    }

    @Override
    public void saveData(ProjectRequest to) {
        UtilsService.securityVueJS(this);
        BeanUtils.copyProperties(this, to);
    }
}
