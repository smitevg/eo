package ru.veb.eo.model.form.firstrequest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import ru.veb.eo.model.crm.Address;
import ru.veb.eo.model.crm.Participant;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Data
@JsonPropertyOrder(alphabetic = true)
public class FirstRequest {

    private Integer projectRequestStatus;
    private String projectId;

    private String applicantNameLast;
    private String applicantNameFirst;
    private String applicantNameMid;
    private String applicantPhoneNumber;
    private String applicantEmail;

    private String initiatorNameShort;
    private Boolean initiatorResident;
    private String initiatorInn;
    private String initiatorKpp;
    private String initiatorKio;
    private Address initiatorAddressRegistration = new Address();
    private Address initiatorAddressFactual = new Address();

    private String initiatorAddressRegistrationName;

    public void setInitiatorAddressRegistrationName(String initiatorAddressRegistrationName) {
        this.initiatorAddressRegistrationName = initiatorAddressRegistrationName;
        this.initiatorAddressRegistration.name = initiatorAddressRegistrationName;
    }

    private String initiatorAddressFactualName;

    public void setInitiatorAddressFactualName(String initiatorAddressFactualName) {
        this.initiatorAddressFactualName = initiatorAddressFactualName;
        this.initiatorAddressFactual.name = initiatorAddressFactualName;
    }

    private String projectNomination;
    private String projectTarget;
    private String projectExpectedResult;
    private String projectType;
    private String projectTypeCustom;
    private String projectDescription;
    private String projectSalePlace;
    public String projectProjectIsAimedTo;
    public String projectSectors1;
    private String projectSectorsCustom;
    private String projectCost;
    private String projectPaybackTime;
    private String projectCertifiedManagerId;
    private Date projectDateCreating;
}
