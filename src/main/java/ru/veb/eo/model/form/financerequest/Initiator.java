package ru.veb.eo.model.form.financerequest;

import com.google.common.base.Strings;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import ru.veb.eo.model.crm.Address;
import ru.veb.eo.model.crm.DateForm;
import ru.veb.eo.model.crm.ProjectRequest;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.service.UtilsService;

import java.util.List;

@Data
public class Initiator implements Form {
    public String back = null;
    public String save = null;
    public String next = null;
    public int numberPage;
    private String projectId;

    public String initiatorFaceRoleAtProject;
    public Boolean initiatorResident;
    public String initiatorFaceType;

    public String initiatorOrganizationalLegalForm;
    public String initiatorNameRus;
    public String initiatorFirmNameRusShort;
    public String initiatorFirmNameForeign;
    public String initiatorFirmNameForeignShort;
    public String initiatorInn;
    public String initiatorKpp;
    public String initiatorKio;
    public String initiatorOgrn;
    public String initiatorRegistrationNumber;
    public Address initiatorAddressRegistration = new Address();
    public Address initiatorAddressFactual = new Address();
    public String initiatorAddressFactualConfirm;
    public Address initiatorMainActivityAddress = new Address();
    public String initiatorActivity;
    public String initiatorActivityTerm;
    public String initiatorEmployeesNumber;
    public String initiatorLastName;
    public String initiatorFirstName;
    public String initiatorMidName;
    public DateForm initiatorBornDate = new DateForm();
    public String initiatorBornPlace;
    public String initiatorCitizenship;
    public String initiatorDocumentName;
    public String initiatorDocumentSeries;
    public String initiatorDocumentNumber;
    public DateForm initiatorDocumentIssueDate = new DateForm();
    public String initiatorDocumentIssueWho;
    public String initiatorDocumentIssueWhoCode;
    public DateForm initiatorStateRegistrationDate = new DateForm();
    public String initiatorMigrationSeries;
    public String initiatorMigrationNumber;
    public DateForm initiatorMigrationTermStartDate = new DateForm();
    public DateForm initiatorMigrationTermEndDate = new DateForm();
    public String initiatorOwnersChain;


    public Boolean executiveControlCompanyDelegate;
    public String executiveControlCompanyDelegateType;
    public String executiveControlCompanyDelegateName;
    public String executiveNameLast;
    public String executiveNameFirst;
    public String executiveNameMid;
    public String executivePosition;
    public DateForm executiveBornDate = new DateForm();
    public Address executiveBornPlaceAddress = new Address();
    public String executiveCitizenship;
    public Address executiveAddressRegistration = new Address();
    public String executiveIdName;
    public String executiveIdSeries;
    public String executiveIdNumber;
    public DateForm executiveIdIssueDate = new DateForm();
    public String executiveIdIssueAgencyName;
    public String executiveIdIssueAgencyCode;
    public String executiveInfoActivitiesParticipation;


    public String employeeNameLast;
    public String employeeNameFirst;
    public String employeeNameMid;
    public String employeePosition;
    public String employeePhoneNumber;
    public String employeeFaxNumber;


    public String employeeEmail;

    public String initiatorPhoneNumber;
    public String initiatorFaxNumber;


    public String initiatorEmail;

    public Address initiatorPostalAddress = new Address();
    public String initiatorRequisitesReceiver;
    public String initiatorRequisitesReceiversAccount;
    public String initiatorRequisitesBankName;
    public String initiatorRequisitesBankPlace;
    public String initiatorRequisitesBankCorrAccount;
    public String initiatorRequisitesBankBik;
    public String initiatorRequisitesForeignCurrency;
    public String initiatorRequisitesForeignReceiver;
    public String initiatorRequisitesForeignReceiverAccount;
    public String initiatorRequisitesForeignBankName;
    public String initiatorRequisitesForeignBankPlace;
    public String initiatorRequisitesForeignBankSwift;
    public String initiatorRequisitesForeignBankCode;
    public String initiatorDissatisfiedClaims;
    public String initiatorTaxArrearsInfo;


    @Override
    public void getData(ProjectRequest from) {
        BeanUtils.copyProperties(from, this);
        if (initiatorBornDate == null) initiatorBornDate = new DateForm();
        if (initiatorDocumentIssueDate == null) initiatorDocumentIssueDate = new DateForm();
        if (initiatorStateRegistrationDate == null) initiatorStateRegistrationDate = new DateForm();
        if (initiatorMigrationTermStartDate == null) initiatorMigrationTermStartDate = new DateForm();
        if (initiatorMigrationTermEndDate == null) initiatorMigrationTermEndDate = new DateForm();
        if (initiatorAddressRegistration == null) initiatorAddressRegistration = new Address();
        if (initiatorMainActivityAddress == null) initiatorMainActivityAddress = new Address();
        if (initiatorAddressFactual == null) initiatorAddressFactual = new Address();
        if (initiatorPostalAddress == null) initiatorPostalAddress = new Address();
        if (executiveBornDate == null) executiveBornDate = new DateForm();
        if (executiveBornPlaceAddress == null) executiveBornPlaceAddress = new Address();
        if (executiveAddressRegistration == null) executiveAddressRegistration = new Address();
        UtilsService.securityVueJS(this);
    }

    @Override
    public void saveData(ProjectRequest to) {

        UtilsService.securityVueJS(this);
        BeanUtils.copyProperties(this, to);
    }


}
