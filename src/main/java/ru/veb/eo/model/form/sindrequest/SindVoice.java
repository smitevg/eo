package ru.veb.eo.model.form.sindrequest;

import lombok.Data;
@Data
public class SindVoice {

    private String voiceType;
    private String voiceSum;
    private String voiceCur;
    private String selectProject = "";
}
