package ru.veb.eo.model.form.resume;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Strings;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import ru.veb.eo.model.crm.Address;
import ru.veb.eo.model.crm.DateForm;
import ru.veb.eo.model.crm.ProjectRequest;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.service.UtilsService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
public class Resume4 implements Form {
    public String back = null;
    public String save = null;
    public String next = null;
    public int numberPage;
    private String projectId;

    public String initiatorPhoneNumber;

    @JsonIgnore
    public String initiatorPhoneNumberAdditional;
    public String initiatorFaxNumber;
    public String initiatorEmail;
    public Address initiatorPostalAddress = new Address();

    @JsonIgnore
    private boolean firstSave = true;


    @Override
    public void getData(ProjectRequest from) {


        // нужно проверить телефон на наличие в нём добавочного номера
        String phone = from.getInitiatorPhoneNumber();
        if(phone != null) {
            Pattern additionalPhonePattern = Pattern.compile("доб. (\\d+)$");
            Matcher additionalPhoneMatcher = additionalPhonePattern.matcher(phone);

            if (additionalPhoneMatcher.find()) {
                String additionalPhone = additionalPhoneMatcher.group(1);
                from.setInitiatorPhoneNumberAdditional(additionalPhone);
            }
            phone = phone.replaceAll(" доб. (\\d+)", "");
            from.setInitiatorPhoneNumber(phone);
        }



        BeanUtils.copyProperties(from, this);
        if (initiatorPostalAddress == null) initiatorPostalAddress = new Address();
        UtilsService.securityVueJS(this);
    }

    @Override
    public void saveData(ProjectRequest to) {
        UtilsService.securityVueJS(this);

        if(this.getInitiatorPhoneNumberAdditional() != null && this.firstSave){

            this.setInitiatorPhoneNumber(this.getInitiatorPhoneNumber() + " доб. " + this.getInitiatorPhoneNumberAdditional());

        }

        BeanUtils.copyProperties(this, to);
        this.firstSave = false;
    }
}
