package ru.veb.eo.model.form.financerequest;

import com.google.common.base.Strings;
import fr.opensagres.xdocreport.itext.extension.ExtendedParagraph;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.thymeleaf.util.ArrayUtils;
import org.thymeleaf.util.ListUtils;
import ru.veb.eo.model.crm.*;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.service.UtilsService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class ProjectCharacteristics implements Form {
    public String back = null;
    public String save = null;
    public String next = null;
    public int numberPage;
    private String projectId;

    public Double projectProjectCostDouble;
    public String projectCostCurrency;
    public Integer projectProjectPaybackTimeDouble;
    public String projectLastCost;
    public String projectProjectUpcomingInvestments;
    public String projectPaybackTime;
    public String projectProjectTermConstructionPhase;
    public String projectTermOperationalPhase;

    public List<Participant> participants;
    public List<IndividualPerson> participantsIndividuals;
    private String id[]; // id
    private String type[]; // физ лицо, юр лицо
    private String status[]; // роль участника
    private String resident[]; // резидент
    // физическое лицо
    private String nameLast[];
    private String nameFirst[];
    private String nameMid[];
    private String bornDate_day[];
    private String bornDate_month[];
    private String bornDate_year[];
    private String bornPlace_name[];
    private String citizenship[];
    // место жительства, регистрации
    private String addressRegistration_region[]; // регион
    private String addressRegistration_city[]; // населенный пункт
    private String addressRegistration_street[]; // улица
    private String addressRegistration_house[]; // дом
    private String addressRegistration_building[]; // корпус
    private String addressRegistration_apartment[]; // квартира
    private String addressRegistration_index[]; // индекс
    // паспортные данные
    private String idName[]; // тип документа
    private String idSeries[]; // серия
    private String idNumber[]; // номер
    private String idIssueDate_day[]; // день выдачи
    private String idIssueDate_month[]; // месяц выдачи
    private String idIssueDate_year[]; // год выдачи
    private String idIssueAgencyName[]; // кем выдан
    private String idIssueAgencyCode[]; // код подразделения
    // юридическое лицо
    private String organizationType[]; // ОПФ
    private String organizationTitle[]; // наименовани
    private String ogrn[]; // огрн
    private String inn[]; // инн
    private String kpp[]; // КПП
    private String kio[]; // КПП
    private String nalogNumber[]; // налоговый номер
    private String regNumber[]; // регистрационный номер

    private Double volume[]; // объём вложеннх средств
    private String rules[]; // условия



    // project-characteristics-1
    public Double projectAmountFinancingRequest;

    public Double projectAmountRecipientsCash;
    public Double projectAmountRecipientsPart;
    public Double projectAmountBanksCash;
    public Double projectAmountBanksPart;

    public Long amountInvestor[];
    public Long amountBondArray[];
    public Long amountFinanceArray[];
    public String amountFinanceSourceArray[];
    public List<List<String>> projectAmountInvestorArray;
    public List<List<String>> projectAmountBondArray;
    public List<List<String>> projectAmountFinanceArray;

    public Double projectAmountOthersCash;
    public Double projectAmountOthersPart;
    public Double projectAmountInvestorCash;
    public Double projectAmountInvestorPart;
    public Double projectAmountBondCash;
    public Double projectAmountBondPart;
    public String projectFinancingSourcesOther;

    public String projectFinancingRequestType;
    public String projectFinancingRequestTypeTime;
    public String projectCreditResourcesCostPercent;
    public String projectDebtRepaymentDelay;
    public String projectDebtRepaymentPeriod;
    public String projectDebtRepaymentSchedule;
    public String projectReturnOnBankEquity;
    public String projectBanksWithdrawalFromProject;
    public String projectWayOfBanksWithdrawalFromProject;
    public String projectBanksOfTheSyndicate;
    public Double projectSyndicatedDebtAmount;
    public String projectSyndicateConditions;
    public String projectFinancingRequestTypeOther;


    public String projectAssuranceGuarantor;
    public Double projectAssuranceAmount;
    public String projectAssuranceCurrency;
    public Integer projectAssuranceType;


    public String projectProjectProvidesForACompetition;
    public String projectProjectProvidesForACompetitionDescription;
    public String projectFundsUsingMonitoringMechanisms;
    public String projectFundsUsingMonitoringMechanismsDescription;
    public String projectNetPresentValue;
    public Double projectProjectIrr;
    public String projectProjectIrrString;


    private String itemsId[];
    private String itemsType[];
    private String itemsGuarantor[];
    private Double itemsValue[];
    private String itemsCurrency[];
    private String itemsDepositType[];
    private String itemsDepositWho[];
    private String itemsDepositCompany[];
    private String itemsDepositCompanyDay[];
    private String itemsDepositCompanyMonth[];
    private String itemsDepositCompanyYear[];

    public List<Supply> supplies;

    public List<TypeOfFinancing> typeOfFinancings;

    public boolean financeLongCheck;
    public String financeLongTerm;
    public String financeLongPercent;
    public Long financeLongDelayYear;
    public Long financeLongDelayMonth;
    public Long financeLongTermReturnYear;
    public Long financeLongTermReturnMonth;
    public String financeLongSсhedule;
    public String financeLongDescription;

    public boolean financeMezonCheck;
    public String financeMezonYield;
    public String financeMezonExitTerm;
    public String financeMezonExitType;

    public boolean financeSidicirCheck;
    public String financeSidicirYield;

    public List<Map<String, String>> syndicateParticipants;
    public String syndicateParticipantsTitle[];
    public String syndicateParticipantsValue[];
    public String financeSidicirTerm;
    public String financeSidicirCondition;

    public boolean financeCapitalCheck;
    public String financeCapitalDescription;

    public boolean financeBondCheck;
    public String financeBondDescription;

    public boolean financeGarantCheck;
    public String financeGarantDescription;

    public boolean financeSuretytCheck;
    public String financeSuretytDescription;

    public boolean financeAccredetivCheck;
    public String financeAccredetivDescription;

    public boolean financeOtherCheck;
    public String financeOtherOther;
    public String financeOtherDescription;


    @Override
    public void getData(ProjectRequest from) {

        BeanUtils.copyProperties(from, this);


        try {
            if (null != projectProjectIrr) projectProjectIrrString = Double.toString(projectProjectIrr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (this.supplies == null) this.supplies = new ArrayList<>();
        if (this.typeOfFinancings == null) this.typeOfFinancings = new ArrayList<>();
        if (this.projectAmountInvestorArray == null) this.projectAmountInvestorArray = new ArrayList<>();
        if (this.projectAmountBondArray == null) this.projectAmountBondArray = new ArrayList<>();
        if (this.projectAmountFinanceArray == null) this.projectAmountFinanceArray = new ArrayList<>();


        if (!ListUtils.isEmpty(typeOfFinancings)) {
            for (TypeOfFinancing financing : typeOfFinancings) {

                if ("0".equals(financing.type)) {
                    financeLongCheck = true;
                    financeLongDescription = financing.description;
                    financeLongPercent = financing.financeLongPercent;
                    financeLongSсhedule = financing.financeLongSсhedule;
                    financeLongTerm = financing.financeLongTerm;
                    financeLongDescription = financing.description;

                    Long aLong = Long.valueOf(financing.financeLongDelay);
                    financeLongDelayMonth = aLong % 12;
                    financeLongDelayYear = (Long) aLong / 12;

                    Long aLong2 = Long.valueOf(financing.financeLongTermReturn);
                    financeLongTermReturnMonth = aLong2 % 12;
                    financeLongTermReturnYear = (Long) aLong2 / 12;
                }
                if ("1".equals(financing.type)) {
                    financeMezonCheck = true;
                    financeMezonExitTerm = financing.financeMezonExitTerm;
                    financeMezonExitType = financing.financeMezonExitType;
                    financeMezonYield = financing.financeMezonYield;
                }
                if ("2".equals(financing.type)) {
                    financeSidicirCheck = true;
                    financeSidicirYield = financing.financeSidicirYield;
                    financeSidicirTerm = financing.financeSidicirTerm;
                    financeSidicirCondition = financing.financeSidicirCondition;

                   try {
                       syndicateParticipants = new ArrayList<>();
                       for (String s : financing.financeSidicirParticipants.split(";")) {
                           String[] strings = s.split("-");
                           Map<String, String> obj = new HashMap<>();
                           obj.put("title", strings[0]);
                           obj.put("value", strings[1]);
                           syndicateParticipants.add(obj);
                       }

                   } catch (Exception e) {
                       e.printStackTrace();
                   }
                }
                if ("3".equals(financing.type)) {
                    financeCapitalCheck = true;
                    financeCapitalDescription = financing.description;
                }
                if ("4".equals(financing.type)) {
                    financeBondCheck = true;
                    financeBondDescription = financing.description;
                }
                if ("5".equals(financing.type)) {
                    financeGarantCheck = true;
                    financeGarantDescription = financing.description;
                }
                if ("6".equals(financing.type)) {
                    financeSuretytCheck = true;
                    financeSuretytDescription = financing.description;
                }
                if ("7".equals(financing.type)) {
                    financeAccredetivCheck = true;
                    financeAccredetivDescription = financing.description;
                }
                if ("8".equals(financing.type)) {
                    financeOtherCheck = true;
                    financeOtherDescription = financing.description;
                    financeOtherOther = financing.financeOtherOther;
                }
            }
        }
        UtilsService.securityVueJS(this);
    }

    @Override
    public void saveData(ProjectRequest to) {
        UtilsService.securityVueJS(this);

        typeOfFinancings = new ArrayList<>();
        if (financeLongCheck) {
            TypeOfFinancing t = new TypeOfFinancing();
            t.type = "0";
            t.description = financeLongDescription;
            t.financeLongDelay = String.valueOf(12 * financeLongDelayYear + financeLongDelayMonth);
            t.financeLongPercent = financeLongPercent;
            t.financeLongSсhedule = financeLongSсhedule;
            t.financeLongTerm = financeLongTerm;
            t.financeLongTermReturn = String.valueOf(12 * financeLongTermReturnYear + financeLongTermReturnMonth);
            typeOfFinancings.add(t);
        }
        if (financeMezonCheck) {
            TypeOfFinancing t = new TypeOfFinancing();
            t.type = "1";
            t.financeMezonExitTerm = financeMezonExitTerm;
            t.financeMezonExitType = financeMezonExitType;
            t.financeMezonYield = financeMezonYield;
            typeOfFinancings.add(t);
        }
        if (financeSidicirCheck) {
            TypeOfFinancing t = new TypeOfFinancing();
            t.type = "2";
            t.financeSidicirYield = financeSidicirYield;
            t.financeSidicirTerm = financeSidicirTerm;
            t.financeSidicirCondition = financeSidicirCondition;

            StringBuilder sb = new StringBuilder();
            if (syndicateParticipantsValue != null && syndicateParticipantsValue.length > 0) {
                for (int i = 0; i < syndicateParticipantsValue.length; i++) {
                    try {
                        sb.append(syndicateParticipantsTitle[i])
                                .append("-")
                                .append(syndicateParticipantsValue[i])
                                .append(";");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            t.financeSidicirParticipants = sb.toString();
            typeOfFinancings.add(t);
        }
        if (financeCapitalCheck) {
            TypeOfFinancing t = new TypeOfFinancing();
            t.type = "3";
            t.description = financeCapitalDescription;
            typeOfFinancings.add(t);
        }
        if (financeBondCheck) {
            TypeOfFinancing t = new TypeOfFinancing();
            t.type = "4";
            t.description = financeBondDescription;
            typeOfFinancings.add(t);
        }
        if (financeGarantCheck) {
            TypeOfFinancing t = new TypeOfFinancing();
            t.type = "5";
            t.description = financeGarantDescription;
            typeOfFinancings.add(t);
        }
        if (financeSuretytCheck) {
            TypeOfFinancing t = new TypeOfFinancing();
            t.type = "6";
            t.description = financeSuretytDescription;
            typeOfFinancings.add(t);
        }
        if (financeAccredetivCheck) {
            TypeOfFinancing t = new TypeOfFinancing();
            t.type = "7";
            t.description = financeAccredetivDescription;
            typeOfFinancings.add(t);
        }
        if (financeOtherCheck) {
            TypeOfFinancing t = new TypeOfFinancing();
            t.type = "8";
            t.financeOtherOther = financeOtherOther;
            t.description = financeOtherDescription;
            typeOfFinancings.add(t);
        }

        if (!ArrayUtils.isEmpty(amountInvestor)) {
            projectAmountInvestorArray = new ArrayList<>();
            for (Long aDouble : amountInvestor) {
                if (aDouble > 0) {
                    List<String> el = new ArrayList<>();
                    el.add(String.format("%d", aDouble));
                    el.add(Double.toString(100d * aDouble / projectProjectCostDouble));
                    projectAmountInvestorArray.add(el);
                }
            }
        }

        if (!ArrayUtils.isEmpty(amountBondArray)) {
            projectAmountBondArray = new ArrayList<>();
            for (Long aDouble : amountBondArray) {
                if (aDouble > 0) {
                    List<String> el = new ArrayList<>();
                    el.add(String.format("%d", aDouble));
                    el.add(Double.toString(100d * aDouble / projectProjectCostDouble));
                    projectAmountBondArray.add(el);
                }
            }
        }

        if (!ArrayUtils.isEmpty(amountFinanceArray)) {
            projectAmountFinanceArray = new ArrayList<>();
            for (int i = 0; i < amountFinanceArray.length; i++) {
                if (amountFinanceArray[i] > 0) {
                    List<String> el = new ArrayList<>();
                    el.add(amountFinanceSourceArray[i]);
                    el.add(String.format("%d", amountFinanceArray[i]));
                    el.add(Double.toString(100d * amountFinanceArray[i] / projectProjectCostDouble));
                    projectAmountFinanceArray.add(el);
                }
            }
        }

        if (this.numberPage == 2) { // only page 3 with update supplies
            supplies = new ArrayList<>();
            try {
            for (int i = 0; i < itemsValue.length; i++) {
                    Supply supply = new Supply();
                    if ("0".equals(itemsType[i])) {
                        supply.setType(TypeSupply.BANK);
                        supply.setValue(itemsValue[i]);
                    } else if ("1".equals(itemsType[i])) {
                        supply.setType(TypeSupply.GUARANTEE);
                        supply.setValue(itemsValue[i]);
                    } else if ("2".equals(itemsType[i])) {
                        supply.setType(TypeSupply.SURETY);
                        supply.setValue(itemsValue[i]);
                    } else if ("3".equals(itemsType[i])) {
                        supply.setType(TypeSupply.PLEDGE);
                        supply.setValue(itemsValue[i]);
                        supply.setDepositWho(itemsDepositWho[i]);
                        supply.setDepositCompany(itemsDepositCompany[i]);
                        try {
                            if (!Strings.isNullOrEmpty(itemsDepositCompanyDay[i]) && !Strings.isNullOrEmpty(itemsDepositCompanyMonth[i]) && !Strings.isNullOrEmpty(itemsDepositCompanyYear[i]))
                                supply.setDepositDate(new DateForm(Integer.valueOf(itemsDepositCompanyDay[i]), Integer.valueOf(itemsDepositCompanyMonth[i]), Integer.valueOf(itemsDepositCompanyYear[i])));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if ("4".equals(itemsType[i])) {
                        supply.setType(TypeSupply.OTHER);
                        supply.setValue(itemsValue[i]);
                    }
                    supply.setId(Strings.isNullOrEmpty(itemsId[i]) ? null : itemsId[i]);
                    supply.setGuarantor(itemsGuarantor[i]);
                    supply.setCurrency(itemsCurrency[i]);
                    if (!Strings.isNullOrEmpty(supply.getCurrency()) && itemsValue[i] > 0) supplies.add(supply);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        projectAmountRecipientsPart = 100d * projectAmountRecipientsCash / projectProjectCostDouble;
        projectAmountBanksPart = 100d * projectAmountBanksCash / projectProjectCostDouble;
//        projectAmountInvestorPart = 100d * projectAmountInvestorCash / projectProjectCostDouble;
//        projectAmountBondPart = 100d * projectAmountBondCash / projectProjectCostDouble;
//        projectAmountOthersPart = 100d * projectAmountOthersCash / projectProjectCostDouble;

        if (volume != null) {
            this.participants = new ArrayList<>();
            this.participantsIndividuals = new ArrayList<>();
            for (int i = 0; i < volume.length; i++) {

                try {
                    switch (this.type[i]){
                        case "ul":
                            Participant participant = new Participant();
                            participant.setId(Strings.isNullOrEmpty(id[i]) ? null : id[i]);
                            participant.setInn(Strings.isNullOrEmpty(inn[i]) ? null : inn[i]);
                            participant.setNalogNumber(Strings.isNullOrEmpty(nalogNumber[i]) ? null : nalogNumber[i]);
                            participant.setOgrn(Strings.isNullOrEmpty(ogrn[i]) ? null : ogrn[i]);
                            participant.setOrganizationTitle(Strings.isNullOrEmpty(organizationTitle[i]) ? null : organizationTitle[i]);
                            participant.setOrganizationType(Strings.isNullOrEmpty(organizationType[i]) ? null : organizationType[i]);
                            participant.setRegNumber(Strings.isNullOrEmpty(regNumber[i]) ? null : regNumber[i]);
                            participant.setResident(Strings.isNullOrEmpty(resident[i]) ? null : "true".equals(resident[i]));
                            participant.setRules(Strings.isNullOrEmpty(rules[i]) ? null : rules[i]);
                            participant.setStatus(Strings.isNullOrEmpty(status[i]) ? null : status[i]);
                            participant.setKpp(Strings.isNullOrEmpty(kpp[i]) ? null : kpp[i]);
                            participant.setVolume(volume[i] == null ? null : volume[i]);
                            this.participants.add(participant);
                            break;
                        case "fl":
                            IndividualPerson individualPerson = new IndividualPerson();
                            individualPerson.setNameFirst(Strings.isNullOrEmpty(this.nameFirst[i]) ? null : this.nameFirst[i]);
                            individualPerson.setNameLast(Strings.isNullOrEmpty(this.nameLast[i]) ? null : this.nameLast[i]);
                            individualPerson.setNameMid(Strings.isNullOrEmpty(this.nameMid[i]) ? null : this.nameMid[i]);
                            if(!Strings.isNullOrEmpty(this.bornDate_day[i]) && !Strings.isNullOrEmpty(this.bornDate_month[i]) && !Strings.isNullOrEmpty(this.bornDate_year[i])){
                                individualPerson.setBornDate(new DateForm(
                                        Strings.isNullOrEmpty(this.bornDate_day[i]) ? null : Integer.valueOf(this.bornDate_day[i]),
                                        Strings.isNullOrEmpty(this.bornDate_month[i]) ? null : Integer.valueOf(this.bornDate_month[i]),
                                        Strings.isNullOrEmpty(this.bornDate_year[i]) ? null : Integer.valueOf(this.bornDate_year[i]))
                                );
                            }

                            Address addressBorn = new Address();
                            addressBorn.setName(Strings.isNullOrEmpty(this.bornPlace_name[i]) ? null : this.bornPlace_name[i]);
                            individualPerson.setBornPlace(addressBorn);

                            individualPerson.setCitizenship(Strings.isNullOrEmpty(this.citizenship[i]) ? null : this.citizenship[i]);
                            individualPerson.setResident(Boolean.valueOf(this.resident[i]));

                            Address addressRegistration = new Address();
                            addressRegistration.setRegion(Strings.isNullOrEmpty(this.addressRegistration_region[i]) ? null : this.addressRegistration_region[i]);
                            addressRegistration.setCity(Strings.isNullOrEmpty(this.addressRegistration_city[i]) ? null : this.addressRegistration_city[i]);
                            addressRegistration.setStreet(Strings.isNullOrEmpty(this.addressRegistration_street[i]) ? null : this.addressRegistration_street[i]);
                            addressRegistration.setHouse(Strings.isNullOrEmpty(this.addressRegistration_house[i]) ? null : this.addressRegistration_house[i]);
                            addressRegistration.setBuilding(Strings.isNullOrEmpty(this.addressRegistration_building[i]) ? null : this.addressRegistration_building[i]);
                            addressRegistration.setApartment(Strings.isNullOrEmpty(this.addressRegistration_apartment[i]) ? null : this.addressRegistration_apartment[i]);
                            addressRegistration.setIndex(Strings.isNullOrEmpty(this.addressRegistration_index[i]) ? null : this.addressRegistration_index[i]);
                            individualPerson.setAddressRegistration(addressRegistration);

                            individualPerson.setIdName(Strings.isNullOrEmpty(this.idName[i]) ? null : this.idName[i]);
                            individualPerson.setIdSeries(Strings.isNullOrEmpty(this.idSeries[i]) ? null : this.idSeries[i]);
                            individualPerson.setIdNumber(Strings.isNullOrEmpty(this.idNumber[i]) ? null : this.idNumber[i]);
                            if(!Strings.isNullOrEmpty(this.idIssueDate_day[i]) && !Strings.isNullOrEmpty(this.idIssueDate_month[i]) && !Strings.isNullOrEmpty(this.idIssueDate_year[i])){
                                individualPerson.setIdIssueDate(new DateForm(
                                        Strings.isNullOrEmpty(this.idIssueDate_day[i]) ? null : Integer.valueOf(this.idIssueDate_day[i]),
                                        Strings.isNullOrEmpty(this.idIssueDate_month[i]) ? null : Integer.valueOf(this.idIssueDate_month[i]),
                                        Strings.isNullOrEmpty(this.idIssueDate_year[i]) ? null : Integer.valueOf(this.idIssueDate_year[i]))
                                );
                            }
                            individualPerson.setIdIssueAgencyName(Strings.isNullOrEmpty(this.idIssueAgencyName[i]) ? null : this.idIssueAgencyName[i]);
                            individualPerson.setIdIssueAgencyCode(Strings.isNullOrEmpty(this.idIssueAgencyCode[i]) ? null : this.idIssueAgencyCode[i]);

                            individualPerson.setRules(Strings.isNullOrEmpty(rules[i]) ? null : rules[i]);
                            individualPerson.setStatus(Strings.isNullOrEmpty(status[i]) ? null : status[i]);
                            individualPerson.setVolume(volume[i] == null ? null : volume[i]);

                            this.participantsIndividuals.add(individualPerson);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            projectProjectIrr = Double.valueOf(projectProjectIrrString.replaceAll(",", "."));
        } catch (Exception e) {
            e.printStackTrace();
        }

        BeanUtils.copyProperties(this, to);
    }
}
