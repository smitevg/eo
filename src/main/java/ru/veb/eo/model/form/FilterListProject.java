package ru.veb.eo.model.form;

import lombok.Data;

@Data
public class FilterListProject {
    private String number;
    private String name;
    private String applicant;
    private String applicantEMail;
    private String manager;
    private String dateEntered;
    private String status;
}
