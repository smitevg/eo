package ru.veb.eo.model.form.sindrequest;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.microsoft.sharepoint.rest.SPFile;
import lombok.Data;
import lombok.Getter;
import org.springframework.beans.BeanUtils;
import ru.veb.eo.entity.Project;
import ru.veb.eo.entity.UserProject;
import ru.veb.eo.model.crm.*;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.service.UtilsService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Data
public class SindRequest implements Form {

        public String back = null;
        public String save = null;
        public String next = null;
        public int numberPage;
        private String projectId;


        public int voiceEnd = 0;

        public String fi = "";
        public String projectNomination ="PN";
        public List<SPFile> listFile = new ArrayList<SPFile>();

        @Getter
        public List<UserProject> listProject;
        @Getter
        public int selectUserProjectId = -1;

        @Getter
        @JsonProperty("selectProject")
        public UserProject selectProject;

        // Установка выбранного проекта
        public void processSelectProject(int id){
            this.selectUserProjectId = id;
            for(UserProject up : listProject){
               if (up.getId() == selectUserProjectId){
                   selectProject = up;
                   this.voiceEnd = 0;
                   if (selectProject.getProject().getDateEnd() != null) {
                       if (selectProject.getProject().getDateEnd().before(Calendar.getInstance().getTime())) {
                           this.voiceEnd = 1;
                       }
                   }
               }
            }
        }

        // инициализация списка из базы
        public void initListProject(List<UserProject> listProject) {
            this.selectProject = null;
            this.selectUserProjectId = -1;
            this.voiceEnd = 0;
            this.listProject = listProject;
        }



        @Override
        public void getData(ProjectRequest from) {
            BeanUtils.copyProperties(from, this);
            UtilsService.securityVueJS(this);
        }

        @Override
        public void saveData(ProjectRequest to) {
            UtilsService.securityVueJS(this);
            BeanUtils.copyProperties(this, to);
        }
}
