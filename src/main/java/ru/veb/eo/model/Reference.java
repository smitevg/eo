package ru.veb.eo.model;

import ru.veb.eo.model.reference.*;

import java.util.List;

public class Reference {
	public List<Days> days;
	public List<DocumentName> documentName;
	public List<FaceType> faceType;
	public List<FaceRole> faceRole;
	public List<FinancingCurrency> financingCurrency;
	public List<Limitation> limitation;
	public List<Months> months;
	public List<Opf> opf;
	public List<ProjectCost> projectCost;
	public List<ProjectPaybackTime> projectPaybackTime;
	public List<ProjectSectors> projectSectors;
	public List<ProjectType> projectType;
	public List<ProjectDirection> projectDirection;
	public List<Region> region;
	public List<Years> years;
    public List<ProjectStatus> projectStatus;
    public List<ProjectDirection> projectDirections;
	public List<ParticipantRole> participantRole;

	public List<DocumentName> documentNameNotResident;
	public List<DocumentName> documentNameResident;

}
