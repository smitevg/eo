package ru.veb.eo.model;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    USER, MANAGER, SIND, SIND_ADM, FAB;

    @Override
    public String getAuthority() {
        return name();
    }
}
