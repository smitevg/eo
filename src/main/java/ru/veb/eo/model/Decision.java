package ru.veb.eo.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
@JsonPropertyOrder(alphabetic = true)
public class Decision {
    private String result;
    private List<User> managers;

    public Decision(String result, List<User> managers) {
        super();
        this.result = result;
        this.managers = managers;
    }

    public Decision(String result, User manager) {
        super();
        this.result = result;
        this.managers = new ArrayList<>();
        this.managers.add(manager);
    }
}
