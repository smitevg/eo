package ru.veb.eo.controller;

import com.google.common.base.Strings;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.pdf.BaseFont;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.itext.extension.font.IFontProvider;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StreamUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.util.ListUtils;
import org.thymeleaf.util.SetUtils;
import ru.veb.eo.VebEoApplication;
import ru.veb.eo.model.Reference;
import ru.veb.eo.model.Role;
import ru.veb.eo.model.User;
import ru.veb.eo.model.crm.*;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.model.form.financerequest.*;
import ru.veb.eo.model.form.financerequest.Initiator;
import ru.veb.eo.model.reference.Field;
import ru.veb.eo.model.reference.Notification;
import ru.veb.eo.model.reference.ProjectEntity;
import ru.veb.eo.repository.*;
import ru.veb.eo.service.*;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;

import static ru.veb.eo.service.UtilsService.getValue;

@Controller
@RequestMapping("/private-office/finance-request")
@SessionAttributes(value = {"user", "project", "projectMain", "directionIndustry", "influence", "initiator", "projectCharacteristics", "businessModel", "sendFinanceRequest", "manager", "region"}, types = {User.class, ProjectRequest.class, ProjectMain.class, DirectionIndustry.class, Influence.class, Initiator.class, ProjectCharacteristics.class, BusinessModel.class, SendFinanceRequest.class, Manager.class, String.class})
public class POFinanceRequestController {
    private static final Logger logger = LoggerFactory.getLogger(POFinanceRequestController.class);
    private static final String TEMPLATE_DIRECTION_INITIATOR = "LK/02/initiator-info-";
    private static final String TEMPLATE_PROJECT_MAIN = "LK/03/global-info-";
    private static final String TEMPLATE_DIRECTION_INDUSTRY = "LK/04/project-direction-";
    private static final String TEMPLATE_DIRECTION_INFLUENCE = "LK/05/impact-info-";
    private static final String TEMPLATE_PROJECT_CHARACTERISTICS = "LK/06/project-characteristics-";
    private static final String TEMPLATE_BUSINESS_MODEL = "LK/07/business-model-";
    private static final String TEMPLATE_SEND_FINANCE_REQUEST = "LK/01/finance-request-";

    private CrmService cs;
    private UserService us;
    private ReferenceService rs;
    private NotificationRepository nr;
    private SharePointService sp;
    private FieldsRepository fr;
    private ProjectRepository projectRepository;
    private UtilsService utilsService;
    private EmailService es;

    @Autowired
    private OpfRepository opfRepository;
    @Autowired
    private ProjectTypeRepository projectTypeRepository;
    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    public POFinanceRequestController(CrmService cs, UserService us, ReferenceService rs, NotificationRepository nr, SharePointService sp, FieldsRepository fr, ProjectRepository projectRepository, UtilsService utilsService, EmailService es) {
        this.cs = cs;
        this.us = us;
        this.rs = rs;
        this.nr = nr;
        this.sp = sp;
        this.fr = fr;
        this.es = es;
        this.projectRepository = projectRepository;
        this.utilsService = utilsService;
    }

    @ModelAttribute("user")
    public User initUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();
        user.setPage("finance-request");
        if (user.getAuthorities().contains(Role.MANAGER))
            user.setNotifications(nr.findByProjectCertifiedManagerIdAndIsViewed(user.getCrmId(), false));
        else
            user.setNotifications(nr.findByApplicantEmailAndIsViewed(user.getUsername(), false));
        return user;
    }

    @ModelAttribute("project")
    public ProjectRequest initProject() {
        return new ProjectRequest();
    }

    @ModelAttribute("manager")
    public Manager initManager() {
        return new Manager();
    }

    @ModelAttribute("region")
    public String initRegion() {
        return null;
    }

    @ModelAttribute("ref")
    public Reference initReference() {
        return rs.getReference();
    }

    @GetMapping("/project")
    @ResponseBody
    public ProjectRequestDataTransfer abstract1Data(@ModelAttribute("project") ProjectRequest project) {
        return new ProjectRequestDataTransfer(project);
    }

    @GetMapping
    private String initialization(Model model, @RequestParam(name = "id", required = false) String id, @ModelAttribute("user") User user) {
        if (id != null) {
            ProjectRequest projectRequest = null;
            if (user.getAuthorities().contains(Role.MANAGER))
                projectRequest = cs.getProjectRequest(id, null, user.getCrmId());
            else
                projectRequest = cs.getProjectRequest(id, user.getUsername(), null);


            model.addAttribute("project", projectRequest);

            Manager manager = null;
            try {
                manager = cs.getManagerById(projectRequest.getProjectCertifiedManagerId());
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (null == manager) {
                model.addAttribute("manager", null);
            } else {
                model.addAttribute("manager", manager);
                try {
                    String region = rs.getRegion(Integer.valueOf(manager.getRegionId()));
                    model.addAttribute("region", region);
                } catch (Exception e) {
                    e.printStackTrace();
                    model.addAttribute("region", "");
                }
            }

            return "redirect:/private-office/finance-request/main";
        } else {
            return "redirect:/private-office";
        }
    }

    @GetMapping("/main")
    public String main_(@ModelAttribute("project") ProjectRequest project, @ModelAttribute("user") User user, Model model) {

        int rating = 0;

        if (user.getAuthorities().contains(Role.MANAGER)) {
            Manager userZnF = new Manager();
            userZnF.setEmail(project.getApplicantEmail());
            userZnF.setNameFirst(project.getApplicantNameFirst());
            userZnF.setNameLast(project.getApplicantNameLast());
            userZnF.setPhone(project.getApplicantPhoneNumber());
            userZnF.setImage(null);
            userZnF.setRegionId(project.getProjectSalePlace());
            model.addAttribute("userZnF", userZnF);

        } else {
            String crmId = project.getProjectCertifiedManagerId();
            if (Strings.isNullOrEmpty(crmId)) {
                model.addAttribute("userZnF", null);
            } else {
                model.addAttribute("userZnF", cs.getManagerById(crmId));

                Map<String, Integer> ratings = cs.getRatings();
                if (ratings.containsKey(crmId)) rating = ratings.get(crmId);
            }

        }

        model.addAttribute("rating", rating);

        AdditionalInfo additionalInfo = new AdditionalInfo();
        additionalInfo.getData(project);
        model.addAttribute("additionInfo", additionalInfo);

        return "LK/01/finance-request";
    }


    @GetMapping(value = "/concept-docx", produces = "application/octet-stream")
    public void getConceptDocx(@ModelAttribute("project") ProjectRequest project, HttpServletResponse response) throws IOException, XDocReportException {
        InputStream in = getClass().getResourceAsStream("/concept-template.docx");
        IXDocReport report = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Velocity);
        IContext context = report.createContext();
        //Map<String, Object> contextObjects = utilsService.getContextObjects(project);
        //contextObjects.forEach((key, value) -> context.put(key, value));

        response.setContentType("application/octet-stream");
        response.addHeader("Content-Disposition", "attachment; filename=concept.docx");
        OutputStream out = response.getOutputStream();
        report.process(context, out);
    }

    @GetMapping(value = "/concept-pdf", produces = "application/octet-stream")
    public void getConceptPdf(@ModelAttribute("project") ProjectRequest project, HttpServletResponse response) throws IOException, XDocReportException {
        InputStream in = getClass().getResourceAsStream("/concept-template.docx");
        IXDocReport report = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Velocity);
        IContext context = report.createContext();
//        Map<String, Object> contextObjects = utilsService.getContextObjects(project);
//        contextObjects.forEach((key, value) -> context.put(key, value));

        Options options = Options.getTo(ConverterTypeTo.PDF);

        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(getClass().getClassLoader());
        Resource[] res = resolver.getResources("classpath:/font/**");
        final String tmpDir = VebEoApplication.env("TMPDIR", System.getProperty("java.io.tmpdir"));
        for (Resource fr : res) {
            File f = new File(tmpDir + "/" + fr.getFilename().toLowerCase());
            if (!f.exists()) {
//                logger.info("Saving " + fr.getFilename() + " to " + f);
                FileOutputStream fos = new FileOutputStream(f);
                try {
                    StreamUtils.copy(fr.getInputStream(), fos);
                } finally {
                    fos.close();
                }
            }
        }

        PdfOptions pdfOptions = PdfOptions.create();
        pdfOptions.fontProvider(new IFontProvider() {
            @Override
            public Font getFont(String familyName, String encoding, float size, int style, Color color) {
                try {
                    String suffix = "";
                    if (style == Font.NORMAL) {
                        suffix = "";
                    } else if (style == Font.ITALIC) {
                        suffix = "i";
                    } else if (style == Font.BOLD) {
                        suffix = "b";
                    } else if (style == Font.BOLDITALIC) {
                        suffix = "bi";
                    }
                    File f = new File(tmpDir + "/" + familyName.toLowerCase() + suffix + ".ttf");
//                    logger.info("Requested font {} checking {}", familyName, f.getAbsolutePath());
                    if (f.exists()) {
//                        logger.info("Returning font {}", f.getAbsolutePath());
                        BaseFont baseFont = BaseFont.createFont(f.getAbsolutePath(), encoding, BaseFont.EMBEDDED);
                        return new Font(baseFont, size, style, color);
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }

                return FontFactory.getFont(familyName, encoding, size, style, color);
            }
        });
        options.subOptions(pdfOptions);

        response.setContentType("application/octet-stream");
        response.addHeader("Content-Disposition", "attachment; filename=concept.pdf");
        OutputStream out = response.getOutputStream();
        report.convert(context, options, out);
    }

    @GetMapping(value = "/business-model-docx", produces = "application/octet-stream")
    public void getDocx(@ModelAttribute("project") ProjectRequest project, HttpServletResponse response) throws IOException, XDocReportException {
        InputStream in = getClass().getResourceAsStream("/business-model-template.docx");
        IXDocReport report = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Velocity);
        IContext context = report.createContext();
        Map<String, Object> contextObjects = utilsService.getContextObjects(project);
        contextObjects.forEach((key, value) -> context.put(key, value));

        response.setContentType("application/octet-stream");
        response.addHeader("Content-Disposition", "attachment; filename=business-model.docx");
        OutputStream out = response.getOutputStream();
        report.process(context, out);
    }

    @GetMapping(value = "/business-model-pdf", produces = "application/octet-stream")
    public void getPdf(@ModelAttribute("project") ProjectRequest project, HttpServletResponse response) throws IOException, XDocReportException {
        InputStream in = getClass().getResourceAsStream("/business-model-template.docx");
        IXDocReport report = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Velocity);
        IContext context = report.createContext();
        Map<String, Object> contextObjects = utilsService.getContextObjects(project);
        contextObjects.forEach((key, value) -> context.put(key, value));

        Options options = Options.getTo(ConverterTypeTo.PDF);

        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(getClass().getClassLoader());
        Resource[] res = resolver.getResources("classpath:/font/**");
        final String tmpDir = VebEoApplication.env("TMPDIR", System.getProperty("java.io.tmpdir"));
        for (Resource fr : res) {
            File f = new File(tmpDir + "/" + fr.getFilename().toLowerCase());
            if (!f.exists()) {
//                logger.info("Saving " + fr.getFilename() + " to " + f);
                FileOutputStream fos = new FileOutputStream(f);
                try {
                    StreamUtils.copy(fr.getInputStream(), fos);
                } finally {
                    fos.close();
                }
            }
        }

        PdfOptions pdfOptions = PdfOptions.create();
        pdfOptions.fontProvider(new IFontProvider() {
            @Override
            public Font getFont(String familyName, String encoding, float size, int style, Color color) {
                try {
                    String suffix = "";
                    if (style == Font.NORMAL) {
                        suffix = "";
                    } else if (style == Font.ITALIC) {
                        suffix = "i";
                    } else if (style == Font.BOLD) {
                        suffix = "b";
                    } else if (style == Font.BOLDITALIC) {
                        suffix = "bi";
                    }
                    File f = new File(tmpDir + "/" + familyName.toLowerCase() + suffix + ".ttf");
//                    logger.info("Requested font {} checking {}", familyName, f.getAbsolutePath());
                    if (f.exists()) {
//                        logger.info("Returning font {}", f.getAbsolutePath());
                        BaseFont baseFont = BaseFont.createFont(f.getAbsolutePath(), encoding, BaseFont.EMBEDDED);
                        return new Font(baseFont, size, style, color);
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }

                return FontFactory.getFont(familyName, encoding, size, style, color);
            }
        });
        options.subOptions(pdfOptions);

        response.setContentType("application/octet-stream");
        response.addHeader("Content-Disposition", "attachment; filename=business-model.pdf");
        OutputStream out = response.getOutputStream();
        report.convert(context, options, out);
    }

    // ----------------------- AdditionInfo -----------------------
    @PostMapping("/additionInfo")
    public String sendAdditionInfoSubmit(@ModelAttribute("additionInfo") AdditionalInfo form, @ModelAttribute("project") ProjectRequest project, Model model,
                                         @RequestParam(name = "files[]", required = false) MultipartFile files[]) throws IOException {
        UtilsService.securityVueJS(form);

        if (files != null && files.length > 0) {
            for (MultipartFile file : files) {
                if (file.getSize() > 0)
                    sp.saveProjectFile(project.projectId, file, project.getInitiatorInn(), project.getProjectCode());
            }
        }
        form.setNext(null);
        form.setProjectId(project.getProjectId());
        cs.updateProject(form);
        return "redirect:/private-office/finance-request?id=" + project.projectId;
    }

    // ----------------------- initiator -----------------------
    @ModelAttribute("initiator")
    public Initiator initInitiator() {
        return new Initiator();
    }

    @GetMapping("/initiator")
    public String initiator(@ModelAttribute("initiator") Initiator initiator, @ModelAttribute("project") ProjectRequest project) {
        initiator.numberPage = 0;
        initiator.getData(project);
        initiator.setInitiatorFaceType("0");
        return TEMPLATE_DIRECTION_INITIATOR + "0";
    }

    @PostMapping("/initiator")
    public String initiatorSubmit(@ModelAttribute("initiator") Initiator initiator, @ModelAttribute("project") ProjectRequest project, BindingResult bindingResult, Model model) {
        UtilsService.securityVueJS(initiator);

        Assessment assessment = project.calcInitiator();

        model.addAttribute("error", true);
        List<String> fieldsName = new ArrayList<>();
        if (!ListUtils.isEmpty(assessment.badFields)) {
            for (String badField : assessment.badFields) {
                Field titleField = fr.findFirstByField(badField);
                fieldsName.add(titleField == null ? badField : titleField.getTitle());
            }
        }

        model.addAttribute("fields", fieldsName);

        return processingSubmitForm(4, TEMPLATE_DIRECTION_INITIATOR, initiator, project);
    }

    @GetMapping("/initiator/data")
    @ResponseBody
    public Initiator initiatorData(@ModelAttribute("project") ProjectRequest project) {
        Initiator initiator = new Initiator();
        initiator.getData(project);
        return initiator;
    }


    // ----------------------- project-main -----------------------
    @ModelAttribute("projectMain")
    public ProjectMain initProjectMain() {
        return new ProjectMain();
    }

    @GetMapping("/project-main")
    public String projectMainGet(@ModelAttribute("project") ProjectRequest project, @ModelAttribute("projectMain") ProjectMain projectMain) {
        projectMain.numberPage = 0;
        projectMain.getData(project);
        return TEMPLATE_PROJECT_MAIN + "0";
    }

    @PostMapping("/project-main")
    public String projectMainSubmit(@ModelAttribute("projectMain") ProjectMain projectMain, @ModelAttribute("project") ProjectRequest project, Model model) {
        UtilsService.securityVueJS(projectMain);

        Assessment assessment = project.calcProjectMain();

        model.addAttribute("error", true);
        List<String> fieldsName = new ArrayList<>();
        if (!ListUtils.isEmpty(assessment.badFields)) {
            for (String badField : assessment.badFields) {
                Field titleField = fr.findFirstByField(badField);
                fieldsName.add(titleField == null ? badField : titleField.getTitle());
            }
        }

        model.addAttribute("fields", fieldsName);

        return processingSubmitForm(1, TEMPLATE_PROJECT_MAIN, projectMain, project);
    }

    @GetMapping("/projectMain")
    @ResponseBody
    public ProjectMain getProjectMain(@ModelAttribute("project") ProjectRequest project) {
        ProjectMain projectMain = new ProjectMain();
        projectMain.getData(project);
        return projectMain;
    }

    // ----------------------- direction-industry -----------------------
    @ModelAttribute("directionIndustry")
    public DirectionIndustry initDirectionIndustry() {
        return new DirectionIndustry();
    }

    @GetMapping("/direction-industry")
    public String directionIndustryGet(@ModelAttribute("directionIndustry") DirectionIndustry directionIndustry, @ModelAttribute("project") ProjectRequest project, Model model) {
        Assessment assessment = project.calcDirectionIndustry();

        model.addAttribute("error", true);
        List<String> fieldsName = new ArrayList<>();
        if (!ListUtils.isEmpty(assessment.badFields)) {
            for (String badField : assessment.badFields) {
                Field titleField = fr.findFirstByField(badField);
                fieldsName.add(titleField == null ? badField : titleField.getTitle());
            }
        }

        model.addAttribute("fields", fieldsName);

        directionIndustry.numberPage = 0;
        directionIndustry.getData(project);
        return TEMPLATE_DIRECTION_INDUSTRY + "0";
    }

    @PostMapping("/direction-industry")
    public String directionIndustrySubmit(@ModelAttribute("directionIndustry") DirectionIndustry directionIndustry, @ModelAttribute("project") ProjectRequest project) {
        UtilsService.securityVueJS(directionIndustry);

        return processingSubmitForm(1, TEMPLATE_DIRECTION_INDUSTRY, directionIndustry, project);
    }

    // ----------------------- influence -----------------------
    @ModelAttribute("influence")
    public Influence initInfluence() {
        return new Influence();
    }

    @GetMapping("/influence")
    public String influence(@ModelAttribute("influence") Influence influence, @ModelAttribute("project") ProjectRequest project) {
        influence.numberPage = 0;
        influence.getData(project);
        return TEMPLATE_DIRECTION_INFLUENCE + "0";
    }

    @PostMapping("/influence")
    public String influenceSubmit(@ModelAttribute("influence") Influence influence, @ModelAttribute("project") ProjectRequest project,
                                  @RequestParam(name = "files[]", required = false) MultipartFile files[], Model model) throws IOException {

        UtilsService.securityVueJS(influence);

        Assessment assessment = project.calcInfluence();

        model.addAttribute("error", true);
        List<String> fieldsName = new ArrayList<>();
        if (!ListUtils.isEmpty(assessment.badFields)) {
            for (String badField : assessment.badFields) {
                Field titleField = fr.findFirstByField(badField);
                fieldsName.add(titleField == null ? badField : titleField.getTitle());
            }
        }

        model.addAttribute("fields", fieldsName);

        if (files != null && files.length > 0) {
            for (MultipartFile file : files) {
                if (file.getSize() > 0)
                    sp.saveProjectFile(project.projectId, file, project.getInitiatorInn(), project.getProjectCode());
            }
        }

        return processingSubmitForm(6, TEMPLATE_DIRECTION_INFLUENCE, influence, project);
    }

    @GetMapping("/influence/data")
    @ResponseBody
    public Influence influenceData(@ModelAttribute("project") ProjectRequest project) {
        Influence influence = new Influence();
        influence.getData(project);
        return influence;
    }

    // ----------------------- project-characteristics -----------------------
    @ModelAttribute("projectCharacteristics")
    public ProjectCharacteristics initProjectCharacteristics() {
        return new ProjectCharacteristics();
    }

    @GetMapping("/project-characteristics")
    public String projectCharacteristics(@ModelAttribute("projectCharacteristics") ProjectCharacteristics projectCharacteristics, @ModelAttribute("project") ProjectRequest project) {
        projectCharacteristics.numberPage = 0;
        projectCharacteristics.getData(project);
        return TEMPLATE_PROJECT_CHARACTERISTICS + "0";
    }

    @PostMapping("/project-characteristics")
    public String projectCharacteristicsSubmit(@ModelAttribute("projectCharacteristics") ProjectCharacteristics projectCharacteristics, @ModelAttribute("project") ProjectRequest project, Model model) {
        UtilsService.securityVueJS(projectCharacteristics);

        Assessment assessment = project.calcProjectCharacteristic();

        model.addAttribute("error", true);
        List<String> fieldsName = new ArrayList<>();
        if (!ListUtils.isEmpty(assessment.badFields)) {
            for (String badField : assessment.badFields) {
                Field titleField = fr.findFirstByField(badField);
                fieldsName.add(titleField == null ? badField : titleField.getTitle());
            }
        }

        model.addAttribute("fields", fieldsName);

        return processingSubmitForm(4, TEMPLATE_PROJECT_CHARACTERISTICS, projectCharacteristics, project);
    }

    @GetMapping("/project-characteristics/data")
    @ResponseBody
    public ProjectCharacteristics projectCharacteristicsData(@ModelAttribute("project") ProjectRequest project, ProjectCharacteristics projectC) throws NoSuchMethodException {
        ProjectCharacteristics projectCharacteristics = new ProjectCharacteristics();
        projectCharacteristics.getData(project);
        return projectCharacteristics;
    }

    public String firstUpperCase(String word){
        if(word == null || word.isEmpty()) return "";//или return word;
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }

    // ----------------------- business-model -----------------------
    @ModelAttribute("businessModel")
    public BusinessModel initBusinessModel() {
        return new BusinessModel();
    }

    @GetMapping("/business-model")
    public String businessModel(@ModelAttribute("businessModel") BusinessModel businessModel, @ModelAttribute("project") ProjectRequest project) {
        businessModel.numberPage = 0;
        businessModel.getData(project);
        return TEMPLATE_BUSINESS_MODEL + "0";
    }

    @PostMapping("/business-model")
    public String businessModelSubmit(@ModelAttribute("businessModel") BusinessModel businessModel, @ModelAttribute("project") ProjectRequest project,
                                      @RequestParam(name = "files[]", required = false) MultipartFile files[], Model model) throws IOException {
        UtilsService.securityVueJS(businessModel);

        Assessment assessment = project.calcBusinessModel();

        model.addAttribute("error", true);
        List<String> fieldsName = new ArrayList<>();
        if (!ListUtils.isEmpty(assessment.badFields)) {
            for (String badField : assessment.badFields) {
                Field titleField = fr.findFirstByField(badField);
                fieldsName.add(titleField == null ? badField : titleField.getTitle());
            }
        }

        model.addAttribute("fields", fieldsName);


        if (files != null && files.length > 0) {
            for (MultipartFile file : files) {
                if (file.getSize() > 0)
                    sp.saveProjectFile(project.projectId, file, project.getInitiatorInn(), project.getProjectCode());
            }
        }
        return processingSubmitForm(5, TEMPLATE_BUSINESS_MODEL, businessModel, project);
    }

    @GetMapping("/business-model/data")
    @ResponseBody
    public BusinessModel businessModelData(@ModelAttribute("project") ProjectRequest project) {
        BusinessModel businessModel = new BusinessModel();
        businessModel.getData(project);
        return businessModel;
    }

    // ----------------------- SendFinanceRequest -----------------------
    @ModelAttribute("sendFinanceRequest")
    public SendFinanceRequest initSendFinanceRequest() {
        return new SendFinanceRequest();
    }

    @GetMapping("/complete")
    public String sendFinanceRequest(@ModelAttribute("sendFinanceRequest") SendFinanceRequest sendFinanceRequest, @ModelAttribute("project") ProjectRequest project) {
        sendFinanceRequest.numberPage = 0;
        sendFinanceRequest.getData(project);
        return TEMPLATE_SEND_FINANCE_REQUEST + "0";
    }

    @PostMapping("/complete")
    public String sendFinanceRequestSubmit(@ModelAttribute("sendFinanceRequest") SendFinanceRequest sendFinanceRequest, @ModelAttribute("project") ProjectRequest project) {
        UtilsService.securityVueJS(sendFinanceRequest);

        if (sendFinanceRequest.getNumberPage() >= 2) {
            sendFinanceRequest.setBack(null);
            sendFinanceRequest.setNext(null);
            return "redirect:/private-office";

        } else if (sendFinanceRequest.getNumberPage() == 1) {

            nr.saveAndFlush(new Notification(project.getApplicantEmail(), project.getProjectCertifiedManagerId(), project.getProjectId(), project.getProjectNomination(), "успешно отправлен запрос на финансирование", true));


            // отправим письмо инициатору
            es.sendSimpleMessage(project.getApplicantEmail(),
                    String.format("Уведомление об изменении статуса запроса на финансирование проекта \"%s\"", project.getProjectNomination()),
                    String.format("Уважаемый(ая), %s %s! \n" +
                            "\n" +
                            "Внешэкономбанк информирует Вас об успешном отправленном запросе на финансирование." +
                            "За дополнительной информацией Вы можете обратиться к своему Региональному менеджеру.", project.getApplicantNameFirst(), project.getApplicantNameMid()));


            // отправим письмо манагеру
            Manager manager = cs.getManagerById(project.getProjectCertifiedManagerId());
            es.sendSimpleMessage(manager.getEmail(),
                    String.format("Уведомление об изменении статуса запроса на финансирование проекта \"%s\"", project.getProjectNomination()),
                    String.format("Уважаемый(ая), %s %s! \n" +
                            "\n" +
                            "Внешэкономбанк информирует Вас об успешном отправленном запросе на финансирование.", manager.getNameFirst(), manager.getNameLast()));




            ProjectEntity projectEntity = projectRepository.findOne(project.getProjectId());
            if (projectEntity != null) {
                BeanUtils.copyProperties(project, projectEntity);
                projectRepository.saveAndFlush(projectEntity);
            }

            cs.addValuation(project.getProjectId(), sendFinanceRequest.managerValuation, sendFinanceRequest.managerValuationComment, project.getProjectCertifiedManagerId());
            cs.addValuation(project.getProjectId(), sendFinanceRequest.platformValuation, sendFinanceRequest.platformValuationComment, null);

            sendFinanceRequest.setProjectRequestStatus(7);
            project.setProjectRequestStatus(sendFinanceRequest.getProjectRequestStatus());
            return processingSubmitForm(2, TEMPLATE_SEND_FINANCE_REQUEST, sendFinanceRequest, project);
        } else {
            return processingSubmitForm(2, TEMPLATE_SEND_FINANCE_REQUEST, sendFinanceRequest, project);
        }
    }

    @GetMapping("/complete/data")
    @ResponseBody
    public SendFinanceRequest sendFinanceRequestData(@ModelAttribute("project") ProjectRequest project) {
        SendFinanceRequest sendFinanceRequest = new SendFinanceRequest();
        sendFinanceRequest.getData(project);
        return sendFinanceRequest;
    }

    // ----------------------- private -----------------------
    private String processingSubmitForm(int maxPage, String baseUrl, Form form, ProjectRequest project) {
        UtilsService.securityVueJS(form);

        if (form.getSave() != null) {

            form.setSave(null);
            cs.updateProject(form);
            return "redirect:/private-office/finance-request?id=" + project.getProjectId();

        } else if (form.getBack() != null && form.getNumberPage() > 0) {
            form.setBack(null);
            form.setNumberPage(form.getNumberPage() - 1);
            return baseUrl + String.valueOf(form.getNumberPage());

        } else { //projectMain.getNext()
            form.saveData(project);

            form.setBack(null);
            form.setNext(null);

            if (form.getNumberPage() >= maxPage) {
                return "redirect:/private-office/finance-request?id=" + project.getProjectId();
            } else {
                if (form.getNumberPage() == maxPage - 1) cs.updateProject(form);
                form.setNumberPage(form.getNumberPage() + 1);
                return baseUrl + String.valueOf(form.getNumberPage());
            }
        }
    }
}
