package ru.veb.eo.controller;

import com.google.common.base.Strings;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.pdf.BaseFont;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.itext.extension.font.IFontProvider;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StreamUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.util.ArrayUtils;
import org.thymeleaf.util.ListUtils;
import org.thymeleaf.util.StringUtils;
import ru.veb.eo.Properties;
import ru.veb.eo.VebEoApplication;
import ru.veb.eo.model.Reference;
import ru.veb.eo.model.Role;
import ru.veb.eo.model.User;
import ru.veb.eo.model.crm.*;
import ru.veb.eo.model.form.Form;
import ru.veb.eo.model.form.resume.*;
import ru.veb.eo.model.reference.Field;
import ru.veb.eo.model.reference.Notification;
import ru.veb.eo.model.reference.ProjectEntity;
import ru.veb.eo.repository.FieldsRepository;
import ru.veb.eo.repository.NotificationRepository;
import ru.veb.eo.repository.ProjectRepository;
import ru.veb.eo.service.*;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;

@Controller
@RequestMapping("/private-office/resume")
@SessionAttributes(value = {"user", "project", "file", "file1", "file2"}, types = {User.class, ProjectRequest.class, FileTmp.class, FileTmp.class, FileTmp.class})
public class POResumeController {
    private static final Logger logger = LoggerFactory.getLogger(POResumeController.class);
    private static final String TEMPLATE_ABSTRACT = "LK/resume/resume-";

    private CrmService cs;
    private ReferenceService rs;
    private NotificationRepository nr;
    private SharePointService sp;
    private FieldsRepository fr;
    private ProjectRepository projectRepository;
    private Properties properties;
    private EmailService es;
    private UtilsService utilsService;

    @Autowired
    public POResumeController(CrmService cs, ReferenceService rs, NotificationRepository nr, SharePointService sp, FieldsRepository fr, ProjectRepository projectRepository, Properties properties, EmailService es, UtilsService utilsService) {
        this.cs = cs;
        this.rs = rs;
        this.nr = nr;
        this.sp = sp;
        this.fr = fr;
        this.projectRepository = projectRepository;
        this.properties = properties;
        this.es = es;
        this.utilsService = utilsService;
    }

    @ModelAttribute("user")
    public User initUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();
        user.setPage("abstract");
        if (user.getAuthorities().contains(Role.MANAGER))
            user.setNotifications(nr.findByProjectCertifiedManagerIdAndIsViewed(user.getCrmId(), false));
        else
            user.setNotifications(nr.findByApplicantEmailAndIsViewed(user.getUsername(), false));
        return user;
    }

    @ModelAttribute("project")
    public ProjectRequest initProject() {
        return new ProjectRequest();
    }

    @ModelAttribute("ref")
    public Reference initReference() {
        return rs.getReference();
    }

    @ModelAttribute("file")
    public FileTmp initFile() {
        return new FileTmp();
    }

    @ModelAttribute("file1")
    public FileTmp initFile1() {
        return new FileTmp();
    }

    @ModelAttribute("file2")
    public FileTmp initFile2() {
        return new FileTmp();
    }

    @GetMapping
    private String initialization(Model model, @RequestParam(name = "id", required = false) String id, @ModelAttribute("user") User user) {
        ProjectRequest project = new ProjectRequest();
        if (id != null) {
            if (user.getAuthorities().contains(Role.MANAGER))
                project = cs.getProjectRequest(id, null, user.getCrmId());
            else
                project = cs.getProjectRequest(id, user.getUsername(), null);
        } else {
            if (properties.getDebugMode()) {
                project = cs.getProjectRequest("57320488-b0dd-52b9-e693-5a2e3e6f27ff", null, "c7f63b4a-7a91-1630-2332-59ae8aa97087");
                if (null != project)
                    project.projectId = null;
                else
                    project = new ProjectRequest();
            }
        }
        model.addAttribute("project", project);
        return "redirect:/private-office/resume/1";
    }

    @GetMapping("/{id:Finish|[0-8]}")
    public String resume(@PathVariable String id, @ModelAttribute("project") ProjectRequest project, Model model)
            throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        Form resume = (Form) Class.forName("ru.veb.eo.model.form.resume.Resume" + id).newInstance();
        resume.getData(project);
        model.addAttribute("resume", resume);
        model.addAttribute("projectId", project.getProjectId());
        logger.debug("Resume get {}. Resume={}", id, resume.toString());
        return TEMPLATE_ABSTRACT + id.toLowerCase();
    }

    @GetMapping("/project")
    @ResponseBody
    public ProjectRequestDataTransfer abstract1Data(@ModelAttribute("project") ProjectRequest project) {
        return new ProjectRequestDataTransfer(project);
    }

    @PostMapping("/1")
    public String resumePost(Resume1 form,
                             @ModelAttribute("project") ProjectRequest project, BindingResult bindingResult,
                             Model model,
                             @RequestParam(name = "file", required = false) MultipartFile file) throws IOException {

        if (!Strings.isNullOrEmpty(project.projectId)) {
            try {
                if (file != null && file.getSize() > 0) {
                    sp.saveProjectFile(project.projectId, file, project.getInitiatorInn(), project.getProjectCode());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (!file.isEmpty()) {
                FileTmp file1 = new FileTmp();
                file1.content = file.getBytes();
                file1.fileName = file.getOriginalFilename();
                model.addAttribute("file", file1);
            }
        }
        return saveProjectData(1, form, project, model);
    }

    @PostMapping("/2")
    public String resumePost(Resume2 form,
                             @ModelAttribute("project") ProjectRequest project, BindingResult bindingResult,
                             Model model) {

        return saveProjectData(2, form, project, model);
    }

    @PostMapping("/3")
    public String resumePost(Resume3 form,
                             @ModelAttribute("project") ProjectRequest project, BindingResult bindingResult,
                             Model model) {

        return saveProjectData(3, form, project, model);
    }

    @PostMapping("/4")
    public String resumePost(Resume4 form,
                             @ModelAttribute("project") ProjectRequest project, BindingResult bindingResult,
                             Model model) {

        return saveProjectData(4, form, project, model);
    }

    @PostMapping("/5")
    public String resumePost(Resume5 form,
                             @ModelAttribute("project") ProjectRequest project, BindingResult bindingResult,
                             Model model) {

        return saveProjectData(5, form, project, model);
    }

    @PostMapping("/6")
    public String resumePost(Resume6 form,
                             @ModelAttribute("project") ProjectRequest project, BindingResult bindingResult,
                             Model model) {

        return saveProjectData(6, form, project, model);
    }

    @PostMapping("/7")
    public String resumePost(Resume7 form,
                             @ModelAttribute("project") ProjectRequest project, BindingResult bindingResult,
                             Model model,
                             @RequestParam(name = "file1", required = false) MultipartFile file1,
                             @RequestParam(name = "file2", required = false) MultipartFile file2) throws IOException {

        if (!Strings.isNullOrEmpty(project.projectId)) {
            try {
                if (file1 != null && file1.getSize() > 0) {
                    sp.saveProjectFile(project.projectId, file1, project.getInitiatorInn(), project.getProjectCode());
                }

                if (file2 != null && file2.getSize() > 0) {
                    sp.saveProjectFile(project.projectId, file2, project.getInitiatorInn(), project.getProjectCode());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (!file1.isEmpty()) {
                FileTmp filet1 = new FileTmp();
                filet1.content = file1.getBytes();
                filet1.fileName = file1.getOriginalFilename();
                model.addAttribute("file1", filet1);
            }
            if (!file2.isEmpty()) {
                FileTmp filet2 = new FileTmp();
                filet2.content = file2.getBytes();
                filet2.fileName = file2.getOriginalFilename();
                model.addAttribute("file2", filet2);
            }
        }

        return saveProjectData(7, form, project, model);
    }

    @PostMapping("/8")
    public String resumePost(Resume8 form,
                             @ModelAttribute("project") ProjectRequest project, BindingResult bindingResult,
                             @ModelAttribute("file") FileTmp file,
                             @ModelAttribute("file1") FileTmp file1,
                             @ModelAttribute("file2") FileTmp file2,
                             Model model) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();
        project.setProjectCertifiedManagerId(user.getCrmId());

        String projectId = cs.addProject(project);
        project = cs.getProjectRequest(projectId, project.applicantEmail, project.projectCertifiedManagerId);

        try {
            if (file != null && file.content.length > 0) {
                sp.saveProjectBytes(project.projectId, file.fileName, file.content, project.getInitiatorInn(), project.getProjectCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (file1 != null && file1.content.length > 0) {
                sp.saveProjectBytes(project.projectId, file1.fileName, file1.content, project.getInitiatorInn(), project.getProjectCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (file2 != null && file2.content.length > 0) {
                sp.saveProjectBytes(project.projectId, file2.fileName, file2.content, project.getInitiatorInn(), project.getProjectCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if ("dzo".equals(form.getWhere_send())) {
            StringBuilder sb = new StringBuilder();
            List<String> dzoMessage = new ArrayList<>();

            cs.updateProjectStage(6, project);

            Manager manager = cs.getManagerById(project.getProjectCertifiedManagerId());
            String managerInfo = "";
            try {
                String region = rs.getRegion(Integer.valueOf(manager.getRegionId()));
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder
                        .append(manager.getNameLast() != null ? " " + manager.getNameLast() : "")
                        .append(manager.getNameFirst() != null ? " " + manager.getNameFirst() : "")
                        .append(manager.getNameMiddle() != null ? " " + manager.getNameMiddle() : "")
                        .append(", ");
                if (!Strings.isNullOrEmpty(manager.getPhone()))
                    stringBuilder.append(manager.getPhone()).append(", ");
                if (!Strings.isNullOrEmpty(manager.getEmail()))
                    stringBuilder.append(manager.getEmail()).append(" ");
                managerInfo = region + stringBuilder.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!ArrayUtils.isEmpty(form.getDzo_send())) {
                // переделать на нормальную модель.... хз почему сразу так не сделали, но сейчас времени нету :(
                List<String> dzoCompanyName = new ArrayList<>();
                List<String> dzo = Arrays.asList(form.getDzo_send());
                if (dzo.contains("fcpf")) {
                    dzoCompanyName.add("ОАО «ФЦПФ»");
                    dzoMessage.add(" - Мукумов Ремир Эркинович, 7-495-777-39-93 доб. 2299, R.Mukumov@fcpf.ru");
                    sendEmail(project, "R.Mukumov@fcpf.ru", ", Ремир Эркинович", managerInfo, project.projectNomination);
                }
                if (dzo.contains("dvostok")) {
                    dzoCompanyName.add("Фонд развития Дальнего Востока (ФРДВ)");
                    dzoMessage.add(" - Карчевский Святослав Владиславович, 7 (499) 540-47-37, karchevsky@fondvostok.ru");
                    sendEmail(project, "karchevsky@fondvostok.ru", ", Святослав Владиславович", managerInfo, project.projectNomination);
                }
                if (dzo.contains("sv-bank")) {
                    dzoCompanyName.add("ПАО АКБ «Связьбанк»");
                    dzoMessage.add(" - Евгений Лаврентьев, (495) 500-0042 (вн.т. 6586), ELavrentev@sviaz-bank.ru");
                    sendEmail(project, "ELavrentev@sviaz-bank.ru", ", Евгений Лаврентьев", managerInfo, project.projectNomination);
                }
                if (dzo.contains("globexpbank")) {
                    dzoCompanyName.add("АО «Глобэксбанк»");
                    dzoMessage.add(" - Оксана Панченко, (495)5140623, o.panchenko@globexbank.ru");
                    sendEmail(project, "o.panchenko@globexbank.ru", ", Оксана Панченко", managerInfo, project.projectNomination);
                }
                if (dzo.contains("veb-lizing")) {
                    dzoCompanyName.add("АО «ВЭБ-Лизинг»");
                    dzoMessage.add(" - Бирюкова Ольга Николаевна, (495) 981-42-40 доб. 2542, O.Biryukova@veb-leasing.ru");
                    sendEmail(project, "O.Biryukova@veb-leasing.ru", ", Ольга Николаевна", managerInfo, project.projectNomination);
                }
                if (dzo.contains("veb-injiring")) {
                    dzoCompanyName.add("ООО «ВЭБ-Инжинирнг»");
                    dzoMessage.add(" - Новицкая Ирина Викторовна, 495 258 84 20 доб. 3208, niv@vebeng.ru");
                    sendEmail(project, "niv@vebeng.ru", ", Ирина Викторовна", managerInfo, project.projectNomination);
                }
                if (dzo.contains("veb-inovation")) {
                    dzoCompanyName.add("ООО «ВЭБ-Инновации»");
                    dzoMessage.add(" - info@vebinnovation.ru");
                    sendEmail(project, "info@vebinnovation.ru", "", managerInfo, project.projectNomination);
                }


                  // отправим письмо инициатору
                    es.sendSimpleMessage(project.getApplicantEmail(),
                            String.format("Резюме проекта \"%s\" отпрвлено в ДЗО", project.getProjectNomination()),
                            String.format("Уважаемый(ая), %s %s! \n" +
                                    "\n" +
                                    "Внешэкономбанк информирует Вас об отправке Вашего резюме следующим ДЗО: %s." +
                                    "За дополнительной информацией Вы можете обратиться к своему Региональному менеджеру.", project.getApplicantNameFirst(), project.getApplicantNameMid(), StringUtils.join(dzoCompanyName.toArray(), ", ")));


                    // отправим письмо манагеру
                    es.sendSimpleMessage(manager.getEmail(),
                            String.format("Резюме проекта \"%s\" отправлено в ДЗО", project.getProjectNomination()),
                            String.format("Уважаемый(ая), %s %s! \n" +
                                    "\n" +
                                    "Внешэкономбанк информирует Вас об отправке резюме проекта \"%s\" следующим ДЗО: %s.", manager.getNameFirst(), manager.getNameLast(), project.getProjectNomination(), StringUtils.join(dzoCompanyName.toArray(), ", ")));


            }



            model.addAttribute("dzo", true);
            model.addAttribute("dzos", dzoMessage);
        } else {
            cs.updateProjectStage(0, project);
            model.addAttribute("dzo", false);
            model.addAttribute("dzos", null);
            // TODO сделать ЛК клиенту, а надо ли?
        }

        return TEMPLATE_ABSTRACT + "finish";
    }

    private void sendEmail(ProjectRequest project, String email, String fio, String managerInfo, String projectName) {
        if (!properties.isDzo())
            email = properties.getDefaultManager();

        try {
            InputStream in = getClass().getResourceAsStream("/resumeTemplate.docx");
            IXDocReport report = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Velocity);
            IContext context = report.createContext();
            Map<String, Object> contextObjects = utilsService.getContextObjects(project);
            contextObjects.forEach((key, value) -> context.put(key, value));
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            report.process(context, out);
            ByteArrayResource out2 = new ByteArrayResource(out.toByteArray());

            StringBuilder sb = new StringBuilder();
            sb.append("Добрый день").append(fio).append("!\n\n");
            sb.append("Региональный менеджер Внешэкономбанка в ").append(managerInfo);
            sb.append("направляет Вам на рассмотрение проект ").append(projectName).append(".");


            es.sendMessageWithAttachment(email, "Новый проект", sb.toString(), "project.docx", out2);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @PostMapping("/finish")
    public String resumePost(
            @ModelAttribute("project") ProjectRequest project, BindingResult bindingResult,
            Model model,
            @RequestParam(name = "files[]", required = false) MultipartFile files[]) {
        return "redirect:/private-office";
    }


    private String saveProjectData(int pageNumber, Form form, ProjectRequest project, Model model) {
        UtilsService.securityVueJS(form);
        model.addAttribute("projectId", project.getProjectId());
        form.setProjectId(project.getProjectId());

        form.saveData(project);
        if (!Strings.isNullOrEmpty(project.projectId)) {
            cs.updateProject(form);
            cs.closeTasks(form.getProjectId(), Arrays.asList("Ввод ПО"), null);
        }

        if (pageNumber >= 7) {
            Assessment assessment1 = project.calcResume1();
            Assessment assessment2 = project.calcResume2();
            if (assessment1.result() + assessment2.result() >= 199) {
                if (!Strings.isNullOrEmpty(project.projectId)) {
                    cs.updateProjectStage(1, project);

                    nr.saveAndFlush(new Notification(project.getApplicantEmail(), project.getProjectCertifiedManagerId(), project.getProjectId(), project.getProjectNomination(), "заполнено резюме", true));

                    // отправим письмо инициатору
                    es.sendSimpleMessage(project.getApplicantEmail(),
                            String.format("Уведомление об изменении статуса заполнения резюме проекта \"%s\"", project.getProjectNomination()),
                            String.format("Уважаемый(ая), %s %s! \n" +
                                    "\n" +
                                    "Внешэкономбанк информирует Вас об успешном заполнении резюме." +
                                    "За дополнительной информацией Вы можете обратиться к своему Региональному менеджеру.", project.getApplicantNameFirst(), project.getApplicantNameMid()));


                    // отправим письмо манагеру
                    Manager manager = cs.getManagerById(project.getProjectCertifiedManagerId());
                    es.sendSimpleMessage(manager.getEmail(),
                            String.format("Уведомление об изменении статуса заполнения резюме проекта \"%s\"", project.getProjectNomination()),
                            String.format("Уважаемый(ая), %s %s! \n" +
                                    "\n" +
                                    "Внешэкономбанк информирует Вас об успешном заполнении резюме.", manager.getNameFirst(), manager.getNameLast()));


                    ProjectEntity projectEntity = projectRepository.findOne(form.getProjectId());
                    if (projectEntity != null) {
                        BeanUtils.copyProperties(form, projectEntity);
                        projectRepository.saveAndFlush(projectEntity);
                    }

                    cs.closeTasks(form.getProjectId(), Arrays.asList("Ввод ПО", "Заполнение резюме проекта"), null);
                    try {
                        form.saveData(project);
                        saveDocx(project);
                        savePdf(project);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    return "redirect:/private-office/resume/8";
                }

                model.addAttribute("error", false);
            } else {
                model.addAttribute("error", true);
                List<String> fieldsName = new ArrayList<>();
                if (!ListUtils.isEmpty(assessment1.badFields)) {
                    for (String badField : assessment1.badFields) {
                        Field titleField = fr.findFirstByField(badField);
                        fieldsName.add(titleField == null ? badField : titleField.getTitle());
                    }
                }
                if (!ListUtils.isEmpty(assessment2.badFields)) {
                    for (String badField : assessment2.badFields) {
                        Field titleField = fr.findFirstByField(badField);
                        fieldsName.add(titleField == null ? badField : titleField.getTitle());
                    }
                }
                model.addAttribute("fields", fieldsName);
            }
        }

        if (!Strings.isNullOrEmpty(form.getBack())) {
            if (pageNumber <= 1)
                return "redirect:/private-office";
            else
                return "redirect:/private-office/resume/" + String.valueOf(pageNumber - 1);
        } else {
            if (pageNumber < 7)
                return "redirect:/private-office/resume/" + String.valueOf(pageNumber + 1);
            else
                return TEMPLATE_ABSTRACT + "finish";
        }
    }

    @GetMapping(value = "/concept-docx", produces = "application/octet-stream")
    public void getConceptDocx(@ModelAttribute("project") ProjectRequest project, HttpServletResponse response) throws IOException, XDocReportException {
        InputStream in = getClass().getResourceAsStream("/concept-template.docx");
        IXDocReport report = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Velocity);
        IContext context = report.createContext();
        Map<String, Object> contextObjects = utilsService.getContextObjects(project);
        contextObjects.forEach((key, value) -> context.put(key, value));

        response.setContentType("application/octet-stream");
        response.addHeader("Content-Disposition", "attachment; filename=concept.docx");
        OutputStream out = response.getOutputStream();
        report.process(context, out);
    }


    private void saveDocx(@ModelAttribute("project") ProjectRequest project) throws IOException, XDocReportException {
        InputStream in = getClass().getResourceAsStream("/resumeTemplate.docx");
        IXDocReport report = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Velocity);
        IContext context = report.createContext();
        Map<String, Object> contextObjects = utilsService.getContextObjects(project);
        contextObjects.forEach((key, value) -> context.put(key, value));
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        report.process(context, out);
        sp.saveProjectBytesStream(project.projectId, "resume.docx", out, project.getInitiatorInn(), project.getProjectCode());
    }

    private void savePdf(@ModelAttribute("project") ProjectRequest project) throws IOException, XDocReportException {
        InputStream in = getClass().getResourceAsStream("/resumeTemplate.docx");
        IXDocReport report = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Velocity);
        IContext context = report.createContext();
        Map<String, Object> contextObjects = utilsService.getContextObjects(project);
        contextObjects.forEach((key, value) -> context.put(key, value));

        Options options = Options.getTo(ConverterTypeTo.PDF);

        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(getClass().getClassLoader());
        Resource[] res = resolver.getResources("classpath:/font/**");
        final String tmpDir = VebEoApplication.env("TMPDIR", System.getProperty("java.io.tmpdir"));
        for (Resource fr : res) {
            File f = new File(tmpDir + "/" + fr.getFilename().toLowerCase());
            if (!f.exists()) {
//                logger.info("Saving " + fr.getFilename() + " to " + f);
                FileOutputStream fos = new FileOutputStream(f);
                try {
                    StreamUtils.copy(fr.getInputStream(), fos);
                } finally {
                    fos.close();
                }
            }
        }

        PdfOptions pdfOptions = PdfOptions.create();
        pdfOptions.fontProvider(new IFontProvider() {
            @Override
            public Font getFont(String familyName, String encoding, float size, int style, Color color) {
                try {
                    String suffix = "";
                    if (style == Font.NORMAL) {
                        suffix = "";
                    } else if (style == Font.ITALIC) {
                        suffix = "i";
                    } else if (style == Font.BOLD) {
                        suffix = "b";
                    } else if (style == Font.BOLDITALIC) {
                        suffix = "bi";
                    }
                    File f = new File(tmpDir + "/" + familyName.toLowerCase() + suffix + ".ttf");
//                    logger.info("Requested font {} checking {}", familyName, f.getAbsolutePath());
                    if (f.exists()) {
//                        logger.info("Returning font {}", f.getAbsolutePath());
                        BaseFont baseFont = BaseFont.createFont(f.getAbsolutePath(), encoding, BaseFont.EMBEDDED);
                        return new Font(baseFont, size, style, color);
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }

                return FontFactory.getFont(familyName, encoding, size, style, color);
            }
        });
        options.subOptions(pdfOptions);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        report.convert(context, options, out);

        sp.saveProjectBytesStream(project.projectId, "resume.pdf", out, project.getInitiatorInn(), project.getProjectCode());
    }
}

class FileTmp {
    String fileName;
    byte[] content;
}