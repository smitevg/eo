package ru.veb.eo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.ListUtils;
import ru.veb.eo.Properties;
import ru.veb.eo.model.Decision;
import ru.veb.eo.model.Role;
import ru.veb.eo.model.User;
import ru.veb.eo.model.crm.ProjectRequest;
import ru.veb.eo.model.crm.Task;
import ru.veb.eo.model.form.firstrequest.FirstRequest;
import ru.veb.eo.model.reference.Notification;
import ru.veb.eo.model.reference.ProjectEntity;
import ru.veb.eo.model.reference.ProjectSectors;
import ru.veb.eo.repository.NotificationRepository;
import ru.veb.eo.repository.ProjectRepository;
import ru.veb.eo.service.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/firstRequest")
public class MainFirstRequestController {
    private static final Logger logger = LoggerFactory.getLogger(MainFirstRequestController.class);

    @Autowired
    private HttpServletRequest servletRequest;
    
    @Autowired
    PasswordService pwd;

    private DecisionService ds;
    private CrmService cs;
    private UserService us;
    private EmailService emailService;
    private ReferenceService rs;
    private final NotificationRepository nr;
    private Properties properties;
    private ProjectRepository projectRepository;

    @Autowired
    public MainFirstRequestController(DecisionService ds, CrmService cs, UserService us, EmailService emailService, ReferenceService rs, NotificationRepository nr, Properties properties, ProjectRepository projectRepository) {
        this.ds = ds;
        this.cs = cs;
        this.us = us;
        this.emailService = emailService;
        this.rs = rs;
        this.nr = nr;
        this.properties = properties;
        this.projectRepository = projectRepository;
    }

    @GetMapping
    public String firstRequest() {
        return "firstRequest";
    }

    @PostMapping("/decision")
    @ResponseBody
    public Decision firstRequestDecision(@RequestBody FirstRequest project) {
        logger.debug("/firstRequest/decision. FirstRequest={}", project.toString());
        UtilsService.securityVueJS(project);
        return ds.calculateDecision(project);
    }

    @PostMapping("/submit")
    @ResponseBody
    public Decision firstRequestSubmit(@RequestBody FirstRequest firstRequest) throws JsonProcessingException {
        logger.debug("/firstRequest/submit. FirstRequest={}", firstRequest.toString());
        UtilsService.securityVueJS(firstRequest);

        firstRequest.setProjectDateCreating(new Date());
        firstRequest.setProjectRequestStatus(0);
        String projectSectors1 = firstRequest.getProjectSectors1();
        ProjectSectors sector = rs.getProjectSectorByOrder(Integer.valueOf(projectSectors1));
        firstRequest.setProjectSectors1(String.valueOf(sector.getId()));
        firstRequest.setProjectProjectIsAimedTo(String.valueOf(sector.getDirectionId()));

        List<User> tmp = us.getManagersByRegion(firstRequest.getProjectSalePlace());
        if (ListUtils.isEmpty(tmp) && Strings.isNullOrEmpty(firstRequest.getProjectCertifiedManagerId()) ) {
            User user = us.getUser(properties.getDefaultManager());
            if (user != null) {
                String crmId = user.getCrmId();
                if (!Strings.isNullOrEmpty(crmId)) firstRequest.setProjectCertifiedManagerId(crmId);
            }
        }
        ProjectRequest projectRequest = new ProjectRequest();
        BeanUtils.copyProperties(firstRequest, projectRequest);
        firstRequest.setProjectId(cs.addProject(projectRequest));

        String path = servletRequest.getScheme() + "://" + servletRequest.getServerName() + ":" + servletRequest.getServerPort() + servletRequest.getContextPath();

        User user = us.getUser(firstRequest.getApplicantEmail().toLowerCase());
        if (user == null) {

            String password = pwd.generatePassword(firstRequest.getApplicantEmail());
            logger.debug("password user {} = {}", firstRequest.getApplicantEmail(), password);

            try {
                emailService.sendSimpleMessage(firstRequest.getApplicantEmail(), "Зарегистрирован проект от вашего имени", "Уважаемый(ая), " + firstRequest.getApplicantNameFirst() + (!Strings.isNullOrEmpty(firstRequest.getApplicantNameMid()) ? " " + firstRequest.getApplicantNameMid() : "") + "! \n" +
                        "\n\nCсылка для входа в систему: " + path + "\n\nЛогин: " + firstRequest.getApplicantEmail() + "\n\nПароль: " + password);
            } catch (Exception e) {
                e.printStackTrace();
            }
            us.saveUser(User.builder()
                    .username(firstRequest.getApplicantEmail().toLowerCase())
                    .password(new BCryptPasswordEncoder().encode(password))
                    .nameFirst(firstRequest.getApplicantNameFirst())
                    .nameLast(firstRequest.getApplicantNameLast())
                    .nameMiddle(firstRequest.getApplicantNameMid())
                    .phone(firstRequest.getApplicantPhoneNumber())
                    .accountNonExpired(true)
                    .accountNonLocked(true)
                    .authorities(Arrays.asList(Role.USER))
                    .enabled(true)
                    .credentialsNonExpired(true)
                    .build()

            );
        } else {
            try {
                emailService.sendSimpleMessage(firstRequest.getApplicantEmail(), "Зарегистрирован проект от вашего имени", "Уважаемый(ая), " + firstRequest.getApplicantNameFirst() + (!Strings.isNullOrEmpty(firstRequest.getApplicantNameMid()) ? " " + firstRequest.getApplicantNameMid() : "") + "! \n" +
                        "\n\nЗарегистрирован проект от вашего имени.\n\nCсылка для входа в систему: " + path + "\n\nЛогин: " + firstRequest.getApplicantEmail());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        nr.saveAndFlush(new Notification(firstRequest.getApplicantEmail(), firstRequest.getProjectCertifiedManagerId(), firstRequest.getProjectId(), firstRequest.getProjectNomination(), "создан проект", true));

        ProjectEntity projectEntity = new ProjectEntity();
        BeanUtils.copyProperties(firstRequest, projectEntity);
        projectRepository.saveAndFlush(projectEntity);

        Decision decision = null;
        User manager = us.getManagerByCrmId(firstRequest.getProjectCertifiedManagerId());

        if (manager != null) {
            decision = new Decision("success", manager);
            try {
                StringBuilder sb = new StringBuilder();
                sb.append("Уважаемый(ая)")
                        .append(manager.getNameLast() != null ? " " + manager.getNameLast() : "")
                        .append(manager.getNameFirst() != null ? " " + manager.getNameFirst() : "")
                        .append(manager.getNameMiddle() != null ? " " + manager.getNameMiddle() : "")
                        .append(", в онлайн-платформе появился новый проект ")
                        .append("\"" + firstRequest.getProjectNomination() + "\"")
                        .append(",  по которому вы назначены региональным менеджером. Инициатор \"")
                        .append(firstRequest.getInitiatorNameShort())
                        .append("\". Вы можете ознакомиться с проектом в своем личном кабинете ")
                        .append(path)
                        .append(" и связаться с инициатором для уточнения деталей.");
                String message = sb.toString();
                if (!properties.getDebugMode())
                    emailService.sendSimpleMessage(manager.getUsername(), "Зарегистрирован проект", message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            decision = ds.calculateDecision(firstRequest);

            try {
                List<User> managers = decision.getManagers();
                if (!ListUtils.isEmpty(managers)) {
                    for (User manager1 : managers) {
                        if (!properties.getDebugMode()) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("Уважаемый(ая) ")
                                    .append(manager1.getNameLast() + " ")
                                    .append(manager1.getNameFirst())
                                    .append(manager1.getNameMiddle() != null ? " " + manager1.getNameMiddle() : "")
                                    .append(", в онлайн-платформе появился новый проект ")
                                    .append("\"" + firstRequest.getProjectNomination() + "\"")
                                    .append(". Инициатор \"")
                                    .append(firstRequest.getInitiatorNameShort())
                                    .append("\". Вы можете выступить в роли регионального менеджера по этому проекту. Для этого Вам необходимо взять проект в работу в своем личном кабинете ")
                                    .append(path)
                                    .append(" и связаться с инициатором для уточнения деталей.");
                            String message = sb.toString();

                            emailService.sendSimpleMessage(manager1.getUsername(), "Зарегистрирован проект", message);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        List<Task> tasks = cs.getTasks(firstRequest.getProjectId());
        if (!ListUtils.isEmpty(tasks)) {
            for (Task task : tasks) {
                if ("Ввод ПО".equals(task.getName())) cs.closeTask(task.getId(), null);
            }
        }

        return decision;
    }
}
