package ru.veb.eo.controller;

import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.pdf.BaseFont;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.itext.extension.font.IFontProvider;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;
import ru.veb.eo.Properties;
import ru.veb.eo.VebEoApplication;
import ru.veb.eo.entity.Project;
import ru.veb.eo.entity.UserProject;
import ru.veb.eo.model.Reference;
import ru.veb.eo.model.Role;
import ru.veb.eo.model.User;
import ru.veb.eo.model.crm.Manager;
import ru.veb.eo.model.crm.ProjectRequest;
import ru.veb.eo.model.crm.ProjectResponsePacket;
import ru.veb.eo.model.form.sindrequest.SindAdmRequest;
import ru.veb.eo.model.form.sindrequest.SindAdminEdit;
import ru.veb.eo.model.form.sindrequest.SindRequest;
import ru.veb.eo.model.form.sindrequest.SindVoice;
import ru.veb.eo.model.reference.Notification;
import ru.veb.eo.repository.MessageRepository;
import ru.veb.eo.repository.NotificationRepository;
import ru.veb.eo.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/private-office/sind")
@SessionAttributes(value = {"user", "sindrequest", "sindadmrequest"}, types = {User.class, SindRequest.class, SindAdmRequest.class})
public class PrivateOfficeSindController {

    private static final Logger logger = LoggerFactory.getLogger(PrivateOfficeSindController.class);
    private static final String redirectUrl = "redirect:/private-office";
    private Properties properties;
    private CrmService cs;
    private UserService us;
    private NotificationRepository nr;
    private ReferenceService rs;
    private EmailService emailService;
    private MessageRepository mr;
    private SharePointService sp;
    private MySqlService sql;


    @Autowired
    public PrivateOfficeSindController(MySqlService sql, CrmService cs, UserService us, NotificationRepository nr, ReferenceService rs, Properties properties, EmailService emailService, MessageRepository mr, SharePointService sp) {
        logger.trace("PrivateOfficeSindController");
        this.sql = sql;
        this.cs = cs;
        this.us = us;
        this.nr = nr;
        this.rs = rs;
        this.properties = properties;
        this.emailService = emailService;
        this.mr = mr;
        this.sp = sp;
    }

    @ModelAttribute("ref")
    public Reference initReference() {
        return rs.getReference();
    }

    @ModelAttribute("user")
    public User initUser() {
        logger.trace("initUser");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();

        user.setPage("private-office");
        if (user.getAuthorities().contains(Role.MANAGER))
            user.setNotifications(nr.findByProjectCertifiedManagerIdAndIsViewed(user.getCrmId(), false));
        else
            user.setNotifications(nr.findByApplicantEmailAndIsViewed(user.getCrmId(), false));

        return user;
    }


    @ModelAttribute("sindrequest")
    public SindRequest initSindRequest() {
        logger.trace("initSindRequest");
        SindRequest sindRequest = new SindRequest();
        return sindRequest;

    }

    @ModelAttribute("sindadmrequest")
    public SindAdmRequest initSindAdmRequest() {
        logger.trace("initSindAdmRequest");
        SindAdmRequest sindAdmRequest = new SindAdmRequest();
        return sindAdmRequest;

    }

    @GetMapping("/info")
    private String sind(@ModelAttribute("sindrequest") SindRequest sindrequest,
                        @ModelAttribute("user") User user,
                        Model model) {
        logger.trace("sind");
        if (isSind(user) == false) {
            return redirectUrl;
        }


        List<UserProject> userProjects = sql.getUserProject(user.getCrmId()).stream().filter(e -> e.getProject().getTypeProj().equals(CrmService.PR_SYNDICATE)).collect(Collectors.toList());
        sindrequest.initListProject(userProjects);

        logger.trace(user.getUsername());

        model.addAttribute("chooseMenu", "sind");
        model.addAttribute("error", true);
        model.addAttribute("notNeedAddFile", true);

        return "LK/sind/sind";
    }

    @GetMapping("/fab")
    private String fab(@ModelAttribute("sindrequest") SindRequest sindrequest,
                        @ModelAttribute("user") User user,
                        Model model) {
        logger.trace("sind");
        if (isFab(user) == false) {
            return redirectUrl;
        }

        List<ProjectResponsePacket> packets = cs.getProjectPacketSind(500,0, CrmService.PR_FABRIC,"","" );

        List<ProjectResponsePacket> packets2 = cs.getProjectPacketSind(500,0, CrmService.PR_SYNDICATE,"","" );

        List<UserProject> userProjects = sql.getUserProject(user.getCrmId()).stream().filter(e -> e.getProject().getTypeProj().equals(CrmService.PR_FABRIC)).collect(Collectors.toList());
        sindrequest.initListProject(userProjects);
        logger.trace(user.getUsername());

        model.addAttribute("chooseMenu", "fab");
        model.addAttribute("error", true);
        model.addAttribute("notNeedAddFile", true);

        return "LK/sind/fab";
    }




    @PostMapping("/info")
    private String sindPost(@ModelAttribute("sindrequest") SindRequest sindrequest,
                            @ModelAttribute("user") User user,
                            Model model) {

        if (isSindFab(user) == false) {
           return redirectUrl;
        }
        model.addAttribute("chooseMenu", "sind");
        model.addAttribute("error", true);

        return "LK/sind/sind";
    }

    @PostMapping("/fab")
    private String fabPost(@ModelAttribute("sindrequest") SindRequest sindrequest,
                           @ModelAttribute("user") User user,
                            Model model) {

        if (isFab(user) == false) {
            return redirectUrl;
        }
        model.addAttribute("chooseMenu", "fab");
        model.addAttribute("error", true);

        return "LK/sind/fab";
    }


    @GetMapping("/voice")
    private String sindVoice(@ModelAttribute("sindrequest") SindRequest sindrequest,
                             @ModelAttribute("user") User user,
                             @RequestParam(name = "id", required = false) String id,
                             Model model) {
        if (isSindFab(user) == false) {
            return redirectUrl;
        }
        logger.trace("sindVoice id was {0} now {1}", sindrequest.selectUserProjectId, id);
        if (id != null && id.length() > 0) {
            sindrequest.processSelectProject(Integer.parseInt(id));
        }
        logger.debug("getTypeVoice: {}", sindrequest.selectProject.getTypeVoice());

        if (CrmService.PR_SYNDICATE.equals(sindrequest.selectProject.getProject().getTypeProj())) {
            model.addAttribute("chooseMenu", "sind");
        } else {
            model.addAttribute("chooseMenu", "fab");
        }

        model.addAttribute("error", true);
        model.addAttribute("notNeedAddFile", true);
        if (sindrequest.selectProject.isVoice()) {
            if (CrmService.PR_SYNDICATE.equals(sindrequest.selectProject.getProject().getTypeProj())) {
                return "LK/sind/sind-voice-ok";
            } else {

                return "LK/sind/fab-voice-ok";
            }
        }
        if (CrmService.PR_SYNDICATE.equals(sindrequest.selectProject.getProject().getTypeProj())) {
            return "LK/sind/sind-voice";
        } else {
            return "LK/sind/fab-voice";
        }

    }

    @PostMapping("/voice")
    private String assessment(@ModelAttribute("sindrequest") SindRequest sindrequest,
                              @ModelAttribute("user") User user,
                              Model model, SindVoice sindVoice) {

        if (isSindFab(user) == false) {
            return redirectUrl;
        }
        UtilsService.securityVueJS(sindVoice);
        logger.debug("getVoiceType():{0}", sindVoice.getVoiceType());
        sindrequest.selectProject.setTypeVoice(sindVoice.getVoiceType());
        sindrequest.selectProject.setSum(sindVoice.getVoiceSum());
        sindrequest.selectProject.setSumCur(sindVoice.getVoiceCur());
        sindrequest.selectProject.setDateVoice(Calendar.getInstance().getTime());

        sql.saveUserProject(sindrequest.selectProject);

        if (CrmService.PR_SYNDICATE.equals(sindrequest.selectProject.getProject().getTypeProj())) {
            model.addAttribute("chooseMenu", "sind");
        } else {
            model.addAttribute("chooseMenu", "fab");
        }
        model.addAttribute("error", true);
        model.addAttribute("notNeedAddFile", true);
        if (sindrequest.selectProject.getTypeVoice() != null) {
            if (CrmService.PR_SYNDICATE.equals(sindrequest.selectProject.getProject().getTypeProj())) {
                return "LK/sind/sind-voice-ok";
            } else {
                return "LK/sind/fab-voice-ok";
            }
        }

        if (CrmService.PR_SYNDICATE.equals(sindrequest.selectProject.getProject().getTypeProj())) {
            return "LK/sind/sind-voice";
        } else {
            return "LK/sind/fab-voice";
        }
    }



    @GetMapping("/data")
    @ResponseBody
    public SindRequest influenceData(@ModelAttribute("sindrequest") SindRequest sindrequest,
                                     @ModelAttribute("project") ProjectRequest project,
                                     @ModelAttribute("user") User user) {

        if (isSindFab(user) == false) {
           return null;
        }
        return sindrequest;
    }


////////////// ADM ////////////////////////////
    //
    private static boolean isSindAdm(User user) {
        if (user.getAuthorities().contains(Role.SIND_ADM)) {
            return true;
        }
        return false;
    }
    private static boolean isSind(User user) {
        if (user.getAuthorities().contains(Role.SIND)) {
            return true;
        }
        return false;
    }
    private static boolean isSindFab(User user) {
        if (user.getAuthorities().contains(Role.SIND)) {
            return true;
        }
        if (user.getAuthorities().contains(Role.FAB)) {
            return true;
        }
        return false;
    }
    private static boolean isFab(User user) {
        if (user.getAuthorities().contains(Role.FAB)) {
            return true;
        }
        return false;
    }

    //Вычисление статуса
    public void calcStatusProject(Project project) {
        List<UserProject> list = sql.listUserProjectByProject(project);
        int cv = 0;
        for(UserProject up : list) {
            if (!(up.getTypeVoice() == null || "".equals(up.getTypeVoice()))) {
                cv++;
            }
        }
        project.setCountTr(cv + "/" + list.size());

        if (project.getProjectRequestStatus() != null && project.getProjectRequestStatus() == 1) {
            project.setStatusTr("arch");
            return;
        }
        project.setStatusTr("end");
        if (project.getDateEnd() != null) {
            if ( project.getDateEnd().before(Calendar.getInstance().getTime()) ) {
                project.setStatusTr("end");
                return;
            }
        }



        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 1);
        Date d = c.getTime();

        if (list == null || list.size() == 0 || (project.getDateBegin() != null && project.getDateBegin().after(d))) {
            project.setStatusTr("no");
            return;
        }
        for(UserProject userProject : list) {
            if (userProject.getTypeVoice() == null || "".equals(userProject.getTypeVoice())){
                project.setStatusTr("activ");
                return;
            }
        }

    }

    @GetMapping("/adm")
    private String adm(@ModelAttribute("sindadmrequest") SindAdmRequest sindAdmRequest,
                       @ModelAttribute("user") User user,
                       @RequestParam(name = "arch", required = false) String arch,
                       Model model) {
        logger.trace("sind-admin");
        if (isSindAdm(user) == false) {
            return redirectUrl;
        }

        sql.addProjectFromCrm(CrmService.PR_FABRIC,
                cs.getProjectPacketSind(500,0,CrmService.PR_FABRIC,"",""));

        sql.addProjectFromCrm(CrmService.PR_SYNDICATE,
                cs.getProjectPacketSind(500,0,CrmService.PR_SYNDICATE,"",""));

        sindAdmRequest.initListProject(sql.getProjects());

        for(Project pr : sindAdmRequest.listProject){
            calcStatusProject(pr);
        }

        model.addAttribute("chooseMenu", "sind-adm");
        model.addAttribute("error", true);
        model.addAttribute("notNeedAddFile", true);
        if ("1".equals(arch)) {
            return "LK/sind/sind-admin-arch";
        }
        return "LK/sind/sind-admin";
    }

    @GetMapping("/admuser")
    private String admUser(@ModelAttribute("sindadmrequest") SindAdmRequest sindAdmRequest,
                           @ModelAttribute("user") User user,
                           Model model) {

        if (isSindAdm(user) == false) {
            return redirectUrl;
        }
        logger.trace("sind-admin-user");
        //sindAdmRequest.initListUserSind(sql.findSindRole());

        model.addAttribute("chooseMenu", "sind-admuser");
        model.addAttribute("error", true);
        model.addAttribute("notNeedAddFile", true);

        return "LK/sind/sind-admin-user";
    }

    @GetMapping("/admuser_view")
    private String admuserView(@RequestParam(name = "id", required = false) String id,
                               @ModelAttribute("user") User user,
                               HttpServletRequest request,
                               Model model) {

        if (isSindAdm(user) == false) {
            return redirectUrl;
        }
        logger.trace("sind-admin-user {0}", id);

        model.addAttribute("chooseMenu", "sind-admuser");
        User user2 = null;
        try {
            user2 = (User) us.loadUserByUsername(id);
        } catch (UsernameNotFoundException e) {
            //return "LK/manager/personal-data";
            return "redirect:" + request.getHeader("Referer");
        }
        Manager client = new Manager();
        client.setEmail(user2.getUsername());
        client.setNameFirst(user2.getNameFirst());
        client.setNameLast(user2.getNameLast());
        client.setPhone(user2.getPhone());
        client.setImage(user2.getImage());
        client.setRegionId(user2.getRegionId());
        model.addAttribute("manager", client);

        model.addAttribute("region", "REGION");

        return "LK/manager/personal-data";
    }

    /**
     * Просмотр статуса проекта
     * @param id
     * @param model
     * @return
     */
    @GetMapping("/adm_view")
    private String admView(@ModelAttribute("sindadmrequest") SindAdmRequest sindAdmRequest,
                           @ModelAttribute("user") User user,
                           @RequestParam(name = "id", required = false) String id,
                           Model model) {

        if (isSindAdm(user) == false) {
            return redirectUrl;
        }
        model.addAttribute("chooseMenu", "sind-adm");
        logger.trace("sind-admin-view {0}", id);


        try {
            sindAdmRequest.processSelectProject(id);
            //Добавление пользователей которых можно прибавить в голосование
            sindAdmRequest.initListUserSind(sql.findUsers(sindAdmRequest.selectProject));
            //пользователи проекта
            sindAdmRequest.initListUserProject(sql.listUserProjectByProject(sindAdmRequest.selectProject));
        } catch (NumberFormatException nfe) {
            logger.error("Param is not int", nfe);
        }

        return "LK/sind/sind-admin-view";
    }

    @GetMapping("/adm_view_time")
    private String admViewTime(@ModelAttribute("sindadmrequest") SindAdmRequest sindAdmRequest,
                           @ModelAttribute("user") User user,
                           @RequestParam(name = "id", required = false) String id,
                           Model model) {

        if (isSindAdm(user) == false) {
            return redirectUrl;
        }
        model.addAttribute("chooseMenu", "sind-adm");
        logger.trace("sind-admin-time {0}", id);


        try {
            sindAdmRequest.processSelectProject(id);

        } catch (NumberFormatException nfe) {
            logger.error("Param is not int", nfe);
        }

        return "LK/sind/sind-admin-time";
    }

    @GetMapping("/adm_view_res")
    private String admViewRes(@ModelAttribute("sindadmrequest") SindAdmRequest sindAdmRequest,
                               @ModelAttribute("user") User user,
                               @RequestParam(name = "id", required = true) String id,
                               Model model) {

        if (isSindAdm(user) == false) {
            return redirectUrl;
        }
        model.addAttribute("chooseMenu", "sind-adm");
        logger.trace("sind-admin-time {0}", id);


        try {
            sindAdmRequest.processSelectProject(id);
            //Добавление пользователей которых можно прибавить в голосование
            sindAdmRequest.initListUserSind(sql.findUsers(sindAdmRequest.selectProject));
            //
            sindAdmRequest.initListUserProject(sql.listUserProjectByProject(sindAdmRequest.selectProject));
        } catch (NumberFormatException nfe) {
            logger.error("Param is not int", nfe);
        }

        return "LK/sind/sind-voice-res";
    }

    @PostMapping("/adm_view_time")
    private String admViewEdit(@ModelAttribute("sindadmrequest") SindAdmRequest sindAdmRequest,
                               @ModelAttribute("sindadmedit") SindAdminEdit sindAdmEdit,
                               @ModelAttribute("user") User user,
                               Model model) {

        if (isSindAdm(user) == false) {
            return redirectUrl;
        }
        model.addAttribute("chooseMenu", "sind-adm");
        try {
            logger.info("-------------");
            logger.info(sindAdmEdit.getDateBegin());
            logger.info(sindAdmEdit.getDateEnd());
            logger.info(sindAdmEdit.getAdminProj());
            logger.info(sindAdmEdit.getId());
            logger.info(sindAdmEdit.getToArch());
            Project project = sql.getProject(sindAdmEdit.getId());
            if ("on".equals(sindAdmEdit.getToArch())) {
                //Архив
                project.setProjectRequestStatus(1);
                sindAdmRequest.selectProject.setProjectRequestStatus(1);
            } else {
                project.setProjectRequestStatus(null);
                sindAdmRequest.selectProject.setProjectRequestStatus(null);
            }

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            try {
                Date date = sdf.parse(sindAdmEdit.getDateBegin());
                project.setDateBegin(date);
                sindAdmRequest.selectProject.setDateBegin(date);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Date date = sdf.parse(sindAdmEdit.getDateEnd());
                project.setDateEnd(date);
                sindAdmRequest.selectProject.setDateEnd(date);
            } catch (Exception e) {
                e.printStackTrace();
            }
            project.setAdminProj(sindAdmEdit.getAdminProj());
            sindAdmRequest.selectProject.setAdminProj(sindAdmEdit.getAdminProj());

            sql.saveProject(project);
            //sindAdmRequest.initListUserProject(sql.listUserProjectByProject(sindAdmRequest.selectProject));
        } catch (NumberFormatException nfe) {
            logger.error("Param is not int", nfe);
        }
        return "redirect:/private-office/sind/adm_view_time?id="+sindAdmEdit.getId();
    }

    @GetMapping("/adm_view_add")
    private String admViewAdd(@ModelAttribute("sindadmrequest") SindAdmRequest sindAdmRequest,
                              @ModelAttribute("user") User user,
                              @RequestParam(name = "id", required = false) String id,
                              Model model) {

        if (isSindAdm(user) == false) {
            return redirectUrl;
        }
        model.addAttribute("chooseMenu", "sind-adm");
        logger.trace("sind-admin-view {0}", id);
        try {
            sql.addUserProject(id, sindAdmRequest.selectProject);
            //sindAdmRequest.initListUserProject(sql.listUserProjectByProject(sindAdmRequest.selectProject));
        } catch (NumberFormatException nfe) {
            logger.error("Param is not int", nfe);
        }
        return "redirect:/private-office/sind/adm_view?id="+sindAdmRequest.selectProject.getProjectId();
        //return "LK/sind/sind-admin-view";
    }

    @GetMapping("/adm_view_remove")
    private String admViewRemove(@ModelAttribute("sindadmrequest") SindAdmRequest sindAdmRequest,
                                 @ModelAttribute("user") User user,
                                 @RequestParam(name = "id", required = false) String id,
                                 Model model) {

        if (isSindAdm(user) == false) {
            return redirectUrl;
        }
        model.addAttribute("chooseMenu", "sind-adm");
        logger.trace("sind-admin-view {0}", id);
        try {
            sql.removeUserProject(id, sindAdmRequest.selectProject);
            //sindAdmRequest.initListUserProject(sql.listUserProjectByProject(sindAdmRequest.selectProject));
        } catch (NumberFormatException nfe) {
            logger.error("Param is not int", nfe);
        }
        return "redirect:/private-office/sind/adm_view?id="+sindAdmRequest.selectProject.getProjectId();
        //return "LK/sind/sind-admin-view";
    }




    @GetMapping(value = "/concept-pdf", produces = "application/octet-stream")
    public void getConceptPdf(@ModelAttribute("project") ProjectRequest project, HttpServletResponse response) throws IOException, XDocReportException {
        InputStream in = getClass().getResourceAsStream("/DocxProjectWithVelocity.docx");
        IXDocReport report = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Velocity);
        IContext context = report.createContext();
        Map<String, Object> contextObjects = new HashMap<>();
        contextObjects.put("project.Name", "11");
        contextObjects.put("name.Fi", "22");

        ArrayList<UserProject> userProjects = new ArrayList<>();
        UserProject userProj = new UserProject();
        userProj.setUsername("name1");
        userProjects.add(userProj);
        UserProject userProj2 = new UserProject();
        userProj2.setUsername("name2");
        userProjects.add(userProj2);
        UserProject userProj3 = new UserProject();
        userProj3.setUsername("name3");
        userProjects.add(userProj3);
        contextObjects.put("userProjects", userProjects);


        FieldsMetadata metadata = new FieldsMetadata();
        metadata.addFieldAsList("userProjects.username");
        report.setFieldsMetadata(metadata);

        contextObjects.forEach((key, value) -> context.put(key, value));


        Options options = Options.getTo(ConverterTypeTo.PDF);

        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(getClass().getClassLoader());
        Resource[] res = resolver.getResources("classpath:/font/**");
        final String tmpDir = VebEoApplication.env("TMPDIR", System.getProperty("java.io.tmpdir"));
        for (Resource fr : res) {
            File f = new File(tmpDir + "/" + fr.getFilename().toLowerCase());
            if (!f.exists()) {
//                logger.info("Saving " + fr.getFilename() + " to " + f);
                FileOutputStream fos = new FileOutputStream(f);
                try {
                    StreamUtils.copy(fr.getInputStream(), fos);
                } finally {
                    fos.close();
                }
            }
        }

        PdfOptions pdfOptions = PdfOptions.create();
        pdfOptions.fontProvider(new IFontProvider() {
            @Override
            public Font getFont(String familyName, String encoding, float size, int style, Color color) {
                try {
                    String suffix = "";
                    if (style == Font.NORMAL) {
                        suffix = "";
                    } else if (style == Font.ITALIC) {
                        suffix = "i";
                    } else if (style == Font.BOLD) {
                        suffix = "b";
                    } else if (style == Font.BOLDITALIC) {
                        suffix = "bi";
                    }
                    File f = new File(tmpDir + "/" + familyName.toLowerCase() + suffix + ".ttf");
//                    logger.info("Requested font {} checking {}", familyName, f.getAbsolutePath());
                    if (f.exists()) {
//                        logger.info("Returning font {}", f.getAbsolutePath());
                        BaseFont baseFont = BaseFont.createFont(f.getAbsolutePath(), encoding, BaseFont.EMBEDDED);
                        return new Font(baseFont, size, style, color);
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }

                return FontFactory.getFont(familyName, encoding, size, style, color);
            }
        });
        options.subOptions(pdfOptions);

        response.setContentType("application/octet-stream");
        response.addHeader("Content-Disposition", "attachment; filename=concept.pdf");
        OutputStream out = response.getOutputStream();
        report.convert(context, options, out);
    }
}
