package ru.veb.eo.controller;

import com.google.common.base.Strings;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.veb.eo.AppVersionInfo;
import ru.veb.eo.Properties;
import ru.veb.eo.model.Role;
import ru.veb.eo.model.User;
import ru.veb.eo.model.crm.ProjectRequest;
import ru.veb.eo.service.EmailService;
import ru.veb.eo.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.veb.eo.service.UtilsService;

import java.util.Arrays;

import static org.aspectj.weaver.tools.cache.SimpleCacheFactory.path;

@Controller
@RequestMapping("/")
@SessionAttributes(value = {"user", "project"}, types = {User.class, ProjectRequest.class})
public class MainController {

    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

    @Autowired
    Properties properties;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    @Autowired(required = false)
    private AppVersionInfo appVersionInfo;

    @ModelAttribute("version")
    public String version() {
        if (null != appVersionInfo) {
            String tag = appVersionInfo.getTags();
            if (tag == null || tag.trim().equals("")) {
                tag = appVersionInfo.getClosestTagName();
            }
            return tag + " " + appVersionInfo.getCommitIdAbbrev() + " " + appVersionInfo.getCommitTime();
        } else
            return null;
    }

    @GetMapping("/")
    public String main_() {
        return "forward:/private-office/sind/info";
    }


    @RequestMapping("/login")
    public String getLogin(@RequestParam(value = "error", required = false) String error, Model model) {
        model.addAttribute("error", error != null);
        return "login";
    }

    @RequestMapping("/403")
    public String getError(Model model) {
        model.addAttribute("error", true);
        return "error/403";
    }

    @RequestMapping("/recovery")
    public String getRecovery(@RequestParam(value = "error", required = false) String error, FormRecovery formRecovery, Model model, HttpServletRequest request) {
        model.addAttribute("error", error != null);

        if (formRecovery != null && !Strings.isNullOrEmpty(formRecovery.login)) {
            try {
                UserDetails user = userService.loadUserByUsername(formRecovery.login);
                model.addAttribute("login", user.getUsername());
                String password = null;
                if (properties.isSimplePasswordCreate() == false) {
                    password = UtilsService.generateRandom(8);
                } else {
                    logger.warn("create simple password!");
                    password = "1";
                }
                String path = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
                try {
                    emailService.sendSimpleMessage(user.getUsername(), "Восстановление пароля", "Cсылка для входа в систему: " + path + "\n\nЛогин: " + user.getUsername() + "\n\nПароль: " + password);

                    User user1 = userService.getUser(user.getUsername());
                    user1.setPassword(new BCryptPasswordEncoder().encode(password));
                    userService.saveUser(user1);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return "reset-request-success";
            } catch (UsernameNotFoundException e) {
                return "redirect:/recovery?error";
            }
        }

        return "reset-request";
    }
}

@Data
class FormRecovery {
    public String login;
}
