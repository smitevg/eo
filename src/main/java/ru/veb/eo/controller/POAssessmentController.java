package ru.veb.eo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.util.ListUtils;
import ru.veb.eo.model.Role;
import ru.veb.eo.model.User;
import ru.veb.eo.model.crm.ProjectRequest;
import ru.veb.eo.model.crm.Task;
import ru.veb.eo.model.form.assessment.Assessment;
import ru.veb.eo.model.reference.Notification;
import ru.veb.eo.model.reference.ProjectEntity;
import ru.veb.eo.repository.NotificationRepository;
import ru.veb.eo.repository.ProjectRepository;
import ru.veb.eo.service.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/private-office/express-assessment")
@SessionAttributes(value = {"user", "project", "assessment"}, types = {User.class, ProjectRequest.class, Assessment.class})
public class POAssessmentController {
    private static final Logger logger = LoggerFactory.getLogger(POAssessmentController.class);

    private final CrmService cs;
    private final ReferenceService rs;
    private final EmailService es;
    private final NotificationRepository nr;
    private final SharePointService sp;
    private final ProjectRepository projectRepository;

    @Autowired
    public POAssessmentController(CrmService cs, ReferenceService rs, EmailService es, NotificationRepository nr, SharePointService sp, ProjectRepository projectRepository) {
        this.cs = cs;
        this.rs = rs;
        this.es = es;
        this.nr = nr;
        this.sp = sp;
        this.projectRepository = projectRepository;
    }

    @ModelAttribute("project")
    public ProjectRequest initProject() {
        return new ProjectRequest();
    }

    @ModelAttribute("assessment")
    Assessment initAssessment() {
        return new Assessment();
    }

    @ModelAttribute("user")
    public User initUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();
        user.setPage("express-assessment");
        if (user.getAuthorities().contains(Role.MANAGER))
            user.setNotifications(nr.findByProjectCertifiedManagerIdAndIsViewed(user.getCrmId(), false));
        else
            user.setNotifications(nr.findByApplicantEmailAndIsViewed(user.getUsername(), false));
        return user;
    }

    @GetMapping("/data")
    @ResponseBody
    public Assessment getAssessment(@ModelAttribute("project") ProjectRequest project) {
        Assessment assessment = new Assessment();
        assessment.getData(project);
        return assessment;
    }

    @GetMapping
    private String assessmentGet(@ModelAttribute("user") User user, Model model, @RequestParam(name = "id", required = true) String id) {

        ProjectRequest project = null;
        if (id != null) {
            if (user.getAuthorities().contains(Role.MANAGER))
                project = cs.getProjectRequest(id, null, user.getCrmId());
            else
                project = cs.getProjectRequest(id, user.getUsername(), null);
        }

        model.addAttribute("project", project);

        Assessment assessment = new Assessment();
        assessment.getData(project);
        UtilsService.securityVueJS(assessment);

        if (project.getProjectType() != null && !project.getProjectType().isEmpty())
            assessment.setProject_type(rs.getProjectType(Integer.valueOf(project.getProjectType())));
        if (project.getProjectSalePlace() != null && !project.getProjectSalePlace().isEmpty())
            assessment.setSale_place(rs.getRegion(Integer.valueOf(project.getProjectSalePlace())));
        if (project.getProjectSectors1() != null && !project.getProjectSectors1().isEmpty())
            assessment.setProject_sectors(rs.getProjectSectors(Integer.valueOf(project.getProjectSectors1())));

        model.addAttribute("assessment", assessment);
        model.addAttribute("paybackTime", rs.getProjectPaybackTine(assessment.project_payback_time));

        return "office-express-assessment";
    }

    @PostMapping
    public String assessmentSave(Model model,
                                 @ModelAttribute("assessment") Assessment assessment,
                                 @ModelAttribute("project") ProjectRequest project,
                                 @RequestParam(name = "fn_reputationFiles[]", required = false) MultipartFile fn_reputationFiles[],
                                 @RequestParam(name = "fn_costFiles[]", required = false) MultipartFile fn_costFiles[],
                                 @RequestParam(name = "fn_industryFiles[]", required = false) MultipartFile fn_industryFiles[],
                                 @RequestParam(name = "fn_strategyFiles[]", required = false) MultipartFile fn_strategyFiles[],
                                 @RequestParam(name = "fn_investedFiles[]", required = false) MultipartFile fn_investedFiles[],
                                 @RequestParam(name = "fn_participationFiles[]", required = false) MultipartFile fn_participationFiles[],
                                 @RequestParam(name = "fn_breakEvenFiles[]", required = false) MultipartFile fn_breakEvenFiles[]
    ) throws IOException {

        UtilsService.securityVueJS(assessment);

        if (fn_reputationFiles != null && fn_reputationFiles.length > 0) {
            for (MultipartFile file : fn_reputationFiles) {
                if (file.getSize() > 0)
                    sp.saveProjectFile(project.projectId, file, project.getInitiatorInn(), project.getProjectCode());
            }
        }

        if (fn_costFiles != null && fn_costFiles.length > 0) {
            for (MultipartFile file : fn_costFiles) {
                if (file.getSize() > 0)
                    sp.saveProjectFile(project.projectId, file, project.getInitiatorInn(), project.getProjectCode());
            }
        }

        if (fn_industryFiles != null && fn_industryFiles.length > 0) {
            for (MultipartFile file : fn_industryFiles) {
                if (file.getSize() > 0)
                    sp.saveProjectFile(project.projectId, file, project.getInitiatorInn(), project.getProjectCode());
            }
        }

        if (fn_strategyFiles != null && fn_strategyFiles.length > 0) {
            for (MultipartFile file : fn_strategyFiles) {
                if (file.getSize() > 0)
                    sp.saveProjectFile(project.projectId, file, project.getInitiatorInn(), project.getProjectCode());
            }
        }

        if (fn_investedFiles != null && fn_investedFiles.length > 0) {
            for (MultipartFile file : fn_investedFiles) {
                if (file.getSize() > 0)
                    sp.saveProjectFile(project.projectId, file, project.getInitiatorInn(), project.getProjectCode());
            }
        }

        if (fn_participationFiles != null && fn_participationFiles.length > 0) {
            for (MultipartFile file : fn_participationFiles) {
                if (file.getSize() > 0)
                    sp.saveProjectFile(project.projectId, file, project.getInitiatorInn(), project.getProjectCode());
            }
        }

        if (fn_breakEvenFiles != null && fn_breakEvenFiles.length > 0) {
            for (MultipartFile file : fn_breakEvenFiles) {
                if (file.getSize() > 0)
                    sp.saveProjectFile(project.projectId, file, project.getInitiatorInn(), project.getProjectCode());
            }
        }

        Result result = checkValidAssessment(assessment);

        model.addAttribute("valid", result.valid);
        model.addAttribute("valuation", result.valuation);

        assessment.saveData(project);
        model.addAttribute("project", project);
        cs.updateProject(project);

        assessment.getData(project);

        if (project.getProjectType() != null && !project.getProjectType().isEmpty())
            assessment.setProject_type(rs.getProjectType(Integer.valueOf(project.getProjectType())));
        if (project.getProjectSalePlace() != null && !project.getProjectSalePlace().isEmpty())
            assessment.setSale_place(rs.getRegion(Integer.valueOf(project.getProjectSalePlace())));
        if (project.getProjectSectors1() != null && !project.getProjectSectors1().isEmpty())
            assessment.setProject_sectors(rs.getProjectSectors(Integer.valueOf(project.getProjectSectors1())));

        model.addAttribute("assessment", assessment);
        model.addAttribute("paybackTime", rs.getProjectPaybackTine(assessment.project_payback_time));


        List<Task> tasks = cs.getTasks(project.getProjectId());
        if (!ListUtils.isEmpty(tasks)) {
            for (Task task : tasks) {
                if ("Ввод ПО".equals(task.getName()) || "Заполнение резюме проекта".equals(task.getName())) ;
            }
        }

        return "office-express-assessment-result";
    }

    class Result {
        public int valuation = 0;
        public boolean valid = true;
    }

    private Result checkValidAssessment(Assessment assessment) {
        Result result = new Result();
        try {
            result.valuation += assessment.getFn_reputationCheck() ? 1 : 0;

            result.valuation += assessment.getFn_breakEvenCheck() ? 1 : 0;
            result.valid &= assessment.getFn_breakEvenCheck();
            result.valuation += assessment.getFn_costCheck() ? 1 : 0;
            result.valid &= assessment.getFn_costCheck();
            result.valuation += assessment.getFn_industryCheck() ? 1 : 0;
            result.valid &= assessment.getFn_industryCheck();

            //System.out.println("assessment.fn_participationValue" + assessment.fn_participationValue);
            //System.out.println("assessment.getFn_participationValue()" + assessment.getFn_participationValue());
            assessment.fn_participationValue = Double.valueOf(assessment.fn_participationValueString.replaceAll(",", ".").replaceAll("[^0-9,\\.]+", "")).intValue();
            //System.out.println("assessment.fn_participationValue" + assessment.fn_participationValue);
            if (assessment.fn_participationValue != null) {
                result.valuation += assessment.fn_participationValue > 15 ? 1 : 0;
                result.valid &= assessment.fn_participationValue > 15;
            } else {
                result.valid &= false;
            }

            result.valuation += assessment.getFn_paybackTimeCheck() ? 1 : 0;
            result.valid &= assessment.getFn_paybackTimeCheck();
            result.valuation += assessment.getFn_strategyCheck() ? 1 : 0;
            result.valid &= assessment.getFn_strategyCheck();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @GetMapping("/submit")
    public String assessmentSubmit(@ModelAttribute("assessment") Assessment assessment, @ModelAttribute("project") ProjectRequest project) {

        Result result = checkValidAssessment(assessment);

        if (result.valid)
            cs.updateProjectStage(2, project);
        else
            cs.updateProjectStage(6, project);

        Notification notification = new Notification();
        notification.setApplicantEmail(project.getApplicantEmail());
        notification.setProjectCertifiedManagerId(project.getProjectCertifiedManagerId());
        notification.setProjectId(project.getProjectId());
        notification.setIsGood(result.valid);
        if (result.valid)
            notification.setMessage(" успешно прошел экспресс-оценку");
        else
            notification.setMessage(" не прошел экспресс-оценку");
        notification.setProjectNomination(project.getProjectNomination());
        nr.saveAndFlush(notification);

        ProjectEntity projectEntity = projectRepository.findOne(project.getProjectId());
        if (projectEntity != null) {
            BeanUtils.copyProperties(project, projectEntity);
            projectRepository.saveAndFlush(projectEntity);
        }

        if (result.valid)
            es.sendSimpleMessage(project.getApplicantEmail(),
                    "Уведомление об изменении статуса прохождения экспресс-оценки проекта",
                    "Уважаемый(ая), " + project.getApplicantNameFirst() + " " + project.getApplicantNameMid() + "! \n" +
                            "\n" +
                            "Внешэкономбанк информирует Вас об успешном прохождении этапа экспресс-оценки проекта. Документы по проекту с результатами экспресс-оценки направлены во Внешэкономбанк для первичного анализа и подтверждения возможности дальнейшей работы над проектом. За дополнительной информацией Вы можете обратиться к своему Региональному менеджеру.");

        else
            es.sendSimpleMessage(project.getApplicantEmail(),
                    "Уведомление об изменении статуса прохождения экспресс-оценки проекта",
                    "Уважаемый(ая), " + project.getApplicantNameFirst() + " " + project.getApplicantNameMid() + "!\n" +
                            "\n" +
                            "Благодарим Вас за предоставленные материалы по проекту. К сожалению, вынуждены сообщить, что в результате экспресс-оценки, проведенной Вашим Региональным менеджером, выявлено, что в настоящий момент проект не соответствует требованиям Внешэкономбанка. Надеемся на возможность сотрудничества при реализации будущих проектов.");

        List<Task> tasks = cs.getTasks(project.getProjectId());
        if (!ListUtils.isEmpty(tasks)) {
            for (Task task : tasks) {
                if ("Ввод ПО".equals(task.getName()) || "Заполнение резюме проекта".equals(task.getName()) || "Экспресс-оценка".equals(task.getName())) {
                    Map<String, Object> variables = new HashMap<>();
                    Map<String, Boolean> isRejected = new HashMap<>();
                    isRejected.put("value", result.valid);
                    variables.put("isRejected", isRejected);
                    cs.closeTask(task.getId(), variables);
                }
            }
        }

        return "redirect:/private-office";
    }
}
