package ru.veb.eo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Strings;
import com.microsoft.sharepoint.rest.SPFile;
import lombok.Builder;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.ListUtils;
import ru.veb.eo.ForbiddenException;
import ru.veb.eo.Properties;
import ru.veb.eo.entity.Project;
import ru.veb.eo.entity.UserProject;
import ru.veb.eo.model.Reference;
import ru.veb.eo.model.Role;
import ru.veb.eo.model.User;
import ru.veb.eo.model.crm.*;
import ru.veb.eo.model.form.ChangeManager;
import ru.veb.eo.model.form.ChangePassword;
import ru.veb.eo.model.form.FilterListProject;
import ru.veb.eo.model.reference.MessageEntity;
import ru.veb.eo.model.reference.Notification;
import ru.veb.eo.repository.MessageRepository;
import ru.veb.eo.repository.NotificationRepository;
import ru.veb.eo.service.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static ru.veb.eo.service.CrmService.PR_SYNDICATE;

@Controller
@RequestMapping("/private-office")
public class POPrivateOfficeController {
    private static final Logger logger = LoggerFactory.getLogger(POPrivateOfficeController.class);


    private Properties properties;
    private CrmService cs;
    private UserService us;
    private NotificationRepository nr;
    private ReferenceService rs;
    private EmailService emailService;
    private MessageRepository mr;
    private SharePointService sp;
    private MySqlService sql;

    @Autowired
    public POPrivateOfficeController(MySqlService sql, CrmService cs, UserService us, NotificationRepository nr, ReferenceService rs, Properties properties, EmailService emailService, MessageRepository mr, SharePointService sp) {
        this.sql = sql;
        this.cs = cs;
        this.us = us;
        this.nr = nr;
        this.rs = rs;
        this.properties = properties;
        this.emailService = emailService;
        this.mr = mr;
        this.sp = sp;
    }

    @ModelAttribute("ref")
    public Reference initReference() {
        return rs.getReference();
    }

    @ModelAttribute("user")
    public User initUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();

        user.setPage("private-office");
        if (user.getAuthorities().contains(Role.MANAGER))
            user.setNotifications(nr.findByProjectCertifiedManagerIdAndIsViewed(user.getCrmId(), false));
        else
            user.setNotifications(nr.findByApplicantEmailAndIsViewed(user.getUsername(), false));

        return user;
    }

    @ModelAttribute("rating")
    public int initRating() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();

        int rating = 0;
        if (user.getAuthorities().contains(Role.MANAGER)) {
            Map<String, Integer> ratings = cs.getRatings();
            if (ratings.containsKey(user.getCrmId()))
                rating = ratings.get(user.getCrmId());
            else
                rating = 0;
        }

        return rating;
    }

    @RequestMapping
    private String index(@ModelAttribute("user") User user, Model model, FilterListProject filterListProject, @RequestParam(required = false) String token) {
        if (!Strings.isNullOrEmpty(token)) user.setAuth2(true);
        if (user.getAuthorities().contains(Role.MANAGER)) {
            List<ProjectResponsePacket> listProjectManager = getListProject(filterListProject, null, null, user.getCrmId());

            // закоментил проекты к которым нет доступа у манагера
            // List<ProjectResponsePacket> listProjectEmpty = getListProject(filterListProject, null, null, "empty");

//            listProjectEmpty = listProjectEmpty.stream()
//                    .filter(el -> user.getRegionId() != null && (user.getRegionId().equals(el.projectRegion)))
//                    .collect(Collectors.toList());

            // listProjectManager.addAll(listProjectEmpty);
            model.addAttribute("projects", listProjectManager);

        } else {
            model.addAttribute("projects", getListProject(filterListProject, null, user.getUsername(), null));
        }
        model.addAttribute("filterListProject", filterListProject == null ? new FilterListProject() : filterListProject);
        model.addAttribute("chooseMenu", "projects");
        return "list-projects";
    }

    @GetMapping("/login-info")
    private String changePassword(Model model) {
        model.addAttribute("chooseMenu", "login-info");
        return "LK/manager/login-info";
    }

    @PostMapping("/login-info")
    private String changePasswordSubmit(@ModelAttribute("user") User user, ChangePassword changePassword, Model model) {
        UtilsService.securityVueJS(changePassword);
        model.addAttribute("chooseMenu", "login-info");

        User usUser = us.getUser(user.getUsername());
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        if (!Strings.isNullOrEmpty(changePassword.getOldPassword()) &&
                !Strings.isNullOrEmpty(changePassword.getNewPassword1()) &&
                !Strings.isNullOrEmpty(changePassword.getNewPassword2()) &&
                changePassword.getNewPassword1().equals(changePassword.getNewPassword2()) &&
                passwordEncoder.matches(changePassword.getOldPassword(), user.getPassword())
                ) {
            user.setPassword(passwordEncoder.encode(changePassword.getNewPassword2()));
            us.saveUser(user);
            model.addAttribute("error", false);
        } else {
            model.addAttribute("error", true);
        }
        return "LK/manager/login-info";
    }


    @GetMapping("/personal")
    private String personal(@ModelAttribute("user") User user, Model model) {
        model.addAttribute("chooseMenu", "personal");

        Manager client = new Manager();
        client.setEmail(user.getUsername());
        client.setNameFirst(user.getNameFirst());
        client.setNameLast(user.getNameLast());
        client.setPhone(user.getPhone());
        client.setImage(user.getImage());
        client.setRegionId(user.getRegionId());
        model.addAttribute("manager", client);

        model.addAttribute("region", rs.getProjectRegion(user.getRegionId()));

        return "LK/manager/personal-data";
    }

    @RequestMapping("/resume-list")
    private String resume(@ModelAttribute("user") User user, Model model, FilterListProject filterListProject) {
        if (user.getAuthorities().contains(Role.MANAGER)) {
            model.addAttribute("projects", getListProject(filterListProject, 0, null, user.getCrmId()));
            model.addAttribute("chooseMenu", "resume");
            return "list-projects";
        } else {
            return "redirect:/";
        }
    }

    @RequestMapping("/dossier-list")
    private String dossier(@ModelAttribute("user") User user, Model model, FilterListProject filterListProject) {
        List<ProjectResponsePacket> listProjectManager = null;
        if (user.getAuthorities().contains(Role.MANAGER))
            listProjectManager = getListProject(filterListProject, null, null, user.getCrmId());
        else
            listProjectManager = getListProject(filterListProject, null, user.getUsername(), null);

        model.addAttribute("projects", listProjectManager);
        model.addAttribute("chooseMenu", "dossier-list");
        return "list-projects";
    }

    @RequestMapping("/express-assessment-list")
    private String assessment(@ModelAttribute("user") User user, Model model, FilterListProject filterListProject) {
        UtilsService.securityVueJS(filterListProject);

        if (user.getAuthorities().contains(Role.MANAGER)) {

            List<ProjectResponsePacket> projects = getListProject(filterListProject, 1, null, user.getCrmId());

            int daysRoll = 7;

            for (ProjectResponsePacket project : projects) {
                if(project.projectRequestStatusDate != null) {
                    Date needDateExpressAssessment = (Date) project.projectRequestStatusDate.clone();
                    needDateExpressAssessment.setTime(needDateExpressAssessment.getTime() + (daysRoll * 24 * 60 * 60 * 1000));
                    project.setNeedDateExpressAssessment(needDateExpressAssessment);
                }
            }

            model.addAttribute("projects", projects);
            model.addAttribute("chooseMenu", "assessment");
            return "list-projects";
        } else {
            return "redirect:/";
        }
    }

    @RequestMapping("/finance-request-list")
    private String finance(@ModelAttribute("user") User user, Model model, FilterListProject filterListProject) {
        UtilsService.securityVueJS(filterListProject);

        if (user.getAuthorities().contains(Role.MANAGER)) {
            model.addAttribute("projects", getListProject(filterListProject, 3, null, user.getCrmId()));
            model.addAttribute("chooseMenu", "finance");
            return "list-projects";
        } else {
            return "redirect:/";
        }
    }

    @GetMapping("/accept")
    private String initialization(@ModelAttribute("user") User
                                          user, @RequestParam(name = "id", required = false) String id) {

        if (id != null && !Strings.isNullOrEmpty(user.getCrmId())) {
            ChangeManager changeManager = new ChangeManager();
            changeManager.setProjectId(id);
            changeManager.setProjectCertifiedManagerId(user.getCrmId());
            cs.updateProject(changeManager);
        }
        return "redirect:/private-office";
    }

    private List<ProjectResponsePacket> getListProject(FilterListProject filterListProject, Integer
            projectRequestStatus, String applicantEmail, String projectCertifiedManagerId) {

        List<ProjectResponsePacket> list = cs.getProjectPacket(1000, null, applicantEmail, projectCertifiedManagerId);
        if (list == null) list = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        list = list.stream()
                .filter(el -> projectRequestStatus == null || (el.getProjectRequestStatus() != null && el.getProjectRequestStatus().equals(projectRequestStatus)))
                .filter(el -> filterListProject == null || filterListProject.getName() == null || (el.getProjectNomination() != null && el.getProjectNomination().contains(filterListProject.getName())))
                .filter(el -> filterListProject == null || filterListProject.getApplicant() == null || ((el.getApplicantNameFirst() + " " + el.getApplicantNameLast()).contains(filterListProject.getApplicant())))
                .filter(el -> filterListProject == null || filterListProject.getApplicantEMail() == null || (el.getApplicantEmail() != null && el.getApplicantEmail().contains(filterListProject.getApplicantEMail())))
                .filter(el -> filterListProject == null || filterListProject.getDateEntered() == null || filterListProject.getDateEntered().isEmpty() || (el.getProjectDateEntered() != null && dateFormat.format(el.getProjectDateEntered()).equals(filterListProject.getDateEntered())))
                .filter(el -> filterListProject == null || filterListProject.getManager() == null || el.getProjectCertifiedManagerName() == null || (el.getProjectCertifiedManagerName()).contains(filterListProject.getManager()))
                .filter(el -> filterListProject == null || filterListProject.getStatus() == null || (el.getProjectStatusName() != null && el.getProjectStatusName().contains(filterListProject.getStatus())))
                .filter(el -> filterListProject == null || filterListProject.getNumber() == null || (el.getProjectCode() != null && el.getProjectCode().contains(filterListProject.getNumber())))
                .sorted((f1, f2) -> Long.compare(Long.valueOf(f2.getProjectCode()), Long.valueOf(f1.getProjectCode())))
                .collect(Collectors.toList());

        return list;
    }

    private boolean filterData(Date projectDateEntered, FilterListProject filterListProject) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String pDate = dateFormat.format(projectDateEntered);
        boolean result = true;
        if (projectDateEntered != null)
            result = pDate.equals(filterListProject.getDateEntered());

        return result;
    }

    @GetMapping("/project")
    private String project(Model model, @RequestParam(name = "id", required = true) String id, @ModelAttribute("user") User user) {
        ProjectRequest project = null;
        if (user.getAuthorities().contains(Role.MANAGER))
            project = cs.getProjectRequest(id, null, user.getCrmId());
        else
            project = cs.getProjectRequest(id, null, /*user.getUsername(),*/ null);

       /* if (user.getAuthorities().contains(Role.SIND) ||
            user.getAuthorities().contains(Role.FAB) )
        {
            //Проект на который назначено голосование
            project = cs.
        }*/
//        if (!UtilsService.checkAccess(project, user)) throw new ForbiddenException();

//        if (user.getAuthorities().contains(Role.MANAGER)) {
        Manager client = new Manager();
        client.setEmail(project.getApplicantEmail());
        client.setNameFirst(project.getApplicantNameFirst());
        client.setNameLast(project.getApplicantNameLast());
        client.setPhone(project.getApplicantPhoneNumber());
        client.setImage(null);
        client.setRegionId(project.getProjectSalePlace());
        model.addAttribute("client", client);


//        Manager collaborator = new Manager();
//        collaborator.setEmail(project.getApplicantEmail());
//        collaborator.setNameFirst(project.getApplicantNameFirst());
//        collaborator.setNameLast(project.getApplicantNameLast());
//        collaborator.setPhone(project.getApplicantPhoneNumber());
//        collaborator.setImage(null);
//        collaborator.setRegionId(project.getProjectSalePlace());
//        model.addAttribute("collaborator", collaborator);
        model.addAttribute("collaborator", null);

        model.addAttribute("manager", cs.getManagerById(project.getProjectCertifiedManagerId()));


        if (!Strings.isNullOrEmpty(project.getProjectSalePlace())) {
            try {
                String region = rs.getRegion(Integer.valueOf(project.getProjectSalePlace()));
                model.addAttribute("regionZnF", region);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            model.addAttribute("regionZnF", "");
        }

        int rating = 0;
        Map<String, Integer> ratings = cs.getRatings();
        if (ratings.containsKey(project.getProjectCertifiedManagerId()))
            rating = ratings.get(project.getProjectCertifiedManagerId());
        model.addAttribute("rating", rating);


//        project.getProjectCode()
        model.addAttribute("project", project);

        model.addAttribute("sector", rs.getProjectSector(project.getProjectSectors1()));
        //model.addAttribute("status", rs.getProjectStatus(project.getProjectRequestStatus()));

        // TODO тут надо посчитить новое значение для диаграммы и сделать
        // model.addAttribute("метка", 'значение');

        if (project.getProjectProjectCostDouble() != null) {
            model.addAttribute("costProject", project.getProjectProjectCostDouble() / 1000000.0);
        } else {
            model.addAttribute("costProject", null);
        }

        if (project.getProjectBudgetaryEffectEstimatedAmount() != null) {
            model.addAttribute("projectBudgetaryEffectEstimatedAmount", Double.valueOf(project.getProjectBudgetaryEffectEstimatedAmount()) / 1000000.0);
        } else {
            model.addAttribute("projectBudgetaryEffectEstimatedAmount", null);
        }

        if (ListUtils.isEmpty(project.projectAmountBondArray)) {
            model.addAttribute("amountBondArraySum", 0);
        } else {
            double sum = 0;
            for (int i = 0; i < project.projectAmountBondArray.size(); i++) {
                sum += Double.valueOf(project.projectAmountBondArray.get(i).get(0));
            }
            model.addAttribute("amountBondArraySum", sum);
        }

        double invSum = 0;
        if (ListUtils.isEmpty(project.projectAmountInvestorArray)) {
            model.addAttribute("amountInvestorCashSum", 0);
        } else {
            for (int i = 0; i < project.projectAmountInvestorArray.size(); i++) {
                invSum += Double.valueOf(project.projectAmountInvestorArray.get(i).get(0));
            }
            model.addAttribute("amountInvestorCashSum", invSum);
        }

        model.addAttribute("amountAllRecipientSumWithInvestors", project.projectAmountRecipientsCash + invSum);

        if (ListUtils.isEmpty(project.projectAmountFinanceArray)) {
            model.addAttribute("amountOthersCashSum", 0);
        } else {
            double sum = 0;
            for (int i = 0; i < project.projectAmountFinanceArray.size(); i++) {
                sum += Double.valueOf(project.projectAmountFinanceArray.get(i).get(1));
            }
            model.addAttribute("amountOthersCashSum", sum);
        }

        return "LK/dashboard";
    }

    @GetMapping("/knowledge-base")
    private String knowledgeBase() {
        return "LK/manager/knowledge-base";
    }

    @GetMapping("/applicant")
    private String applicant(Model model) {
        model.addAttribute("chooseMenu", "applicant");
        return "LK/manager/client-info";
    }

    @GetMapping("/initiator")
    private String initiator(Model model) {
        model.addAttribute("chooseMenu", "initiator");
        return "LK/manager/originator-info";
    }

    public List<SPFile> filter(List<DocumentSP> documentSPList, List<SPFile> listFile, User user, String id) {
        /* закоментировали - фильтрум для всех
        if (user.getAuthorities().contains(Role.SIND_ADM)) {
            return listFile;
        }
        if (!(user.getAuthorities().contains(Role.SIND) ||
              user.getAuthorities().contains(Role.FAB)))
        {
            //не фильтруем
            return listFile;
        }*/
        Project project = sql.getProject(id);

        if ( new Integer(1).equals(project.getProjectRequestStatus())) {
            //Архивный проект - доступа к файлам у Синдикатов и Фабриконтов
            return new ArrayList<SPFile>();
        }
        List<UserProject> userProjects = sql.listUserProjectByProject(project);
        UserProject userProject = null;
        if (userProjects != null && userProjects.size() > 0 && user != null) {
            userProject = userProjects.stream().filter(e -> e.getUserId().equalsIgnoreCase(user.getCrmId()))
                    .findFirst().orElse(null);
        }
        if (userProject == null){
            logger.debug("filer. User's golos not found :" + user.getCrmId() );
        } else {
            logger.debug("filer. User's golos is found :" + userProject.getTypeVoice() );
        }

        for(DocumentSP doc : documentSPList) {
            doc.id = doc.id.replace(" ", "%20");
        }

        Map<SPFile, DocumentSP> r = new HashMap<SPFile, DocumentSP>();
        for(SPFile file : listFile) {//цикл по шарику
            boolean has = false;
            for(DocumentSP doc : documentSPList) {

                if (file.getEditLink().contains("'" + doc.id.replace(" ", "%20") + "'")){
                    if (has) {
                        logger.error("error put already: " + file.getEditLink());
                    }
                    if (!has) {
                        r.put(file, doc);
                        has = true;
                    }
                }
            }
            if (!has) {
                logger.error("put not found: " + file.getEditLink());
                r.put(file, null);
            }
        }
        List<SPFile> res = new ArrayList<SPFile>();

        for(Map.Entry<SPFile, DocumentSP> entry : r.entrySet()){
            SPFile spFile = includeByGolos(userProject, entry, project);
            if (spFile != null) {
                res.add(spFile);
            }
        }
        documentSPList.removeAll(r.values());
        logger.info("!!! CRM not found count: " + documentSPList.size());
        for (DocumentSP dsp : documentSPList) {
            logger.info("!!! CRM not found: " + dsp.getId());
        }
        return res;
    }

    //логика включения на основе логосования
    public SPFile includeByGolos(UserProject userProject, Map.Entry<SPFile, DocumentSP> entry, Project project) {
        if (userProject == null) return entry.getKey();
        if (entry.getValue() == null) return entry.getKey();
        DocumentSP documentSP = entry.getValue();
        SPFile result = entry.getKey();
        //Подмена заголовка
        if (documentSP.getTitle() != null) {
            logger.info("Подмена заголовка было {0} б стало {0}", result.getTitle(), documentSP.getTitle());
            result.setTitle(documentSP.getTitle());
        }

        if (PR_SYNDICATE.equalsIgnoreCase(project.getTypeProj())) {
            if ("1".equals(documentSP.getSyndicateLayerTwo())) {
                //доступен когда проголосовал
                if ("t1".equalsIgnoreCase(userProject.getTypeVoice()) || "t3".equalsIgnoreCase(userProject.getTypeVoice()) ) {
                    return result; //проголосовал положительно
                }
                return null;//неголосовал - уровень два недоступен
            }
            if ("t2".equalsIgnoreCase(userProject.getTypeVoice())) {
                return null; //проголосовал отрицательно
            }
        } else {
            if ("1".equals(documentSP.getFabricLayerTwo())) {
                //доступен когда проголосовал
                if ("t1".equalsIgnoreCase(userProject.getTypeVoice()) || "t3".equalsIgnoreCase(userProject.getTypeVoice()) ) {
                    return result; //проголосовал положительно
                }
                return null;//неголосовал - уровень два недоступен
            }
            if ("t2".equalsIgnoreCase(userProject.getTypeVoice())) {
                return null; //проголосовал отрицательно
            }
        }
        return result; // нет ограничения на видимость второго этапа
    }

    @GetMapping("/dossier/data")
    @ResponseBody
    private String dossierData(Model model,
                               @ModelAttribute("user") User user,
                               @ModelAttribute("project") ProjectRequest project2,
                               @RequestParam(name = "id", required = true) String id) {

        logger.debug("/dossier/data?id={}", id);
        ProjectRequest project = null;
        if (id != null) {
            if (user.getAuthorities().contains(Role.MANAGER))
                project = cs.getProjectRequest(id, null, user.getCrmId());
            else if ( user.getAuthorities().contains(Role.SIND_ADM) ||
                      user.getAuthorities().contains(Role.FAB) ||
                      user.getAuthorities().contains(Role.SIND) )
            {
                project = cs.getProjectRequest(id, null, null);
            }
            else
                project = cs.getProjectRequest(id, user.getUsername(), null);
        }

        ObjectMapper mapper = new ObjectMapper();
        ArrayNode arrayNode = mapper.createArrayNode();



        String folderPath = "Shared%20Documents/" + project.getInitiatorInn() + "/" + project.getProjectCode();

        List<SPFile> listFile = sp.getListFile(folderPath);
        if (listFile != null && listFile.size() > 0) {
            List<DocumentSP> documentSPList = cs.getSPFileProp(project.projectId);
            listFile = filter(documentSPList, listFile, user, id);
        }
        if (!ListUtils.isEmpty(listFile)) {
            for (SPFile file : listFile) {
//                logger.debug(file.getEditLink());
                ObjectNode node = mapper.createObjectNode();

                try {
                    String fileName = null;
                    String fileExt = null;
                    if (file.getTitle() == null) {
                        String [] ss = file.getEditLink().split("/");
                        file.setTitle(ss[ss.length-1]);

                    }
                        int index = file.getTitle().lastIndexOf(".");

                        if (index > 0) {
                            fileName = file.getTitle().substring(0, index);
                            if (index < file.getTitle().length())
                                fileExt = file.getTitle().substring(index + 1);
                            else
                                fileExt = null;
                        } else {
                            fileName = file.getTitle();
                            fileExt = null;
                        }

                    node.put("name", fileName);
                    node.put("ext", fileExt);
                    node.put("size", file.getLength());
                    node.put("undeletable", true);

                    node.put("url", "/private-office/dossier/file/" + fileName + "?id=" + id + "&ext=" + fileExt);

                    arrayNode.add(node);
                } catch (ArrayIndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
        }

        return arrayNode.toString();
    }

    @GetMapping("/dossier/file/{file}")
    private void dossierFile(@ModelAttribute("user") User user, @PathVariable(name = "file", required = true) String fileName, @RequestParam(name = "id", required = true) String id, @RequestParam(name = "ext", required = true) String ext, HttpServletResponse response) throws IOException {

        logger.debug("/dossier/data?id={}", id);
        ProjectRequest project = null;
        if (id != null) {
            if (user.getAuthorities().contains(Role.MANAGER))
                project = cs.getProjectRequest(id, null, user.getCrmId());
            else if ( user.getAuthorities().contains(Role.SIND_ADM) ||
                    user.getAuthorities().contains(Role.FAB) ||
                    user.getAuthorities().contains(Role.SIND) )
            {

                project = cs.getProjectRequest(id, null, null);
            }
            else
                project = cs.getProjectRequest(id, user.getUsername(), null);
        }

        ObjectMapper mapper = new ObjectMapper();
        ArrayNode arrayNode = mapper.createArrayNode();

        String folderPath = "Shared%20Documents/" + project.getInitiatorInn() + "/" + project.getProjectCode();
//        logger.debug(folderPath);

        List<SPFile> listFile = sp.getListFile(folderPath);
        for (SPFile file : listFile) {
            if (file.getTitle() == null) {
                logger.error(file.getEditLink() +" :Title is null");
            } else {
                int index = file.getTitle().lastIndexOf(".");
                String fileNameN = null;
                String fileExtN = null;
                if (index > 0) {
                    fileNameN = file.getTitle().substring(0, index);
                    if (index < file.getTitle().length())
                        fileExtN = file.getTitle().substring(index + 1);
                    else
                        fileExtN = null;
                } else {
                    fileNameN = file.getTitle();
                    fileExtN = null;
                }

                if (fileName.equals(fileNameN) && ((fileExtN == null && ext.equals("null")) || (fileExtN.equals(ext)))) {
                    response.setContentType("application/octet-stream");
                    response.addHeader("Content-Disposition", "attachment; filename=" + fileName + "." + ext);
                    OutputStream out = response.getOutputStream();
                    byte[] file1 = sp.getFile(file.getEditLink());
                    if (file1 == null) {
                        logger.error("Неудалось прочитать файл: " + file.getEditLink());
                    } else {
                        out.write(file1);
                    }
                    out.close();
                    return;
                }
            }
        }
        ///////////////////////////
        String fn = fileName + "." + ext + "')";
        for (SPFile file : listFile) {
            if (file.getTitle() == null) {
                if (file.getEditLink().contains(fn)) {
                    response.setContentType("application/octet-stream");
                    response.addHeader("Content-Disposition", "attachment; filename=" + fileName + "." + ext);
                    OutputStream out = response.getOutputStream();
                    byte[] file1 = sp.getFile(file.getEditLink());
                    if (file1 == null) {
                        logger.error("Неудалось прочитать файл: " + file.getEditLink());
                    } else {
                        out.write(file1);
                    }
                    out.close();
                    return;
                }
            }
        }
    }

    @GetMapping("/dossier")
    private String dossier(Model model, @RequestParam(name = "id", required = true) String id, @ModelAttribute("user") User user) {

        model.addAttribute("chooseMenu", "dossier");
        model.addAttribute("notNeedAddFile", true);

        ProjectRequest project = null;
        if (id != null) {
            if (user.getAuthorities().contains(Role.MANAGER))
                project = cs.getProjectRequest(id, null, user.getCrmId());
            else
                project = cs.getProjectRequest(id, /*user.getUsername()*/null, null);
        }
        model.addAttribute("project", project);


        Manager client = new Manager();
        client.setEmail(project.getApplicantEmail());
        client.setNameFirst(project.getApplicantNameFirst());
        client.setNameLast(project.getApplicantNameLast());
        client.setPhone(project.getApplicantPhoneNumber());
        client.setImage(null);
        client.setRegionId(project.getProjectSalePlace());
        model.addAttribute("client", client);
        model.addAttribute("collaborator", null);
        model.addAttribute("manager", cs.getManagerById(project.getProjectCertifiedManagerId()));


        if (project.getProjectSalePlace() != null) {
            String region = rs.getRegion(Integer.valueOf(project.getProjectSalePlace()));
            model.addAttribute("regionZnF", region);
        } else {
            model.addAttribute("regionZnF", "");
        }

        return "LK/manager/dossier";
    }

    @GetMapping("/notification")
    private String notification(Model model, @ModelAttribute("user") User user, @RequestParam(name = "id", required = false) String id) {
        model.addAttribute("chooseMenu", "notification");

        List<Notification> nots = null;
        if (user.getAuthorities().contains(Role.MANAGER))
            nots = nr.findByProjectCertifiedManagerId(user.getCrmId());
        else
            nots = nr.findByApplicantEmail(user.getUsername());

        List<Element> projects = new ArrayList<>();
        if (!ListUtils.isEmpty(nots)) {
            for (Notification notification : nots) {
                List<Element> list = null;
                if (ListUtils.isEmpty(projects))
                    list = projects.stream().filter(el -> el.getProjectId().equals(notification.getProjectId())).collect(Collectors.toList());
                if (ListUtils.isEmpty(list)) projects.add(Element.builder()
                        .projectId(notification.getProjectId())
                        .projectNomination(notification.getProjectNomination())
                        .count(1)
                        .build());
                else
                    list.get(0).count++;
            }
        }
        model.addAttribute("projects", projects);


        if (!Strings.isNullOrEmpty(id)) {
            ProjectRequest project = null;
            if (user.getAuthorities().contains(Role.MANAGER))
                project = cs.getProjectRequest(id, null, user.getCrmId());
            else
                project = cs.getProjectRequest(id, user.getUsername(), null);

            List<Notification> notifications = nr.findByProjectId(id);

            if (!ListUtils.isEmpty(notifications)) {
                for (Notification notification : notifications) {
                    notification.setViewed(true);
                    nr.saveAndFlush(notification);
                }
            }
            model.addAttribute("project", project);
            model.addAttribute("notifications", notifications);
            model.addAttribute("projectId", project.getProjectId());


//            Manager userZnF = new Manager();
//            userZnF.setEmail(project.getApplicantEmail());
//            userZnF.setNameFirst(project.getApplicantNameFirst());
//            userZnF.setNameLast(project.getApplicantNameLast());
//            userZnF.setPhone(project.getApplicantPhoneNumber());
//            userZnF.setImage(null);
//            userZnF.setRegionId(project.getProjectSalePlace());
//            model.addAttribute("collaborator", userZnF);
            model.addAttribute("collaborator", null);

            if(user.getAuthorities().contains(Role.MANAGER)) {
                Declarer declarer = new Declarer();
                declarer.setApplicantNameFirst(project.getApplicantNameFirst());
                declarer.setApplicantNameLast(project.getApplicantNameLast());
                declarer.setApplicantNameMid(project.getApplicantNameMid());
                declarer.setApplicantPhoneNumbers(project.getApplicantPhoneNumbers());
                declarer.setApplicantEmails(project.getApplicantEmails());
                model.addAttribute("declarer", declarer);
            }


            Manager manager = cs.getManagerById(project.getProjectCertifiedManagerId());
            if (null == manager) {
                model.addAttribute("manager", null);
            } else {
                model.addAttribute("manager", manager);
                if (manager.getRegionId() != null)
                    model.addAttribute("regionZnF", rs.getRegion(Integer.valueOf(manager.getRegionId())));
                else
                    model.addAttribute("regionZnF", "");
            }

            int rating = 0;
            Map<String, Integer> ratings = cs.getRatings();
            if (ratings.containsKey(project.getProjectCertifiedManagerId()))
                rating = ratings.get(project.getProjectCertifiedManagerId());
            model.addAttribute("rating", rating);

        } else {
            model.addAttribute("notifications", new ArrayList<Notification>());
            model.addAttribute("collaborator", null);
            model.addAttribute("manager", null);
            model.addAttribute("regionZnF", null);
            model.addAttribute("rating", 0);
            model.addAttribute("project", null);
        }

        return "LK/manager/notification";
    }

    @GetMapping("/message")
    private String message(Model model, @ModelAttribute("user") User user, @RequestParam(name = "id", required = false) String id,  @RequestParam(name = "who", required = false) String who) {
        List<ProjectResponsePacket> projectsResponse = null;
        if (user.getAuthorities().contains(Role.MANAGER))
            projectsResponse = cs.getProjectPacket(500, 0, null, user.getCrmId());
        else
            projectsResponse = cs.getProjectPacket(500, 0, user.getUsername(), null);

        if (!UtilsService.checkAccess(projectsResponse, user)) throw new ForbiddenException();

        model.addAttribute("projects", projectsResponse);
        model.addAttribute("projectId", id);


        if (id != null) {
            ProjectResponse project = null;
            if (user.getAuthorities().contains(Role.MANAGER))
                project = cs.getProject(id, null, user.getCrmId());
            else
                project = cs.getProject(id, user.getUsername(), null);


            if (!UtilsService.checkAccess(project, user) || project == null) throw new ForbiddenException();


            try {
                updateMessage(id, project.projectManager != null ? project.projectManager.getCrmId() : null);
            } catch (Exception e) {
                e.printStackTrace();
            }

            List<MessageEntity> messages;
            if(who != null){
                messages = mr.findAllByProjectIdAndRecipientsID(id, who);
            }else{
                messages = mr.findAllByProjectIdAndRecipientsID(id, "");
            }

            if (!ListUtils.isEmpty(messages)) messages.forEach(item -> UtilsService.securityVueJS(item));

            model.addAttribute("messages", messages);
            model.addAttribute("currentProject", project);
        } else {
            model.addAttribute("messages", new ArrayList<MessageEntity>());
        }

        model.addAttribute("chooseMenu", "message");
        return "LK/manager/messages";
    }

    @PostMapping("/message")
    private String messageSend(Model model, @ModelAttribute("user") User user, @RequestParam(name = "id", required = true) String id, @RequestParam(name = "who", required = true) String who, MessageEntity messageSend) throws Exception {
        UtilsService.securityVueJS(messageSend);

        ProjectResponse project = null;
        if (user.getAuthorities().contains(Role.MANAGER))
            project = cs.getProject(id, null, user.getCrmId());
        else
            project = cs.getProject(id, user.getUsername(), null);

        if (!UtilsService.checkAccess(project, user)) throw new ForbiddenException();

        Message message = new Message();
        message.setSubject("Сообщение с Электронного офиса.");
        message.setText(messageSend.getText());

        Set<String> recipients = new HashSet<>();
        try {
            // добавим текущего пользователя, чтобы клиент смог сделать выборку по нему
            recipients.add(user.getCrmId());

            // если есть кому конкретно отправить сообщение
            if(who != null){
                recipients.add(who);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        message.setRecipients(recipients);

        String crmId = cs.sendMessage(id, message);

        if(who != null){
            messageSend.setRecipientsID(recipients);
        }

        messageSend.setCrmId(crmId);
        messageSend.setDateEntered(new Date());
        messageSend.setProjectId(id);
        if (user.getAuthorities().contains(Role.MANAGER)) {
            messageSend.setAuthorRole(ru.veb.eo.Role.MANAGER);
            messageSend.setAuthorId("MANAGER");
            try {
                if (project.projectManager != null)
                    messageSend.setAuthorTitle(project.projectManager.getNameFirst() + " " + project.projectManager.getNameLast());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            messageSend.setAuthorRole(ru.veb.eo.Role.CLIENT);
            messageSend.setAuthorId("CLIENT");
            try {
                if (project.declarer != null)
                    messageSend.setAuthorTitle(project.declarer.getApplicantNameFirst() + " " + project.declarer.getApplicantNameLast());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!Strings.isNullOrEmpty(crmId))
            mr.saveAndFlush(messageSend);

        return String.format("redirect:/private-office/message?id=%s&who=%s", id, who);
    }

    private void updateMessage(String id, String idManager) {
        List<Message> messages = cs.getMessages(id);
        if (!ListUtils.isEmpty(messages)) {
            for (Message message : messages) {
                if (mr.findOne(message.getCrmId()) == null) {
                    MessageEntity messageEntity = new MessageEntity();
                    messageEntity.setCrmId(message.getCrmId());
                    messageEntity.setSubject(message.getSubject());
                    messageEntity.setText(message.getText());
                    messageEntity.setDateEntered(message.getDateEntered());
                    messageEntity.setAuthorTitle("Сотрудник банка");
                    messageEntity.setAuthorRole(ru.veb.eo.Role.EMPLOYEE);
//                    if (message.getRecipients() != null && message.getRecipients().size() > 1) {
//                        List<ru.veb.eo.Role> recipients = new ArrayList<>();
//                        recipients.add(ru.veb.eo.Role.MANAGER);
//                        recipients.add(ru.veb.eo.Role.CLIENT);
//                        messageEntity.setRecipients(recipients);
//                    } else if (message.getRecipients() != null && message.getRecipients().contains(idManager)) {
//                        List<ru.veb.eo.Role> recipients = new ArrayList<>();
//                        recipients.add(ru.veb.eo.Role.MANAGER);
//                        messageEntity.setRecipients(recipients);
//                    } else {
//                        List<ru.veb.eo.Role> recipients = new ArrayList<>();
//                        recipients.add(ru.veb.eo.Role.CLIENT);
//                        messageEntity.setRecipients(recipients);
//                    }
                    messageEntity.setAuthorId(null);
                    messageEntity.setProjectId(id);

                    mr.saveAndFlush(messageEntity);
                }
            }

        }
    }
}

@Data
@Builder
class Element {
    public String projectId;
    public String projectNomination;
    public Integer count;
    public String collaborator;
    public String collaboratorImage;
    public Integer collaboratorCount;
    public String applicant;
    public String applicantImage;
    public Integer applicantCount;
    public String manager;
    public String managerImage;
    public Integer managerCount;
}