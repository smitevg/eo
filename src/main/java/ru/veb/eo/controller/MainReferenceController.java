package ru.veb.eo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.util.ListUtils;
import org.thymeleaf.util.SetUtils;
import ru.veb.eo.model.Role;
import ru.veb.eo.model.User;
import ru.veb.eo.model.crm.Manager;
import ru.veb.eo.model.crm.Rating;
import ru.veb.eo.model.crm.Supply;
import ru.veb.eo.model.crm.TypeSupply;
import ru.veb.eo.model.reference.*;
import ru.veb.eo.repository.DistrictRepository;
import ru.veb.eo.repository.ProjectDirectionRepository;
import ru.veb.eo.repository.ProjectRepository;
import ru.veb.eo.repository.RegionRepository;
import ru.veb.eo.service.CrmService;
import ru.veb.eo.service.ReferenceService;
import ru.veb.eo.service.UserService;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/reference")
public class MainReferenceController {
    private static final Logger logger = LoggerFactory.getLogger(MainReferenceController.class);

    @Autowired
    private ReferenceService rs;

    @Autowired
    private CrmService crmService;

    @Autowired
    private UserService us;

    @Autowired
    private RegionRepository rr;

    @Autowired
    private DistrictRepository dr;

    @Autowired
    private ProjectRepository pr;

    @Autowired
    private ProjectDirectionRepository pdr;

    @RequestMapping("/projectCost")
    private List<ProjectCost> projectCost() {
        return rs.getProjectCost();
    }

    @GetMapping("/projectPaybackTime")
    private List<ProjectPaybackTime> projectPaybackTime() {
        return rs.getProjectPaybackTime();
    }

    @GetMapping("/projectSectors")
    private List<ProjectSectors> projectIndustry() {
        return rs.getProjectSectors();
    }

    @GetMapping("/projectSectorsWithDirections")
    private String projectIndustryWithDirections() {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode node = mapper.createObjectNode();

        List<ProjectDirection> directions = rs.getProjectDirections();
        if (!ListUtils.isEmpty(directions)) {
            for (ProjectDirection direction : directions) {
                List<ProjectSectors> sectors = rs.getProjectSectorsByDirectionId(direction.getId());
                if (!ListUtils.isEmpty(sectors)) {
                    sectors.stream().sorted(Comparator.comparing(ProjectSectors::getTitle))
                            .forEach(e -> {
                                node.put(String.valueOf(e.getSortOrder()), direction.getTitle() + " / " + e.getTitle());
                            });
                }
            }
        }


        return node.toString();
    }

    @GetMapping("/projectType")
    private List<ProjectType> projectType() {
        return rs.getProjectType();
    }

    @GetMapping("/projectRegion")
    private List<Region> projectRegion() {
        return rs.getRegion();
    }

    @GetMapping("/projectDirections")
    private List<ProjectDirection> projectDirections() {
        return rs.getProjectDirections();
    }

    @GetMapping("/districts")
    private String districts() {
        List<District> districts = rs.getDistricts();
        if (!ListUtils.isEmpty(districts)) {
            ObjectMapper mapper = new ObjectMapper();
            ObjectNode node = mapper.createObjectNode();
            for (District district : districts) {

//                logger.debug("district = {}", district.getTitle());
                ObjectNode regions = mapper.createObjectNode();

                List<Region> regionSet = district.getRegions();
                if (!ListUtils.isEmpty(regionSet)) {
                    for (Region region : regionSet) {
//                        logger.debug("region = {}", region.getTitle());
                        regions.put(String.valueOf(region.getId()), region.getTitle());
                    }
                }
                ObjectNode districtNode = mapper.createObjectNode();
                districtNode.put("name", district.getTitle());
                districtNode.set("regions", regions);

                node.set(String.valueOf(district.getId()), districtNode);
            }
            return node.toString();
        } else {
            return null;
        }
    }

    @GetMapping("/mapData")
    private String mapData() {

        Map<String, Integer> ratings = crmService.getRatings();

        ObjectMapper mapper = new ObjectMapper();

        ObjectNode node = mapper.createObjectNode();

        ObjectNode dataFields = mapper.createObjectNode();

        ArrayNode appeals = mapper.createArrayNode();
//        (0, 'Новый');
//        (1, 'Экпресс-оценка');
//        (2, 'Первичная фильтрация проекта Банком');
//        (3, 'Подготовка к предварителной экспертизе');
        appeals.add("Направлено в банк");
//        (7, 'Экспресс-экспертиза');
//        (8, 'Предварительная экспертиза');
//        (9, 'Повторная предварительная экспертиза');
//        (10, 'Подготовка к комплексной экспертизе');
//        (11, 'Комплексная экспертиза');
//        (12, 'Повторная комплексная экспертиза');
        appeals.add("На рассмотрении");
//        (13, 'КОД подписана');
//        (14, 'Финансирование открыто');
//        (15, 'Финансирование завершено');
        appeals.add("Одобрено");
//        (4, 'Отложен/Необходима доработка Клиентом');
//        (5, 'Отказ клиента/Архив');
//        (6, 'Отказ/Архив');
        appeals.add("Отклонено");
        dataFields.set("appeals", appeals);


        ArrayNode financing = mapper.createArrayNode();
        List<ProjectDirection> projectDirections = pdr.findAll(new Sort("id"));
        if (!ListUtils.isEmpty(projectDirections)) {
            for (ProjectDirection projectDirection : projectDirections) {
                financing.add(projectDirection.getTitle());
            }
        }
        dataFields.set("financing", financing);

        node.set("dataFields", dataFields);

        ObjectNode regions = mapper.createObjectNode();

        List<String> regionsId = new ArrayList<>();
        List<Region> all = rr.findAll();
        if (!ListUtils.isEmpty(all)) {
            for (Region region : all) {
                if (!regionsId.contains(region.getFrontId())) regionsId.add(region.getFrontId());
            }
        }

        if (!ListUtils.isEmpty(regionsId)) {
            for (String region : regionsId) {
                ObjectNode regionNode = mapper.createObjectNode();
                ArrayNode managersRegion = mapper.createArrayNode();

                List<Region> regionsInRep = rr.findAllByFrontIdEquals(region);

                List<Integer> financingRegionList = new ArrayList<>();
                if (!ListUtils.isEmpty(projectDirections)) {
                    for (ProjectDirection projectDirection : projectDirections) {
                        financingRegionList.add(0);
                    }
                }

                List<Integer> appealsRegionList = new ArrayList<>();
                appealsRegionList.add(0);
                appealsRegionList.add(0);
                appealsRegionList.add(0);
                appealsRegionList.add(0);

                // перебор регионов с одинаковым id на карте
                if (!ListUtils.isEmpty(regionsInRep)) {
                    for (Region region1 : regionsInRep) {

                        if (!ListUtils.isEmpty(projectDirections)) {
                            for (ProjectDirection projectDirection : projectDirections) {
                                int index = projectDirection.getId();
                                financingRegionList.set(index, financingRegionList.get(index) + pr.countAllByProjectSalePlaceAndProjectProjectIsAimedTo(String.valueOf(region1.getId()), String.valueOf(projectDirection.getId())));
                            }
                        }
                        appealsRegionList.set(0, appealsRegionList.get(0) + pr.countAllByProjectSalePlaceAndProjectRequestStatusIn(String.valueOf(region1.getId()), Arrays.asList(new Integer[]{0, 1, 2, 3})));
                        appealsRegionList.set(1, appealsRegionList.get(1) + pr.countAllByProjectSalePlaceAndProjectRequestStatusIn(String.valueOf(region1.getId()), Arrays.asList(new Integer[]{7, 8, 9, 10, 11, 12})));
                        appealsRegionList.set(2, appealsRegionList.get(2) + pr.countAllByProjectSalePlaceAndProjectRequestStatusIn(String.valueOf(region1.getId()), Arrays.asList(new Integer[]{13, 14, 15})));
                        appealsRegionList.set(3, appealsRegionList.get(3) + pr.countAllByProjectSalePlaceAndProjectRequestStatusIn(String.valueOf(region1.getId()),Arrays.asList( new Integer[]{4, 5, 6})));

                        List<User> managers = us.getManagersByRegion(String.valueOf(region1.getId()));

                        if (!ListUtils.isEmpty(managers)) {
                            for (User manager : managers) {
                                ObjectNode managerNode = mapper.createObjectNode();
                                managerNode.put("name", manager.getNameFirst() + " " + manager.getNameLast());
                                managerNode.put("photo", manager.getImage());
                                if (ratings.containsKey(manager.getCrmId()))
                                    managerNode.put("rating", ratings.get(manager.getCrmId()));
                                else
                                    managerNode.put("rating", 0);
                                managersRegion.add(managerNode);
                            }
                        }
                    }
                }

                ArrayNode financingRegion = mapper.createArrayNode();
                for (Integer integer : financingRegionList) {
                    financingRegion.add(integer);
                }

                ArrayNode appealsRegion = mapper.createArrayNode();
                for (Integer integer : appealsRegionList) {
                    appealsRegion.add(integer);
                }

                regionNode.set("financing", financingRegion);
                regionNode.set("appeals", appealsRegion);
                regionNode.set("managers", managersRegion);

                if (!Strings.isNullOrEmpty(region))
                    regions.set(region, regionNode);
            }
        }

        node.set("regions", regions);
        return node.toString();
    }

    @GetMapping("/managers")
    private String managers() {

        Map<String, Integer> ratings = crmService.getRatings();

        ObjectMapper mapper = new ObjectMapper();
        ArrayNode node = mapper.createArrayNode();

        List<User> managers = us.getManagers();

        if (!ListUtils.isEmpty(managers)) {
            for (User manager : managers) {
                try {
                    String regionId = manager.getRegionId();
                    if (!Strings.isNullOrEmpty(regionId)) {
                        Region region = rr.findOne(Integer.valueOf(manager.getRegionId()));
                        if (region != null) {
                            District district = dr.findFirstByRegionsContains(region);
                            ObjectNode managerJson = mapper.createObjectNode();
                            managerJson.put("id", manager.getId());
                            managerJson.put("first_name", manager.getNameFirst());
                            managerJson.put("last_name", manager.getNameLast());
                            managerJson.put("image", manager.getImage());
                            managerJson.put("district", district.getId());
                            managerJson.put("region", region.getId());
                            if (ratings.containsKey(manager.getCrmId()))
                                managerJson.put("rating", ratings.get(manager.getCrmId()));
                            else
                                managerJson.put("rating", 0);
                            node.add(managerJson);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return node.toString();
        } else {
            return null;
        }
    }

    @GetMapping("/countManagers")
    private String countManagers() {
        List<User> managers = us.getManagers();
        if (!ListUtils.isEmpty(managers)) {
            return String.valueOf(managers.size());
        } else {
            return null;
        }
    }
}
