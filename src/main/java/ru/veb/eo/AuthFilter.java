package ru.veb.eo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;
import ru.veb.eo.model.Role;
import ru.veb.eo.model.User;
import ru.veb.eo.service.UserService;

import javax.annotation.PostConstruct;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.util.Arrays;

@Component
public class AuthFilter extends GenericFilterBean {
    private static final Logger logger = LoggerFactory.getLogger(AuthFilter.class);

    @Autowired
    UserService userService;

    @Autowired
    InitBean initBean;

    @Autowired
    Properties properties;

    private User admin;

    @PostConstruct
    private void init() {
        try {

            if (properties.getDebugMode()) {
                //admin = (User) userService.loadUserByUsername("al-treb@yandex.ru");
//                admin = (User) userService.loadUserByUsername("TikhonovaAV@veb.ru");
//            admin = (User) userService.loadUserByUsername("a.sheifler@inpglobal.com");
            }
        } catch (UsernameNotFoundException e) {
//            e.printStackTrace();
        }
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (null != admin) SecurityContextHolder.getContext().setAuthentication(new UserAuthentication(admin));

        chain.doFilter(request, response);
    }
}
