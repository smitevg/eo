package ru.veb.eo;

import lombok.Data;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:/git.properties")
@ConditionalOnResource(resources = "classpath:/git.properties")
@Data
public class AppVersionInfo {
    @Value("${git.branch}")
    String branch;

    @Value("${git.closest.tag.name}")
    String closestTagName;

    @Value("${git.commit.id.abbrev}")
    String commitIdAbbrev;

    @Value("${git.commit.time}")
    String commitTime;

    @Value("${git.tags}")
    String tags;
}
