package ru.veb.eo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class Properties {
    @Autowired
    Environment env;

    private static final Logger logger = LoggerFactory.getLogger(Properties.class);

    private final String AUTH2 = "eo.auth2";
    private final String DEBUG_MODE = "eo.debug";
    private final String PASSWORD_ENABLE = "eo.password.enable";
    private final String PASSWORD_CREATE_SIMPLE = "eo.password.create_simple";

    private final String PROXY_HOST = "eo.proxy.host";
    private final String PROXY_PORT = "eo.proxy.port";

    private final String CRM_HOST = "eo.crm.host";
    private final String CRM_SECRET = "eo.crm.secret";
    private final String CRM_SCHEMA = "eo.crm.schema";
    private final String CRM_USERNAME = "eo.crm.username";
    private final String CRM_PASSWORD = "eo.crm.password";

    private final String DEFAULT_MANAGER = "eo.manager";
    private final String RATING_ON = "eo.manager.rating";
    private final String DZO_ON = "eo.manager.dzo";
    private final String MANAGER_AVATARS = "eo.manager.avatars";


    private final String SMTP_HOST = "eo.smtp.host";
    private final String SMTP_PORT = "eo.smtp.port";
    private final String SMTP_USERNAME = "eo.smtp.username";
    private final String SMTP_PASSWORD = "eo.smtp.password";
    private final String SMTP_AUTH = "eo.smtp.auth";
    private final String SMTP_STARTTLS = "eo.smtp.starttls";
    private final String SMTP_PROTOCOL = "eo.smtp.protocol";

    private final String SHAREPOINT_WEBSERVICE = "eo.sharepoint.url.service";
    private final String SHAREPOINT_USERNAME = "eo.sharepoint.username";
    private final String SHAREPOINT_PASSWORD = "eo.sharepoint.password";
    private final String SHAREPOINT_DOMAIN = "eo.sharepoint.domain";
    private final String SHAREPOINT_AUTH = "eo.sharepoint.auth";
    private final String SHAREPOINT_DESTINATION_URL = "eo.sharepoint.url.destination";


    public boolean isEnablePassword() {
        try {
            if (env.getProperty(PASSWORD_ENABLE) != null && !env.getProperty(PASSWORD_ENABLE).isEmpty())
                return "true".equals(env.getProperty(PASSWORD_ENABLE)) || "TRUE".equals(env.getProperty(PASSWORD_ENABLE));
            else
                return true;
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }

    public boolean isSimplePasswordCreate() {
        try {
            String prop = env.getProperty(PASSWORD_CREATE_SIMPLE);
            return "true".equalsIgnoreCase(prop);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getPathToAvatarsOfManager(){
        return System.getProperty("user.dir") + env.getProperty(MANAGER_AVATARS);
    }

    public boolean isRating() {
        try {
            if (env.getProperty(RATING_ON) != null && !env.getProperty(RATING_ON).isEmpty())
                return "true".equals(env.getProperty(RATING_ON)) || "TRUE".equals(env.getProperty(RATING_ON));
            else
                return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean isDzo() {
        try {
            if (env.getProperty(DZO_ON) != null && !env.getProperty(DZO_ON).isEmpty())
                return "true".equals(env.getProperty(DZO_ON)) || "TRUE".equals(env.getProperty(DZO_ON));
            else
                return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getCrmAuthUsername() {
        return env.getProperty(CRM_USERNAME);
    }

    public String getCrmAuthPassword() {
        return env.getProperty(CRM_PASSWORD);
    }

    public String getDefaultManager() {
        return env.getProperty(DEFAULT_MANAGER);
    }

    public String getSharepointDestinationUrl() {
        return env.getProperty(SHAREPOINT_DESTINATION_URL);
    }

    public String getSharepointPathWS() {
        return env.getProperty(SHAREPOINT_WEBSERVICE);
    }

    public String getSharepointUsername() {
        return env.getProperty(SHAREPOINT_USERNAME);
    }

    public String getSharepointPassword() {
        return env.getProperty(SHAREPOINT_PASSWORD);
    }

    public String getSharepointDomain() {
        return env.getProperty(SHAREPOINT_DOMAIN);
    }

    public String getSharepointAuth() {
        try {
            return env.getProperty(SHAREPOINT_AUTH);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean getAuth2() {
        try {
            if (env.getProperty(AUTH2) != null && !env.getProperty(AUTH2).isEmpty())
                return "true".equals(env.getProperty(AUTH2));
            else
                return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getSmtpHost() {
        return env.getProperty(SMTP_HOST);
    }

    public int getSmtpPort() {
        try {
            if (env.getProperty(SMTP_PORT) != null && !env.getProperty(SMTP_PORT).isEmpty())
                return Integer.valueOf(env.getProperty(SMTP_PORT));
            else
                return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public String getSmtpUsername() {
        try {
            return env.getProperty(SMTP_USERNAME);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getSmtpPassword() {

        try {
            return env.getProperty(SMTP_PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean getSmtpAuth() {
        try {
            if (env.getProperty(SMTP_AUTH) != null && !env.getProperty(SMTP_AUTH).isEmpty()) {
                return "true".equals(env.getProperty(SMTP_AUTH));
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean getSmtpStarttls() {
        try {
            if (env.getProperty(SMTP_STARTTLS) != null && !env.getProperty(SMTP_STARTTLS).isEmpty()) {
                return "true".equals(env.getProperty(SMTP_AUTH));
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getSmtpProtocol() {

        try {
            return env.getProperty(SMTP_PROTOCOL);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public String getCrmHost() {
        if (env.getProperty(CRM_HOST) != null && !env.getProperty(CRM_HOST).isEmpty() && env.getProperty(CRM_SCHEMA) != null && !env.getProperty(CRM_SCHEMA).isEmpty()) {
            return env.getProperty(CRM_SCHEMA) + "://" + env.getProperty(CRM_HOST) + "/index.php";
        }
        return null;
    }

    public String getCrmSecret() {

        try {
            return env.getProperty(CRM_SECRET);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int getProxyPort() {
        try {
            if (env.getProperty(PROXY_PORT) != null && !env.getProperty(PROXY_PORT).isEmpty()) {
                return Integer.valueOf(env.getProperty(PROXY_PORT));
            } else {
                return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }

    }

    public String getProxyHost() {
        try {
            return env.getProperty(PROXY_HOST);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean getDebugMode() {
        try {

            if (env.getProperty(DEBUG_MODE) != null && !env.getProperty(DEBUG_MODE).isEmpty()) {
                return "true".equals(env.getProperty(DEBUG_MODE));
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
