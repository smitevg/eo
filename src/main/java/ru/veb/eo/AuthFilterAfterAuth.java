package ru.veb.eo;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;
import ru.veb.eo.model.Role;
import ru.veb.eo.model.User;
import ru.veb.eo.service.EmailService;
import ru.veb.eo.service.UserService;

import javax.annotation.PostConstruct;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Map;

import static ru.veb.eo.service.UtilsService.generateRandom;

@Component
public class AuthFilterAfterAuth extends GenericFilterBean {
    private static final Logger logger = LoggerFactory.getLogger(AuthFilterAfterAuth.class);

    @Autowired
    protected UserService userService;

    @Autowired
    protected Properties properties;

    @Autowired
    private EmailService emailService;

    private final RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        String page = "";

        if (authentication != null && authentication.getPrincipal() instanceof User && properties.getAuth2()) {
            User user = (User) authentication.getPrincipal();

            if (!user.isAuth2()) {
                HttpServletRequest request = (HttpServletRequest) servletRequest;
                String path = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/private-office?token=";
                if (Strings.isNullOrEmpty(user.getToken())) {

                    String token = generateRandom(30);
                    user.setToken(token);
                    userService.saveUser(user);
                    path += token;
                    emailService.sendSimpleMessage(user.getUsername(), "Ссылка для входа в Электронный офис", "Уважаемый, " + user.getNameFirst() + (!Strings.isNullOrEmpty(user.getNameLast()) ? " " + user.getNameLast() : "") + "! \n\n\nДля Вас сгенерированная ссылка для входа в систему: " + path);

                } else {
                    String tokenRequest = request.getParameter("token");
                    if (!Strings.isNullOrEmpty(tokenRequest) && tokenRequest.equals(user.getToken())) {
                        user.setAuth2(true);
                        user.setToken(null);
                        userService.saveUser(user);
                    } else {
                        path += user.getToken();
                        emailService.sendSimpleMessage(user.getUsername(), "Ссылка для входа в Электронный офис", "Уважаемый, " + user.getNameFirst() + (!Strings.isNullOrEmpty(user.getNameLast()) ? " " + user.getNameLast() : "") + "! \n\n\nДля Вас сгенерированная ссылка для входа в систему: " + path);
                    }
                }

                if (!user.isAuth2()) {
                    page = "/403.html";
                }
            }
        }

        if (!Strings.isNullOrEmpty(page)) {
            this.redirectStrategy.sendRedirect((HttpServletRequest) servletRequest, (HttpServletResponse) response, page);
        } else {
            chain.doFilter(servletRequest, response);
        }
    }
}