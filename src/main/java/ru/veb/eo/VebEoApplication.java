package ru.veb.eo;

import com.google.common.base.Strings;
import org.apache.catalina.authenticator.BasicAuthenticator;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.event.ApplicationFailedEvent;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.event.EventListener;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import javax.sql.DataSource;
import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.Proxy;

@SpringBootApplication
@EnableScheduling
public class VebEoApplication {
    private static final Logger logger = LoggerFactory.getLogger(VebEoApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(VebEoApplication.class, args);
    }

    public static String env(String n, String def) {
        String v = System.getenv(n);
        if (v != null) {
            return v;
        } else {
            return def;
        }
    }

    @EventListener
    public void onApplicationReady(ApplicationReadyEvent ev) {
        logger.info("Application ready");
    }

    @EventListener
    public void onApplicationFailed(ApplicationFailedEvent afe) {
        logger.info(afe.toString());
    }

    @Autowired
    private Environment env;

    @Autowired
    private Properties properties;

    @Bean("restTemplateCRM")
    @Autowired
    public RestTemplate configProxy(RestTemplateBuilder restTemplateBuilder, Properties p) {

        String username = properties.getCrmAuthUsername();
        String password = properties.getCrmAuthPassword();
        if (!Strings.isNullOrEmpty(username) && !Strings.isNullOrEmpty(password)) {
            String login = username;
            NtlmAuthenticator authenticator = new NtlmAuthenticator(login, password);
            Authenticator.setDefault(authenticator);
        }
        if (p.getProxyHost() != null && !p.getProxyHost().isEmpty() && p.getProxyPort() > 0) {
            SimpleClientHttpRequestFactory clientHttpReq = new SimpleClientHttpRequestFactory();
            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(p.getProxyHost(), p.getProxyPort()));
            clientHttpReq.setProxy(proxy);
            return restTemplateBuilder.requestFactory(clientHttpReq).build();
        } else {
            return restTemplateBuilder.build();
        }
    }

    @Bean(name = "getJavaMailSender")
    @Autowired
    public JavaMailSender getJavaMailSender(Properties properties) {
        JavaMailSenderImpl mailSenderImpl = new JavaMailSenderImpl();

        if (Strings.isNullOrEmpty(properties.getSmtpHost()) || properties.getSmtpPort() == 0) return null;

        mailSenderImpl.setHost(properties.getSmtpHost());
        mailSenderImpl.setPort(properties.getSmtpPort());

        if (!Strings.isNullOrEmpty(properties.getSmtpUsername()))
            mailSenderImpl.setUsername(properties.getSmtpUsername());

        if (!Strings.isNullOrEmpty(properties.getSmtpPassword()))
            mailSenderImpl.setPassword(properties.getSmtpPassword());

        java.util.Properties prop = mailSenderImpl.getJavaMailProperties();
        prop.put("mail.transport.protocol", properties.getSmtpProtocol());
        prop.put("mail.smtp.auth", String.valueOf(properties.getSmtpAuth()));
        prop.put("mail.smtp.starttls.enable", String.valueOf(properties.getSmtpStarttls()));
//        prop.put("mail.debug", "true");
        prop.put("mail.debug", String.valueOf(properties.getDebugMode()));

        return mailSenderImpl;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
        PropertySourcesPlaceholderConfigurer propsConfig = new PropertySourcesPlaceholderConfigurer();
        propsConfig.setLocation(new ClassPathResource("git.properties"));
        propsConfig.setIgnoreResourceNotFound(true);
        propsConfig.setIgnoreUnresolvablePlaceholders(true);
        return propsConfig;
    }



    @Primary
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource dataSource() {
        DataSource ds = DataSourceBuilder.create().build();
        logger.trace("H2 Connect");
        return ds;


    }

    @Bean(value = "dataSourceMySQL")
    @ConditionalOnProperty(name = "SQL_SERVICE", havingValue="suite")
    @ConfigurationProperties(prefix="jdbc")
    public DataSource secondDataSource() {

        //DataSource ds =  DataSourceBuilder.create().build();
        //logger.trace("MYSQL Connect");
        //return ds;
        ComboPooledDataSource ds = new ComboPooledDataSource();
        try {
            ds.setDriverClass(env.getRequiredProperty("jdbc.driverClassName"));
        } catch (Exception e) {e.printStackTrace();}
        ds.setJdbcUrl(env.getRequiredProperty("jdbc.url"));
        ds.setUser(env.getRequiredProperty("jdbc.username"));
        ds.setPassword(env.getRequiredProperty("jdbc.password"));
        ds.setAcquireIncrement(5);
        ds.setIdleConnectionTestPeriod(60);
        ds.setMaxPoolSize(100);
        ds.setMaxStatements(50);
        ds.setMinPoolSize(10);
        return ds;
    }

    @Bean
    @Primary
    public PhysicalNamingStrategy physicalNamingStrategy() {
        return new PhysicalNamingStrategyStandardImpl();
        //return new SpringPhysicalNamingStrategy();
    }


    /*@Bean
    @Primary
    public ImprovedNamingStrategy springNamingStrategy() {
        return new ImprovedNamingStrategy();
    }*/
}
