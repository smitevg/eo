package ru.veb.eo;


import org.hibernate.boot.model.naming.ImplicitNamingStrategy;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyLegacyJpaImpl;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.hibernate.cfg.DefaultNamingStrategy;
import org.hibernate.cfg.EJB3NamingStrategy;
import org.hibernate.cfg.ImprovedNamingStrategy;
import org.hibernate.cfg.NamingStrategy;
import org.hibernate.jpa.AvailableSettings;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.boot.orm.jpa.hibernate.SpringNamingStrategy;
import org.springframework.context.annotation.*;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.context.support.StandardServletEnvironment;
import ru.veb.eo.service.CrmService;
import ru.veb.eo.service.CrmServiceSuite;

import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Configuration
//@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"ru.veb.eo.repository", "ru.veb.eo.model"},
                       entityManagerFactoryRef = "entityManager",
                       transactionManagerRef = "transactionManager")        // 2
public class H2DBConfiguration {

    @Autowired
    private Environment env;


    @Autowired
    DataSource ds;

//    @PersistenceContext(unitName = "primary")   // 4
    ////    @Primary
    ////    @Bean(name = "entityManager")
    ////    public LocalContainerEntityManagerFactoryBean mySqlEntityManagerFactory(EntityManagerFactoryBuilder builder) {
    ////        return builder.dataSource(ds).persistenceUnit("primary").properties(jpaProperties())
    ////                .packages("ru.veb.eo.repository", "ru.veb.eo.model").build();
    //    //}

    private HibernateJpaVendorAdapter vendorAdaptor() {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        //vendorAdapter.setShowSql(true);
        //vendorAdapter.setPrepareConnection(true);
        return vendorAdapter;
    }

    @Primary
    @Bean(name = "entityManager")
    public LocalContainerEntityManagerFactoryBean entityManager() {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setJpaVendorAdapter(vendorAdaptor());
        entityManagerFactoryBean.setDataSource(ds);
        entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        entityManagerFactoryBean.setPackagesToScan("ru.veb.eo.repository", "ru.veb.eo.model", "ru.veb.eo.entity");
        entityManagerFactoryBean.setJpaProperties(jpaProperties());
        entityManagerFactoryBean.afterPropertiesSet();
        return entityManagerFactoryBean;


    }

    @Primary
    @Bean(name = "transactionManager")
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManager().getObject());
        return transactionManager;
    }


    //@ConditionalOnProperty(prefix = "spring.social.", value = "auto-connection-views")
    private Properties jpaProperties() {

        Properties props = new Properties();
        props.put("hibernate.ddl-auto", env.getProperty("spring.jpa.hibernate.ddl-auto"));

        //props.put("hibernate.ejb.naming_strategy", env.getProperty("spring.jpa.hibernate.ejb.naming_strategy", "org.springframework.boot.orm.jpa.hibernate.SpringNamingStrategy"));
        //props.put("hibernate.physical_naming_strategy", env.getProperty("spring.jpa.hibernate.ejb.naming_strategy", "org.springframework.boot.orm.jpa.hibernate.SpringNamingStrategy"));
        //props.put("hibernate.ejb.naming_strategy", "org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl");
        //props.put("hibernate.ejb.naming_strategy","org.hibernate.cfg.ImprovedNamingStrategy");
        //props.put("hibernate.ejb.naming_strategy", "org.springframework.boot.orm.jpa.hibernate.SpringNamingStrategy");
        //props.put("hibernate.naming.physical-strategy", "org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl");
        //props.put("hibernate.physical_naming_strategy", "org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl");
        //props.put("hibernate.dialect","org.hibernate.dialect.H2Dialect");

        //props.put("hibernate.physical_naming_strategy", "org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy");
        props.put("hibernate.physical_naming_strategy",
            env.getProperty("spring.jpa.hibernate.physical_naming_strategy"));

        //props.put("hibernate.implicit_naming_strategy", "org.hibernate.boot.model.naming.ImplicitNamingStrategy");
        //props.put("hibernate.implicit_naming_strategy", "org.hibernate.boot.model.naming.ImplicitNamingStrategyLegacyJpaImpl");


        props.put("generate-ddl", env.getProperty("spring.jpa.generate-ddl"));
        return props;
    }


    @Bean
    public ImprovedNamingStrategy namingStrategy(){
        return new SpringNamingStrategy();
    }
    @Bean
    public ImplicitNamingStrategy implicitNamingStrategy() {
        return new ImplicitNamingStrategyLegacyJpaImpl();
    }
}
