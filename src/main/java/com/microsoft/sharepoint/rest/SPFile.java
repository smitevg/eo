package com.microsoft.sharepoint.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SPFile {

    @JsonProperty("odata.editLink")
    private String editLink;


    @JsonProperty("Title")
    private String title;


    @JsonProperty("Length")
    private String length;
}
