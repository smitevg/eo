
package com.microsoft.sharepoint.ws.copy;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.1
 * 
 */
@WebServiceClient(name = "Copy", targetNamespace = "http://schemas.microsoft.com/sharepoint/soap/", wsdlLocation = "/wsdl/copy.wsdl")
public class Copy
    extends Service
{

    private final static URL COPY_WSDL_LOCATION;
    private final static WebServiceException COPY_EXCEPTION;
    private final static QName COPY_QNAME = new QName("http://schemas.microsoft.com/sharepoint/soap/", "Copy");

    static {
        COPY_WSDL_LOCATION = com.microsoft.sharepoint.ws.copy.Copy.class.getResource("/wsdl/copy.wsdl");
        WebServiceException e = null;
        if (COPY_WSDL_LOCATION == null) {
            e = new WebServiceException("Cannot find '/wsdl/copy.wsdl' wsdl. Place the resource correctly in the classpath.");
        }
        COPY_EXCEPTION = e;
    }

    public Copy() {
        super(__getWsdlLocation(), COPY_QNAME);
    }

    public Copy(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    /**
     * 
     * @return
     *     returns CopySoap
     */
    @WebEndpoint(name = "CopySoap")
    public CopySoap getCopySoap() {
        return super.getPort(new QName("http://schemas.microsoft.com/sharepoint/soap/", "CopySoap"), CopySoap.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns CopySoap
     */
    @WebEndpoint(name = "CopySoap")
    public CopySoap getCopySoap(WebServiceFeature... features) {
        return super.getPort(new QName("http://schemas.microsoft.com/sharepoint/soap/", "CopySoap"), CopySoap.class, features);
    }

    private static URL __getWsdlLocation() {
        if (COPY_EXCEPTION!= null) {
            throw COPY_EXCEPTION;
        }
        return COPY_WSDL_LOCATION;
    }

}
