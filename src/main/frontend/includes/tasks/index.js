import TaskHtml from './TaskHtml';
import TaskSass from './TaskSass';
import TaskJs   from './TaskJs';
import TaskSvgSprite   from './TaskSvgSprite';
import TaskCopy   from './TaskCopy';

export {TaskHtml, TaskSass, TaskJs, TaskSvgSprite, TaskCopy};