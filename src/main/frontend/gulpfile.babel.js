import Collector from './includes/Collector';

var roots = {
	src:  'src',
	dest: '../resources',
};

var collector = new Collector({

	parts: [
		{
			name: 'project',
			tasks: [
				{
					type: 'copy',
					src: `${roots.src}/project/img/static/**/*.*`,
					dest: `${roots.dest}/static/img/static`,
					watch: `${roots.src}/project/img/static/*.*`,
				},
				{
					type: 'svgSprite',
					src: `${roots.src}/project/img/svg/**/*.*`,
					rawDest: `${roots.dest}/static/img/svg`,
					spriteDest: [
						`${roots.dest}/static/svg`,
						`${roots.src}/project/temp/svg`,
					],
					watch: `${roots.src}/project/img/svg/**/*.*`,
					templates: [`${roots.src}/project/config/svg-symbols.js`],
				},
				{
					type: 'copy',
					src: `${roots.src}/project/fonts/**/*.*`,
					dest: `${roots.dest}/static/fonts`,
					watch: `${roots.src}/project/fonts/**/*.*`,
				},
				{
					type: 'copy',
					src: `${roots.src}/project/vendor/**/*.*`,
					dest: `${roots.dest}/static/vendor`,
					watch: `${roots.src}/project/vendor/**/*.*`,
				},
				{
					type: 'sass',
					src: `${roots.src}/project/sass/main.scss`,
					dest: `${roots.dest}/static/css/`,
					watch: [
						`${roots.src}/project/sass/main.scss`,
						`${roots.src}/project/components/**/*.scss`,
						`${roots.src}/project/temp/sass/**/*.*`,
					],
				},
				{
					type: 'js',
					src: `${roots.src}/project/js/main.js`,
					dest: `${roots.dest}/static/js/`,
					watch: [
						`${roots.src}/project/js/**/*.*`,
						`${roots.src}/project/components/**/*.js`,
					],
				},
				{
					type: 'copy',
					src: `${roots.src}/project/js/hereDoc.js`,
					dest: `${roots.dest}/static/js/`,
					watch: `${roots.src}/project/js/hereDoc.js`,
				},
				{
					type: 'html',
					src: [
						`${roots.src}/project/html/**/*.html`,
						`!${roots.src}/project/html/map.html`,
						`!${roots.src}/project/html/manager-list.html`,
						`!${roots.src}/project/html/project-history.html`,
						`!${roots.src}/project/html/industries-finance.html`,
					],
					dest: `${roots.dest}/templates/`,
					watch: [
						`${roots.src}/project/html/**/*.*`,
						`${roots.src}/project/components/**/*.html`,
						`${roots.src}/project/temp/svg/svg-symbols.svg`,
					],
				},
				{
					type: 'html',
					src: [
						`${roots.src}/project/html/map.html`,
						`${roots.src}/project/html/manager-list.html`,
						`${roots.src}/project/html/project-history.html`,
						`${roots.src}/project/html/industries-finance.html`,
					],
					dest: `${roots.dest}/static/`,
					watch: [
						`${roots.src}/project/html/**/*.*`,
						`${roots.src}/project/components/**/*.html`,
						`${roots.src}/project/temp/svg/svg-symbols.svg`,
					],
				},
				{
					type: 'copy',
					src: `${roots.src}/project/root/**/*.*`,
					dest: `${roots.dest}/static/`,
					watch: `${roots.src}/project/root/**/*.*`,
				},
				{
                    type: 'copy',
                    src: `${roots.src}/project/js/cryptopro/**/*.*`,
                    dest: `${roots.dest}/static/js/cryptopro/`,
                    watch: `${roots.src}/project/js/cryptopro/**/*.*`,
                },
				{
					type: 'copy',
					src: [
						`node_modules/vue/dist/vue.min.js`,
						`node_modules/vee-validate/dist/vee-validate.min.js`
					],
					dest: `${roots.dest}/static/vendor`,
					watch: [
						`node_modules/vue/dist/vue.min.js`,
						`node_modules/vee-validate/dist/vee-validate.min.js`
					],
				},
			],
		},
	],

});

collector.run();
