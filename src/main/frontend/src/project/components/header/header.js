export default class
{
	constructor(el) {
		this.el = el;
		this.menu = el.querySelector('.header-menu');
		this.burgerLink = el.querySelector('.header-burger');
		this.burger = this.burgerLink.querySelector('.burger');
		this.opened = false;

		this.burgerLink.addEventListener('click', () => {this.toggleMenu()});
		this.burgerLink.addEventListener('touch', () => {this.toggleMenu()});
	}

	toggleMenu() {
		if (this.opened) {
			this.opened = false;
			this.burger.classList.remove('-opened');
			this.menu.classList.remove('-opened');
		} else {
			this.opened = true;
			this.burger.classList.add('-opened');
			this.menu.classList.add('-opened');
		}
	}
}