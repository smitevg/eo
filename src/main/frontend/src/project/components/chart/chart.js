/**
 * Форматирует число
 * @param v mixed - число
 * @param n number - число знаков после запятой
 * @param c string - сивол отделения десятичных
 * @param s string - символ отделения разрядов
 * @returns string
 */
function numberFormat(v, n, c, s) {
    var x;
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')';
    var num = ($.type(v) === "string") ? v.replace(/\s+/g, '') : v;
    num = parseFloat(num).toFixed(Math.max(0, ~~n));

		var result = (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
		if (result == "NaN") return "0";
    else return result;
}

export default class SibChart
{
	constructor(id, config)
	{
		let el = document.querySelector(`#${id}`);

		if (el) {
			el.chart = this;
			this.el = el;
			this.elId = id;
			this.vendorChart = null;
			this.legend = document.querySelector(`[data-chart=${this.elId}]`);
			this.config = jQuery.extend(true, config, {
				bindto: `#${this.elId}`,
				legendConfig: {},
			});
			if (!this.config.legendConfig.disableTwoWayHover) {
				this.config.data.onover = d => {
					if (this.legend) {
						const labels = [... this.legend.querySelectorAll('.-active')];
						labels.forEach(item => {item.classList.remove('-active')});
						const label = this.legend.querySelector(`[data-id=${d.id}]`);
						label.classList.add('-active');
					}
				};
				this.config.data.onout = d => {
					if (this.legend) {
						let labels = [... this.legend.querySelectorAll('.-active')];
						labels.forEach(item => {item.classList.remove('-active')});
					}
				}
			}

			this.configInitial = jQuery.extend(true, {}, this.config);
			this.configEmpty = jQuery.extend(true, {}, this.config);

			this.configEmpty.data.columns = this.configEmpty.data.columns.map((item) => {
				return new Array(item.length).fill(0).map((val, index) => {
					if (index == 0) {
						return item[0];
					} else {
						return val;
					}
				});
			});
			this.toggle(false);
			this.vendorChart = bb.generate(this.configInitial);
			this.toggleLegend(false);
			this.updateLegend();
			this.vendorChart.load(this.configEmpty.data);
			this.toggle(true);
		}
	}

	toggle(show = true) {
		if (this.el) {
			if (show) {
				this.el.style.visibility = 'visible';
			} else {
				this.el.style.visibility = 'hidden';
			}
		}
	}

	toggleLegend(show = true) {
		if (this.legend) {
			if (show) {
				this.legend.style.visibility = 'visible';
			} else {
				this.legend.style.visibility = 'hidden';
			}
		}
	}

	updateLegend() {
		if (this.legend) {
			let legend = d3.select(`.chart-legend[data-chart="${this.elId}"]`);
			legend
				.selectAll('div')
				.remove();
			legend
				.selectAll('div')
				.data(this.vendorChart.data().map((item) => {
					return item.id;
				}))
				.enter()
				.append('div')
				.attr('class', 'chart-legend-label')
				.attr('data-id', (id) => {return id})
				.html((id) => {
					return this.getLegendLabelHtml(id);
				})
				.on('mouseover', (id) => {
					this.vendorChart.focus(id);
				})
				.on('mouseout',(id) => {
					this.vendorChart.revert();
				})
				.on('touchstart', (id) => {
					$(this.legend).find(`[data-id="${id}"]`).addClass('-active');
					this.vendorChart.focus(id);
				})
				.on('touchend',(id) => {
					$(this.legend).find(`[data-id="${id}"]`).removeClass('-active');
					this.vendorChart.revert();
				});

		}
	}

	getLegendLabelHtml(id) {
		let data = this.vendorChart.data(id)[0];
		let title = id;
		if (this.config.data.names && this.config.data.names[id]) {
			title = this.config.data.names[id];
		}
		let result = `
			<span class="chart-legend-mark" style="background-color:${this.vendorChart.color(id)}"></span>
			<span class="chart-legend-text">${title}</span>
		`;

		if (!this.config.legendConfig.disableValue) {
			result += `
			<span class="chart-legend-value">${numberFormat(data.values[0].value, 0, '.', ' ')}</span>
		`;
		}

		return result;
		// TODO в value попадает инфа из конфига а не проценты
	};

	load(config = null) {
		if (config === null) {
			config = this.configInitial.data;
		}
		this.config.data.columns = config.columns;
		this.vendorChart.load(config);
		//this.updateLegend();
		this.toggleLegend(true);
		$(this.el).trigger('loaded');
	}

	unload() {
		let config = this.configEmpty.data;
		this.config.data.columns = config.columns;
		this.vendorChart.load(config);
		//this.updateLegend();
		this.toggleLegend(false);
	}
}

export function isObject(item) {
	return (item && typeof item === 'object' && !Array.isArray(item) && item !== null);
}

export function mergeDeep(target, source) {
	let output = Object.assign({}, target);
	if (isObject(target) && isObject(source)) {
		Object.keys(source).forEach(key => {
			if (isObject(source[key])) {
				if (!(key in target))
					Object.assign(output, { [key]: source[key] });
				else
					output[key] = mergeDeep(target[key], source[key]);
			} else {
				Object.assign(output, { [key]: source[key] });
			}
		});
	}
	return output;
}
