export default class SibChart
{
	constructor(el, config)
	{
		el.chart = this;

		this.el = el;
		this.config = mergeDeep(config, {
			options: {
				legend: {
					display: false,
				},
				legendCallback: this.getLegendBuilder(),
			},
		});
		this.chartPlugin = new Chart(this.el.querySelector('.chart-graph canvas'), this.config);
		this.el.querySelector('.chart-legend').innerHTML = this.chartPlugin.generateLegend();
	}

	getLegendBuilder(chart) {
		return (chart) => {
			let container = $('<div>').addClass('chart-legend');
			if (chart.data.datasets.length > 1) {
				for (let i = 0; i < chart.data.datasets.length; i++) {
					let item = $('<div>').addClass('chart-legend-item');
					let mark = $('<span>').addClass('chart-legend-mark').attr('style', `background-color: ${chart.data.datasets[i].backgroundColor}`);
					let label = $('<span>').addClass('chart-legend-label').text(chart.data.datasets[i].label);
					item.append(mark).append(label);
					container.append(item);
				}
			} else {
				for (let i = 0; i < chart.data.datasets[0].data.length; i++) {
					let item = $('<div>').addClass('chart-legend-item');
					let mark = $('<span>').addClass('chart-legend-mark').attr('style', `background-color: ${chart.data.datasets[0].backgroundColor[i]}`);
					let label = $('<span>').addClass('chart-legend-label').text(chart.data.labels[i]);
					item.append(mark).append(label);
					container.append(item);
				}
			}
			return container[0].outerHTML;
		}
	}
}

export function isObject(item) {
	return (item && typeof item === 'object' && !Array.isArray(item) && item !== null);
}

export function mergeDeep(target, source) {
	let output = Object.assign({}, target);
	if (isObject(target) && isObject(source)) {
		Object.keys(source).forEach(key => {
			if (isObject(source[key])) {
				if (!(key in target))
					Object.assign(output, { [key]: source[key] });
				else
					output[key] = mergeDeep(target[key], source[key]);
			} else {
				Object.assign(output, { [key]: source[key] });
			}
		});
	}
	return output;
}