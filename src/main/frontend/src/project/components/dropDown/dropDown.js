export default class
{
	constructor(el) {
		this.config = {
			gutter: 10, // TODO
		};
		this.el = el;
		this.init();
	}

	init() {
		this.clickOutside = this.el.getAttribute('data-dropDown-clickOutside') !== null;
		this.hover = this.el.getAttribute('data-dropDown-hover') !== null;
		this.outcast = this.el.getAttribute('data-dropDown-outcast') !== null;

		this.link = this.el.querySelector('.dropDown-link');
		this.content = this.el.querySelector('.dropDown-content');
		this.contentInner = this.content.querySelector('.dropDown-content-inner');

		if (this.outcast) {
			this.content.style.display = 'none';
			this.content.style.position = 'fixed';
			document.body.appendChild(this.content);
		}

		if (this.hover) {
			this.link.addEventListener('mouseenter', this.open.bind(this));
			this.link.addEventListener('mouseleave', this.close.bind(this));
		} else {
			this.link.addEventListener('click', this.toggle.bind(this));
		}
		this.link.addEventListener('touch', this.toggle.bind(this));

		if (!this.clickOutside) {
			document.addEventListener('click', function(event) {
				if (event.target.closest('.dropDown') != this.el) {
					this.close();
				}
			}.bind(this));
		}
	}

	toggle(event) {
		if (this.el.classList.contains('-open')) {
			this.close(event);
		} else {
			this.open(event);
		}
	}

	open(event) {
		if (event) {
			event.preventDefault();
		}
		if (this.outcast) {
			this.content.style.display = null;
		}
		this.el.classList.add('-open');
		this.position();
	}

	close(event) {
		if (event) {
			event.preventDefault();
		}
		if (this.outcast) {
			this.content.style.display = 'none';
		}
		this.el.classList.remove('-open');
	};

	position() {
		if (this.outcast) {
			let linkRect = this.link.getBoundingClientRect();
			let contentRect = this.content.getBoundingClientRect();
			let top = 0;
			let left = 0;

			top = linkRect.top + linkRect.height;
			if (this.el.classList.contains('_center')) {
				left = linkRect.left + linkRect.width/2 - contentRect.width/2;
			} else if (this.el.classList.contains('_right')) {
				left = linkRect.left + linkRect.width - contentRect.width;
			} else {
				left = linkRect.left;
			}

			this.content.style.top = `${top}px`;
			this.content.style.left = `${left}px`;
		}

		if (this.contentInner) {
			let rect = this.content.getBoundingClientRect();
			let width = rect.width;
			let height = rect.height;
			let deltaX = 0;
			let deltaY = 0;

			if (width > window.innerWidth) {
				this.contentInner.style.width = `${window.innerWidth}px`;
			}
			if (height > window.innerHeight) {
				this.contentInner.style.height = `${window.innerHeight}px`;
			}

			rect = this.content.getBoundingClientRect();

			if (rect.left < 0) {
				deltaX = Math.abs(rect.left);
			} else if (rect.left + rect.width > window.innerWidth) {
				deltaX = window.innerWidth - (rect.left + rect.width);
			}
			if (rect.top < 0) {
				deltaY = Math.abs(rect.top);
			} else if (rect.top + rect.height > window.innerHeight) {
				deltaY = window.innerHeight - (rect.top + rect.height);
			}
			this.contentInner.style.transform = `translate(${deltaX}px, ${deltaY}px)`;
		}
	}
}