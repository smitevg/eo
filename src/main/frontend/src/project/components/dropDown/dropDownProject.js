import DropDown from './dropDown';
import SibChart from '../chart/chart';

export default class extends DropDown
{
	constructor(el)
	{
		super(el);
		this.stepsSliderEl = this.el.querySelector('.dropDownProject-grid.owlSlider');

		$(this.stepsSliderEl).addClass('owl-carousel');
		this.stepsSlider = $(this.stepsSliderEl).owlCarousel({
			controls: false,
			nav: true,
			items: 1,
			navText: ['',''],
			responsive: {
				0: {
					items: 1,
				},
				544: {
					items: 3,
				},
				768: {
					items: 3,
				},
				992: {
					items: 5,
				},
				1200: {
					items: 5,
				},
				1320: {
					items: 7,
				},
			},
		});
	}

	init() {
		super.init();
		this.charts = [];
		let charts = [...this.el.querySelectorAll('.chart')];
		let projectID = this.el.getAttribute('data-project-id');
		charts.forEach((chartEl) => {
			let graph = chartEl.querySelector('.chart-graph');
			let id = null;
			if (graph) {
				id = graph.getAttribute('id');
			}

			let grapthFirstID = id.split("__")[0];
            let diagramParams = window.officeFinanceRequest.diagramParams[projectID];

            if (id && diagramParams && diagramParams[grapthFirstID]) {

				let chart = new SibChart(id, diagramParams[grapthFirstID], grapthFirstID);
				this.charts.push(chart);
				chart.load();
			}
		});
	}

	open(event) {
		super.open(event);
		this.charts.forEach((chart) => {
			chart.vendorChart.flush();
		});
	}
}
