export default class
{
	constructor(el) {
		this.el = el;
		this.menu = el.querySelector('.office-sideMenu');
		this.burgerLink = [...el.querySelectorAll('.office-burger')];
		this.opened = false;

		this.burgerLink.forEach((el) => {
			el.addEventListener('click', this.toggleMenu.bind(this));
			el.addEventListener('touch', this.toggleMenu.bind(this));
		});
	}

	toggleMenu(event) {
		if (event) {
			event.preventDefault();
		}
		if (this.opened) {
			this.opened = false;
			this.menu.classList.remove('-opened');
			this.burgerLink.forEach((el) => {
				el.querySelector('.burger').classList.remove('-opened');
			});
		} else {
			this.opened = true;
			this.menu.classList.add('-opened');
			this.burgerLink.forEach((el) => {
				el.querySelector('.burger').classList.add('-opened');
			});
		}
	}
}