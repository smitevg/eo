import Vue from 'vue/dist/vue';

if (document.getElementById('mainTabs')) {
	new Vue({
		el: '#mainTabs',
		data: {
			currentIndex: 0,
		},
		computed: {},
		filters: {},
		methods: {
			setCurrentIndex: function (index) {
				this.currentIndex = index;
			},
		},
	});
}

document.addEventListener('DOMContentLoaded', function() {

	let sliderEl = $('#mainTabs .mainTabs-tabGrid');
	let slider = null;

	if (sliderEl.length) {
		function toggleSlider() {
			if (window.innerWidth > 540) {
				if (slider) {
					sliderEl.removeClass('owl-carousel');
					slider.trigger('destroy.owl.carousel');
				}
			} else {
				sliderEl.addClass('owl-carousel');
				slider = $('#mainTabs .mainTabs-tabGrid').owlCarousel({
					controls: false,
					nav: true,
					items: 1,
					navText: ['',''],
				});
				slider.on('changed.owl.carousel', function(event) {
					sliderEl.find('.mainTabs-link').eq(event.page.index)[0].click();
				})
			}
		}

		$(window).on('resize', function(e) {
			if (this.resizeTO) {
				clearTimeout(this.resizeTO);
			}
			this.resizeTO = setTimeout(function() {
				$(this).trigger('resizeEnd');
			}, 500); // 500 - время задержки в ms
		});

		$(window).on('resizeEnd', function(e) {
			toggleSlider();
		});

		toggleSlider();
	}

});