(function(){
	function hereDoc(f) {
		return f.toString().
		replace(/^[^\/]+\/\*!?/, '').
		replace(/\*\/[^\/]+$/, '');
	}
	let svgSymbols = hereDoc(function(){/*
	 <svg xmlns="http://www.w3.org/2000/svg" style="width:0; height:0; display:none;"><% if(defs) {%>
	 <defs>
	 <%= defs %>
	 </defs><% } %><% _.forEach( icons, function( icon ){ %>
	 <symbol id="<%= icon.id %>" viewBox="<%= icon.svg.viewBox %>"><% if (icon.title) {%>
	 <title><%= icon.title %></title><% }%>
	 <%= icon.svg.content %>
	 </symbol><%
	 }); %></svg>
	 */});
	let svg = document.createElement('div');
	svg.innerHTML = svgSymbols;
	document.body.appendChild(svg);
})();