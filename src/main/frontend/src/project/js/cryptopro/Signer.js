﻿var isPluginEnabled = false;

//Переменнные для async-версии заводятся ЗДЕСЬ
var async_code_included = 0;
var async_Promise;
var async_resolve;

var CADESCOM_CADES_X_LONG_TYPE_1 = 0x5d,
    CAPICOM_CURRENT_USER_STORE = 2,
    CAPICOM_MY_STORE = "My",
    CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED = 2,
    CAPICOM_CERTIFICATE_FIND_SUBJECT_NAME = 1,
    CADES_BES = 1;

window.NPAPISigner = {
    GetCertificate: function (thumbprint) {
        var promise = new Promise(function (resolve, reject) {
            try {
                var oStore = cadesplugin.CreateObject("CAdESCOM.Store");
                oStore.Open();
            } catch (err) {
                alert('Не удалось создать CAdESCOM.Store: ' + cadesplugin.getLastError(err));
                reject && reject();
            }

            var CAPICOM_CERTIFICATE_FIND_SHA1_HASH = 0;
            var oCerts = oStore.Certificates.Find(CAPICOM_CERTIFICATE_FIND_SHA1_HASH, thumbprint);

            if (oCerts.Count == 0) {
                alert("Сертификат не найден");
                reject && reject();
            }
            var oCert = oCerts.Item(1);
            resolve(oCert);
        });
        return promise;
    },

    CheckForPlugIn: function () {
        var data = {
            plugin_state: false,
            plugin_state_text: "",
            csp_name: "",
            csp_version: "",
            plugin_version: ""
        };

        function GetCSPVersion_NPAPI() {
            oAbout = oAbout || cadesplugin.CreateObject("CAdESCOM.About");
            var ver = oAbout.CSPVersion("", 75);
            return ver.MajorVersion + "." + ver.MinorVersion + "." + ver.BuildVersion;
        }

        function GetCSPName_NPAPI() {
            oAbout = oAbout || cadesplugin.CreateObject("CAdESCOM.About");
            var sCSPName = oAbout.CSPName(75);
            return sCSPName;
        }

        var isPluginLoaded = false;
        var isPluginWorked = false;
        var isActualVersion = false;

        try {
            var oAbout = cadesplugin.CreateObject("CAdESCOM.About");
            isPluginLoaded = true;
            isPluginEnabled = true;
            isPluginWorked = true;
            // Это значение будет проверяться сервером при загрузке демо-страницы
            var CurrentPluginVersion;
            CurrentPluginVersion = oAbout.PluginVersion;
            if (typeof (CurrentPluginVersion) == "undefined")
                CurrentPluginVersion = oAbout.Version;
            data.plugin_version = "Версия плагина: " + CurrentPluginVersion;
        }
        catch (err) {
            // Объект создать не удалось, проверим, установлен ли 
            // вообще плагин. Такая возможность есть не во всех браузерах
            var mimetype = navigator.mimeTypes["application/x-cades"];
            if (mimetype) {
                isPluginLoaded = true;
                var plugin = mimetype.enabledPlugin;
                if (plugin) {
                    isPluginEnabled = true;
                }
            }
        }

        if (isPluginWorked) {
            data.plugin_state = true;
            data.plugin_state_text = "Плагин загружен.";
            if (typeof (CurrentPluginVersion) != "string") {
                data.csp_version = "Версия криптопровайдера: " + GetCSPVersion_NPAPI();
            }
            var sCSPName = GetCSPName_NPAPI();
            if (sCSPName != "") {
                data.csp_name = "Криптопровайдер: " + sCSPName;
            }
        }
        else { // плагин не работает, объекты не создаются
            if (isPluginLoaded) { // плагин загружен
                if (!isPluginEnabled) { // плагин загружен, но отключен
                    data.plugin_state = false;
                    data.plugin_state_text = "Плагин загружен, но отключен в настройках браузера.";
                }
                else { // плагин загружен и включен, но объекты не создаются
                    data.plugin_state = false;
                    data.plugin_state_text = "Плагин загружен, но не удается создать объекты. Проверьте настройки браузера.";
                }
            }
            else { // плагин не загружен
                data.plugin_state = false;
                data.plugin_state_text = "Плагин не загружен.";
            }
        }
        app.G.plugin_data = data;
        return data;
    },

    FillCertList: function (lstId) {
        try {
            var oStore = cadesplugin.CreateObject("CAdESCOM.Store");
            oStore.Open();
        }
        catch (ex) {
            alert("Ошибка при открытии хранилища: " + cadesplugin.getLastError(ex));
            return;
        }

        var lst = document.getElementById(lstId);
        if (!lst) {
            return;
        }

        var certCnt;
        try {
            certCnt = oStore.Certificates.Count;
            if (certCnt == 0)
                throw "Не работает Certificates.Count (0x80092004)";
        }
        catch (ex) {
            oStore.Close();
            return;
        }

        for (var i = 1; i <= certCnt; i++) {
            var cert;
            try {
                cert = oStore.Certificates.Item(i);
            }
            catch (ex) {
                alert("Ошибка при перечислении сертификатов: " + cadesplugin.getLastError(ex));
                return;
            }

            var oOpt = document.createElement("OPTION");
            var dateObj = new Date();
            try {
                if (dateObj < cert.ValidToDate && cert.HasPrivateKey() && cert.IsValid().Result) {
                    var certObj = new CertificateObj(cert);
                    oOpt.text = certObj.GetCertString();
                }
                else {
                    continue;
                }
            }
            catch (ex) {
                alert("Ошибка при получении свойства SubjectName: " + cadesplugin.getLastError(ex));
            }
            try {
                oOpt.value = cert.Thumbprint;
            }
            catch (ex) {
                alert("Ошибка при получении свойства Thumbprint: " + cadesplugin.getLastError(ex));
            }

            lst.options.add(oOpt);
        }
        if (lst.options.length > 0) {
            lst.selectedIndex = 0;
        }
        oStore.Close();
    },

    SignCadesBES: function (dataToSign, certObject, includeWholeChain, setDisplayData) {
        var promise = new Promise(function (resolve, reject) {
            var errormes = "", signature;

            if (!dataToSign) {
                alert("Подпись не создана, нет данных для подписания");
                resolve(null);
            }

            try {
                var oSigner = cadesplugin.CreateObject("CAdESCOM.CPSigner");
            } catch (err) {
                alert("Не удалось создать CAdESCOM.CPSigner: " + err.number);
                resolve(null);
            }

            if (oSigner) {
                oSigner.Certificate = certObject;
            }
            else {
                alert("Не удалось создать CAdESCOM.CPSigner");
                resolve(null);
            }

            try {
                var oSignedData = cadesplugin.CreateObject("CAdESCOM.CadesSignedData");
            } catch (err) {
                alert('Не удалось создать CAdESCOM.CadesSignedData: ' + err.number);
                resolve(null);
            }

            oSignedData.ContentEncoding = 1; //CADESCOM_BASE64_TO_BINARY

            if (typeof (setDisplayData) != 'undefined') { //Set display data flag flag for devices like Rutoken PinPad
                oSignedData.DisplayData = 1;
            }

            if (includeWholeChain) {
                oSigner.Options = 1; //CAPICOM_CERTIFICATE_INCLUDE_WHOLE_CHAIN
            }

            oSignedData.Content = dataToSign;

            try {
                signature = oSignedData.SignCades(oSigner, 1, true); //cadesplugin.CADESCOM_CADES_BES
            }
            catch (err) {
                alert("Не удалось создать подпись из-за ошибки: " + GetErrorMessage(err));
                resolve(null);
            }

            resolve(signature);
        });

        return promise;
    },

    SignCadesXLong: function (dataToSign, certObj, includeWholeChain, tspService) {
        var promise = new Promise(function (resolve, reject) {
            var errormes = "", signature;

            if (!dataToSign) {
                alert("Подпись не создана, нет данных для подписания");
                resolve(null);
            }

            try {
                var oSigner = cadesplugin.CreateObject("CAdESCOM.CPSigner");
            } catch (err) {
                alert("Не удалось создать CAdESCOM.CPSigner: " + err.number);
                resolve(null);
            }

            if (oSigner) {
                oSigner.Certificate = certObject;
            }
            else {
                alert("Не удалось создать CAdESCOM.CPSigner");
                resolve(null);
            }

            try {
                var oSignedData = cadesplugin.CreateObject("CAdESCOM.CadesSignedData");
            } catch (err) {
                alert('Не удалось создать CAdESCOM.CadesSignedData: ' + err.number);
                resolve(null);
            }

            oSigner.Options = 1; //CAPICOM_CERTIFICATE_INCLUDE_WHOLE_CHAIN

            oSigner.TSAAddress = tspService;

            oSignedData.Content = dataToSign;
            oSignedData.ContentEncoding = 1;

            var CADESCOM_CADES_X_LONG_TYPE_1 = 0x5d;
            try {
                signature = oSignedData.SignCades(oSigner, CADESCOM_CADES_X_LONG_TYPE_1);

            }
            catch (err) {
                errormes = "Не удалось создать подпись из-за ошибки: " + cadesplugin.getLastError(err);
                alert(errormes);
                resolve(null);
            }

            resolve(signature);
        });

        return promise;
    }
};

//----------------------- API ---------------------------------------
function includeAsync() {
    if (async_code_included) {
        return async_Promise;
    }
    var fileref = document.createElement('script');
    fileref.setAttribute("type", "text/javascript");
    fileref.setAttribute("src", "asyncSigner.js?ts=" + (new Date()).getTime());
    document.getElementsByTagName("head")[0].appendChild(fileref);
    async_Promise = new Promise(function (resolve, reject) {
        async_resolve = resolve;
    });
    async_code_included = 1;
    return async_Promise;
}

function common(funcName, args) {
    var canAsync = !!cadesplugin.CreateObjectAsync;
    if (canAsync) {
        includeAsync().then(function () {
            return window.asyncSigner[funcName].apply(null, args);
        });
    } else {
        return window.NPAPISigner[funcName].apply(null, args);
    }
}

function common_promise(func, args) {
    var promise = new Promise(function (resolve, reject) {
        var canAsync = !!cadesplugin.CreateObjectAsync;
        if (canAsync) {
            includeAsync().then(function () {
                window.asyncSigner[func].apply(null, args).then(function (result) {
                    resolve(result);
                });
            });
        }
        else {
            window.NPAPISigner[func].apply(null, args).then(function (result) {
                resolve(result);
            });
        }
    });
    return promise;
}

//-----------------------------Signer---------------------------------------
function Signer() { }

Signer.prototype.checkPlugin = function () {
    return common("CheckForPlugIn", []);
};

Signer.prototype.fillCertList = function Common_FillCertList(certListBoxId) {
    return common("FillCertList", [certListBoxId]);
};

Signer.prototype.getCertificate = function Common_GetCertificate(certListBoxId) {
    var e = document.getElementById(certListBoxId);
    var selectedCertID = e.selectedIndex;
    if (selectedCertID == -1) {
        alert("Сертификат не выбран");
        reject && reject();
    }

    var thumbprint = e.options[selectedCertID].value.split(" ").reverse().join("").replace(/\s/g, "").toUpperCase();
    return common_promise("GetCertificate", [thumbprint]);
};

Signer.prototype.signCadesBES = function Common_SignCadesBES(dataToSign, certObj, includeWholeChain, setDisplayData) {
    return common_promise("SignCadesBES", [dataToSign, certObj, includeWholeChain, setDisplayData]);
};

Signer.prototype.signCadesXLong = function Common_SignCadesXLong(dataToSign, certObj, includeWholeChain, tspService) {
    return common_promise("SignCadesXLong", [dataToSign, certObj, includeWholeChain, tspService]);
};

Signer.prototype.verifyBES = function Common_VerifyBES(sSignature, dataToVerify) {
    return true;
}

Signer.prototype.sign = function (signatureType, tspService) {
    if (signatureType == "bes")
        return this.signCadesBES(this.data, this.certificate, app.G.INCLUDE_WHOLE_CHAIN);
    if (signatureType == "xlong")
        return this.signCadesXLong(this.data, this.certificate, app.G.INCLUDE_WHOLE_CHAIN, tspService);
};