window.asyncSigner = {
    GetCertificate: function (thumbprint) {
        var promise = new Promise(function (resolve, reject) {
            cadesplugin.async_spawn(function* () {
                try {
                    var oStore = yield cadesplugin.CreateObjectAsync("CAdESCOM.Store");
                    yield oStore.Open();
                } catch (err) {
                    alert('Ошибка при создании CAdESCOM.Store: ' + err.number);
                    reject && reject();
                }

                var all_certs = yield oStore.Certificates;
                var oCerts = yield all_certs.Find(cadesplugin.CAPICOM_CERTIFICATE_FIND_SHA1_HASH, thumbprint);

                if ((oCerts.Count) == 0) {
                    alert("Сертификат не найден");
                    reject && reject();
                }
                var certificate = yield oCerts.Item(1);
                resolve(certificate);
            });
        });

        return promise;
    },

    CheckForPlugIn: function () {
        var CurrentPluginVersion;
        cadesplugin.async_spawn(function* () {
            try {
                var oAbout = yield cadesplugin.CreateObjectAsync("CAdESCOM.About");
                CurrentPluginVersion = yield oAbout.PluginVersion;
            }
            catch (err) {
                app.G.plugin_data.plugin_state = false;
                app.G.plugin_data.plugin_state_text = "Плагин не загружен.";
            }

            app.G.plugin_data.plugin_state_text = "Плагин загружен.";
            app.G.plugin_data.plugin_state = true;

            var isActualVersion = true;

            app.G.plugin_data.plugin_version = "Версия плагина: " + (yield CurrentPluginVersion.toString());
            var oAbout = yield cadesplugin.CreateObjectAsync("CAdESCOM.About");
            var ver = yield oAbout.CSPVersion("", 75);
            var ret = (yield ver.MajorVersion) + "." + (yield ver.MinorVersion) + "." + (yield ver.BuildVersion);
            app.G.plugin_data.csp_version = "Версия криптопровайдера: " + ret;

            try {
                var sCSPName = yield oAbout.CSPName(75);
                app.G.plugin_data.csp_name = "Криптопровайдер: " + sCSPName;
            }
            catch (err) { }
            yield true;
        });
    },


    FillCertList: function (lstId) {
        cadesplugin.async_spawn(function* () {
            try {
                var oStore = yield cadesplugin.CreateObjectAsync("CAdESCOM.Store");
                if (!oStore) {
                    alert("Ошибка при создании Store");
                    return;
                }

                yield oStore.Open();
            }
            catch (ex) {
                alert("Ошибка при открытии хранилища: " + cadesplugin.getLastError(ex));
                return;
            }

            var lst = document.getElementById(lstId);
            if (!lst) {
                return;
            }
            var certCnt;
            var certs;

            try {
                certs = yield oStore.Certificates;
                certCnt = yield certs.Count;
            }
            catch (ex) {
                var errormes = document.getElementById("boxdiv").style.display = '';
                return;
            }

            if (certCnt == 0) {
                var errormes = document.getElementById("boxdiv").style.display = '';
                return;
            }

            for (var i = 1; i <= certCnt; i++) {
                var cert;
                try {
                    cert = yield certs.Item(i);
                }
                catch (ex) {
                    alert("Ошибка при перечислении сертификатов: " + cadesplugin.getLastError(ex));
                    return;
                }

                var oOpt = document.createElement("OPTION");
                var dateObj = new Date();
                try {
                    var ValidToDate = new Date((yield cert.ValidToDate));
                    var ValidFromDate = new Date((yield cert.ValidFromDate));
                    var Validator = yield cert.IsValid();
                    var IsValid = yield Validator.Result;
                    if (dateObj < ValidToDate && (yield cert.HasPrivateKey()) && IsValid) {
                        oOpt.text = new CertificateAdjuster().GetCertInfoString(yield cert.SubjectName, ValidFromDate);
                    }
                    else {
                        continue;
                    }
                }
                catch (ex) {
                    alert("Ошибка при получении свойства SubjectName: " + cadesplugin.getLastError(ex));
                }
                try {
                    oOpt.value = yield cert.Thumbprint;
                }
                catch (ex) {
                    alert("Ошибка при получении свойства Thumbprint: " + cadesplugin.getLastError(ex));
                }

                lst.options.add(oOpt);
            }
            if (lst.options.length > 0) {
                lst.selectedIndex = 0;
            }
            yield oStore.Close();
        });
    },

    SignCadesBES: function (dataToSign, certObject, includeWholeChain, setDisplayData) {
        var promise = new Promise(function (resolve, reject) {
            cadesplugin.async_spawn(function* () {
                try {
                    var errormes = "";
                    try {
                        var oSigner = yield cadesplugin.CreateObjectAsync("CAdESCOM.CPSigner");
                    } catch (err) {
                        alert("Failed to create CAdESCOM.CPSigner: " + err.number);
                        resolve(null);
                    }

                    if (oSigner) {
                        yield oSigner.propset_Certificate(certObject);
                    }
                    else {
                        errormes = "Не удалось создать CAdESCOM.CPSigner";
                        alert(errormes);
                        resolve(null);
                    }

                    var oSignedData = yield cadesplugin.CreateObjectAsync("CAdESCOM.CadesSignedData");
                    var signature = null;

                    if (dataToSign) {
                        yield oSignedData.propset_ContentEncoding(1); //cadesplugin.CADESCOM_BASE64_TO_BINARY

                        if (includeWholeChain) {
                            yield oSigner.propset_Options(1); //cadesplugin.CAPICOM_CERTIFICATE_INCLUDE_WHOLE_CHAIN
                        }

                        if (typeof (setDisplayData) != 'undefined') {
                            //Set display data flag flag for devices like Rutoken PinPad
                            yield oSignedData.propset_DisplayData(1);
                        }

                        yield oSignedData.propset_Content(dataToSign);

                        try {
                            signature = yield oSignedData.SignCades(oSigner, 1, true); //cadesplugin.CADESCOM_CADES_BES
                        }
                        catch (err) {
                            errormes = "Не удалось создать подпись из-за ошибки: " + cadesplugin.getLastError(err);
                            alert(errormes);
                            resolve(null);
                        }
                    }
                    resolve(signature);
                }
                catch (err) {
                    resolve(null);
                }
            });
        });
        return promise;
    },

    SignCadesXLong: function (dataToSign, certObj, includeWholeChain, tspService) {
        var promise = new Promise(function (resolve, reject) {
            cadesplugin.async_spawn(function* () {
                try {
                    var errormes = "";
                    try {
                        var oSigner = yield cadesplugin.CreateObjectAsync("CAdESCOM.CPSigner");
                    } catch (err) {
                        alert("Не удалось создать CAdESCOM.CPSigner: " + err.number);
                        resolve(null);
                    }

                    if (oSigner) {
                        yield oSigner.propset_Certificate(certObj);
                    }
                    else {
                        errormes = "Не удалось создать CAdESCOM.CPSigner";
                        alert(errormes);
                        resolve(null);
                    }

                    var oSignedData = yield cadesplugin.CreateObjectAsync("CAdESCOM.CadesSignedData");
                    var signature = null;

                    if (dataToSign) {
                        yield oSignedData.propset_ContentEncoding(1); //cadesplugin.CADESCOM_BASE64_TO_BINARY

                        yield oSigner.propset_Options(1); //cadesplugin.CAPICOM_CERTIFICATE_INCLUDE_WHOLE_CHAIN

                        yield oSigner.propset_TSAAddress(tspService);

                        yield oSignedData.propset_Content(dataToSign);

                        var CADESCOM_CADES_X_LONG_TYPE_1 = 0x5d;

                        try {
                            signature = yield oSignedData.SignCades(oSigner, CADESCOM_CADES_X_LONG_TYPE_1);
                        }
                        catch (err) {
                            errormes = "Не удалось создать подпись из-за ошибки: " + cadesplugin.getLastError(err);
                            alert(errormes);
                            resolve(null);
                        }
                    }
                    resolve(signature);
                }
                catch (err) {
                    resolve(null);
                }
            });
        });

        return promise;
    }
};

async_resolve();