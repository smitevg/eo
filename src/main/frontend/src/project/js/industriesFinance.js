import Vue from 'vue/dist/vue';

if (document.getElementById('industriesFinance')) {

	Vue.component('industries-finance-card', {
		template: '#industriesFinance-card',
		data: function () {
			return {
				currentItem: null,
			};
		},
		props: {
			item: {required: true},
			depth: {required: true},
		},
		watch: {
			item: function() {
				this.currentItem = null;
			},
		},
		methods: {
			setCurrentItem: function(item) {
				this.currentItem = item;
			},
			emitBack: function() {
				this.$emit('back');
			},
			back: function() {
				this.currentItem = null;
			},
		},
	});

	new Vue({
		el: '#industriesFinance',
		data: {
			currentItem: null,
			items: [
				{
					title: 'Промышленность высоких переделов',
					icon: '#icon-factory',
					image: '',
					items: [
						{ title: 'Микроэлектроника' },
						{ title: 'Радиоэлектроника' },
						{ title: 'Приборостроение' },
						{ title: 'Энергетическое машиностроение' },
						{ title: 'Станкостроение' },
						{ title: 'Электротехническое оборудование' },
						{ title: 'Новые материалы' },
						{ title: 'Инфокомм технологии' },
						{ title: 'Редкоземельные металлы' },
						{ title: 'Химия полимеров' },
						{ title: 'Авиационная промышленность' },
						{ title: 'Космическая промышленность' },
						{ title: 'Двигателестроение' },
						{ title: 'Медицинская техника' },
						{ title: 'Фармацевтика' },
						{ title: 'Внедрение технологий' },
						{ title: 'Информационно-коммуникационные технологии, включая аппаратное и программное обеспечение' },
					],
				},
				{
					title: 'Инфраструктура',
					icon: '#icon-bridge',
					image: './img/static/industries/2/img-01.jpg',
					items: [
						{
							title: 'Транспортная',
							image: './img/static/industries/2/1/img-01.jpg',
							items: [
								{
									title: 'Железнодорожная инфраструктура',
                                    image: './img/static/industries/2/1/2/img-01.jpg',
									items: [
										{ title: 'Подъездная инфраструктура' },
										{ title: 'Высокоскоростные магистрали' },
										{ title: 'Транспортные коридоры' },
										{ title: 'Ж/Д терминалы' },
									],
								},
								{
									title: 'Дороги и мосты',
                                    image: './img/static/industries/2/1/1/img-01.jpg',
									items: [
										{ title: 'Платные автодороги' },
										{ title: 'Автомобильная инфраструктура, включая переезды через ж/д' },
									],
								},
								{
									title: 'Портовая инфраструктура',
                                    image: './img/static/industries/2/1/4/img-01.jpg',
									items: [
										{ title: 'Причальные стенки' },
										{ title: 'Терминальная инфраструктура' },
									],
								},
								{
									title: 'Инфраструктура аэропортов',
                                    image: './img/static/industries/2/1/3/img-01.jpg',
									items: [
										{ title: 'Модернизация крупных международных узловых аэропортов, включая строительство и реконструкцию терминалов (без ВПП)' },
									],
								},
							],
						},
						{
							title: 'Высокотехнологичная инфраструктура здравоохранения',
                            image: './img/static/industries/2/3/img-01.jpg',
							items: [
								{ title: 'Высокотехнологичные медицинские центры, медицинские технопарки, диагностические и реабилитационные центры' },
							],
						},
						{
							title: 'Энергетическая',
                            image: './img/static/industries/2/2/img-01.jpg',
							items: [
								{
									title: 'Генерация и распределение электроэнергии',
                                    image: './img/static/industries/2/2/1/img-01.jpg',
									items: [
										{ title: 'Строительство и обновление отраслей' },
									],
								},
							],
						},
						{
							title: 'Инфраструктура для промышленных площадок',
                            image: './img/static/industries/3/img-01.jpg',
							items: [
								{ title: 'Первая фаза развития территорий с возможным последующим рефинансированием коммерческими банками' }
							],
						},
					],
				},
				{
					title: 'Поддержка экспорта',
					icon: '#icon-export',
                    image: './img/static/industries/4/img-01.jpg',
					items: [
						{ title: 'Поставка высокотехнологичной продукции за рубеж' },
					],
				},
				{
					title: 'Конверсия ОПК',
					icon: '#icon-military',
					image: './img/static/img-02.jpg',
					items: [
						{ title: 'Новое производство, обновление, экспорт' },
					],
				},
				{
					title: 'Высокотехнологичная сфера',
					icon: '#icon-chip',
					image: './img/static/industries/1/img-01.jpg',
					items: [
						{ title: 'Блокчейн' },
						{ title: 'Инфраструктура для новых технологий' },
						{ title: 'Частный капитал и космические технологии' },
						{ title: 'НБИКС-Технологии' },
						{ title: 'MARKETPLACE B2B' },
					],
				},
			],
		},
		computed: {},
		filters: {},
		methods: {
			setCurrentItem: function(item) {
				this.currentItem = item;
			}
		},
		mounted: function() {
			this.currentItem = this.items[0];
		},
	});
}

document.addEventListener('DOMContentLoaded', function() {

});
