import 'babel-polyfill';
import Header from '../components/header/header';
import Office from '../components/office/office';
import DropDown from '../components/dropDown/dropDown';
import DropDownProject from '../components/dropDown/dropDownProject';
import Vue from 'vue/dist/vue';
import VueResource from 'vue-resource';
import VeeMessages from 'vee-validate/dist/locale/ru';
import VeeValidate, { Validator } from 'vee-validate';
import VueMask from 'v-mask';
import VueMoney from 'v-money';
import VueAutoSize from 'vue-autosize';
import autosize from 'autosize';
import './map';
import '../components/mainTabs/mainTabs';
import './industriesFinance';
import _ from 'lodash';
// import './managers';
//import FileUpload from 'vue-upload-component';

function getCurrency(code) {
    if (!code) {
        return null;
    }
    switch(code.toString().toUpperCase()) {
        case 'RUB': return '&#8381;';
        case 'USD': return '&#36;';
        case 'EUR': return '&#8364;';
        case 'AUD': return '&#36;';
        case 'BYN': return 'Br';
        case 'GBP': return '&#163;';
        case 'CNY': return '&#165; or元';
        case 'JPY': return '&#165;';
    }
    return null;
}

document.addEventListener('DOMContentLoaded', function() {
    let header = document.querySelector('.header');
    if (header) {
        new Header(header);
    }
    let office = document.querySelector('.office');
    if (office) {
        new Office(office);
    }
    let dropDown = [...document.querySelectorAll('.dropDown:not(.dropDownProject)')];
    if (dropDown.length) {
        dropDown.forEach((el) => {
            new DropDown(el);
        });
    }
    let dropDownProject = [...document.querySelectorAll('.dropDownProject')];
    if (dropDownProject.length) {
        dropDownProject.forEach((el) => {
            new DropDownProject(el);
        });
    }
    $('[data-mask="phone"]').mask('+7 (000) 000-00-00');
    $('[data-mask="index"]').mask('000000');
    $('.form-inputDate:not([data-inputDate-range]) input').each((i, el) => {
        $(el).dateRangePicker({
            format: 'DD-MM-YYYY',
            singleDate: true,
            singleMonth: true,
            autoClose: true,
        });
    });
    $('.form-inputDate[data-inputDate-range] input').each((i, el) => {
        $(el).dateRangePicker({
            format: 'DD-MM-YYYY',
        });
    });
    $('.storiesSlider').owlCarousel({
        controls: true,
        nav: false,
        items: 1,
        navText: ['',''],
    });
    autosize($('[autosize]'));
});

Vue.config.devtools = true;

Vue.use(VeeValidate, {
    locale: 'ru',
	errorBagName: 'errors',
	events: 'blur',
	dictionary: {
    	ru: VeeMessages,
	}
});

var validateInnFn = function (value) {
    if (value.match(/\D/)) return false;
    var inn = value.match(/(\d)/g);
    if (inn.length == 10) {
        return inn[9] == String(((
            2 * inn[0] + 4 * inn[1] + 10 * inn[2] +
            3 * inn[3] + 5 * inn[4] + 9 * inn[5] +
            4 * inn[6] + 6 * inn[7] + 8 * inn[8]
        ) % 11) % 10);
    } else if (inn.length == 12) {
        return inn[10] == String(((
            7 * inn[0] + 2 * inn[1] + 4 * inn[2] +
            10 * inn[3] + 3 * inn[4] + 5 * inn[5] +
            9 * inn[6] + 4 * inn[7] + 6 * inn[8] +
            8 * inn[9]
        ) % 11) % 10) && inn[11] == String(((
            3 * inn[0] + 7 * inn[1] + 2 * inn[2] +
            4 * inn[3] + 10 * inn[4] + 3 * inn[5] +
            5 * inn[6] + 9 * inn[7] + 4 * inn[8] +
            6 * inn[9] + 8 * inn[10]
        ) % 11) % 10);
    }
    return false;
}

var validateOgrnFn = function (value) {
    if (value.match(/\D/)) return false;
    var ogrn = value.match(/(\d)/g);
	if (ogrn.length == 13) {
//		var n13 = parseInt((parseInt(ogrn.slice(0, -1)) % 11).toString().slice(-1));
//		if (n13 === parseInt(ogrn[12])) {
			return true;
//		}
	}
	return false;
}

var customValidate = [ {
    name:'validateInn',
    message:'Не валидный номер ИНН',
    validatorFn: value =>  validateInnFn(value)
  },{
    name:'validateOgrn',
    message:'Не валидный номер ОГРН',
    validatorFn: value =>  validateOgrnFn(value)
  },{
      name:'validatePhone',
      message:'Поле имеет ошибочный формат',
      validatorFn:value =>!!value.match(/^\+7\s\(\d{3}\)\s\d{3}-\d{2}-\d{2}$/)
  },{
      name:'validateRuName',
      message:'Поле содежит недопустимые символы',
      validatorFn:value =>!!value.match(/^[А-ЯЁ]{1}[А-ЯЁа-яё\-]{0,100}$/i)
  },{
      name:'validateRuText',
      message:'Поле содежит недопустимые символы',
      validatorFn: value =>!!value.match(/^[А-ЯЁа-яё\-\_\s\"]{0,255}$/i)
  }
];

customValidate.forEach(item=>{
        VeeValidate.Validator.extend(item.name, {
            getMessage: field => item.message,
            validate: item.validatorFn
        });
    }
);

//Vue.use(VueResource);
Vue.use(VueMask);
Vue.use(VueMoney, {
    decimal: '.',
    thousands: ' ',
    precision: 0,
});
Vue.use(VueAutoSize);

Vue.directive('filter', {
    bind: function(el, binding) {
        el.filterInputHandler = function(e) {
            let ch = String.fromCharCode(e.which);
            let re = new RegExp(el.getAttribute('data-filter-pattern'));
            if (!ch.match(re) && e.which != 8) {
                e.preventDefault();
            }
        };
        el.addEventListener('keypress', el.filterInputHandler);
    },
    unbind: function(el) {
        el.removeEventListener('keypress', el.filterInputHandler);
    },
});



// обрезать интуп по кол-ву символов (cnt)
const sliceResult = function(field, cnt){

    // получим значение инпута
    let curValue = _.get(this, field);

    // установим обрезанное значние на велечину cnt
    _.set(this, field, curValue.slice(0, cnt));

};


let getNumber = function(val) {
    return parseFloat((''+val).replace(/\s/g, ''));
};

Vue.component('field', {
    template: '#formCard-field',
    props: {
        name: { type: String, required: true },
        label: { type: String, required: true },
        placeholder: { type: String, required: true },
        type: { type: String, default: 'text' },
        value: {},
        selectArray: { default: false },
        help: { type: String },
        validate: { type: String, default: 'required' },
        mask: { type: String, default: null },
        filter: { type: String, default: '' },
        options: { default: () => {} },
        cells: { type: Array, default: () => ['_md-7', '_md-5'] },
        autosize: { default: false },
    },
    watch: {
        mask: function() {
            this.$mount();
        },
        filter: function() {
            this.$mount();
        },
    },
    methods: {
        updateValue: function(value) {
            this.$emit('input', value);
        },
        multiselectOnChange(e) {
            var values = this.$refs.options.filter(el => el.selected).map(el => el.value);
            this.$emit('input', values);
        },
        multiselectIsSelected(option) {
            return this.value.includes(option);
        }
    },
});

if (document.getElementById('formFast')) {
    let pageMixin = {
        props: {
            loading: {type: Boolean, default: false},
        },
        data: function () {
            return {
                inputs: {},
            };
        },
        watch: {
            inputs: {
                deep: true,
                handler: function () {
                    this.$emit('inputs-update', this.inputs);
                }
            },
        },
        mounted: function() {
            setTimeout(() => {
                this.$el.querySelector('input, textarea, select').focus();
            }, 200)
        },
        methods: {
            validate: function () {
                let result = [
                    this.$validator.validateAll(),
                ];
                this.$children.forEach(vm => {
                    result.push(vm.$validator.validateAll());
                });
                return Promise.all(result);
            },
            nextPage: function () {
                this.validate().then((result) => {
                    if (!result.includes(false)) {
                        this.$emit('next-page');
                    }
                }).catch(() => {
                });
            },
            prevPage: function () {
                this.$emit('prev-page');
            },
            submitForm: function () {
                this.validate().then((result) => {
                    if (!result.includes(false)) {
                        this.$emit('submit-form');
                    }
                }).catch(() => {
                });
            },
            submitManager: function () {
                this.validate().then((result) => {
                    if (!result.includes(false)) {
                        this.$emit('submit-manager');
                    }
                }).catch(() => {
                });
            },
        },
    };

    let page1 = {
        mixins: [pageMixin],
        template: '#formCard-page1',
        data: function () {
            return {
                inputs: {
                    name1: '',
                    name2: '',
                    name3: '',
                    phone: '',
                    email: '',
                }
            };
        },
        computed: {
            fullname: function () {
                let fullname = `${this.inputs.name2} ${this.inputs.name3}`;
                return fullname;
            }
        },
        watch: {
            fullname: function () {
                this.$emit('update-fullname', this.fullname);
            },
        },
    };

    let page2 = {
        mixins: [pageMixin],
        template: '#formCard-page2',
        data: function () {
            return {
                inputs: {
                    organizationTitle: '',
                    resident: "true",
                    inn: '',
                    kpp: '',
                    taxNumber: '',
                    addressRegister: '',
                    addressActual: '',
                },
            };
        },
    };

    let page3 = {
        mixins: [pageMixin],
        template: '#formCard-page3',
        data: function () {
            return {
                inputs: {
                    projectTitle: '',
                    projectTarget: '',
                    projectResult: '',
                    projectType: '',
                    projectTypeCustom: '',
                    projectDesc: '',
                    projectPlacement: '',
                    projectIndustry: '',
                    projectIndustryCustom: '',
                    projectCost: '',
                    projectRecoupment: '',
                },
                options: {
                    projectPlacement: {},
                    projectType: {},
                    projectIndustry: {},
                    projectRecoupment: {},
                    projectCost: {},
                }
            };
        },
        mounted: function() {
            // GET
            this.$http.get('/reference/projectCost').then(
                function (response) { // Success.
                    let normalized = {};
                    response.data.forEach((item) => {
                        normalized[item.id] = item.title;
                    });
                    this.options.projectCost = normalized;
                },
                function (response) { // Error.
                    console.log('An error occurred.');
                }
            );
            this.$http.get('/reference/projectPaybackTime').then(
                function (response) { // Success.
                    let normalized = {};
                    response.data.forEach((item) => {
                        normalized[item.id] = item.title;
                    });
                    this.options.projectRecoupment = normalized;
                },
                function (response) { // Error.
                    console.log('An error occurred.');
                }
            );
            this.$http.get('/reference/projectSectorsWithDirections').then(
                function (response) { // Success.
                    // let normalized = {};
                    // response.data.forEach((item) => {
                    //     normalized[item.id] = item.title;
                    // });
                    this.options.projectIndustry = response.data;
                },
                function (response) { // Error.
                    console.log('An error occurred.');
                }
            );
            this.$http.get('/reference/projectType').then(
                function (response) { // Success.
                    let normalized = {};
                    response.data.forEach((item) => {
                        normalized[item.id] = item.title;
                    });
                    this.options.projectType = normalized;
                },
                function (response) { // Error.
                    console.log('An error occurred.');
                }
            );
            this.$http.get('/reference/projectRegion').then(
                function (response) { // Success.
                    let normalized = {};
                    response.data.forEach((item) => {
                        normalized[item.id] = item.title;
                    });
                    this.options.projectPlacement = normalized;
                },
                function (response) { // Error.
                    console.log('An error occurred.');
                }
            );
        },
    };

    let page4 = {
        mixins: [pageMixin],
        template: '#formCard-page4',
        props: ['managers'],
        data: function () {
            return {
                inputs: {
                    manager: '',
                },
                options: {
                    manager: [],
                }
            };
        },
    };

    let pageSuccess = {
        template: '#formCard-pageSuccess',
        props: {
            managers: {type: Array, default: () => []},
        },
        data: function () {
            return {};
        },
    };

    let pageFail = {
        template: '#formCard-pageFail',
        props: {
            managers: {type: Array, default: () => []},
            fullname: {type: String, default: ''},
            projectname: {type: String, default: ''},
        },
        data: function () {
            return {};
        },
    };

    let formFast = new Vue({
        el: '#formFast',
        data: {
            loading: false,
            currentAnimation: 'next',
            currentPageIndex: 0,
            status: null,
            pages: {
                0: 'page1',
                1: 'page2',
                2: 'page3',
                3: 'page4',
            },
            inputs: {},
            managers: [],
            fullname: '',
        },
        computed: {
            currentPage: function () {
                return this.pages[this.currentPageIndex];
            },
        },
        methods: {
            nextPage: function () {
                this.currentAnimation = 'next';
                this.currentPageIndex++;
            },
            prevPage: function () {
                this.currentAnimation = 'prev';
                this.currentPageIndex--;
            },
            inputsUpdate: function (inputs) {
                this.inputs = Object.assign({}, this.inputs, inputs);
            },
            submitForm: function () {
                console.log('submit form');
                this.loading = true;
                this.$http.post(`./firstRequest/decision`, this.normalizeSubmitData(this.inputs)).then(response => {
                    this.loading = false;
                    console.log(response.body);
                    if (response.body && response.body.result == 'success') {
                        this.currentAnimation = 'next';
                        this.managers = response.body.managers;
                        this.currentPageIndex++;
                    } else {
                        this.currentAnimation = 'next';
                        this.currentPageIndex++;
                        this.currentPageIndex++;
                        this.managers = response.body.managers;
                        this.status = 'fail';
                    }
                }, response => {
                    console.log('fail', response);
                });
            },
            submitManager: function () {
                console.log('submit managers');
                this.loading = true;
                this.$http.post(`./firstRequest/submit`, this.normalizeSubmitData(this.inputs)).then(response => {
                    this.loading = false;
                    this.managers = response.body.managers;
                    console.log(response.body);
                    this.status = 'success';
                }, response => {
                    console.log('fail', response);
                });
            },
            updateFullname: function (val) {
                this.fullname = val;
            },
            normalizeSubmitData: function (data) {
                let newData = {
                    applicantNameLast: data.name1,
                    applicantNameFirst: data.name2,
                    applicantNameMid: data.name3,
                    applicantPhoneNumber: data.phone,
                    applicantEmail: data.email,
                    initiatorNameShort: data.organizationTitle,
                    initiatorResident: data.resident,
                    initiatorInn: data.inn,
                    initiatorKpp: data.kpp,
                    initiatorKio: data.taxNumber,
                    initiatorAddressRegistrationName: data.addressRegister,
                    initiatorAddressFactualName: data.addressActual,
                    projectNomination: data.projectTitle,
                    projectTarget: data.projectTarget,
                    projectExpectedResult: data.projectResult,
                    projectType: data.projectType,
                    projectTypeCustom: data.projectTypeCustom,
                    projectDescription: data.projectDesc,
                    projectSalePlace: data.projectPlacement,
                    projectSectors1: data.projectIndustry,
                    projectSectorsCustom: data.projectIndustryCustom,
                    projectCost: data.projectCost,
                    projectPaybackTime: data.projectRecoupment,
                    projectCertifiedManagerId: data.manager,
                };
                return newData;
            },
        },
        components: {
            page1,
            page2,
            page3,
            page4,
            pageSuccess,
            pageFail,
        },
    });
}

Vue.component('officeToggleGroup', {
    template: '#office-toggleGroup',
    props: {
        value: {},
        name: { type: String },
    },
    computed: {
        radioValue: {
            get: function() {
                return this.value;
            },
            set: function(val) {
                this.$emit('change', val);
            }
        }
    }
});

Vue.component('formInputFiles', {
    template: '#form-inputFiles',
    props: {
        files: { type: Array },
        name: { type: String },
        cellClass: { type: String },
        areaClass: { type: String },
    },
    data: function() {
        return {};
    },
    methods: {
        getFileTypeMod: function(file) {
            return `_${file.type}`;
        },
        getFileSize: function(size) {
            if (!size) {
                return 0 + ' B';
            }
            let i = Math.floor(Math.log(size) / Math.log(1024));
            return (size / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
        },
        removeFile: function(index) {
            this.files.splice(index, 1);
            fileContent2.splice(index, 1);
            i--;
        },
        cryptoproFile: function cryptoproFile(index) {
            fileContent = fileContent2[index];
            let currentSignInput = undefined;
            let signInputs = this.$el.querySelectorAll('textarea[id="filesgn"]');
            let lastSignInput = signInputs[signInputs.length - 1];
            currentSignInput = signInputs[index];
            currentSignInput.value = 'Подписываем...';

            let certInputs = this.$el.querySelectorAll('select[id="CertListBox"]');
            let currentCertInput = certInputs[index];

            if ('' == fileContent2[index] || undefined == fileContent)
                { alert('Выбран пустой файл'); return; }
            else
                Common_SignCadesBES_File(currentCertInput, currentSignInput)
        },
        addFile: function() {
            this.files.push({
                name: 'Пустой файл',
                ext: null,
                size: null,
                url: null,
            });
            this.$nextTick(function() {
                let fileInputs = this.$el.querySelectorAll('input[type="file"]');
                let lastFileInput = fileInputs[fileInputs.length - 1];
                lastFileInput.click();
            });
        },
        updateFile: function(file, event) {
            let certInputs = this.$el.querySelectorAll('select[id="CertListBox"]');
            let lastCertInput = certInputs[certInputs.length - 1];
            //startCrypto(lastCertInput);

            let files = event.target.files;
            file.name = files[0].name;
            file.ext = files[0].name.split('.').pop();
            file.size = files[0].size;
        },
    },
});

Vue.component('countdown', {
    template: '#countdown',
    props: {
        string: { type: String },
        max: { type: Number },
    },
    computed: {
        diff: function() {
            return this.max - this.string.length;
        },
        errorMax: function() {
            return this.diff < 0;
        },
    },
});

if (document.getElementById('formAssessment')) {
    let formAssessment = new Vue({
        el: '#formAssessment',
        data: {
            currentPageIndex: 0,
            inputs: {
                reputationCheck: null,
                reputationDesc: '',
                reputationFiles: [],
                costCheck: null,
                costDesc: '',
                costFiles: [],
                paybackTimeCheck: null,
                paybackTimeDesc: '',
                paybackTimeFiles: [],
                industryCheck: null,
                industryDesc: '',
                industryFiles: [],
                strategyCheck: null,
                strategyDesc: '',
                strategyFiles: [],
                investedCheck: null,
                investedDesc: '',
                investedFiles: [],
                participationCheck: null,
                participationValue: 0,
                participationFiles: [],
                breakEvenCheck: null,
                breakEvenDesc: '',
                breakEvenFiles: [],
            },
            validation: {
                0: {
                    reputationCheck: {error: null},
                    reputationDesc: {error: null},
                },
                1: {
                    costCheck: {error: null},
                },
                2: {
                    paybackTimeCheck: {error: null},
                },
                3: {
                    industryCheck: {error: null},
                },
                4: {
                    strategyCheck: {error: null},
                },
                5: {
                    investedCheck: {error: null},
                },
                6: {
                    //participationCheck: {error: null},
                },
                7: {
                    breakEvenCheck: {error: null},
                },
            },
        },
        mounted: function() {
            this.$http.get('/private-office/express-assessment/data').then(
                function (response) { // Success.
                    if (response.body) {
                        this.inputs.reputationCheck = response.body.fn_reputationCheck;
                        this.inputs.reputationDesc = response.body.fn_reputationDesc;
                        this.inputs.costCheck = response.body.fn_costCheck;
                        this.inputs.costDesc = response.body.fn_costDesc;
                        this.inputs.paybackTimeCheck = response.body.fn_paybackTimeCheck;
                        this.inputs.paybackTimeDesc = response.body.fn_paybackTimeDesc;
                        this.inputs.industryCheck = response.body.fn_industryCheck;
                        this.inputs.industryDesc = response.body.fn_industryDesc;
                        this.inputs.strategyCheck = response.body.fn_strategyCheck;
                        this.inputs.strategyDesc = response.body.fn_strategyDesc;
                        //this.inputs.investedCheck = response.body.fn_investedCheck;
                        //this.inputs.investedDesc = response.body.fn_investedDesc;
                        //this.inputs.participationCheck = response.body.fn_participationCheck;
                        this.inputs.participationValue = response.body.fn_participationValue;
                        this.inputs.breakEvenCheck = response.body.fn_breakEvenCheck;
                        this.inputs.breakEvenDesc = response.body.fn_breakEvenDesc;
                    }
                },
                function (response) { // Error.
                    console.log('An error occurred.');
                }
            );
        },
        methods: {
            nextPage: function () {
                let nxtPage = this.switchNextPage;
                this.validate().then(function (result) {
                    if (!(!result.includes(false) && result.includes(true))) {

                    } else {
                        nxtPage();
                    }
                });
            },
            switchNextPage: function () {
                if (this.currentPageIndex == 4) {
                    // Disable page 5
                    this.currentPageIndex = 6;
                } else {
                    this.currentPageIndex++;
                }
            },
            prevPage: function () {
                if (this.currentPageIndex == 6) {
                    // Disable page 5
                    this.currentPageIndex = 4;
                } else {
                    this.currentPageIndex--;
                }
            },
            submit: function () {
                let doSubmit = this.$el;
                this.validate().then(function (result) {
                    if (!(!result.includes(false) && result.includes(true))) {

                    } else {
                        doSubmit.submit();
                    }
                });
            },
            validate: function () {
                let result = [this.$validator.validateAll()];
                this.$children.forEach(function (vm) {
                    result.push(vm.$validator.validateAll());
                });
                return Promise.all(result);
            },
            validateField: function (pageIndex, field) {
                let validationField = this.validation[pageIndex];
                let isValid = () => !this.inputs[field];
                if (isValid.field) {
                    if (validationField[field]){
                        validationField[field].error = 'Поле обязательно к заполнению';
                    }
                    return false;
                } else {
                    if (validationField[field]){
                        validationField[field].error = null;
                    }
                    return true;
                }
                return true;
            },
            validatePage: function (index) {
                let pageFields = this.validation[index];
                let valid = true;
                Object.keys(pageFields).map((field) => {
                    if (!this.validateField(index, field)) {
                        valid = false;
                    }
                });
                return valid;
            },

        },
        components: {
            //FileUpload
        },
    });
}

console.log(Vue);

if (document.getElementById('formResume')) {
    let formResume = new Vue({
        el: '#formResume',
        data: {
            inputs: {
                projectType1: null,
                currency: null,
                files: [],
                chains_of_owners: "",
                executiveResident: null,
                extraPlaces: [],
                where_send: null,
                dzo_send: [],
                extraParticipants: [{
                    id: null,
                    type: "ul",
                    status: null,
                    resident: "true",
                    nameLast: null,
                    nameFirst: null,
                    nameMid: null,
                    bornDate: {
                        day: null,
                        month: null,
                        year: null
                    },
                    bornPlace: {
                        name: null
                    },
                    citizenship: null,
                    addressRegistration: {
                        region: null, // регион
                        city: null, // населенный пункт
                        street: null, // улица
                        house_corps: null, // дом
                        building: null, // корпус
                        apartment: null, // квартира
                    },
                    idName: null, // тип документа
                    idSeries: null, // серия
                    idNumber: null, // номер
                    idIssueDate: {
                        day: null,
                        month: null,
                        year: null
                    },
                    idIssueAgencyName: null, // кем выдан
                    idIssueAgencyCode: null, // код подразделения
                    organizationType: null,
                    organizationTitle: null,
                    ogrn: null,
                    inn: null,
                    kpp: null,
                    kio: null,
                    nalogNumber: null,
                    regNumber: null,
                    volume: null,
                    rules: null
                }],
                projectProjectCostDouble: 1000,
                projectAmountRecipientsCash: 0,
                projectAmountBanksCash: 0,
                executiveControlCompanyDelegate: null,
                supply: {
                    bank: {
                        check: null,
                        items : [
                            {
                                id: null,
                                type: "0",
                                guarantor: null,
                                value: null,
                                currency: "",
                                depositType: null,
                                depositWho: null,
                                depositCompany: null,
                                depositCompanyDay: null,
                                depositCompanyMonth: null,
                                depositCompanyYear: null,
                            },
                        ],
                    },
                    guarantee: {
                        check: null,
                        items : [
                            {
                                id: null,
                                type: "1",
                                guarantor: null,
                                value: null,
                                currency: "",
                                depositType: null,
                                depositWho: null,
                                depositCompany: null,
                                depositCompanyDay: null,
                                depositCompanyMonth: null,
                                depositCompanyYear: null,
                            },
                        ],
                    },
                    surety: {
                        check: null,
                        items : [
                            {
                                id: null,
                                type: "2",
                                guarantor: null,
                                value: null,
                                currency: "",
                                depositType: null,
                                depositWho: null,
                                depositCompany: null,
                                depositCompanyDay: null,
                                depositCompanyMonth: null,
                                depositCompanyYear: null,
                            },
                        ],
                    },
                    pledge: {
                        check: null,
                        items : [
                            {
                                id: null,
                                type: "3",
                                guarantor: null,
                                value: null,
                                currency: "",
                                depositType: null,
                                depositWho: null,
                                depositCompany: null,
                                depositCompanyDay: null,
                                depositCompanyMonth: null,
                                depositCompanyYear: null,
                            },
                        ],
                    },
                    other: {
                        check: null,
                        items : [
                            {
                                id: null,
                                type: "4",
                                guarantor: null,
                                value: null,
                                currency: "",
                                depositType: null,
                                depositWho: null,
                                depositCompany: null,
                                depositCompanyDay: null,
                                depositCompanyMonth: null,
                                depositCompanyYear: null,
                            },
                        ],
                    }
                },
                projectAmountInvestor: [
                    {
                        volume: 0,
                    }
                ],
                projectAmountBond: [
                    {
                        volume: 0,
                    }
                ],
                projectAmountOthers: [
                    {
                        volume: 0,
                        source: '',
                    }
                ],
                guarantors: [
                    {
                        title: null,
                        value: null,
                        currency: null,
                    },
                ],
                surety: [
                    {
                        title: null,
                        value: null,
                        currency: null,
                    },
                ],
                pledge: [
                    {
                        type: null,
                        value: null,
                        currency: null,
                        depositeType: null,
                        assessment: null,
                        assessmentTitle: null,
                        assessmentDay: null,
                        assessmentMonth: null,
                        assessmentYear: null,
                    },
                ],
                other: [
                    {
                        title: null,
                        value: null,
                        currency: null,
                    },
                ],
                checks: {
                    c10: null,
                    c11: null,
                    c12: null,
                    c13: null,
                    c14: null,
                    c15: null,
                },
                amountOfFinancing: null,
                govSupportNot: null,
                govSupport1: null,
                govSupport2: null,
                govSupport3: null,
                projectDescription: '',
                projectTarget: '',
                projectExpectedResult: '',
                projectExperienceProjectManagers: '',
                projectDegree: '',
            },
            access_credit_file: "", // файл Согласие на запрос кредитного отчета
            personal_data_file: "", // файл Согласие на обработку персональных данных
            chains_of_owners: ""
        },
        mounted: function() {
            this.$http.get('/private-office/resume/project').then(
                function (response) { // Success.

                    // если резидент не пришел, то будет тру по дефолту
                    this.inputs.executiveResident = 'true';
                    if(_.has(response.body, 'executiveResident')){
                        response.body.executiveResident === true ? 'true' : 'false';
                    }

                    this.inputs.executiveControlCompanyDelegate = response.body.executiveControlCompanyDelegate === true ? 'true' : 'false';

                    if (response.body.projectAmountInvestorArray) {
                        if (response.body.projectAmountInvestorArray.length > 0) this.inputs.projectAmountInvestor = [];
                        response.body.projectAmountInvestorArray.map((item) =>
                            this.inputs.projectAmountInvestor.push({
                                volume: Number(item[0]),
                            })
                        )
                    }
                    if (response.body.projectAmountBondArray) {
                        if (response.body.projectAmountBondArray.length > 0) this.inputs.projectAmountBond = [];
                        response.body.projectAmountBondArray.map((item) =>
                            this.inputs.projectAmountBond.push({
                                volume: Number(item[0]),
                            })
                        )
                    }
                    if (response.body.projectAmountFinanceArray) {
                        if (response.body.projectAmountFinanceArray.length > 0) this.inputs.projectAmountOthers = [];
                        response.body.projectAmountFinanceArray.map((item) =>
                            this.inputs.projectAmountOthers.push({
                                volume: Number(item[1]),
                                source: item[0]
                            })
                        )
                    }

                    if (response.body.projectSalePlaceCustom) {
                        response.body.projectSalePlaceCustom.map((el) =>
                            this.inputs.extraPlaces.push({
                                value: el,
                            })
                        )
                    }


                    const initialExtraParticipants = _.clone(this.inputs.extraParticipants[0]);

                    if(!_.isEmpty(response.body.participants)){
                        this.inputs.extraParticipants = [];

                        response.body.participants.forEach(function(participient, i){

                            let curFields = _.clone(initialExtraParticipants);

                            for(let field in initialExtraParticipants){
                                if(!(field in participient))
                                    continue

                                if (field === 'resident') {
                                    curFields[field] = participient[field] ? 'true' : 'false';
                                } else {
                                    curFields[field] = participient[field] || null;
                                }
                            }

                            this.inputs.extraParticipants.push(curFields)

                        }.bind(this));

                    }

                    if (typeof response.body.supplies != "undefined"  && response.body.supplies.length > 0) {
                        this.inputs.supply.bank.items = [];
                        this.inputs.supply.guarantee.items = [];
                        this.inputs.supply.surety.items = [];
                        this.inputs.supply.pledge.items = [];
                        this.inputs.supply.other.items = [];
                        this.inputs.supply.bank.check = true;
                        this.inputs.supply.guarantee.check = true;
                        this.inputs.supply.surety.check = true;
                        this.inputs.supply.pledge.check = true;
                        this.inputs.supply.other.check = true;
                        response.body.supplies.map((item) =>
                            {
                                if (item.name === "0") {
                                    this.inputs.supply.bank.items.push({
                                        id: item.id,
                                        type: item.name,
                                        guarantor: item.guarantor_name,
                                        value: item.supply_amount,
                                        currency: item.supply_currency,
                                        depositType: null,
                                        depositWho: null,
                                        depositCompany: null,
                                        depositCompanyDay: null,
                                        depositCompanyMonth: null,
                                        depositCompanyYear: null,
                                    })
                                } else if (item.name === "1") {
                                    this.inputs.supply.guarantee.items.push({
                                        id: item.id,
                                        type: item.name,
                                        guarantor: item.guarantor_name,
                                        value: item.supply_amount,
                                        currency: item.supply_currency,
                                        depositType: null,
                                        depositWho: null,
                                        depositCompany: null,
                                        depositCompanyDay: null,
                                        depositCompanyMonth: null,
                                        depositCompanyYear: null,
                                    })
                                } else if (item.name === "2") {
                                    this.inputs.supply.surety.items.push({
                                        id: item.id,
                                        type: item.name,
                                        guarantor: item.guarantor_name,
                                        value: item.supply_amount,
                                        currency: item.supply_currency,
                                        depositType: null,
                                        depositWho: null,
                                        depositCompany: null,
                                        depositCompanyDay: null,
                                        depositCompanyMonth: null,
                                        depositCompanyYear: null,
                                    })
                                } else if (item.name === "3") {
                                    this.inputs.supply.pledge.items.push({
                                        id: item.id,
                                        type: item.name,
                                        guarantor: item.guarantor_name,
                                        value: item.market_value_of_collateral,
                                        currency: item.supply_currency,
                                        depositType: null,
                                        depositWho: item.valuation_of_collateral_was_made,
                                        depositCompany: item.company_name,
                                        depositCompanyDay: item.date_of_evaluation ? Number(item.date_of_evaluation.substring(8)) : null,
                                        depositCompanyMonth: item.date_of_evaluation ? Number(item.date_of_evaluation.substring(5,7)) : null,
                                        depositCompanyYear: item.date_of_evaluation ? Number(item.date_of_evaluation.substring(0,4)) : null,
                                    })
                                } else if (item.name === "4") {
                                    this.inputs.supply.other.items.push({
                                        id: item.id,
                                        type: item.name,
                                        guarantor: item.guarantor_name,
                                        value: item.supply_amount,
                                        currency: item.supply_currency,
                                        depositType: null,
                                        depositWho: null,
                                        depositCompany: null,
                                        depositCompanyDay: null,
                                        depositCompanyMonth: null,
                                        depositCompanyYear: null,
                                    })
                                }
                            }
                        );

                        if (this.inputs.supply.bank.items.length == 0) {
                            this.inputs.supply.bank.check = false;
                            this.inputs.supply.bank.items.push({
                                id: null,
                                type: "0",
                                guarantor: null,
                                value: null,
                                currency: "",
                                depositType: null,
                                depositWho: null,
                                depositCompany: null,
                                depositCompanyDay: null,
                                depositCompanyMonth: null,
                                depositCompanyYear: null,
                            })
                        }
                        if (this.inputs.supply.guarantee.items.length == 0) {
                            this.inputs.supply.guarantee.check = false;
                            this.inputs.supply.guarantee.items.push({
                                id: null,
                                type: "1",
                                guarantor: null,
                                value: null,
                                currency: "",
                                depositType: null,
                                depositWho: null,
                                depositCompany: null,
                                depositCompanyDay: null,
                                depositCompanyMonth: null,
                                depositCompanyYear: null,
                            })
                        }
                        if (this.inputs.supply.surety.items.length == 0) {
                            this.inputs.supply.surety.check = false;
                            this.inputs.supply.surety.items.push({
                                id: null,
                                type: "2",
                                guarantor: null,
                                value: null,
                                currency: "",
                                depositType: null,
                                depositWho: null,
                                depositCompany: null,
                                depositCompanyDay: null,
                                depositCompanyMonth: null,
                                depositCompanyYear: null,
                            })
                        }
                        if (this.inputs.supply.pledge.items.length == 0) {
                            this.inputs.supply.pledge.check = false;
                            this.inputs.supply.pledge.items.push({
                                id: null,
                                type: "3",
                                guarantor: null,
                                value: null,
                                currency: "",
                                depositType: null,
                                depositWho: null,
                                depositCompany: null,
                                depositCompanyDay: null,
                                depositCompanyMonth: null,
                                depositCompanyYear: null,
                            })
                        }
                        if (this.inputs.supply.other.items.length == 0) {
                            this.inputs.supply.other.check = false;
                            this.inputs.supply.other.items.push({
                                id: null,
                                type: "4",
                                guarantor: null,
                                value: null,
                                currency: "",
                                depositType: null,
                                depositWho: null,
                                depositCompany: null,
                                depositCompanyDay: null,
                                depositCompanyMonth: null,
                                depositCompanyYear: null,
                            })
                        }
                    } else {
                        this.inputs.supply.bank.check = false;
                        this.inputs.supply.guarantee.check = false;
                        this.inputs.supply.surety.check = false;
                        this.inputs.supply.pledge.check = false;
                        this.inputs.supply.other.check = false;
                    }
                    if (response.body) {
                        this.inputs.projectDescription = response.body.projectDescription;
                        this.inputs.projectTarget = response.body.projectTarget;
                        this.inputs.projectExpectedResult = response.body.projectExpectedResult;
                        this.inputs.projectExperienceProjectManagers = response.body.projectExperienceProjectManagers;
                        this.inputs.projectDegree = response.body.projectDegree;

                        this.inputs.projectAmountRecipientsCash = response.body.projectAmountRecipientsCash;
                        this.inputs.projectAmountBanksCash = response.body.projectAmountBanksCash;


                        this.inputs.projectProjectCostDouble = response.body.projectProjectCostDouble;
                        this.inputs.currency = response.body.projectCostCurrency;
                        this.inputs.projectType1 = response.body.projectType1;
                        // this.inputs.govSupportNot = true;
                        if (response.body.projectImpactInfoAvailabilityOfStateSupportComment0) this.inputs.govSupport1 = true;
                        if (response.body.projectImpactInfoAvailabilityOfStateSupportComment1) this.inputs.govSupport2 = true;
                        if (response.body.projectImpactInfoAvailabilityOfStateSupportComment2) this.inputs.govSupport3 = true;
                    }
                },
                function (response) { // Error.
                    console.log('An error occurred.');
                }
            );
        },
        computed: {
            currencySuffix: function() {
                return getCurrency(this.inputs.currency);
            },
            govSupportAnyChecked: function() {
                return this.inputs.govSupport1 || this.inputs.govSupport2 || this.inputs.govSupport3;
            },
        },
        filters: {
            float: function (value) {
                return value.toFixed(2);
            }
        },
        methods: {
            countDown: function (string, max) {
                return max - string.length;
            },
            addExtraPlace: function () {
                this.inputs.extraPlaces.push({
                    value: null,
                });
            },
            removeExtraPlace: function (index) {
                this.inputs.extraPlaces.splice(index, 1);
            },
            addExtraParticipant: function () {
                this.inputs.extraParticipants.push({
                    id: null,
                    type: "ul",
                    status: null,
                    resident: "true",
                    nameLast: null,
                    nameFirst: null,
                    nameMid: null,
                    bornDate: {
                        day: null,
                        month: null,
                        year: null
                    },
                    bornPlace: {
                        name: null
                    },
                    citizenship: null,
                    addressRegistration: {
                        region: null, // регион
                        city: null, // населенный пункт
                        street: null, // улица
                        house_corps: null, // дом
                        building: null, // корпус
                        apartment: null, // квартира
                    },
                    idName: null, // тип документа
                    idSeries: null, // серия
                    idNumber: null, // номер
                    idIssueDate: {
                        day: null,
                        month: null,
                        year: null
                    },
                    idIssueAgencyName: null, // кем выдан
                    idIssueAgencyCode: null, // код подразделения
                    organizationType: null,
                    organizationTitle: null,
                    ogrn: null,
                    inn: null,
                    kpp: null,
                    kio: null,
                    nalogNumber: null,
                    regNumber: null,
                    volume: null,
                    rules: null
                });
            },
            removeExtraParticipant: function (index) {
                this.inputs.extraParticipants.splice(index, 1);
            },
            addGuarantorBank: function () {
                this.inputs.supply.bank.items.push({
                    id: null,
                    type: "0",
                    guarantor: null,
                    value: null,
                    currency: null,
                    depositType: null,
                    depositWho: null,
                    depositCompany: null,
                    depositCompanyDay: null,
                    depositCompanyMonth: null,
                    depositCompanyYear: null,
                });
            },
            removeGuarantorBank: function (index) {
                this.inputs.supply.bank.items.splice(index, 1);
            },
            addGuarantor: function () {
                this.inputs.supply.guarantee.items.push({
                    id: null,
                    type: "1",
                    guarantor: null,
                    value: null,
                    currency: null,
                    depositType: null,
                    depositWho: null,
                    depositCompany: null,
                    depositCompanyDay: null,
                    depositCompanyMonth: null,
                    depositCompanyYear: null,
                });
            },
            removeGuarantor: function (index) {
                this.inputs.supply.guarantee.items.splice(index, 1);
            },
            addSurety: function () {
                this.inputs.supply.surety.items.push({
                    id: null,
                    type: "2",
                    guarantor: null,
                    value: null,
                    currency: null,
                    depositType: null,
                    depositWho: null,
                    depositCompany: null,
                    depositCompanyDay: null,
                    depositCompanyMonth: null,
                    depositCompanyYear: null,
                });
            },
            removeSurety: function (index) {
                this.inputs.supply.surety.items.splice(index, 1);
            },
            addPledge: function () {
                this.inputs.supply.pledge.items.push({
                    id: null,
                    type: "3",
                    guarantor: null,
                    value: null,
                    currency: null,
                    depositType: null,
                    depositWho: null,
                    depositCompany: null,
                    depositCompanyDay: null,
                    depositCompanyMonth: null,
                    depositCompanyYear: null,
                });
            },
            removePledge: function (index) {
                this.inputs.supply.pledge.items.splice(index, 1);
            },
            addOther: function () {
                this.inputs.supply.other.items.push({
                    id: null,
                    type: "4",
                    guarantor: null,
                    value: null,
                    currency: null,
                    depositType: null,
                    depositWho: null,
                    depositCompany: null,
                    depositCompanyDay: null,
                    depositCompanyMonth: null,
                    depositCompanyYear: null,
                });
            },
            removeOther: function (index) {
                this.inputs.supply.other.items.splice(index, 1);
            },
            partOfGlobalSum: function(sum) {
                return (getNumber(sum) / getNumber(this.inputs.projectProjectCostDouble) * 100).toFixed(2);
            },
            addProjectAmountInvestor: function() {
                this.inputs.projectAmountInvestor.push({
                    volume: 0,
                });
            },
            removeProjectAmountInvestor: function(index) {
                this.inputs.projectAmountInvestor.splice(index, 1);
            },
            addProjectAmountBond: function() {
                this.inputs.projectAmountBond.push({
                    volume: 0,
                });
            },
            removeProjectAmountBond: function(index) {
                this.inputs.projectAmountBond.splice(index, 1);
            },
            addProjectAmountOthers: function() {
                this.inputs.projectAmountOthers.push({
                    volume: 0,
                    source: '',
                });
            },
            removeProjectAmountOthers: function(index) {
                this.inputs.projectAmountOthers.splice(index, 1);
            },
            // получить расширение файла по его названию
            getFileExt: function(filename){
                return filename.split('.').pop();
            },

            getClassByExt: function(ext){


                let classname = "_upload";

                if(!ext)
                    return classname;

                switch (ext){
                    case "doc":
                    case "ods":
                    case "docx":
                        classname = "__upload_doc";
                        break;
                    case "xls":
                    case "xlsx":
                        classname = "__upload_xls";
                        break;
                    case "pdf":
                        classname = "__upload_pdf";
                        break;
                    default:
                        classname = "__upload_default";
                        break;
                }

                return classname;
            },

            setDateFor: function(prop, e){

                this[prop] = e.target.value;
            },

            sliceResult: sliceResult,

            validate: function () {
                let result = [
                    this.$validator.validateAll(),
                ];
                this.$children.forEach(vm => {
                    result.push(vm.$validator.validateAll());
                });
                return Promise.all(result);
            },
            onSubmit: function(event, skipValidate) {
                if (!skipValidate){
                    this.validate().then((result) => {
                        if (!(!result.includes(false) && result.includes(true))) {
                           event.preventDefault();
                        } else {
                            $("input[name='next']").prop("disabled", true);
                            $("#formResume").submit();
                        }
                    }).catch(() => {
                        event.preventDefault();
                    });
                }
            }
        },
        watch: {
            'inputs.govSupportNot': function() {
                if (this.inputs.govSupportNot) {
                    this.inputs.govSupport1 = this.inputs.govSupport2 = this.inputs.govSupport3 = false;
                }
            },
            'govSupportAnyChecked': function() {
                if (this.govSupportAnyChecked) {
                    this.inputs.govSupportNot = false;
                }
            },
        }
    });
}

if (document.getElementById('office')) {
    let office = new Vue({
        el: '#office',
        data: {
            inputs: {
                files: [],
            },
        },
        computed: {},
        filters: {},
        methods: {},
    });
}

if (document.getElementById('officeFinanceRequest')) {

    let projectID = $(".dropDownProject").attr("data-project-id");
    window.officeFinanceRequest = new Vue({
        el: '#officeFinanceRequest',
        data: {
            inputs: {
                files: [],
            },
            diagramParams: {
            }
        },
        computed: {},
        filters: {},
        methods: {
            slideDashboard: function(e){
                let a = e.target,
                    dropdownContentSelector = ".dropDownProject",
                    href = a.href,
                    $a = $(a),
                    $tr = $a.closest('tr'),
                    $dashboardTr = $tr.next(dropdownContentSelector);

                // если есть загруженный дашборд
                if($dashboardTr.length){

                    $dashboardTr.slideToggle();

                }else{

                    $a.addClass("loading");

                    this.$http.get(href)
                        .then(function(res){
                            var $data = $(res.body), // ответ
                                $content = $data.find(dropdownContentSelector), // содержимое дашборда
                                $diagramParams = $data.filter("#diagramParams"),
                                newProjectID = $content.attr('data-project-id'),
                                $newTd = $('<td colspan="8"/>'), // новый столбец
                                $newTr = $('<tr class="dropDownProject"/>'); // новая строка


                            var self = this;
                            var attrIdProject = "project_" + newProjectID;
                            $content.attr("id", attrIdProject);


                            $('body').on('loadChartsData', function(e, chartsData){

                                Vue.set(self.diagramParams, this.newProjectID, chartsData)



                                $content.find(".chart-graph").each(function(){
                                    let $this = $(this);
                                    let grapthInitID = $this.attr("id");
                                    let grapthID = grapthInitID + "__" + newProjectID;

                                    $this.attr("id", grapthID);

                                    //console.log($(`[data-chart="${grapthInitID}"]`));;
                                    $content.find(`[data-chart="${grapthInitID}"]`).attr('data-chart', grapthID);

                                });



                                new DropDownProject(this.$content[0]);

                                $newTr.slideDown();

                                $('body').off('loadChartsData');
                            }.bind({
                                newProjectID: newProjectID,
                                $content: $content
                            }));

                            $newTd.append($content);
                            $newTd.append($diagramParams);
                            $newTr.append($newTd);

                            $tr.after($newTr);


                        })
                        .catch(function(){
                            console.error(arguments);
                            alert("Ошибка получения дашборда..");
                        })
                        .then(function(){
                            $a.removeClass("loading")
                        });
                }


            },

            slideUsersForChat: function(e){

                let $a = $(e.target).closest("a"),
                    $projectNameWrap = $a.closest(".dropDown-link"),
                    $contactList = $projectNameWrap.next(".chat-contact-list");


                if(!$contactList.length){
                    $a.addClass("loading");
                    $.get($a.attr("href"))
                        .then(function(data){
                            let $data = $(data),
                                $contactList = $data.find(".chat-contact-list");


                            $projectNameWrap.after($contactList);
                            $contactList.slideToggle();

                        }, function(){
                            console.error(arguments)
                            alert("Ошибка загрузки контактов");
                        })
                        .then(function(){

                            $a.removeClass("loading");
                        });
                }else{
                    $contactList.slideToggle();
                }



            },

            validate: function () {
                let result = [
                    this.$validator.validateAll(),
                ];
                this.$children.forEach(vm => {
                    result.push(vm.$validator.validateAll());
                });
                return Promise.all(result);
            },
            onSubmit: function(event) {
                this.validate().then((result) => {
                    console.log(result);
                    if (!result.includes(false)) {
                        // this.$emit('submit');
                        this.$refs.form.submit();
                    }
                }).catch(() => {
                });
            }
        },
    });

    if(typeof projectID != 'undefined'){
        Vue.set(officeFinanceRequest.$data.diagramParams, projectID, chartsData);
    }

}

if (document.getElementById('officeInitiatorInfo')) {
    let officeInitiatorInfo = new Vue({
        el: '#officeInitiatorInfo',
        data: {
            inputs: {
                initiatorResident: null,
                initiatorFaceType: "0",
                executiveControlCompanyDelegate: null,
                foreignCurrency: "false"
            },
            options: {
                residentEntities: {
                    0: 'Юридическое лицо',
                    3: 'Физическое лицо',
                    4: 'Индивидуальный предприниматель',
                    5: 'Физическое лицо, занимающееся частной практикой в установленном законодательством Российской Федерации порядке'
                },
                nonResidentEntities: {
                    1: 'Юридическое лицо - нерезидент РФ',
                    2: 'Иностранная структура без образования юридического лица',
                }
            }
        },
        mounted: function() {
            this.$http.get('/private-office/finance-request/initiator/data').then(
                function (response) { // Success.

                    // резидент рф по дефолту
                    this.inputs.initiatorResident = "true";
                    if(response.body.initiatorResident){
                        this.inputs.initiatorResident = response.body.initiatorResident ? 'true' : 'false';
                    }

                    this.inputs.initiatorFaceType = "0";//response.body.initiatorFaceType;
                    this.inputs.executiveControlCompanyDelegate = response.body.executiveControlCompanyDelegate ? 'true' : 'false';
                    this.inputs.foreignCurrency = response.body.initiatorRequisitesForeignCurrency && response.body.initiatorRequisitesForeignCurrency.length >= 1 ? "true" : " false";
                },
                function (response) { // Error.
                    console.log('An error occurred.');
                }
            );
        },
        computed: {
            entityTypes: function () {
                return (this.inputs.initiatorResident === "true")
                    ? this.options.residentEntities
                    : this.options.nonResidentEntities;
            }
        },
        filters: {},
        methods: {
            validate: function () {
                let result = [
                    this.$validator.validateAll(),
                ];
                this.$children.forEach(vm => {
                    result.push(vm.$validator.validateAll());
                });
                return Promise.all(result);
            },
            onSubmit: function(event, skipValidate) {
                if (!skipValidate){
                    this.validate().then((result) => {
                        if (!(!result.includes(false) && result.includes(true))) {
                           event.preventDefault();
                        }
                    }).catch(() => {
                        event.preventDefault();
                    });
                }
            }
        },
    });
}

if (document.getElementById('generalProjectInfo')) {
    let generalProjectInfo = new Vue({
        el: '#generalProjectInfo',
        data: {
            inputs: {
                extraPlaces: [],
                projectTarget: '',
                projectType1: '',
                projectDescription: '',
                projectExpectedResult: '',
                projectExperienceProjectManagers: ''
            },
            managerExperience: ""
        },
        mounted: function() {
            this.$http.get('/private-office/finance-request/projectMain').then(
                function (response) { // Success.
                    if (response.body.projectSalePlaceCustom) {
                        response.body.projectSalePlaceCustom.map((el) =>
                            this.inputs.extraPlaces.push({
                                value: el,
                            })
                        )
                    }
                    if (response.body) {
                        this.inputs.projectTarget = response.body.projectTarget;
                        this.inputs.projectExpectedResult = response.body.projectExpectedResult;
                        this.inputs.projectType1 = response.body.projectType1;
                        this.inputs.projectDescription = response.body.projectDescription;
                        this.inputs.projectExperienceProjectManagers = response.body.projectExperienceProjectManagers;
                    }
                },
                function (response) { // Error.
                    console.log('An error occurred.');
                }
            );
        },
        computed: {},
        filters: {},
        methods: {
            countDown: function (string, max) {
                return max - string.length;
            },
            addExtraPlace: function () {
                this.inputs.extraPlaces.push({
                    value: null,
                });
            },
            removeExtraPlace: function (index) {
                this.inputs.extraPlaces.splice(index, 1);
            },
            sliceResult: sliceResult,
            validate: function () {
                let result = [
                    this.$validator.validateAll(),
                ];
                this.$children.forEach(vm => {
                    result.push(vm.$validator.validateAll());
                });
                return Promise.all(result);
            },
            onSubmit: function(event, skipValidate) {
                if (!skipValidate){
                    this.validate().then((result) => {
                        if (!(!result.includes(false) && result.includes(true))) {
                           event.preventDefault();
                        }
                    }).catch(() => {
                        event.preventDefault();
                    });
                }
            }
        },
    });
}

if (document.getElementById('projectDirectionInfo')) {

    $(document).ready(function(){
        if ($("input[name='projectProjectIsAimedTo']:checked").val() === '5') $("#projectProjectIsAimedToCustomId").show();
        else $("#projectProjectIsAimedToCustomId").hide();
        $("input[name=projectProjectIsAimedTo]").click(function() {
            if ($(this).val() === '5') $("#projectProjectIsAimedToCustomId").show();
            else $("#projectProjectIsAimedToCustomId").hide();
        });



        if ($("input[name='projectSectors1']:checked").val() === '53') $("#projectSectorsCustomId").show();
        else $("#projectSectorsCustomId").hide();
        $("input[name=projectSectors1]").click(function() {
            if ($(this).val() === '53') $("#projectSectorsCustomId").show();
            else $("#projectSectorsCustomId").hide();
        });
    });

    let projectDirectionInfo = new Vue({
        el: '#projectDirectionInfo',
        data: {
            inputs: {
                direction: [null],
                projectBranch: null
            },
            options: {
                direction: {},
                projectBranch: {}
            }
        },
        computed: {},
        filters: {},
        methods: {
            validate: function () {
                let result = [
                    this.$validator.validateAll(),
                ];
                this.$children.forEach(vm => {
                    result.push(vm.$validator.validateAll());
                });
                return Promise.all(result);
            },
            onSubmit: function(event, skipValidate) {
                if (!skipValidate){
                    this.validate().then((result) => {
                        if (!(!result.includes(false) && result.includes(true))) {
                           event.preventDefault();
                        }
                    }).catch(() => {
                        event.preventDefault();
                    });
                }
            }
        },
    });
}

if (document.getElementById('projectCharacteristics')) {
    let projectCharacteristics = new Vue({
        el: '#projectCharacteristics',
        data: {
            inputs: {
                currency: 'RUB',
                projectProjectCostDouble: 1000,
                projectLastCost: 0,
                projectProjectUpcomingInvestments: 0,
                extraParticipants: [{
                    id: null,
                    type: "ul",
                    status: null,
                    resident: "true",
                    nameLast: null,
                    nameFirst: null,
                    nameMid: null,
                    bornDate: {
                        day: null,
                        month: null,
                        year: null
                    },
                    bornPlace: {
                        name: null
                    },
                    citizenship: null,
                    addressRegistration: {
                        region: null, // регион
                        city: null, // населенный пункт
                        street: null, // улица
                        house_corps: null, // дом
                        building: null, // корпус
                        apartment: null, // квартира
                    },
                    idName: null, // тип документа
                    idSeries: null, // серия
                    idNumber: null, // номер
                    idIssueDate: {
                        day: null,
                        month: null,
                        year: null
                    },
                    idIssueAgencyName: null, // кем выдан
                    idIssueAgencyCode: null, // код подразделения
                    organizationType: null,
                    organizationTitle: null,
                    ogrn: null,
                    inn: null,
                    kpp: null,
                    kio: null,
                    nalogNumber: null,
                    regNumber: null,
                    volume: null,
                    rules: null
                }],
                supply: {
                    bank: {
                        check: null,
                        items : [
                            {
                                id: null,
                                type: "0",
                                guarantor: null,
                                value: null,
                                currency: "",
                                depositType: null,
                                depositWho: null,
                                depositCompany: null,
                                depositCompanyDay: null,
                                depositCompanyMonth: null,
                                depositCompanyYear: null,
                            },
                        ],
                    },
                    guarantee: {
                        check: null,
                        items : [
                            {
                                id: null,
                                type: "1",
                                guarantor: null,
                                value: null,
                                currency: "",
                                depositType: null,
                                depositWho: null,
                                depositCompany: null,
                                depositCompanyDay: null,
                                depositCompanyMonth: null,
                                depositCompanyYear: null,
                            },
                        ],
                    },
                    surety: {
                        check: null,
                        items : [
                            {
                                id: null,
                                type: "2",
                                guarantor: null,
                                value: null,
                                currency: "",
                                depositType: null,
                                depositWho: null,
                                depositCompany: null,
                                depositCompanyDay: null,
                                depositCompanyMonth: null,
                                depositCompanyYear: null,
                            },
                        ],
                    },
                    pledge: {
                        check: null,
                        items : [
                            {
                                id: null,
                                type: "3",
                                guarantor: null,
                                value: null,
                                currency: "",
                                depositType: null,
                                depositWho: null,
                                depositCompany: null,
                                depositCompanyDay: null,
                                depositCompanyMonth: null,
                                depositCompanyYear: null,
                            },
                        ],
                    },
                    other: {
                        check: null,
                        items : [
                            {
                                id: null,
                                type: "4",
                                guarantor: null,
                                value: null,
                                currency: "",
                                depositType: null,
                                depositWho: null,
                                depositCompany: null,
                                depositCompanyDay: null,
                                depositCompanyMonth: null,
                                depositCompanyYear: null,
                            },
                        ],
                    }
                },
                syndicateParticipants: [
                    {
                        title: null,
                        value: null,
                    },
                ],
                bank: [
                    {
                        title: null,
                        value: null,
                        currency: null,
                    },
                ],
                guarantors: [
                    {
                        title: null,
                        value: null,
                        currency: null,
                    },
                ],
                surety: [
                    {
                        title: null,
                        value: null,
                        currency: null,
                    },
                ],
                pledge: [
                    {
                        type: null,
                        value: null,
                        currency: null,
                        assessment: null,
                        assessmentTitle: null,
                        assessmentDay: null,
                        assessmentMonth: null,
                        assessmentYear: null,
                    },
                ],
                other: [
                    {
                        title: null,
                        value: null,
                        currency: null,
                    },
                ],
                checks: {
                    financeLongCheck: null,
                    financeMezonCheck: null,
                    financeSidicirCheck: null,
                    financeCapitalCheck: null,
                    financeBondCheck: null,
                    financeGarantCheck: null,
                    financeSuretytCheck: null,
                    financeAccredetivCheck: null,
                    financeOtherCheck: null,
                    // 3 форма
                    c10: null,
                    c11: null,
                    c12: null,
                    c13: null,
                    c14: null,
                    c15: null,
                },
                projectAmountRecipientsCash: 0,
                projectAmountBanksCash: 0,
                projectAmountInvestor: [
                    {
                        volume: 0,
                    }
                ],
                projectAmountBond: [
                    {
                        volume: 0,
                    }
                ],
                projectAmountOthers: [
                    {
                        volume: 0,
                        source: '',
                    }
                ],
            },
        },
        computed: {
            currencySuffix: function() {
                return getCurrency(this.inputs.currency);
            },
        },
        filters: {},
        methods: {
            addExtraParticipant: function () {
                this.inputs.extraParticipants.push({
                    id: null,
                    type: "ul",
                    status: null,
                    resident: "true",
                    nameLast: null,
                    nameFirst: null,
                    nameMid: null,
                    bornDate: {
                        day: null,
                        month: null,
                        year: null
                    },
                    bornPlace: {
                        name: null
                    },
                    citizenship: null,
                    addressRegistration: {
                        region: null, // регион
                        city: null, // населенный пункт
                        street: null, // улица
                        house_corps: null, // дом
                        building: null, // корпус
                        apartment: null, // квартира
                    },
                    idName: null, // тип документа
                    idSeries: null, // серия
                    idNumber: null, // номер
                    idIssueDate: {
                        day: null,
                        month: null,
                        year: null
                    },
                    idIssueAgencyName: null, // кем выдан
                    idIssueAgencyCode: null, // код подразделения
                    organizationType: null,
                    organizationTitle: null,
                    ogrn: null,
                    inn: null,
                    kpp: null,
                    kio: null,
                    nalogNumber: null,
                    regNumber: null,
                    volume: null,
                    rules: null
                });
            },
            removeExtraParticipant: function (index) {
                this.inputs.extraParticipants.splice(index, 1);
            },
            addSyndicateParticipant: function () {
                this.inputs.syndicateParticipants.push({
                    title: null,
                    value: null,
                });
            },
            removeSyndicateParticipant: function (index) {
                this.inputs.syndicateParticipants.splice(index, 1);
            },
            addGuarantorBank: function () {
                this.inputs.supply.bank.items.push({
                    id: null,
                    type: "0",
                    guarantor: null,
                    value: null,
                    currency: null,
                    depositType: null,
                    depositWho: null,
                    depositCompany: null,
                    depositCompanyDay: null,
                    depositCompanyMonth: null,
                    depositCompanyYear: null,
                });
            },
            removeGuarantorBank: function (index) {
                this.inputs.supply.bank.items.splice(index, 1);
            },
            addGuarantor: function () {
                this.inputs.supply.guarantee.items.push({
                    id: null,
                    type: "1",
                    guarantor: null,
                    value: null,
                    currency: null,
                    depositType: null,
                    depositWho: null,
                    depositCompany: null,
                    depositCompanyDay: null,
                    depositCompanyMonth: null,
                    depositCompanyYear: null,
                });
            },
            removeGuarantor: function (index) {
                this.inputs.supply.guarantee.items.splice(index, 1);
            },
            addSurety: function () {
                this.inputs.supply.surety.items.push({
                    id: null,
                    type: "2",
                    guarantor: null,
                    value: null,
                    currency: null,
                    depositType: null,
                    depositWho: null,
                    depositCompany: null,
                    depositCompanyDay: null,
                    depositCompanyMonth: null,
                    depositCompanyYear: null,
                });
            },
            removeSurety: function (index) {
                this.inputs.supply.surety.items.splice(index, 1);
            },
            addPledge: function () {
                this.inputs.supply.pledge.items.push({
                    id: null,
                    type: "3",
                    guarantor: null,
                    value: null,
                    currency: null,
                    depositType: null,
                    depositWho: null,
                    depositCompany: null,
                    depositCompanyDay: null,
                    depositCompanyMonth: null,
                    depositCompanyYear: null,
                });
            },
            removePledge: function (index) {
                this.inputs.supply.pledge.items.splice(index, 1);
            },
            addOther: function () {
                this.inputs.supply.other.items.push({
                    id: null,
                    type: "4",
                    guarantor: null,
                    value: null,
                    currency: null,
                    depositType: null,
                    depositWho: null,
                    depositCompany: null,
                    depositCompanyDay: null,
                    depositCompanyMonth: null,
                    depositCompanyYear: null,
                });
            },
            removeOther: function (index) {
                this.inputs.supply.other.items.splice(index, 1);
            },
            partOfGlobalSum: function(sum) {
                return (getNumber(sum) / getNumber(this.inputs.projectProjectCostDouble) * 100).toFixed(2);
            },
            addProjectAmountInvestor: function() {
                this.inputs.projectAmountInvestor.push({
                    volume: 0,
                });
            },
            removeProjectAmountInvestor: function(index) {
                this.inputs.projectAmountInvestor.splice(index, 1);
            },
            addProjectAmountBond: function() {
                this.inputs.projectAmountBond.push({
                    volume: 0,
                });
            },
            removeProjectAmountBond: function(index) {
                this.inputs.projectAmountBond.splice(index, 1);
            },
            addProjectAmountOthers: function() {
                this.inputs.projectAmountOthers.push({
                    volume: 0,
                    source: '',
                });
            },
            removeProjectAmountOthers: function(index) {
                this.inputs.projectAmountOthers.splice(index, 1);
            },
            validate: function () {
                let result = [
                    this.$validator.validateAll(),
                ];
                this.$children.forEach(vm => {
                    result.push(vm.$validator.validateAll());
                });
                return Promise.all(result);
            },
            onSubmit: function(event, skipValidate) {
                if (!skipValidate){
                    this.validate().then((result) => {
                        if (!(!result.includes(false) && result.includes(true))) {
                           event.preventDefault();
                        }
                    }).catch(() => {
                        event.preventDefault();
                    });
                }
            }
        },
        mounted: function () {
            this.$http.get('/private-office/finance-request/project').then(
                function (response) { // Success.

                    const initialExtraParticipants = _.clone(this.inputs.extraParticipants[0]);

                    if(response.body.participants){
                        this.inputs.extraParticipants = [];

                        response.body.participants.forEach(function(participient, i){

                            let curFields = _.clone(initialExtraParticipants);

                            for(let field in initialExtraParticipants){
                                if(!(field in participient))
                                    continue

                                if (field === 'resident') {
                                    curFields[field] = participient[field] ? 'true' : 'false';
                                } else {
                                    curFields[field] = participient[field] || null;
                                }
                            }

                            this.inputs.extraParticipants.push(curFields)

                        }.bind(this));

                    }
                },
                function (response) { // Error.
                    console.log('An error occurred.');
                }
            );

            this.$http.get('/private-office/finance-request/project-characteristics/data').then(
                function (response) { // Success.

                    if (response.body) {
                        this.inputs.checks.financeLongCheck = response.body.financeLongCheck;
                        this.inputs.checks.financeMezonCheck = response.body.financeMezonCheck;
                        this.inputs.checks.financeSidicirCheck = response.body.financeSidicirCheck;
                        this.inputs.checks.financeCapitalCheck = response.body.financeCapitalCheck;
                        this.inputs.checks.financeBondCheck = response.body.financeBondCheck;
                        this.inputs.checks.financeGarantCheck = response.body.financeGarantCheck;
                        this.inputs.checks.financeSuretytCheck = response.body.financeSuretytCheck;
                        this.inputs.checks.financeAccredetivCheck = response.body.financeAccredetivCheck;
                        this.inputs.checks.financeOtherCheck = response.body.financeOtherCheck;

                        if (response.body.syndicateParticipants) {
                            if (response.body.syndicateParticipants.length > 0) this.inputs.syndicateParticipants = [];
                            response.body.syndicateParticipants.map((item) =>
                                this.inputs.syndicateParticipants.push({
                                    title: item.title,
                                    value: item.value
                                })
                            )
                        }
                    }
                    if (response.body) {
                        this.inputs.currency = response.body.projectCostCurrency;
                        this.inputs.projectProjectCostDouble = response.body.projectProjectCostDouble;
                        this.inputs.projectLastCost = response.body.projectLastCost;
                        this.inputs.projectProjectUpcomingInvestments = response.body.projectProjectUpcomingInvestments;


                        this.inputs.projectAmountRecipientsCash = response.body.projectAmountRecipientsCash;
                        this.inputs.projectAmountBanksCash = response.body.projectAmountBanksCash;

						if (response.body.projectAmountInvestorArray) {
							if (response.body.projectAmountInvestorArray.length > 0) this.inputs.projectAmountInvestor = [];
							response.body.projectAmountInvestorArray.map((item) =>
								this.inputs.projectAmountInvestor.push({
									volume: Number(item[0]),
								})
							)
						}
						if (response.body.projectAmountBondArray) {
							if (response.body.projectAmountBondArray.length > 0) this.inputs.projectAmountBond = [];
							response.body.projectAmountBondArray.map((item) =>
								this.inputs.projectAmountBond.push({
									volume: Number(item[0]),
								})
							)
						}
						if (response.body.projectAmountFinanceArray) {
							if (response.body.projectAmountFinanceArray.length > 0) this.inputs.projectAmountOthers = [];
							response.body.projectAmountFinanceArray.map((item) =>
								this.inputs.projectAmountOthers.push({
									volume: Number(item[1]),
									source: item[0]
								})
							)
						}

                        if (typeof response.body.supplies != "undefined"  && response.body.supplies.length > 0) {
                            this.inputs.supply.bank.items = [];
                            this.inputs.supply.guarantee.items = [];
                            this.inputs.supply.surety.items = [];
                            this.inputs.supply.pledge.items = [];
                            this.inputs.supply.other.items = [];
                            this.inputs.supply.bank.check = true;
                            this.inputs.supply.guarantee.check = true;
                            this.inputs.supply.surety.check = true;
                            this.inputs.supply.pledge.check = true;
                            this.inputs.supply.other.check = true;
                            response.body.supplies.map((item) =>
                                {
                                    if (item.name === "0") {
                                        this.inputs.supply.bank.items.push({
                                            id: item.id,
                                            type: item.name,
                                            guarantor: item.guarantor_name,
                                            value: item.supply_amount,
                                            currency: item.supply_currency,
                                            depositType: null,
                                            depositWho: null,
                                            depositCompany: null,
                                            depositCompanyDay: null,
                                            depositCompanyMonth: null,
                                            depositCompanyYear: null,
                                        })
                                    } else if (item.name === "1") {
                                        this.inputs.supply.guarantee.items.push({
                                            id: item.id,
                                            type: item.name,
                                            guarantor: item.guarantor_name,
                                            value: item.supply_amount,
                                            currency: item.supply_currency,
                                            depositType: null,
                                            depositWho: null,
                                            depositCompany: null,
                                            depositCompanyDay: null,
                                            depositCompanyMonth: null,
                                            depositCompanyYear: null,
                                        })
                                    } else if (item.name === "2") {
                                        this.inputs.supply.surety.items.push({
                                            id: item.id,
                                            type: item.name,
                                            guarantor: item.guarantor_name,
                                            value: item.supply_amount,
                                            currency: item.supply_currency,
                                            depositType: null,
                                            depositWho: null,
                                            depositCompany: null,
                                            depositCompanyDay: null,
                                            depositCompanyMonth: null,
                                            depositCompanyYear: null,
                                        })
                                    } else if (item.name === "3") {
                                        this.inputs.supply.pledge.items.push({
                                            id: item.id,
                                            type: item.name,
                                            guarantor: item.guarantor_name,
                                            value: item.market_value_of_collateral,
                                            currency: item.supply_currency,
                                            depositType: null,
                                            depositWho: item.valuation_of_collateral_was_made,
                                            depositCompany: item.company_name,
                                            depositCompanyDay: item.date_of_evaluation ? Number(item.date_of_evaluation.substring(8)) : null,
                                            depositCompanyMonth: item.date_of_evaluation ? Number(item.date_of_evaluation.substring(5,7)) : null,
                                            depositCompanyYear: item.date_of_evaluation ? Number(item.date_of_evaluation.substring(0,4)) : null,
                                        })
                                    } else if (item.name === "4") {
                                        this.inputs.supply.other.items.push({
                                            id: item.id,
                                            type: item.name,
                                            guarantor: item.guarantor_name,
                                            value: item.supply_amount,
                                            currency: item.supply_currency,
                                            depositType: null,
                                            depositWho: null,
                                            depositCompany: null,
                                            depositCompanyDay: null,
                                            depositCompanyMonth: null,
                                            depositCompanyYear: null,
                                        })
                                    }
                                }
                            );

                            if (this.inputs.supply.bank.items.length == 0) {
                                this.inputs.supply.bank.check = false;
                                this.inputs.supply.bank.items.push({
                                    id: null,
                                    type: "0",
                                    guarantor: null,
                                    value: null,
                                    currency: "",
                                    depositType: null,
                                    depositWho: null,
                                    depositCompany: null,
                                    depositCompanyDay: null,
                                    depositCompanyMonth: null,
                                    depositCompanyYear: null,
                                })
                            }
                            if (this.inputs.supply.guarantee.items.length == 0) {
                                this.inputs.supply.guarantee.check = false;
                                this.inputs.supply.guarantee.items.push({
                                    id: null,
                                    type: "1",
                                    guarantor: null,
                                    value: null,
                                    currency: "",
                                    depositType: null,
                                    depositWho: null,
                                    depositCompany: null,
                                    depositCompanyDay: null,
                                    depositCompanyMonth: null,
                                    depositCompanyYear: null,
                                })
                            }
                            if (this.inputs.supply.surety.items.length == 0) {
                                this.inputs.supply.surety.check = false;
                                this.inputs.supply.surety.items.push({
                                    id: null,
                                    type: "2",
                                    guarantor: null,
                                    value: null,
                                    currency: "",
                                    depositType: null,
                                    depositWho: null,
                                    depositCompany: null,
                                    depositCompanyDay: null,
                                    depositCompanyMonth: null,
                                    depositCompanyYear: null,
                                })
                            }
                            if (this.inputs.supply.pledge.items.length == 0) {
                                this.inputs.supply.pledge.check = false;
                                this.inputs.supply.pledge.items.push({
                                    id: null,
                                    type: "3",
                                    guarantor: null,
                                    value: null,
                                    currency: "",
                                    depositType: null,
                                    depositWho: null,
                                    depositCompany: null,
                                    depositCompanyDay: null,
                                    depositCompanyMonth: null,
                                    depositCompanyYear: null,
                                })
                            }
                            if (this.inputs.supply.other.items.length == 0) {
                                this.inputs.supply.other.check = false;
                                this.inputs.supply.other.items.push({
                                    id: null,
                                    type: "4",
                                    guarantor: null,
                                    value: null,
                                    currency: "",
                                    depositType: null,
                                    depositWho: null,
                                    depositCompany: null,
                                    depositCompanyDay: null,
                                    depositCompanyMonth: null,
                                    depositCompanyYear: null,
                                })
                            }
                        } else {
                            this.inputs.supply.bank.check = false;
                            this.inputs.supply.guarantee.check = false;
                            this.inputs.supply.surety.check = false;
                            this.inputs.supply.pledge.check = false;
                            this.inputs.supply.other.check = false;
                        }
                        // TODO get from backend
                        //this.inputs.projectAmountRecipientsCash = 0;
                        //this.inputs.projectAmountBanksCash = 0;
                        //this.inputs.projectAmountInvestor = [];
                        //this.inputs.projectAmountBond = [];
                        //this.inputs.projectAmountOthers = [];
                        // TODO remove:
                        //this.inputs.percentSourceMain = response.body.projectProjectCostDouble;
                        //this.inputs.percentSource1 = response.body.projectAmountRecipientsCash;
                        //this.inputs.percentSource2 = response.body.projectAmountOthersCash;
                        //this.inputs.percentSource3 = response.body.projectAmountFinancingRequest;
                        //this.inputs.percentSource4 = response.body.projectAmountInvestorCash;
                        //this.inputs.percentSource5 = response.body.projectAmountBondCash;
                    }
                },
                function (response) { // Error.
                    console.log('An error occurred.');
                }
            );
            // let inputProjectCost = document.getElementById('input-project-cost');
            // if (inputProjectCost) {
            //     this.inputs.percentSourceMain = parseFloat(inputProjectCost.value) || 1;
            // }
        },
    });
}

if (document.getElementById('projectImpactInfo')) {
    let projectImpactInfo = new Vue({
        el: '#projectImpactInfo',
        data: {
            inputs: {
                files: [],
                projectImplementationCorrespondsToPriorities: null,
                projectProjectImplementationPublicPrivatePartnership: null,
                projectProjectFinancingExpenseInvestmentFund: null,
                projectHaveFinancingAtRfBefore: null,
                projectCanProjectFinancedByFinancialInstitutions: null,
                projectFinancialInstitutionsReadyToFinanceTheProject: null,
                projectEnvironmentalExpertiseConclusionNotRequiredIs: null,
                projectEnvironmentalExpertiseIsNotRequiredIs: null,
                govSupportNot: null,
                govSupport1: null,
                govSupport2: null,
                govSupport3: null,
            },
        },
        mounted: function() {
            this.$http.get('/private-office/finance-request/influence/data').then(
                function (response) { // Success.
                    this.inputs.projectImplementationCorrespondsToPriorities = response.body.projectImplementationCorrespondsToPriorities ? 'true' : 'false';
                    this.inputs.projectProjectImplementationPublicPrivatePartnership = response.body.projectProjectImplementationPublicPrivatePartnership ? 'true' : 'false';
                    this.inputs.projectProjectFinancingExpenseInvestmentFund = response.body.projectProjectFinancingExpenseInvestmentFund ? 'true' : 'false';
                    this.inputs.projectHaveFinancingAtRfBefore = response.body.projectHaveFinancingAtRfBefore;
                    this.inputs.projectCanProjectFinancedByFinancialInstitutions = response.body.projectCanProjectFinancedByFinancialInstitutions;
                    this.inputs.projectFinancialInstitutionsReadyToFinanceTheProject = response.body.projectFinancialInstitutionsReadyToFinanceTheProject;
                    this.inputs.projectEnvironmentalExpertiseConclusionNotRequiredIs = response.body.projectEnvironmentalExpertiseConclusionNotRequired && response.body.projectEnvironmentalExpertiseConclusionNotRequired.length > 0;
                    this.inputs.projectEnvironmentalExpertiseIsNotRequiredIs = response.body.projectEnvironmentalExpertiseIsNotRequired && response.body.projectEnvironmentalExpertiseIsNotRequired.length > 0;
                    // this.inputs.govSupportNot = true;
                    if (response.body.projectImpactInfoAvailabilityOfStateSupportComment0) this.inputs.govSupport1 = true;
                    if (response.body.projectImpactInfoAvailabilityOfStateSupportComment1) this.inputs.govSupport2 = true;
                    if (response.body.projectImpactInfoAvailabilityOfStateSupportComment2) this.inputs.govSupport3 = true;
                },
                function (response) { // Error.
                    console.log('An error occurred.');
                }
            );
        },
        computed: {
            govSupportAnyChecked: function() {
                return this.inputs.govSupport1 || this.inputs.govSupport2 || this.inputs.govSupport3;
            },
        },
        filters: {},
        methods: {
            validate: function () {
                let result = [
                    this.$validator.validateAll(),
                ];
                this.$children.forEach(vm => {
                    result.push(vm.$validator.validateAll());
                });
                return Promise.all(result);
            },
            onSubmit: function(event, skipValidate) {
                if (!skipValidate){
                    this.validate().then((result) => {
                        if (!(!result.includes(false) && result.includes(true))) {
                           event.preventDefault();
                        }
                    }).catch(() => {
                        event.preventDefault();
                    });
                }
            }
        },
        watch: {
            'inputs.govSupportNot': function() {
                if (this.inputs.govSupportNot) {
                    this.inputs.govSupport1 = this.inputs.govSupport2 = this.inputs.govSupport3 = false;
                }
            },
            'govSupportAnyChecked': function() {
                if (this.govSupportAnyChecked) {
                    this.inputs.govSupportNot = false;
                }
            },
        },
    });
}

if (document.getElementById('projectBusinessModel')) {
    let projectBusinessModel = new Vue({
        el: '#projectBusinessModel',
        data: {
            inputs: {
                files: []
            },
        },
        computed: {},
        filters: {},
        methods: {
            validate: function () {
                let result = [
                    this.$validator.validateAll(),
                ];
                this.$children.forEach(vm => {
                    result.push(vm.$validator.validateAll());
                });
                return Promise.all(result);
            },
            onSubmit: function(event, skipValidate) {
                if (!skipValidate){
                    this.validate().then((result) => {
                        if (!(!result.includes(false) && result.includes(true))) {
                           event.preventDefault();
                        }
                    }).catch(() => {
                        event.preventDefault();
                    });
                }
            }
        },
    });
}

if (document.getElementById('officeUserApplicantInfo')) {
    let officeUserApplicantInfo = new Vue({
        el: '#officeUserApplicantInfo',
        data: {
            'static': true,
        },
        methods: {
            edit: function() {
                this.static = false;
            },
            cancel: function() {
                this.static = true;
            },
        },
    });
}

if (document.getElementById('officeUserAddress')) {
    let officeUserAddress = new Vue({
        el: '#officeUserAddress',
        data: {
            'static': true,
        },
        methods: {
            edit: function() {
                this.static = false;
            },
            cancel: function() {
                this.static = true;
            },
        },
    });
}

if (document.getElementById('officeUserInitiatorInfo')) {
    let officeUserInitiatorInfo = new Vue({
        el: '#officeUserInitiatorInfo',
        data: {
            'static': true,
        },
        methods: {
            edit: function() {
                this.static = false;
            },
            cancel: function() {
                this.static = true;
            },
        },
    });
}

if (document.getElementById('officeUserAgencyInfo')) {
    let officeUserAgencyInfo = new Vue({
        el: '#officeUserAgencyInfo',
        data: {
            'static': true,
        },
        methods: {
            edit: function() {
                this.static = false;
            },
            cancel: function() {
                this.static = true;
            },
        },
    });
}

if (document.getElementById('officeDossier')) {
    let officeDossier = new Vue({
        el: '#officeDossier',
        data: {
            inputs: {
                files: [
                ],
            },
        },
        mounted: function() {
            var parseQueryString = function () {
                var str = window.location.search;
                var objURL = {};
                str.replace(
                    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
                    function ($0, $1, $2, $3) {
                        objURL[$1] = $3;
                    }
                );
                return objURL;
            };
            this.$http.get('/private-office/dossier/data', {params:  {id: parseQueryString().id}} ).then(
                function (response) { // Success.
                    this.inputs.files = response.body;
                },
                function (response) { // Error.
                    console.log('An error occurred.');
                }
            );
        },
    });
}

if (document.getElementById('projectHistory')) {
    let projectHistory = new Vue({
        el: '#projectHistory',
        data: {
            inputs: {
                search: "",
                district: "",
                region: ""
            },
            districts: {},
            projects: [
                          // {id:1,image:'',industry:'Инфраструктура ЖКХ', desc: 'Инвестиционная программа ОАО «ПО Водоканал» направлена на реконструкцию и модернизацию системы ВиВ г. Ростова-на-Дону. Вместе с тем программа предусматривает: - улучшение качества очистки воды; - автоматизацию системы управления процессами.', participationScope: '4.180 млрд. руб.', cost: '4.796 млрд. руб.',district:'8',region:'62',title: 'Строительство и реконструкция объектов водоснабжения и водоотведения муниципального образования (городской округ г. Ростова-на-Дону) '},
                          // {id:2,image:'',industry:'Железнодорожная инфраструктура', desc: 'Проект направлен на обновление электропоездов старых модификаций на действующих линиях Московского метрополитена. Проект реализуется в рамках контракта жизненного цикла, в соответствии с которым осуществляется поставка 664 вагонов метро и дальнейшее их обслуживание в течение 30 лет. ', participationScope: '27.620 млрд. руб.', cost: '45.079 млрд. руб.',district:'7',region:'7',title: 'Приобретение и предоставление во владение и пользование (лизинг) вагонов Московского метро'},
                          // {id:3,image:'',industry:'Инфраструктура аэропортов', desc: 'Финансирование проекта по строительству терминала D в аэропорту «Шереметьево» ', participationScope: '39.232 млрд. руб.', cost: '62.740 млрд. руб.',district:'5',region:'32',title: 'Строительство аэровокзального комплекса «Шереметьево - D» в Химкинском районе Московской области '},
                          // {id:4,image:'',industry:'Железнодорожная инфраструктура', desc: 'Проект направлен на модернизацию транспортной инфраструктуры России путем создания крупного «сухового порта», переваливающего контейнерные экспортно-импортные грузы.', participationScope: '5.002 млрд. руб.', cost: '7.305 млрд. руб.',district:'3',region:'29',title: 'Строительство транспортно-логистического центра «Янино» Ленинградская область'},
                          // {id:5,image:'',industry:'Дороги и мосты', desc: 'Проект направлен на модернизацию транспортной инфраструктуры РФ.', participationScope: '14.600 млрд. руб.', cost: '70.100 млрд. руб.',district:'7',region:'32',title: 'Строительство скоростной автомобильной магистрали «Москва - Санкт-Петербург» на участке 15-58 км Московская область'},
                          // {id:6,image:'',industry:'Инфраструктура аэропортов', desc: 'Проект направлен на строительство нового терминала в аэропорту "Кневичи" (г. Владивосток).', participationScope: '2.316 млрд. руб.', cost: '7.598 млрд. руб.',district:'1',region:'43',title: 'Строительство нового пассажирского терминала в Международном аэропорту в г. Владивостоке'},
                          // {id:7,image:'',industry:'Дороги и мосты', desc: 'Проект направлен на обеспечение связи южной, западной и северной зон г. Санкт-Петербурга, минуя исторический центр, что позволит снизить нагрузку на мосты и улично-дорожную сеть центральной части города, сократить задержки транспорта, повысить безопасность движения, сохранить памятники культуры и архитектуры, значительно улучшить экологическую ситуацию.', participationScope: '25.000 млрд. руб.', cost: '120.209 млрд. руб.',district:'3',region:'65',title: 'Строительство платной автомобильной дороги «Западный скоростной диаметр» г. Санкт-Петербург'},
                          {id:8,image:'./img/static/Kurumoch.jpg',industry:'Инфраструктура аэропортов', desc: 'Проект направлен на строительство нового терминала в аэропорту "Курумоч" (г. Самара).', participationScope: '4.550 млрд. руб.', cost: '7.513 млрд. руб.',district:'7',region:'64',title: 'Развитие международного аэропорта «Курумоч» г. Самара'},
                          {id:9,image:'./img/static/Транспорто-логистический комплекс Кал.обл..jpg',industry:'Железнодорожная инфраструктура', desc: 'Создание логистического комплекса на базе двух логистических площадок «Ворсино» и «Росва» в Калужской области для обработки грузопотоков Московского транспортного узла и резидентов индустриальных парков Калужской области', participationScope: '3.700 млрд. руб.', cost: '4.624 млрд. руб.',district:'7',region:'19',title: 'Создание объединенного транспортно-логистического комплекса на территории Калужской области'},
                          // {id:10,image:'',industry:'Генерация и распределение электроэнергии', desc: 'Результатом реализации проекта является ввод в начале 2015 году в эксплуатацию всех 9 гидроагрегатов (3000 МВт) Богучанской ГЭС.', participationScope: '28.100 млрд. руб.', cost: '72.500 млрд. руб.',district:'8',region:'25',title: 'Строительство Богучанской ГЭС г. Кодинск, Красноярский край'},
                          // {id:11,image:'',industry:'Генерация и распределение электроэнергии', desc: 'Проект направлен на создание современной энергетической инфраструктуры и повышение эффективности использования топлива.', participationScope: '4.900 млрд. руб.', cost: '6.170 млрд. руб.',district:'3',region:'65',title: 'Строительство первого блока ТЭЦ ПГУ в Колпинском районе г. Санкт-Петербурга'},
                          {id:12,image:'./img/static/Ижевский ТЭЦ.jpg',industry:'Генерация и распределение электроэнергии', desc: 'Проект направлен на создание современной энергетической инфраструктуры и повышение эффективности использования топлива.', participationScope: '8.300 млрд. руб.', cost: '10.268 млрд. руб.',district:'10',region:'77',title: 'Реконструкция Ижевской ТЭЦ-1 г. Ижевск'},
                          // {id:13,image:'',industry:'Генерация и распределение электроэнергии', desc: 'Введена в эксплуатацию электростанция установленной электрической мощностью 23,9 МВт и тепловой мощностью 48,4 Гкал/ч ', participationScope: '2.040 млрд. руб.', cost: '2.633 млрд. руб.',district:'5',region:'27',title: 'Модернизация систем коммунального теплоснабжения Курганской области на 2010-2015 годы Курганская область'},
                          {id:14,image:'./img/static/Нижнетуринская ГРЭС.jpg',industry:'Генерация и распределение электроэнергии', desc: 'Проект направлен на создание современной энергетической инфраструктуры и повышение эффективности использования топлива. Реконструируется Нижнетуринская ГРЭС в Свердловской области, с целью увеличения мощности станции до 460 МВт.', participationScope: '15.500 млрд. руб.', cost: '19.910 млрд. руб.',district:'5',region:'68',title: 'Реконструкция Нижнетуринской ГРЭС г. Нижняя Тура, Свердловская область'},
                          {id:16,image:'./img/static/Superjet.jpg',industry:'Авиационная промышленность', desc: 'Целью проекта является создание современного российского регионального самолета. Проект реализуется в рамках программы «Суперджет 100», которая включена в ФЦП «Развитие гражданской авиационной техники России на 2002-2010 годы и на период до 2015 года» и направлен на создание и вывод на международный рынок конкурентоспособной продукции отечественного авиапрома – самолета «Сухой Суперджет 100».', participationScope: '72.982 млрд. руб.', cost: '150.799 млрд. руб.',district:'7',region:'11',title: 'Создание семейства российских региональных самолетов Sukhoi Superjet 100 г. Москва, г. Жуковский, г. Комсомольск-на-Амуре'},
                          {id:17,image:'./img/static/МС21.jpg',industry:'Авиационная промышленность', desc: 'Производство нового самолета с нуля с применением композитных материалов, которые влияют на массу воздушного судна и расход топлива. Целью проекта является приобретение оборудования (оснастки) для производства воздушных судов.', participationScope: '7.146 млрд. руб.', cost: '291.536 млрд. руб.',district:'',region:'16',title: 'Приобретение АО «ВЭБ-лизинг» технологического оборудования в рамках реализации Проекта по разработке и организации серийного производства среднемагистрального самолета семейства МС-21 и передаче его в лизинг ПАО «Корпорация «Иркут» Иркутская область'},
                          // {id:18,image:'',industry:'Двигателестроение', desc: 'Разработка и создание нового семейства дизельных двигателей ЯМЗ-530 с привлечением передового мирового опыта в области проектирования и испытания двигателей. Используются лучшие потребительские характеристики (габариты, удельная масса, моторесурс) по сравнению с существующими отечественными двигателями.', participationScope: '5.800 млрд. руб.', cost: '10.254 млрд. руб.',district:'7',region:'86',title: 'Создание и организация производства нового семейства двигателей ЯМЗ-530 с мощностным диапазоном 100-315 л.с. г. Ярославль'},
                          // {id:20,image:'',industry:'Химия полимеров', desc: 'Повышение эффективности использования природных ресурсов, охрана окружающей среды и улучшение экологической обстановки, а также повышение энергоэффективности', participationScope: '1 441 млн. долл США. млрд. руб.', cost: ' 2 395 млн. долл. США, млрд. руб.',district:'5',region:'76',title: 'Строительство комплекса по производству полипропилена мощностью 500 тыс. тонн в год в г. Тобольске'},
                          {id:21,image:'./img/static/sapfir4.jpg',industry:'Микроэлектроника', desc: 'Создание предприятия, технологические возможности которого позволят осуществлять полный цикл производства искусственного сапфира: изготовление сырья, выращивание искусственных сапфиров для производства на его основе различных специализированных компонентов для применения в микроэлектронике (заготовки для производства светодиодов и интегральных схем), оптоэлектронике, оптике, медицине, часовой, химической промышленности и других отраслях промышленности.', participationScope: '2.958 млрд. руб.', cost: '5.160 млрд. руб.',district:'10',region:'59',title: 'Производство синтетического сапфира для оптоэлектроники г. Набережные Челны'},
                          {id:22,image:'./img/static/supercomputer2.jpg',industry:'Стратегические компьютерные технологии и программное обеспечение', desc: 'Создание суперкомпьютерных платформ и программного обеспечения в интересах развития инноваций и модернизации науки и промышленности.', participationScope: '0.798 млрд. руб.', cost: '1.099 млрд. руб.',district:'7',region:'11',title: 'Разработка и экспорт на международный рынок инновационных российских суперкомпьютерных технологий и услуг г. Москва'},
                          {id:23,image:'./img/static/АМ-7_2.jpg',industry:'Космическая промышленность', desc: 'Финансирование производства спутников КА «Экспресс-АМ7» для создания дополнительного объема спутниковой емкости в целях обеспечения населения регионов России современными средствами коммуникаций и создания условий для интенсивного развития новых инфокоммуникационных технологий, в т.ч. подвижных сетей президентской и правительственной связи.', participationScope: '7.737 млрд. руб.', cost: '11.738 млрд. руб.',district:'7',region:'11',title: 'Создание космического аппарата «Экспресс-АМ7» г. Москва'},
                          {id:24,image:'./img/static/data-center_3.jpg',industry:'Информационно-коммуникационные технологии, включая аппаратное и программное обеспечение', desc: 'Строительство и запуск центра обработки данных по собственной запатентованной технологии StackКУБ в г. Санкт-Петербург. Технология проекта StackКУБ является инновационной и не имеет прямых аналогов.', participationScope: '2.173 млрд. руб.', cost: '3.080 млрд. руб.',district:'3',region:'65',title: 'Строительство дата-центра в г. Санкт-Петербурге'},
                          {id:25,image:'./img/static/science_3.png',industry:'Авиационная промышленность', desc: 'Проект направлен на создание современного высокотехнологичного комплекса по производству комплектующих для авиационной и космической техники. Реализация проекта будет способствовать развитию производства отечественных высокотехнологичных авиационных агрегатов (соответствующих мировым стандартам, либо превышающие их).', participationScope: '1.002 млрд. руб.', cost: '2.228 млрд. руб.',district:'7',region:'7',title: 'Создание производства авиационных агрегатов в Першинском филиале  ОАО НПО «Наука», Владимирская область'},
                          {id:26,image:'./img/static/Airport_Sochi_2.jpg',industry:'Инфраструктура аэропортов', desc: 'Проект направлен на создание современной авиапортовой инфраструктуры России.', participationScope: '6.100 млрд. руб.', cost: '8.262 млрд. руб.',district:'8',region:'25',title: 'Реконструкция аэровокзального комплекса в г. Сочи '}
                      ],


            pageCurrent: 1,
            pageSize: 6,
        },
        computed: {
            pageCount: function() {
                return Math.ceil(this.projectsFiltered.length / this.pageSize);
            },
            districtsFlat: function() {
                let result = {};
                Object.keys(this.districts).map(districtKey => {
                    let district = this.districts[districtKey];

                    if(_.findIndex(this.projects, ["district", districtKey]) === -1){
                        return;
                    }

                    result[districtKey] = district.name;
                });
                return result;
            },
            regionsFlat: function() {
                let result = {};
                Object.keys(this.districts).map(districtKey => {
                    let district = this.districts[districtKey];
                    Object.keys(district.regions).map(regionKey => {
                        result[regionKey] = district.regions[regionKey];
                    });
                });
                return result;
            },
            allowedRegionsFlat: function() {
                if (this.inputs.district) {
                    const output = {};
                    let regions = this.districts[this.inputs.district].regions;

                    for(let id in regions){

                        if(_.findIndex(this.projects, ["region", id]) === -1){
                            continue;
                        }

                        output[id] = regions[id];
                    }

                    return output;
                } else {
                    return null;
                }
            },
            projectsFiltered: function() {
                return this.projects.filter(item => {
                    let result = 0;
                    if (this.inputs.district) {
                        if (this.inputs.district == item.district) {
                            result++;
                        }
                    } else {
                        result++;
                    }
                    if (this.inputs.region) {
                        if (this.inputs.region == item.region) {
                            result++;
                        }
                    } else {
                        result++;
                    }
                    return result == 2;
                });
            },
            projectsPaginated: function() {
                return this.projectsFiltered.filter((item, index) => {
                    return index >= ((this.pageCurrent-1) * this.pageSize) &&
                        index < (this.pageCurrent * this.pageSize);
                });
            },
            loadProjects: function() { // TODO

            },

            // активен ли фильтр
            isActiveFilter: function () {
                let active = false;

                for(let key in this.inputs){

                    if(!!this.inputs[key]){
                        active = true;
                        break;
                    }

                }

                return active;
            }
        },
        watch: {
            'projectsFiltered': function() {
                this.pageCurrent = 1;
            },
            'inputs.district': function() {
                this.inputs.region = '';
            },
        },
        methods: {
            getDistrictName: function(code) {
                return this.districtsFlat[code];
            },
            getRegionName: function(code) {
                return this.regionsFlat[code];
            },
            setPageCurrent: function(number) {
                this.pageCurrent = number;
            },
            clearFilter: function() {

                for(let key in this.inputs){
                    this.inputs[key] = "";
                }
            }
        },
        mounted: function() {
            // GET
            this.$http.get('/reference/districts').then(
                function (response) { // Success.
                    this.districts = response.body;
                },
                function (response) { // Error.
                    console.log('An error occurred.');
                }
            );
        }
    });
}

if (document.getElementById('managerList')) {
    let managerList = new Vue({
        el: '#managerList',
        data: {
            inputs: {
                search: null,
                district: '',
                region: '',
            },
            districts: {},
            managers: [],
            pageCurrent: 1,
            pageSize: 10,
        },
        computed: {
            pageCount: function() {
                return Math.ceil(this.managersFiltered.length / this.pageSize);
            },
            districtsFlat: function() {
                let result = {};
                Object.keys(this.districts).map(districtKey => {
                    let district = this.districts[districtKey];
                    result[districtKey] = district.name;
                });
                return result;
            },
            regionsFlat: function() {
                let result = {};
                Object.keys(this.districts).map(districtKey => {
                    let district = this.districts[districtKey];
                    Object.keys(district.regions).map(regionKey => {
                        result[regionKey] = district.regions[regionKey];
                    });
                });
                return result;
            },
            allowedRegionsFlat: function() {
                if (this.inputs.district) {
                    return this.districts[this.inputs.district].regions;
                } else {
                    return null;
                }
            },
            managersFiltered: function() {
                let search = this.inputs.search;
                if (search) {
                    search = this.inputs.search
                        .toString()
                        .toLowerCase()
                        .replace(/\s+/g, ' ')
                        .trim();
                        //.split(/\s+/);
                } else {
                    search = '';
                }
                return this.managers.filter(item => {
                    let result = 0;
                    if (search) {
                        if (`${item.first_name} ${item.last_name}`.toLowerCase().indexOf(search.toLowerCase()) >= 0) {
                            result++;
                        }
                    } else {
                        result++;
                    }
                    if (this.inputs.district) {
                        if (this.inputs.district == item.district) {
                            result++;
                        }
                    } else {
                        result++;
                    }
                    if (this.inputs.region) {
                        if (this.inputs.region == item.region) {
                            result++;
                        }
                    } else {
                        result++;
                    }
                    return result == 3;
                });
            },
            managersPaginated: function() {
                return this.managersFiltered.filter((item, index) => {
                    return index >= ((this.pageCurrent-1) * this.pageSize) &&
                           index < (this.pageCurrent * this.pageSize);
                });
            },
        },
        watch: {
            'managersFiltered': function() {
                this.pageCurrent = 1;
            },
            'inputs.district': function() {
                this.inputs.region = '';
            },
        },
        methods: {
            getDistrictName: function(code) {
                return this.districtsFlat[code];
            },
            getRegionName: function(code) {
                return this.regionsFlat[code];
            },
            setPageCurrent: function(number) {
                this.pageCurrent = number;
            }
        },
        mounted: function() {
            // GET
            this.$http.get('/reference/districts').then(
                function (response) { // Success.
                    this.districts = response.body;
                },
                function (response) { // Error.
                    console.log('An error occurred.');
                }
            );
            this.$http.get('/reference/managers').then(
                function (response) { // Success.
                    this.managers = response.body;
                },
                function (response) { // Error.
                    console.log('An error occurred.');
                }
            );
        }
    });
}


if (document.getElementById('projectSindInfo')) {
    let projectSindInfo = new Vue({
        el: '#projectSindInfo',
        data: {
            inputs: {
                files: [],
                fi : null
            }
        }
        ,
        mounted: function mounted() {
            var parseQueryString = function parseQueryString() {
                var str = window.location.search;
                var objURL = {};
                str.replace(new RegExp("([^?=&]+)(=([^&]*))?", "g"), function ($0, $1, $2, $3) {
                    objURL[$1] = $3;
                });
                return objURL;
            };
            this.$http.get('/private-office/dossier/data', { params: { id: parseQueryString().id } }).then(function (response) {
                // Success.
                this.inputs.files = response.body;
            }, function (response) {
                // Error.
                console.log('An error occurred.');
            });
        }


    });
}

if (document.getElementById('projectSindVoice')) {
    let projectSindVoice = new Vue({
        el: '#projectSindVoice',
        data: {
            typeVoice: {
                t1: "Готов рассмотреть возможность финансирования в рамках \"фабрики\"",
                t2: "Не готов рассмотреть возможность финансирования в рамках \"фабрики\"",
                t3: "Готов рассмотреть возможность финансирования вне \"фабрики\""
            },
            districts: {},
            pageCurrent: 1,
            pageSize: 6,
            voiceReq : null,
            sum : 0,
            sumErr : null,
            curErr : null
        },
        methods: {
            onChange: function(event) {
                if (this.voiceReq != 't1' & this.voiceReq != 't2' & this.voiceReq != 't3')
                {
                    return;
                }


                if (this.voiceReq == 't1'){
                    if (this.sum > 0) {
                        this.sumErr = null;
                    } else {
                        this.sumErr = "Сумма должна быть больше 0";
                    }
                    if (this.voiceCur === undefined || this.voiceCur.length == 0) {
                        this.curErr = "Выберите валюту";
                    } else {
                        this.curErr = null;
                    }

                    /*
                    if (this.sum > 0) {
                        this.sumErr = null;
                        if (this.voiceCur === undefined) {
                            this.curErr = "Выберите валюту";
                            return;
                        } else {
                            if (this.voiceCur.length == 0) {
                                this.curErr = "Выберите валюту";
                                return;
                            }
                            this.curErr = null;
                        }
                    } else {

                        this.sumErr = "Сумма должны быть больше 0";
                        return;
                    }
                    */
                }
            },
            onVoiceSubmit: function(event) {
                if (this.voiceReq != 't1' & this.voiceReq != 't2' & this.voiceReq != 't3')
                {
                    return;
                }
                if (this.voiceReq == 't1'){
                    if (this.sumErr != null) {
                        return;
                    }
                    if (this.curErr != null) {
                        return;
                    }
                }
                document.getElementById("voiceFormId").submit();
            }

            /*
            onSubmit: function(event, skipValidate) {
                if (!skipValidate){
                    this.validate().then((result) => {
                        if (!(!result.includes(false) && result.includes(true))) {
                           event.preventDefault();
                        } else {
                            $("input[name='next']").prop("disabled", true);
                            $("#formResume").submit();
                        }
                    }).catch(() => {
                        event.preventDefault();
                    });
                }
            }*/


        },
    });
}


