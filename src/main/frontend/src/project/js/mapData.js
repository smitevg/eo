export default {
    "dataFields": {
        "appeals": ["Направлено в банк", "На рассмотрении", "Одобрено", "Отклонено"],
        "financing": ["Выс. тех. сфера", "ОПК", "Экспорт", "Инфраструктура", "Промышленность"]
    },
    "regions": {
        "al": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "am": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ar": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "as": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "bl": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": [{
                "name": "Андрей Беляев",
                "photo": "http://veb.sugartalk.pro/photos/%D0%91%D0%B5%D0%BB%D1%8F%D0%B5%D0%B2%D0%90%D0%92.jpg",
                "rating": "10%"
            }]
        },
        "bn": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "vm": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": [{
                "name": "Максим Никеров",
                "photo": "http://veb.sugartalk.pro/photos/%D0%9D%D0%B8%D0%BA%D0%B5%D1%80%D0%BE%D0%B2%D0%9C%D0%90.jpg",
                "rating": "20%"
            }, {
                "name": "Алексей Рохлин",
                "photo": "http://veb.sugartalk.pro/photos/%D0%A0%D0%BE%D1%85%D0%BB%D0%B8%D0%BD%D0%90%D0%90.jpg",
                "rating": "40%"
            }]
        },
        "vl": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "vo": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": [{
                "name": "Ирина Лодыгина",
                "photo": "http://veb.sugartalk.pro/photos/%D0%9B%D0%BE%D0%B4%D1%8B%D0%B3%D0%B8%D0%BD%D0%B0%D0%98%D0%A1.jpg",
                "rating": "50%"
            }, {
                "name": "Оксана Яковлева",
                "photo": "http://veb.sugartalk.pro/photos/%D0%AF%D0%BA%D0%BE%D0%B2%D0%BB%D0%B5%D0%B2%D0%B0%D0%9E%D0%92.jpg",
                "rating": "90%"
            }]
        },
        "vn": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": [{
                "name": "Антон Ганжа",
                "photo": "http://veb.sugartalk.pro/photos/%D0%93%D0%B0%D0%BD%D0%B6%D0%B0%D0%90%D0%92.jpg",
                "rating": "100%"
            }, {
                "name": "Олег Цуцаев",
                "photo": "http://veb.sugartalk.pro/photos/%D0%A6%D1%83%D1%86%D0%B0%D0%B5%D0%B2%D0%9E%D0%92.jpg",
                "rating": "10%"
            }]
        },
        "mc": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": [{
                "name": "Кристина Волконицкая",
                "photo": "http://veb.sugartalk.pro/photos/%D0%92%D0%BE%D0%BB%D0%BA%D0%BE%D0%BD%D0%B8%D1%86%D0%BA%D0%B0%D1%8F%D0%9A%D0%93.jpg",
                "rating": "30%"
            }, {
                "name": "Константин Гусельников",
                "photo": "http://veb.sugartalk.pro/photos/%D0%93%D1%83%D1%81%D0%B5%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%9A%D0%A1.jpg",
                "rating": "80%"
            }]
        },
        "eu": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "zb": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "iv": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ir": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "kb": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "kn": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": [{
                "name": "Олег Скворцов",
                "photo": "http://veb.sugartalk.pro/photos/%D0%A1%D0%BA%D0%B2%D0%BE%D1%80%D1%86%D0%BE%D0%B2%D0%9E%D0%A1.jpg",
                "rating": "70%"
            }]
        },
        "kj": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ka": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "kc": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "km": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ki": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "kt": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ks": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": [{
                "name": "Александр Полиди",
                "photo": "http://veb.sugartalk.pro/photos/%D0%9F%D0%BE%D0%BB%D0%B8%D0%B4%D0%B8%D0%90%D0%90.jpg",
                "rating": "60%"
            }]
        },
        "kr": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ku": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ky": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "le": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": [{
                "name": "Степан Коростелев",
                "photo": "http://veb.sugartalk.pro/photos/%D0%9A%D0%BE%D1%80%D0%BE%D1%81%D1%82%D0%B5%D0%BB%D0%BE%D0%B2%D0%A1%D0%9C.jpg",
                "rating": "50%"
            }, {
                "name": "Геннадий Киркин",
                "photo": "http://veb.sugartalk.pro/photos/%D0%9A%D0%B8%D1%80%D0%BA%D0%B8%D0%BD%D0%93%D0%92.jpg",
                "rating": "10%"
            }]
        },
        "lp": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ma": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "mu": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ne": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "nn": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "no": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": [{
                "name": "Григорий Степанов",
                "photo": "http://veb.sugartalk.pro/photos/%D0%A1%D1%82%D0%B5%D0%BF%D0%B0%D0%BD%D0%BE%D0%B2%D0%93%D0%A4.jpg",
                "rating": "80%"
            }, {
                "name": "Алла Заводина",
                "photo": "http://veb.sugartalk.pro/photos/%D0%97%D0%B0%D0%B2%D0%BE%D0%B4%D0%B8%D0%BD%D0%B0%D0%90%D0%92.jpg",
                "rating": "10%"
            }]
        },
        "nv": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "om": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ob": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "or": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "pz": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "pe": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": [{
                "name": "Александр Кинев",
                "photo": "http://veb.sugartalk.pro/photos/%D0%9A%D0%B8%D0%BD%D0%B5%D0%B2%D0%90%D0%A1.jpg",
                "rating": "10%"
            }]
        },
        "pr": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ps": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ad": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "lt": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "bs": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "br": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": [{
                "name": "Константин Ботоев",
                "photo": "http://veb.sugartalk.pro/photos/%D0%91%D0%BE%D1%82%D0%BE%D0%B5%D0%B2%D0%9A%D0%9F.jpg",
                "rating": "60%"
            }]
        },
        "da": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "in": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "kk": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "kl": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": [{
                "name": "Алексей Ляхов",
                "photo": "http://veb.sugartalk.pro/photos/%D0%9B%D1%8F%D1%85%D0%BE%D0%B2%D0%90%D0%AE.jpg",
                "rating": "90%"
            }]
        },
        "ko": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "cr": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ml": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "mr": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "sa": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "so": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ta": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": [{
                "name": "Марат Гайнуллин",
                "photo": "http://veb.sugartalk.pro/photos/%D0%93%D0%B0%D0%B9%D0%BD%D1%83%D0%BB%D0%BB%D0%B8%D0%BD%D0%9C%D0%A0.jpg",
                "rating": "10%"
            }, {
                "name": "Рауль Ханин",
                "photo": "http://veb.sugartalk.pro/photos/%D0%A5%D0%B0%D0%BD%D0%B8%D0%BD%D0%A0%D0%92.jpg",
                "rating": "100%"
            }]
        },
        "tv": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "hk": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ro": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "rz": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ss": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": [{
                "name": "Иван Новиков",
                "photo": "http://veb.sugartalk.pro/photos/%D0%9D%D0%BE%D0%B2%D0%B8%D0%BA%D0%BE%D0%B2%D0%98%D0%90.jpg",
                "rating": "10%"
            }, {
                "name": "Сергей Богданов",
                "photo": "http://veb.sugartalk.pro/photos/%D0%91%D0%BE%D0%B3%D0%B4%D0%B0%D0%BD%D0%BE%D0%B2%D0%A1%D0%90.jpg",
                "rating": "100%"
            }, {
                "name": "Сергей Субботин",
                "photo": "http://veb.sugartalk.pro/photos/%D0%A1%D1%83%D0%B1%D0%B1%D0%BE%D1%82%D0%B8%D0%BD%D0%A1%D0%92.jpg",
                "rating": "20%"
            }]
        },
        "sr": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "sh": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "sv": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "sm": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "st": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "tb": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "tr": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "tm": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "tl": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": [{
                "name": "Олег Липатов",
                "photo": "http://veb.sugartalk.pro/photos/%D0%9B%D0%B8%D0%BF%D0%B0%D1%82%D0%BE%D0%B2%D0%9E%D0%AE.jpg",
                "rating": "10%"
            }, {
                "name": "Дмитрий Пронин",
                "photo": "http://veb.sugartalk.pro/photos/%D0%9F%D1%80%D0%BE%D0%BD%D0%B8%D0%BD%D0%94%D0%94.jpg",
                "rating": "100%"
            }]
        },
        "tu": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ud": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": [{
                "name": "Руслан Рахматуллин",
                "photo": "http://veb.sugartalk.pro/photos/%D0%A0%D0%B0%D1%85%D0%BC%D0%B0%D1%82%D1%83%D0%BB%D0%BB%D0%B8%D0%BD%D0%A0%D0%90.jpg",
                "rating": "10%"
            }, {
                "name": "Роман Вахитов",
                "photo": "http://veb.sugartalk.pro/photos/%D0%92%D0%B0%D1%85%D0%B8%D1%82%D0%BE%D0%B2%D0%A0%D0%9D.jpg",
                "rating": "20%"
            }, {
                "name": "Николай Коротков",
                "photo": "http://veb.sugartalk.pro/photos/%D0%9A%D0%BE%D1%80%D0%BE%D1%82%D0%BA%D0%BE%D0%B2%D0%9D%D0%90.jpg",
                "rating": "30%"
            }]
        },
        "ul": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": [{
                "name": "Евгений Федоров",
                "photo": "http://veb.sugartalk.pro/photos/%D0%A4%D0%B5%D0%B4%D0%BE%D1%80%D0%BE%D0%B2%D0%95%D0%92.jpg",
                "rating": "50%"
            }, {
                "name": "Ольга Уханова",
                "photo": "http://veb.sugartalk.pro/photos/%D0%A3%D1%85%D0%B0%D0%BD%D0%BE%D0%B2%D0%B0%D0%9E%D0%90.jpg",
                "rating": "10%"
            }, {
                "name": "Вадим Павлов",
                "photo": "http://veb.sugartalk.pro/photos/%D0%9F%D0%B0%D0%B2%D0%BB%D0%BE%D0%B2%D0%92%D0%92.jpg",
                "rating": "100%"
            }]
        },
        "ha": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ht": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "cl": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "cc": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "cu": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ch": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "ya": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": []
        },
        "yr": {
            "financing": [0, 0, 0, 0, 0],
            "appeals": [0, 0, 0, 0],
            "managers": [{
                "name": "Александр Белозёров",
                "photo": "http://veb.sugartalk.pro/photos/%D0%91%D0%B5%D0%BB%D0%BE%D0%B7%D0%B5%D1%80%D0%BE%D0%B2%D0%90%D0%95.jpg",
                "rating": "10%"
            }]
        }
    }
}
