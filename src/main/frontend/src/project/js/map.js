import Vue from 'vue/dist/vue';
import DataSet from './mapData';
import VueResource from 'vue-resource/dist/vue-resource';

Vue.use(VueResource);

$(document).ready(function () {
    let colorRegion = 'transparent'; // Цвет всех регионов
    let mapEl = $('#vmap');
    if (mapEl.length) {
        $('#vmap').vectorMap({
            map: 'russia',
            backgroundColor: 'transparent',
            hoverColor: "#09a8dc",
            selectedColor: "#09a8dc",
            borderColor: '#018acf',
            borderWidth: 2,
            color: colorRegion,
            hoverOpacity: 0,
            enableZoom: true,
            showTooltip: true,

            onRegionClick: function (element, code, region) {
                console.log(region + ' - ' + code);
                window.location = `map.html#${code}`;
            }
        });
    }
});

let FederalDistricts = [
    {
        name: 'Центральный ФО',
        code: 'cen',
        regions: ['bl', 'bn', 'vm', 'vn', 'iv', 'kj', 'kt', 'ky', 'lp', 'mc', 'or', 'rz', 'sm', 'tb', 'tr', 'tl', 'yr']
    },
    {name: 'Северо-Западный ФО', code: 'sez', regions: ['kl', 'ko', 'ar', 'vo', 'kn', 'le', 'mu', 'no', 'ps', 'ne']},
    {name: 'Южный ФО', code: 'uzh', regions: ['ad', 'kk', 'cr', 'ks', 'as', 'vl', 'ro']},
    {name: 'Северо-Кавказский ФО', code: 'sek', regions: ['da', 'in', 'kb', 'kc', 'so', 'cc', 'st']},
    {
        name: 'Приволжский ФО',
        code: 'pvo',
        regions: ['bs', 'ml', 'mr', 'ta', 'ud', 'cu', 'pe', 'ki', 'nn', 'ob', 'pz', 'ss', 'sr', 'ul']
    },
    {name: 'Уральский ФО', code: 'ura', regions: ['ku', 'sv', 'tu', 'cl', 'ht', 'ya']},
    {
        name: 'Сибирский ФО',
        code: 'sib',
        regions: ['lt', 'br', 'tv', 'hk', 'al', 'zb', 'kr', 'ir', 'km', 'nv', 'om', 'tm']
    },
    {name: 'Дальневосточный ФО', code: 'dvo', regions: ['sa', 'ka', 'pr', 'ha', 'am', 'ma', 'sh', 'eu', 'ch']}
];
let highlightedDistrict = null;

let hoverRegionColor = "#12a7da";
let selectedRegionColor = "#86bfe9";
let defaultRegionColor = "transparent";
let financingChart = null;

if (document.getElementById('financingMapPage')) {
    let financingMapPage = new Vue({
        el: '#financingMapPage',
        data: function () {
            return {
                loading: false,
                mapMode: 'region',
                selectedAreaName: null,
                selectedArea: null,
                dataSet: {},
                chartColors: ["#0086cd", "#f9b35c", "#f9675c", "#937eb9", "#12a7da", "#f9f213", "#7af265", "#a1a1a1", "#047c24"],

            };
        },
        computed: {
            dataLabels() {
                return this.dataSet.dataFields;
            },
            selectedAreaData() {
                if (this.mapMode === 'all')
                    return this.dataSet.all;

                if (!this.selectedArea) return null;
                if (this.mapMode === 'region' || this.mapMode === 'district') {
                    let data = this.dataSet.regions[this.selectedArea];
                    if (data && !data.noInfo)
                        return this.dataSet.regions[this.selectedArea];
                }
                return null;
            },

            appealsCount() {
                if (this.selectedAreaData) {
                    let count = 0;
                    let appeals = this.selectedAreaData.appeals;
                    for (let i = 0; i < appeals.length; i++) count += appeals[i];
                    return count;
                }
                return 0;
            }
        },
        watch: {
            selectedAreaData() {
                // Rereender chartjs chart when selected area changes
                let areaData = this.selectedAreaData;
                if (areaData)
                    renderHorizontalChart(areaData.financing, this.dataSet.dataFields.financing, this.chartColors, 'financingChart');
            }
        },
        created: function () {

        },
        methods: {
            fillDataSetAll: function () {
                let allRussiaData = {};
                allRussiaData.managers = [];
                let appealsCount = this.dataSet.dataFields.appeals.length;
                let financingCount = this.dataSet.dataFields.financing.length;
                allRussiaData.appeals = new Array(appealsCount).fill(0);
                allRussiaData.financing = new Array(financingCount).fill(0);

                FederalDistricts.forEach((district) => {
                    let data = {};
                    data.managers = [];
                    data.noInfo = true;
                    data.appeals = new Array(appealsCount).fill(0);
                    data.financing = new Array(financingCount).fill(0);
                    let regions = district.regions;
                    for (let i = 0; i < regions.length; i++) {
                        let regionCode = regions[i];
                        let regionData = this.dataSet.regions[regionCode];
                        if (regionData) {
                            data.noInfo = false;
                            regionData.appeals.forEach((value, index) => {
                                data.appeals[index] += value;
                                allRussiaData.appeals[index] += value;
                            });
                            regionData.financing.forEach((value, index) => {
                                data.financing[index] += value;
                                allRussiaData.financing[index] += value;
                            });
                            data.managers = data.managers.concat(regionData.managers);
                        }
                    }
                    allRussiaData.managers = allRussiaData.managers.concat(data.managers);
                    this.dataSet.regions[district.code] = data;
                });
                this.dataSet['all'] = allRussiaData;
            },
            changeMapMode(mode) {
                this.mapMode = mode;
                this.selectedArea = null;
                this.selectedAreaName = null;
                let clearColor = defaultRegionColor;
                if (mode === 'all') {
                    clearColor = selectedRegionColor;
                    this.selectedArea = 'all';
                }
                clearMap(clearColor);
            },

            getManagerPhoto(manager) {
                if (manager.photo) return manager.photo;
                else return './img/static/default-photo.png';
            },

            initMap: function () {
                $('#vmapBlue').vectorMap({
                    map: 'russia',
                    backgroundColor: 'transparent',
                    hoverColor: hoverRegionColor,
                    selectedColor: selectedRegionColor,
                    borderColor: '#ffffff',
                    borderWidth: 2,
                    color: defaultRegionColor,
                    hoverOpacity: 1,
                    enableZoom: false,
                    showTooltip: true,

                    onRegionOver: (element, code, region) => {
                        switch (this.mapMode) {
                            case 'district':
                                highlightedDistrict = getDistrictByRegion(code).code;
                                if (highlightedDistrict !== this.selectedArea) {
                                    highlightDistrict(highlightedDistrict, hoverRegionColor);
                                }
                                if (this.selectedArea) highlightDistrict(this.selectedArea, selectedRegionColor);
                                break;
                            case 'region':
                                if (code !== this.selectedArea) highlightRegion(code, hoverRegionColor);
                                if (this.selectedArea) highlightRegion(this.selectedArea, selectedRegionColor);
                                break;
                            case 'all':
                                break;
                        }
                    },

                    onRegionOut: (element, code, region) => {
                        switch (this.mapMode) {
                            case 'district':
                                if (highlightedDistrict !== this.selectedArea) {
                                    highlightDistrict(highlightedDistrict, defaultRegionColor);
                                }
                                if (this.selectedArea) highlightDistrict(this.selectedArea, selectedRegionColor);
                                break;
                            case 'region':
                                if (code !== this.selectedArea) highlightRegion(code, defaultRegionColor);
                                if (this.selectedArea) highlightRegion(this.selectedArea, selectedRegionColor);
                                break;
                            case 'all':
                                highlightRegion(code, selectedRegionColor);
                                break;
                        }
                    },

                    onRegionClick: (element, code, region) => {
                        switch (this.mapMode) {
                            case 'district':
                                let clickedDistrict = getDistrictByRegion(code);
                                if (this.selectedArea === clickedDistrict.code) {
                                    highlightDistrict(this.selectedArea, defaultRegionColor);
                                    this.selectedArea = null;
                                    this.selectedAreaName = null;
                                }
                                else {
                                    if (this.selectedArea) highlightDistrict(this.selectedArea, defaultRegionColor);
                                    highlightDistrict(clickedDistrict.code, selectedRegionColor);
                                    this.selectedArea = clickedDistrict.code;
                                    this.selectedAreaName = clickedDistrict.name;
                                }
                                break;
                            case 'region':
                                if (code !== this.selectedArea) {
                                    if (this.selectedArea) highlightRegion(this.selectedArea, defaultRegionColor);
                                    highlightRegion(code, selectedRegionColor);
                                    this.selectedAreaName = region;
                                    this.selectedArea = code;
                                }
                                else {
                                    highlightRegion(code, defaultRegionColor);
                                    this.selectedArea = null
                                    this.selectedAreaName = null;
                                }
                                break;
                            case 'all':
                                break;
                        }
                    },

                    onLabelShow: (event, label, code) => {
                        if (this.mapMode === 'district') {
                            $(label).text(getDistrictByRegion(code).name);
                        }
                    }
                });
            }
        },
        mounted: function () {
            // GET
            this.loading = true;
            this.$http.get('/reference/mapData').then(
               function (response) { // Success.
                   this.dataSet = response.body;
                   this.fillDataSetAll();
                   this.loading = false;
               },
               function (response) { // Error.
                   this.dataSet = DataSet;
                   this.loading = false;
                       console.log('An error occurred.');
               }
            );
        },
    });

    $(document).ready(() => {
        financingMapPage.initMap();
        let hash = window.location.hash;
        hash = hash.replace('#', '');
        $(`#jqvmap1_${hash}`).click();
    });
}

function getDistrictByRegion(code) {
    for (let i = 0; i < FederalDistricts.length; i++) {
        let district = FederalDistricts[i];
        if (district.regions.indexOf(code) !== -1)
            return district;
    }
}

function highlightDistrict(districtCode, color) {
    let colors = {};
    let district = null;
    for (let i = 0; i < FederalDistricts.length; i++) {
        if (FederalDistricts[i].code === districtCode)
            district = FederalDistricts[i];
    }
    if (!district) return;
    for (let i = 0; i < district.regions.length; i++) {
        let region = district.regions[i];
        colors[region] = color;
    }
    $('#vmapBlue').vectorMap('set', 'colors', colors);
}

function highlightRegion(code, color) {
    let colors = {};
    colors[code] = color;
    $('#vmapBlue').vectorMap('set', 'colors', colors);
}

function clearMap(color) {
    for (let i = 0; i < FederalDistricts.length; i++) {
        let district = FederalDistricts[i];
        highlightDistrict(district.code, color);
    }
}

function hexToRgb(hex) {
    if (hex.charAt(0) === '#') hex = hex.slice(1);
    var arrBuff = new ArrayBuffer(4);
    var vw = new DataView(arrBuff);
    vw.setUint32(0, parseInt(hex, 16), false);
    var arrByte = new Uint8Array(arrBuff);

    return arrByte[1] + "," + arrByte[2] + "," + arrByte[3];
};

function renderHorizontalChart(data, captions, colors, target) {
    if (financingChart) financingChart.destroy();
    let ctx = document.getElementById(target);
    financingChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: captions,
            datasets: [{
                data: data,
                backgroundColor: colors
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {display: false},
            scales: {
                yAxes: [{
                    display: false,
                    maxBarThickness: 7
                }],
                xAxes: [{
                    display: true,
                    gridLines: {drawBorder: false},
                    ticks: {
                        beginAtZero: true,
                        fontSize: 12,
                        fontFamily: "Arial",
                        fontColor: 'rgba(113,113,113,.5)',
                    },
                }]
            },
        }
    });
}
