package ru.veb.eo.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.ConverterTypeVia;
import fr.opensagres.xdocreport.converter.Options;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata;
import ru.veb.eo.model.crm.ProjectRequest;
import ru.veb.eo.model.crm.ProjectResponse;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class TestTemplate {

	@Test
	public void testTemplate() throws JsonParseException, JsonMappingException, IOException, XDocReportException {
		ObjectMapper om = new ObjectMapper();
		ProjectRequest r = om.readValue(getClass().getResource("/response.json"), ProjectRequest.class);

		InputStream in = getClass().getResourceAsStream("/test2.docx");
		IXDocReport report = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Velocity);

		// 2) Create context Java model
		IContext context = report.createContext();
		context.put("project", r);

		// 3) Generate report by merging Java model with the Docx
		OutputStream out = new FileOutputStream(new File("response.pdf"));
//		report.process(context, out);

		Options options = Options.getTo(ConverterTypeTo.PDF);
		report.convert(context, options, out);


		File xmlFieldsFile = new File("project.fields.xml");
		FieldsMetadata fieldsMetadata = new FieldsMetadata(TemplateEngineKind.Velocity.name());
		fieldsMetadata.load("project", ProjectResponse.class);	
	    fieldsMetadata.saveXML(new FileOutputStream(xmlFieldsFile), true);
	}
}
