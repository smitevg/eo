package ru.veb.eo.test;

import com.microsoft.sharepoint.rest.SPFile;
import fr.opensagres.xdocreport.core.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import ru.veb.eo.InitBean;
import ru.veb.eo.Properties;
import ru.veb.eo.VebEoApplication;
import ru.veb.eo.controller.MainController;
import ru.veb.eo.controller.POPrivateOfficeController;
import ru.veb.eo.model.User;
import ru.veb.eo.model.crm.DocumentSP;
import ru.veb.eo.model.crm.Manager;
import ru.veb.eo.model.crm.ProjectResponse;
import ru.veb.eo.model.crm.ProjectResponsePacket;
import ru.veb.eo.model.exchange.CrmGetRequest;
import ru.veb.eo.model.exchange.CrmGetResponsePacket;
import ru.veb.eo.service.CrmService;
import ru.veb.eo.service.SharePointService;

import javax.print.Doc;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

//@RunWith(SpringRunner.class)
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration( classes={VebEoApplication.class})
@ActiveProfiles("crm9")
public class TestFilter {
    private final Logger logger = LoggerFactory.getLogger(TestFilter.class);

    @Autowired
    SharePointService sp;

    @Autowired
    InitBean initBean;

    @Autowired
    Properties properties;

    @Autowired
    CrmService cs;

    @Autowired
    @Qualifier("restTemplateCRM")
    private RestTemplate restTemplate;

    @Autowired
    POPrivateOfficeController poPrivateOfficeController;
//http://192.168.211.110:7700/sites/portal/BusinessBlock/_api/web/GetFolderByServerRelativeUrl('Shared%20Documents/456456456456/163')/Files


    @Test
    public void getCrmManager() {
        List<Manager> m = cs.getManagersByRegion(new Integer(1));
        System.out.println(m.size());
    }

    @Test
    public void getCrmUser() {


        List<Manager> listAdm = cs.getUserSind(CrmService.ADMIN);
        List<Manager> listFab = cs.getUserSind(CrmService.FABRIC);
        List<Manager> listSynd = cs.getUserSind(CrmService.SYNDICATE);

        logger.info("listAdm:");
        for(Manager m : listAdm) {
            logger.info(m.getCrmId() + " - " +  m.getEmail());
        }

        logger.info("listFab:");
        for(Manager m : listFab) {
            logger.info(m.getCrmId() + " - " +  m.getEmail());
        }

        logger.info("listSynd:");
        for(Manager m : listSynd) {
            logger.info(m.getCrmId() + " - " +  m.getEmail());
        }

    }

    @Test
    public void getList() {
        List<SPFile> listFile = sp.getListFile("Shared Documents/456456456456/163");
        for (SPFile file : listFile) {
            logger.debug(file.getTitle());
            try {
                byte [] bb = sp.getFile(file.getEditLink());
                if (bb != null) {
                    logger.debug("size = " + bb.length);
                } else {
                    logger.debug("size = NULL");
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }



    @Test
    public void getCRMProject() {
        List<ProjectResponsePacket> projectResponses = cs.getProjectPacketSind(500,0,CrmService.PR_FABRIC,null,null);
        if (projectResponses != null) {
            System.out.println("projectResponses.size() " + projectResponses.size());
        }
/*
        CrmGetResponsePacket response = null;

            List<ProjectResponse> result = new ArrayList<>();

            CrmGetRequest request = new CrmGetRequest(null, 500, 0, properties.getCrmSecret(), "", "");
            logger.debug("CRM getProjectPacketSind. Request={}", request.toString());

            HttpEntity<Object> entity = cs.getRequestWithAuthHeaders(request);
            logger.debug("CRM getProjectPacketSind. type={}", CrmService.PR_FABRIC);
            response = restTemplate.postForObject(properties.getCrmHost() + "?entryPoint=request&action=get&type=" + CrmService.PR_FABRIC, entity, CrmGetResponsePacket.class);

            logger.debug("CRM getProjectPacketSind. Response={}", response.toString());
*/
    }


    @Test
    public void ping() {
        String ipAddress = "192.168.36.186";
        InetAddress inet = null;
        try {
            inet = InetAddress.getByName(ipAddress);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        System.out.println("Sending Ping Request to " + ipAddress);
        try {

            System.out.println(inet.isReachable(5000) ? "Host is reachable" : "Host is NOT reachable");
        } catch (IOException e) {
            e.printStackTrace();
        }

        ipAddress = "192.168.36.186";
        try {
            inet = InetAddress.getByName(ipAddress);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        System.out.println("Sending Ping Request to " + ipAddress);
        try {
            System.out.println(inet.isReachable(5000) ? "Host is reachable" : "Host is NOT reachable");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void filter() {



        List<SPFile> listFile = sp.getListFile("Shared Documents/456456456456/163");

        User user = new User();
        String id = "d99cfd7c-de01-5d5a-dcce-5ab51886caf3";
        List<DocumentSP> documentSPList = cs.getSPFileProp(id);
        logger.info("SP  listFile size: " + listFile.size());
        logger.info("CRM listFile size: " + documentSPList.size());
        if (listFile != null && listFile.size() > 0) {
            listFile = poPrivateOfficeController.filter(documentSPList, listFile, user, id);
        } else {
            logger.info("after filter:null");
            return;
        }

        for (DocumentSP dsp : documentSPList) {
            logger.info("--CRM--");
            logger.info(dsp.getId());
            logger.info(dsp.getTitle());
        }

        logger.info("after filter");
        for (SPFile spFile : listFile) {
            logger.info("-------");
            logger.info(spFile.getEditLink());
            logger.info(spFile.getTitle());
        }
    }



}
