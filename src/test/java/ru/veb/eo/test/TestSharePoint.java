package ru.veb.eo.test;

import com.microsoft.sharepoint.rest.SPFile;
import fr.opensagres.xdocreport.core.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;
import ru.veb.eo.service.SharePointService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestSharePoint {
    private static final Logger logger = LoggerFactory.getLogger(TestSharePoint.class);

    @Autowired
    SharePointService sp;


    @Test
    public void test() {
        String res = "sd{{constructor.constructor(&#039;alert(1)&#039;)()}}4r23rsw";
        res = res.replaceAll("\\{\\{(.*)\\}\\}", "");

    }


    @Test
    public void createFolder() {
//       String fileName = "C:\\Новая папка\\Новый текстовый документ.txt";
        sp.createFolder("Shared Documents/test/test");
    }


    @Test
    public void getList() {
        List<SPFile> listFile = sp.getListFile("Shared Documents/010203040506/124073");
        for (SPFile file : listFile) {
            logger.debug(file.getEditLink());
            try {
                byte[] fileF = sp.getFile(file.getEditLink());

                FileOutputStream output = new FileOutputStream(new File("D://" + file.getTitle()));
                IOUtils.write(fileF, output);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @Test
    public void getFile() {


    }

    @Test
    public void uploadFile() {

        String locationFile = "C:\\Новая папка\\Новый текстовый документ.txt";

        Path path = Paths.get(locationFile);
        String name = "Code3.txt";
        String originalFileName = name;
        String contentType = "text/plain;charset=utf-8";
        byte[] content = null;
        try {
            content = Files.readAllBytes(path);
        } catch (final IOException e) {
            e.printStackTrace();
        }
        MultipartFile file = new MockMultipartFile(name, originalFileName, contentType, content);


        sp.saveProjectFile(null, file, "test", "test");
    }
}
