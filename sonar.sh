#!/bin/bash

# Analyse Java
mvn sonar:sonar -Dsonar.host.url=https://sonar.bpmtech.ru -Dsonar.projectKey=veb-eo -Dsonar.projectName=veb-eo \
    -Dsonar.login=09a1508dc90eaaf4e484a06f751d7d1dbb56f130

# Analyse Web (JS, etc) into separate project
#mvn sonar:sonar -Dsonar.host.url=https://sonar.bpmtech.ru -Dsonar.projectKey=veb-eo-frontend -Dsonar.projectName=veb-eo-frontend \
#    -Dsonar.login=09a1508dc90eaaf4e484a06f751d7d1dbb56f130 \
#    -Dsonar.sources=src/main/frontend -N
