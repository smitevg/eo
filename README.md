## Building

Полная сборка:

`mvn clean package`



## Запуск через мавен

Сборка и запуск без плагинов: gitcommitid, docker

`mvn package -Dmaven.gitcommitid.skip=true -Ddocker.skip=true -Drun.jvmArguments=-Dspring.profiles.active=dev spring-boot:run`

Синтаксис для PowerShell

`mvn package "-Ddocker.skip=true"`


## Запуск для отладки
Выполнить в консоле:

`cd path/to/project/src/main/frontend`

`gulp watch`

Затем запустить 

`cd path/to/project`
	
`mvn package -Dmaven.gitcommitid.skip=true -Ddocker.skip=true -Drun.jvmArguments=-Dspring.profiles.active=dev spring-boot:run`

Полезные настройки Intellij Idea:

http://www.mkyong.com/spring-boot/intellij-idea-spring-boot-template-reload-is-not-working/

Расширение для Chrome:

https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei


Refresh проекта:

You can use the "synchronize" button (two yellow arrows) or in short Ctrl+Alt+Y

## Run locally

java -jar target/eo-exec.jar --server.port=8083 --spring.profiles.active=test

## Run in docker

docker run --rm -p 8083:8080 -e JAVA_OPTS="-Dspring.profiles.active=test" eo

## Login

Use email 
test@ya.ru
Password
123
